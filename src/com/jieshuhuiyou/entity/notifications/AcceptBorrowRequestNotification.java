package com.jieshuhuiyou.entity.notifications;


import com.alibaba.fastjson.annotation.JSONCreator;
import com.alibaba.fastjson.annotation.JSONField;
import com.jieshuhuiyou.entity.Borrow;
import com.jieshuhuiyou.entity.Notification.Type;
import com.jieshuhuiyou.entity.User;

/**
 * 同意借阅请求通知
 * @author PSJay
 *
 */
public class AcceptBorrowRequestNotification extends AbstractNotification {

	private Borrow borrow;
	
	@JSONCreator
	public AcceptBorrowRequestNotification(@JSONField(name = "borrow") Borrow borrow) {
		this.borrow = borrow;
	}

	@Override
	public String toHTML() {
		return "<a href=\"\">" + borrow.getSharing().getUser().getName() + "同意借给我《" + borrow.getSharing().getBook().getTitle() + "》</a>";
	}
	
	@Override
	public User getOwner() {
		return borrow.getUser();
	}

	@Override
	public Type getType() {
		return Type.ACCEPT_BORROW_REQUEST;
	}
	
	// setters and getters
	public Borrow getBorrow() {
		return borrow;
	}

	public void setBorrow(Borrow borrow) {
		this.borrow = borrow;
	}

}
