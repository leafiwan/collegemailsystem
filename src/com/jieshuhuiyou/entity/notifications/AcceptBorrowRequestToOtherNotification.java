package com.jieshuhuiyou.entity.notifications;

import com.alibaba.fastjson.annotation.JSONCreator;
import com.alibaba.fastjson.annotation.JSONField;
import com.jieshuhuiyou.entity.BorrowRequest;
import com.jieshuhuiyou.entity.Notification.Type;
import com.jieshuhuiyou.entity.User;

/**
 * 借阅请求同意其他用户通知
 * @author PSJay
 *
 */
public class AcceptBorrowRequestToOtherNotification extends
		AbstractNotification {

	private BorrowRequest request;
	
	@JSONCreator
	public AcceptBorrowRequestToOtherNotification(@JSONField(name = "request") BorrowRequest request) {
		this.request = request;
	}
	
	@Override
	public String toHTML() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public User getOwner() {
		return request.getUser();
	}


	@Override
	public Type getType() {
		return Type.ACCEPT_BORROW_REQUEST_TO_OTHER;
	}
	
	// setters and getters
	public BorrowRequest getRequest() {
		return request;
	}


	public void setRequest(BorrowRequest request) {
		this.request = request;
	}

}
