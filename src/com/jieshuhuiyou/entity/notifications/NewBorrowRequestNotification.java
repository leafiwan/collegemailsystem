package com.jieshuhuiyou.entity.notifications;

import com.alibaba.fastjson.annotation.JSONCreator;
import com.alibaba.fastjson.annotation.JSONField;
import com.jieshuhuiyou.entity.BorrowRequest;
import com.jieshuhuiyou.entity.Notification.Type;
import com.jieshuhuiyou.entity.User;

/**
 * 新借阅请求通知
 * @author PSJay
 *
 */
public class NewBorrowRequestNotification extends AbstractNotification {

	private BorrowRequest request;
	
	@JSONCreator
	public NewBorrowRequestNotification(@JSONField(name = "request") BorrowRequest request) {
		this.request = request;
	}


	@Override
	public String toHTML() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public User getOwner() {
		return request.getSharing().getUser();
	}


	@Override
	public Type getType() {
		return Type.NEW_BORROW_REQUEST;
	}

	// setters and getters
	public BorrowRequest getRequest() {
		return request;
	}


	public void setRequest(BorrowRequest request) {
		this.request = request;
	}

}
