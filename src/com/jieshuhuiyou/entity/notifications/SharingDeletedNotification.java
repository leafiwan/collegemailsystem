package com.jieshuhuiyou.entity.notifications;

import com.alibaba.fastjson.annotation.JSONCreator;
import com.alibaba.fastjson.annotation.JSONField;
import com.jieshuhuiyou.entity.Notification.Type;
import com.jieshuhuiyou.entity.Sharing;
import com.jieshuhuiyou.entity.User;

/**
 * 分享被取消通知
 * @author PSJay
 *
 */
public class SharingDeletedNotification extends AbstractNotification {

	private Sharing sharing;
	
	private User user;
	
	@JSONCreator
	public SharingDeletedNotification(@JSONField(name = "sharing") Sharing sharing
			, @JSONField(name = "user") User user) {
		this.sharing = sharing;
		this.user = user;
	}
	
	@Override
	public String toHTML() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getOwner() {
		return user;
	}

	@Override
	public Type getType() {
		return Type.SHARING_DELETED;
	}
	
	// setters and getters
	public Sharing getSharing() {
		return sharing;
	}

	public void setSharing(Sharing sharing) {
		this.sharing = sharing;
	}

}
