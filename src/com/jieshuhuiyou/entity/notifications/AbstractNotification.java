package com.jieshuhuiyou.entity.notifications;

import java.util.Date;

import com.alibaba.fastjson.JSON;
import com.jieshuhuiyou.entity.Notification;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.entity.Notification.Type;

/**
 * 抽象通知类
 * @author PSJay
 */
public abstract class AbstractNotification {
	
	/**
	 * 生成相应的 Notification 实体 
	 * @return 生成的实体
	 */
	public Notification toNotificationEntity() {
		Notification notification = new Notification();
		notification.setType(getType());
		notification.setUser(getOwner());
		notification.setDate(new Date());
		notification.setJsonContent(JSON.toJSONString(this));
		
		return notification;
	}
	
	/**
	 * 生成 HTML 代码
	 * @return
	 */
	public abstract String toHTML();
	
	/**
	 * 得到通知的拥有者
	 * @return
	 */
	public abstract User getOwner();
	
	/**
	 * 得到通知的类型
	 * @return
	 */
	public abstract Type getType();
	
}
