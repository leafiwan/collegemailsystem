package com.jieshuhuiyou.entity.notifications;

import com.alibaba.fastjson.annotation.JSONCreator;
import com.alibaba.fastjson.annotation.JSONField;
import com.jieshuhuiyou.entity.Borrow;
import com.jieshuhuiyou.entity.Sharing;
import com.jieshuhuiyou.entity.Notification.Type;
import com.jieshuhuiyou.entity.User;

/**
 * 信誉评价完成通知
 * @author Michael
 *
 */
public class EvaluatedNotification extends AbstractNotification{

	private Borrow borrow;	

	@JSONCreator
	public EvaluatedNotification(@JSONField(name = "borrow") Borrow borrow) {
	this.borrow = borrow;
	}
	
	
	@Override
	public String toHTML() {
		return "<a href=\"\\user\\" + Integer.toString(borrow.getSharing().getUser().getId()) + "\">" + borrow.getSharing().getUser().getName() + "</a>" + "对于你还了<a href=\"\\book\\" + Integer.toString(borrow.getSharing().getBook().getId()) + "\">《" + borrow.getSharing().getBook().getTitle() + "》</a>这本书，进行了信誉评价！赶快去看看你得信誉值又没有改动吧！";
		
	}

	@Override
	public User getOwner() {
		return borrow.getUser();
	}

	@Override
	public Type getType() {
		return Type.EVALUATE_FINISHED;
	}
	
	public Borrow getBorrow() {
		return borrow;
	}

	public void setBorrow(Borrow borrow) {
		this.borrow = borrow;
	}

}
