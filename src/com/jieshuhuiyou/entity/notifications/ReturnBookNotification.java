package com.jieshuhuiyou.entity.notifications;

import com.alibaba.fastjson.annotation.JSONCreator;
import com.alibaba.fastjson.annotation.JSONField;
import com.jieshuhuiyou.entity.Borrow;
import com.jieshuhuiyou.entity.Notification.Type;
import com.jieshuhuiyou.entity.User;

/**
 * 还书通知
 * @author PSJay
 *
 */
public class ReturnBookNotification extends AbstractNotification {

	private Borrow borrow;
	private User operator;
	private boolean operateByBorrower;
	
	@JSONCreator
	public ReturnBookNotification(@JSONField(name = "borrow") Borrow borrow
			, @JSONField(name = "operator") User operator) {
		this.borrow = borrow;
		this.operator = operator;
	}
	
	@Override
	public String toHTML() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public User getOwner() {
		if(operator == null || operator.equals(borrow.getSharing().getUser())) {
			return borrow.getSharing().getUser();
		} 
		
		operateByBorrower = true;
		return borrow.getUser();
		
	}


	@Override
	public Type getType() {
		return Type.RETURN_BOOK;
	}
	
	// setters and getters
	public Borrow getBorrow() {
		return borrow;
	}

	public void setBorrow(Borrow borrow) {
		this.borrow = borrow;
	}

	public boolean isOperateByBorrower() {
		return operateByBorrower;
	}

	public void setOperateByBorrower(boolean operateByBorrower) {
		this.operateByBorrower = operateByBorrower;
	}

}
