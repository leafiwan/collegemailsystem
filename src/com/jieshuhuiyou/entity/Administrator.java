package com.jieshuhuiyou.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 * 管理员实体
 * @author psjay
 *
 */
@Entity
public class Administrator implements Serializable {

	private static final long serialVersionUID = -384413091365699534L;

	@Id
	@GeneratedValue
	private int id;
	
	@OneToOne
	private User user;	// 对应的用户
	
	@Column(length = 32, nullable = false)
	private String password; // 密码
	
	@Column(length = 32, nullable = false)
	private String passwordSalt; //密码盐

	// setters and getters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordSalt() {
		return passwordSalt;
	}

	public void setPasswordSalt(String passwordSalt) {
		this.passwordSalt = passwordSalt;
	}

}
