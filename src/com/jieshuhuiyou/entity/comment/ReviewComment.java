package com.jieshuhuiyou.entity.comment;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.jieshuhuiyou.entity.Comment;
import com.jieshuhuiyou.entity.Review;

/**
 * 书评评论
 * @author psjay
 *
 */
@Entity
public class ReviewComment extends Comment {

	private static final long serialVersionUID = -1867709454145160771L;
	
	@ManyToOne
	@Cascade({CascadeType.SAVE_UPDATE})
	private Review review;
	
	// empty constructor
	public ReviewComment() {
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((review == null) ? 0 : review.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReviewComment other = (ReviewComment) obj;
		if (review == null) {
			if (other.review != null)
				return false;
		} else if (!review.equals(other.review))
			return false;
		return true;
	}

	// setters and getters
	public Review getReview() {
		return review;
	}

	public void setReview(Review review) {
		this.review = review;
	}
	
}
