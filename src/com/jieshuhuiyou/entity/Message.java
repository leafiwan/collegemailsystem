package com.jieshuhuiyou.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;


@Entity
public class Message implements Serializable{

	private static final long serialVersionUID = -822565795612556097L;

	@Id
	@GeneratedValue
	private int id;
	
	@Column(length=20)
	private String title;
	
	@Column(length=200)
	private String content;

	@ManyToOne
	private User sendUser;
	
	@ManyToOne
	private User receiveUser;

	private int replyTimes;

	private boolean isRead;
	
	@Column(columnDefinition = "timestamp not null default current_timestamp")
	private Date date;//	创建时间

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((receiveUser == null) ? 0 : receiveUser.hashCode());
		result = prime * result
				+ ((sendUser == null) ? 0 : sendUser.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Message other = (Message) obj;
		if (receiveUser == null) {
			if (other.receiveUser != null)
				return false;
		} else if (!receiveUser.equals(other.receiveUser))
			return false;
		if (sendUser == null) {
			if (other.sendUser != null)
				return false;
		} else if (!sendUser.equals(other.sendUser))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	// getters and setters ...
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public User getSendUser() {
		return sendUser;
	}

	public void setSendUser(User sendUser) {
		this.sendUser = sendUser;
	}

	public User getReceiveUser() {
		return receiveUser;
	}

	public void setReceiveUser(User receiveUser) {
		this.receiveUser = receiveUser;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public boolean getIsRead() {
		return isRead;
	}

	public void setIsRead(boolean isRead) {
		this.isRead = isRead;
	}

	public int getReplyTimes() {
		return replyTimes;
	}

	public void setReplyTimes(int replyTimes) {
		this.replyTimes = replyTimes;
	}

}
