package com.jieshuhuiyou.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
public class UserFollowRelation implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 159477967072460627L;

	//关系映射id
	@Id
	@GeneratedValue
	private int id;
	
	//被关注的人
	@ManyToOne
	@Cascade({CascadeType.SAVE_UPDATE})
	private User followed;
	
	//关注的人
	@ManyToOne
	@Cascade({CascadeType.SAVE_UPDATE})
	private User follow;
	
	public int getId() {
		return id;
	}

	public User getFollowed() {
		return followed;
	}

	public User getFollow() {
		return follow;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setFollowed(User followed) {
		this.followed = followed;
	}

	public void setFollow(User follow) {
		this.follow = follow;
	}
}
