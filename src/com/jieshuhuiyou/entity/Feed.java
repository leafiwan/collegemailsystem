package com.jieshuhuiyou.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * feed 实体
 * @author psjay
 *
 */
@Entity
public class Feed implements Serializable {

	private static final long serialVersionUID = 5891931149659013487L;

	@Id
	@GeneratedValue
	private long id;
	
	@ManyToOne
	private User owner;
	
	@Column(columnDefinition = "TEXT", nullable = false)
	private String jsonContent;

	// empty constructor
	public Feed() {
		
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((jsonContent == null) ? 0 : jsonContent.hashCode());
		result = prime * result + ((owner == null) ? 0 : owner.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Feed other = (Feed) obj;
		if (jsonContent == null) {
			if (other.jsonContent != null)
				return false;
		} else if (!jsonContent.equals(other.jsonContent))
			return false;
		if (owner == null) {
			if (other.owner != null)
				return false;
		} else if (!owner.equals(other.owner))
			return false;
		return true;
	}

	// setters and getters
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public String getJsonContent() {
		return jsonContent;
	}

	public void setJsonContent(String jsonContent) {
		this.jsonContent = jsonContent;
	}
	
}
