package com.jieshuhuiyou.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.apache.struts2.json.annotations.JSON;

import com.alibaba.fastjson.annotation.JSONField;
import com.jieshuhuiyou.Config;


/**
 * 用户类
 * @author psjay
 *
 */
@Entity
public class User implements Serializable {

	private static final long serialVersionUID = -2883585313597518351L;
	
	@Id
	@GeneratedValue
	private int id;	//ID
	
	private String name;	//姓名
	
	@Column(length = 100, nullable = false)
	private String email;	//邮箱

	@Column(length = 32, nullable = false)
	private String password;	//密码+salt（MD5）
	
	@Column(length = 16, nullable = false)
	private String passwordSalt;//salt(MD5)
	
	@Column(length = 15)
	private String qq;	//QQ号码
	
	@Column(length = 15)
	private String phoneNumber;	//电话号码
	
	private String smallAvatar;	//小头像
	
	private String largeAvatar; // 大头像
	
	@ManyToOne
	private School school;		//学校
	
	@Column(length = 200)
	private String description;	//自我描述
	
	@Column(columnDefinition = "timestamp not null default current_timestamp")
	private Date signUpDate;	//注册时间
	
	@Column(nullable = false)
	private boolean isBlock = false;	//用户是否被关黑屋
	
	@Column(nullable = false)
	private int integral = Config.INTEGRAL_DEFAULT_NUM;	//积分

	@Column(nullable = false)
	private float credit = 0;		//信誉
	
	@Column(nullable = false)
	private int evalutedNum = 0; //已经给他评价信誉度的人数
	
	private int sharingCount;	//共享总数
	
	private int borrowedCount;	//借阅总数
	
	private int receivedBorrowRequestsCount; // 收到的借阅请求数
	
	private int sendedBorrowRequestsCount; //发出的借阅请求数
	
	private int borrowingCount; 	// 借出总数
	
	private int reviewsCount;	// 书评总数
	
	private int followingBookCount;	//关注的书籍数
	
	private int followingUserCount;	//关注的用户数
	
	private int followersCount;	// 关注者的数量
	
	public User() {
		
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		return result;
	}
	
	// setters and getters
	@JSONField(serialize = false)
	public int getBorrowedCount() {
		return borrowedCount;
	}
	
	@JSONField(serialize = false)
	@JSON(serialize = false)
	public String getDescription() {
		return description;
	}
	
	@JSONField(serialize = false)
	@JSON(serialize = false)
	public String getEmail() {
		return email;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLargeAvatar() {
		return largeAvatar;
	}

	public String getName() {
		return name;
	}
	
	@JSONField(serialize = false)
	@JSON(serialize = false)
	public String getPassword() {
		return password;
	}
	
	@JSONField(serialize = false)
	@JSON(serialize = false)
	public String getPasswordSalt() {
		return passwordSalt;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public String getQq() {
		return qq;
	}
	
	@JSONField(serialize = false)
	public int getSharingCount() {
		return sharingCount;
	}
	
	@JSONField(serialize = false)
	@JSON(serialize = false)
	public Date getSignUpDate() {
		return signUpDate;
	}

	@JSONField(serialize = false)
	public int getEvalutedNum() {
		return evalutedNum;
	}

	public void setEvalutedNum(int evalutedNum) {
		this.evalutedNum = evalutedNum;
	}
	
	public String getSmallAvatar() {
		return smallAvatar;
	}

	public School getSchool() {
		return school;
	}

	public void setBorrowedCount(int borrowedCount) {
		this.borrowedCount = borrowedCount;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setLargeAvatar(String largeAvatar) {
		this.largeAvatar = largeAvatar;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setPasswordSalt(String passwordSalt) {
		this.passwordSalt = passwordSalt;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public void setSharingCount(int sharingCount) {
		this.sharingCount = sharingCount;
	}
	
	public void setSignUpDate(Date signUpDate) {
		this.signUpDate = signUpDate;
	}

	public void setSmallAvatar(String smallAvatar) {
		this.smallAvatar = smallAvatar;
	}
	
	@JSONField(serialize = false)
	public int getReceivedBorrowRequestsCount() {
		return receivedBorrowRequestsCount;
	}

	public void setReceivedBorrowRequestsCount(int receivedBorrowRequestsCount) {
		this.receivedBorrowRequestsCount = receivedBorrowRequestsCount;
	}
	
	@JSONField(serialize = false)
	public int getSendedBorrowRequestsCount() {
		return sendedBorrowRequestsCount;
	}

	public void setSendedBorrowRequestsCount(int sendedBorrowRequestsCount) {
		this.sendedBorrowRequestsCount = sendedBorrowRequestsCount;
	}
	
	@JSONField(serialize = false)
	public int getBorrowingCount() {
		return borrowingCount;
	}

	public void setBorrowingCount(int borrowingCount) {
		this.borrowingCount = borrowingCount;
	}

	public void setSchool(School school) {
		this.school = school;
	}
	
	@JSONField(serialize = false)
	public int getReviewsCount() {
		return reviewsCount;
	}

	public void setReviewsCount(int reviewsCount) {
		this.reviewsCount = reviewsCount;
	}

	public int getFollowingBookCount() {
		return followingBookCount;
	}

	public void setFollowingBookCount(int followingBookCount) {
		this.followingBookCount = followingBookCount;
	}

	public int getFollowingUserCount() {
		return followingUserCount;
	}

	public void setFollowingUserCount(int followingUserCount) {
		this.followingUserCount = followingUserCount;
	}

	public int getFollowersCount() {
		return followersCount;
	}

	public void setFollowersCount(int followersCount) {
		this.followersCount = followersCount;
	}
	
	public int getIntegral() {
		return integral;
	}

	public void setIntegral(int integral) {
		this.integral = integral;
	}

	public float getCredit() {
		return credit;
	}

	public void setCredit(float f) {
		this.credit = f;
	}

	public boolean isBlock() {
		return isBlock;
	}

	public void setBlock(boolean isBlock) {
		this.isBlock = isBlock;
	}
}
