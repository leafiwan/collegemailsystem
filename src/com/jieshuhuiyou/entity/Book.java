package com.jieshuhuiyou.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.apache.struts2.json.annotations.JSON;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.alibaba.fastjson.annotation.JSONField;


@Entity
public class Book implements Serializable {

	private static final long serialVersionUID = -8742258193699363818L;
	
	@Id
	@GeneratedValue
	private int id;	//ID
	
	@Column(length = 10)
	private String isbn10;	// 10 位 ISBN 号
	
	@Column(length = 13)
	private String isbn13;	// 13位 ISBN 号
	
	@Column(length = 50, nullable = false)
	private String title;	//	书名
	
	@Column(length = 30, nullable = false)
	private String author;	//	作者，多个用“/”分隔
	
	@Column(columnDefinition="TEXT")
	private String intro;	//	简介
	
	@Column(length = 30)
	private String translator;	//	译者
	
	private int pages;	//	总页数
	
	@Column(length = 50)
	private String price;	//	价格
	
	@Column(length = 30)
	private String publisher;//	出版社
	
	@Column(length = 20, nullable = false)
	private String doubanId;//	豆瓣的 subject id
	
	@Column(columnDefinition="TEXT")
	private String authorIntro;//	作者简介
	
	private float rate;	//	评分
	
	private String publishDate;//	出版日期
	
	private int inventory;//	库存数量
	
	private int count;//	总数
	
	private int borrowedCount;	// 被借阅次数
	
	@Column(length = 100)
	private String smallImg;
	
	@Column(length = 100)
	private String middleImg;
	
	
	@OneToMany(mappedBy = "book", orphanRemoval = true)
	@Cascade({CascadeType.DELETE})
	private Set<Sharing> sharings;
	
	private int reviewsCount;	//书评总数
	
	
	private int followersCount;	// 关注者总数
	
	public Book() {}
	

	// setters and getters
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}


	@JSONField(serialize = false)
	@JSON(serialize = false)
	public String getIsbn10() {
		return isbn10;
	}

	public void setIsbn10(String isbn10) {
		this.isbn10 = isbn10;
	}
	
	@JSONField(serialize = false)
	@JSON(serialize = false)
	public String getIsbn13() {
		return isbn13;
	}

	public void setIsbn13(String isbn13) {
		this.isbn13 = isbn13;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}
	
	@JSONField(serialize = false)
	@JSON(serialize = false)
	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	@JSON(serialize = false)
	public String getTranslator() {
		return translator;
	}

	public void setTranslator(String translator) {
		this.translator = translator;
	}

	@JSON(serialize = false)
	public int getPages() {
		return pages;
	}

	public void setPages(int pages) {
		this.pages = pages;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	@JSON(serialize = false)
	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	
	@JSONField(serialize = false)
	@JSON(serialize = false)
	public String getDoubanId() {
		return doubanId;
	}

	public void setDoubanId(String doubanId) {
		this.doubanId = doubanId;
	}
	
	@JSONField(serialize = false)
	@JSON(serialize = false)
	public String getAuthorIntro() {
		return authorIntro;
	}

	public void setAuthorIntro(String authorIntro) {
		this.authorIntro = authorIntro;
	}

	public float getRate() {
		return rate;
	}

	public void setRate(float rate) {
		this.rate = rate;
	}

	public String getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(String publishDate) {
		this.publishDate = publishDate;
	}
	
	@JSONField(serialize = false)
	public int getInventory() {
		return inventory;
	}

	public void setInventory(int inventory) {
		this.inventory = inventory;
	}
	
	public int getCount() {
		return count;
	}

	public String getSmallImg() {
		return smallImg;
	}
	
	@JSONField(serialize = false)
	@JSON(serialize = false)
	public Set<Sharing> getSharings() {
		return sharings;
	}

	public void setSharings(Set<Sharing> sharings) {
		this.sharings = sharings;
	}

	public void setSmallImg(String smallImg) {
		this.smallImg = smallImg;
	}

	public String getMiddleImg() {
		return middleImg;
	}

	public void setMiddleImg(String middleImg) {
		this.middleImg = middleImg;
	}

	public int getReviewsCount() {
		return reviewsCount;
	}


	public int getFollowersCount() {
		return followersCount;
	}


	public void setFollowersCount(int followersCount) {
		this.followersCount = followersCount;
	}


	public void setReviewsCount(int reviewsCount) {
		this.reviewsCount = reviewsCount;
	}


	public void setCount(int count) {
		this.count = count;
	};
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((doubanId == null) ? 0 : doubanId.hashCode());
		result = prime * result + ((isbn10 == null) ? 0 : isbn10.hashCode());
		result = prime * result + ((isbn13 == null) ? 0 : isbn13.hashCode());
		return result;
	}

	public int getBorrowedCount() {
		return borrowedCount;
	}


	public void setBorrowedCount(int borrowedCount) {
		this.borrowedCount = borrowedCount;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		if (doubanId == null) {
			if (other.doubanId != null)
				return false;
		} else if (!doubanId.equals(other.doubanId))
			return false;
		if (isbn10 == null) {
			if (other.isbn10 != null)
				return false;
		} else if (!isbn10.equals(other.isbn10))
			return false;
		if (isbn13 == null) {
			if (other.isbn13 != null)
				return false;
		} else if (!isbn13.equals(other.isbn13))
			return false;
		return true;
	}


	
}
