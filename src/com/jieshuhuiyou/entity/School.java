package com.jieshuhuiyou.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.apache.struts2.json.annotations.JSON;

@Entity
public class School implements Serializable {

	private static final long serialVersionUID = 1694397487538663227L;
	
	@Id
	@GeneratedValue
	private int id;
	
	@Column(length = 40, nullable = false)
	private String name;//学校名称
	
	@Column(length = 40, nullable = false)
	private String address;//地址

	private int usersCount;//用户数量
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + usersCount;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		School other = (School) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (usersCount != other.usersCount)
			return false;
		return true;
	}

	//getters and setters...
	public int getId() {
		return id;
	}

	@JSON(serialize = false)
	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getUsersCount() {
		return usersCount;
	}

	public void setUsersCount(int usersCount) {
		this.usersCount = usersCount;
	}
	
}
