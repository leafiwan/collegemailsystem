package com.jieshuhuiyou.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.jieshuhuiyou.util.MD5er;

@Entity
public class ResetPasswordRecord implements Serializable {

	private static final long serialVersionUID = -2698550230561021987L;

	@Id
	@GeneratedValue
	private int id;
	
	@OneToOne(fetch = FetchType.EAGER)
	private User user;
	
	@Column(length = 32, unique = true)
	private String resetKey;

	@Column(columnDefinition = "timestamp not null default current_timestamp")
	private Date createDate;
	
	//	empty constructor 
	public ResetPasswordRecord() {
		
	}
	
	public ResetPasswordRecord(User user) {
		this.user = user;
		this.resetKey = MD5er.md5( user.getEmail() + String.valueOf(new Date().getTime()));
	}
	
	// setters and getters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getResetKey() {
		return resetKey;
	}

	public void setResetKey(String resetKey) {
		this.resetKey = resetKey;
	}

}
