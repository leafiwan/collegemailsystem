package com.jieshuhuiyou.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
public class BorrowRequest {

	@Id
	@GeneratedValue
	private int id;//	ID
	
	@ManyToOne
	@Cascade({CascadeType.SAVE_UPDATE})
	private User user;//	借阅者
	
	@ManyToOne
	@Cascade({CascadeType.SAVE_UPDATE})
	private Sharing sharing;//	需要借阅的 Sharing
	
	@Column(length = 200)
	private String message;		//留言
	
	@Column(columnDefinition = "timestamp not null default current_timestamp")
	private Date requestDate;	//创建时间
	
	public BorrowRequest() {}
	
	public BorrowRequest(User user, Sharing sharing, String message) {
		this.user = user;
		this.sharing = sharing;
		this.message = message;
	}

	// setters and getters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Sharing getSharing() {
		return sharing;
	}

	public void setSharing(Sharing sharing) {
		this.sharing = sharing;
	}


	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}

