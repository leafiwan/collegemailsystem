package com.jieshuhuiyou.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;


/**
 * 书籍关注关系实体
 * @author psjay
 *
 */
@Entity
public class BookFollowRelation implements Serializable {

	private static final long serialVersionUID = -2581212875589319449L;

	@Id
	@GeneratedValue
	private long id;
	
	// 关系中的 user
	@ManyToOne
	@Cascade({CascadeType.SAVE_UPDATE})
	private User user;
	
	// 关系中的 book
	@ManyToOne
	@Cascade({CascadeType.SAVE_UPDATE})
	private Book book;
	
	// 关机建立的时间
	@Column(columnDefinition = "timestamp not null default current_timestamp")
	private Date followDate;

	// empty constructor
	public BookFollowRelation() {
		
	}
	
	public BookFollowRelation(User user, Book book) {
		this.user = user;
		this.book = book;
	}
	
	public BookFollowRelation(User user, Book book, Date followDate) {
		this(user, book);
		this.followDate = followDate;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((book == null) ? 0 : book.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BookFollowRelation other = (BookFollowRelation) obj;
		if (book == null) {
			if (other.book != null)
				return false;
		} else if (!book.equals(other.book))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	// setters and getters
	public User getUser() {
		return user;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public Date getFollowDate() {
		return followDate;
	}

	public void setFollowDate(Date followDate) {
		this.followDate = followDate;
	}
	
}
