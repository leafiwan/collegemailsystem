package com.jieshuhuiyou.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
/**
 *  Email 账户实体
 * @author psjay
 *
 */

@Entity
public class EmailAccount implements Serializable {
	
	private static final long serialVersionUID = -3887065524124860810L;

	@Id
	private String email;
	
	@Column(length = 50)
	private String password;
	
	private String remark;
	
	@Column(columnDefinition = "timestamp not null default current_timestamp")
	private Date createDate;
	
	// empty constructor
	public EmailAccount() {
		
	}
	
	public EmailAccount(String email, String password) {
		this.email = email;
		this.password = password;
	}
	
	public EmailAccount(String email, String password, String remark) {
		this(email, password);
		this.remark = remark;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmailAccount other = (EmailAccount) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		return true;
	}

	// setters and getters
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
}
