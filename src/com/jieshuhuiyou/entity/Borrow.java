package com.jieshuhuiyou.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;


@Entity
public class Borrow implements Serializable {

	private static final long serialVersionUID = 108996052074210304L;
	
	@Id
	@GeneratedValue
	private int id;
	
	@ManyToOne
	@Cascade({CascadeType.SAVE_UPDATE})
	private User user;
	
	@OneToOne
	@Cascade({CascadeType.SAVE_UPDATE})
	private Sharing sharing;
	
	@Column(columnDefinition = "timestamp not null default current_timestamp")
	private Date borrowDate;
	
	private Date returnDate;
	
	private boolean returned;
	
	private boolean evaluated;
	
	public boolean isEvaluated() {
		return evaluated;
	}

	public void setEvaluated(boolean evaluated) {
		this.evaluated = evaluated;
	}

	public Borrow(User user, Sharing sharing) {
		this.user = user;
		this.sharing = sharing;
	}
	
	// empty constructor
	public Borrow() {
		
	}


	// setters and getters
	public Date getBorrowDate() {
		return borrowDate;
	}
	public void setBorrowDate(Date borrowDate) {
		this.borrowDate = borrowDate;
	}
	public Date getReturnDate() {
		return returnDate;
	}
	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public boolean isReturned() {
		return returned;
	}
	public boolean getReturned() {
		return returned;
	}
	public void setReturned(boolean returned) {
		this.returned = returned;
	}

	public Sharing getSharing() {
		return sharing;
	}

	public void setSharing(Sharing sharing) {
		this.sharing = sharing;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
}
