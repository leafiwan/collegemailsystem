package com.jieshuhuiyou.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.apache.struts2.json.annotations.JSON;

import com.jieshuhuiyou.Config;
import com.jieshuhuiyou.util.UserUtil;
import com.jieshuhuiyou.util.memcachedHelper;




@Entity
public class Event implements Serializable{

	public static enum EventType {
		SHARE_NEW_BOOK,
		BORROW_BOOK,
		RETURN_BOOK,
		WRITE_NEW_REVIEW,
		EVALUATE_CREDIT,
		NEW_CONDITION,
	}
	
	public static enum HtmlType {
		A,B,C,	
	}
	
	private static final long serialVersionUID = 2269757536905469852L;

	//事件id
	@Id
	@GeneratedValue
	private int id;
	
	//事件内容，json格式
	@Column(columnDefinition = "TEXT",nullable = false)
	private String content;
	
	//事件类型
	@Column(nullable = false,length = 32)
	@Enumerated(EnumType.STRING)
	private EventType type;
	
	//事件时间轴
	@Column(columnDefinition = "timestamp not null default current_timestamp")
	private Date timeline;
	
	//该事件属于哪个用户
	@ManyToOne
	private User user;
	
	@ManyToOne
	private Book book;
	
	
	//此用户是否已经被关注
	@SuppressWarnings("unused")
	@Transient
	private boolean userHasFollowed;
	
	//此书是否已经被关注
	@SuppressWarnings("unused")
	@Transient
	private boolean bookHasFollowed;
	
	@Transient
	private boolean me;
	
	public int getId() {
		return id;
	}

	public String getContent() {
		return content;
	}

	@JSON(serialize = false)
	public EventType getType() {
		return type;
	}

	public Date getTimeline() {
		return timeline;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setType(EventType type) {
		this.type = type;
	}

	public void setTimeline(Date timeline) {
		this.timeline = timeline;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	@JSON(serialize = false)
	public Book getBook() {
		return book;
	}
	
	@JSON(serialize = false)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}



	public void setUserHasFollowed(boolean userHasFollowed) {
		this.userHasFollowed = userHasFollowed;
	}

	@SuppressWarnings("unchecked")
	public boolean isUserHasFollowed() {
		//如果找到了
		memcachedHelper memcached = memcachedHelper.getInstance();
		if(user != null && ((List<Integer>)memcached.get(Config.MEMCACHED_KEY_UIDLIST)).indexOf(user.getId()) != -1 )
			return true;
		else
			return false;
	}

	/*
	public void setBookHasFollowed(String bookHasFollowed) {
		this.bookHasFollowed = bookHasFollowed;
	}*/

	@SuppressWarnings("unchecked")
	public boolean isBookHasFollowed() {
		//如果找到了
		memcachedHelper memcached = memcachedHelper.getInstance();
		if(book != null && ((List<Integer>)memcached.get(Config.MEMCACHED_KEY_BOOKIDLIST)).indexOf(book.getId()) != -1)
			return true;
		else
			return false;
	}

	public void setMe(boolean me) {
		this.me = me;
	}

	public boolean isMe() {
		if(user != null && user.equals(UserUtil.getCurrentUser()))
			return true;
		else
			return false;
	}

}
