package com.jieshuhuiyou.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.alibaba.fastjson.annotation.JSONField;

@Entity
public class Sharing implements Serializable {

	private static final long serialVersionUID = -6034564658523627436L;
	
	@Id
	@GeneratedValue
	private int id;//	ID
	@ManyToOne
	@Cascade({CascadeType.SAVE_UPDATE})
	private User user;//	共享的用户
	@ManyToOne
	@Cascade({CascadeType.SAVE_UPDATE})
	private Book book;//	共享的书
	@Column(nullable = false)
	private int maxBorrowDay;//	最大借阅时间
	@Column(length = 300)
	private String requirements;//	要求
	
	@Column(columnDefinition = "timestamp not null default current_timestamp")
	private Date shareDate;//	创建时间
	
	private int borrowedTimes;	// 借出的次数
	
	private boolean avalible = true;
	
	private boolean canceled;
	
	public Sharing(){}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((book == null) ? 0 : book.hashCode());
		result = prime * result
				+ ((shareDate == null) ? 0 : shareDate.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sharing other = (Sharing) obj;
		if (book == null) {
			if (other.book != null)
				return false;
		} else if (!book.equals(other.book))
			return false;
		if (shareDate == null) {
			if (other.shareDate != null)
				return false;
		} else if (!shareDate.equals(other.shareDate))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	// setters and getters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public int getMaxBorrowDay() {
		return maxBorrowDay;
	}

	public void setMaxBorrowDay(int maxBorrowDay) {
		this.maxBorrowDay = maxBorrowDay;
	}
	
	@JSONField(serialize = false)
	public String getRequirements() {
		return requirements;
	}

	public void setRequirements(String requirements) {
		this.requirements = requirements;
	}

	public Date getShareDate() {
		return shareDate;
	}

	public void setShareDate(Date shareDate) {
		this.shareDate = shareDate;
	}

	@JSONField(serialize = false)
	public boolean isAvalible() {
		return avalible;
	}
	
	@JSONField(serialize = false)
	public boolean getAvalible() {
		return avalible;
	}

	public void setAvalible(boolean avalible) {
		this.avalible = avalible;
	}

	@JSONField(serialize = false)
	public int getBorrowedTimes() {
		return borrowedTimes;
	}

	public void setBorrowedTimes(int borrowedTimes) {
		this.borrowedTimes = borrowedTimes;
	}
	
	@JSONField(serialize = false)
	public boolean getCanceled() {
		return canceled;
	}
	
	@JSONField(serialize = false)
	public boolean isCanceled() {
		return canceled;
	}

	public void setCanceled(boolean canceled) {
		this.canceled = canceled;
	}
}
