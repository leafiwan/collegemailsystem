package com.jieshuhuiyou.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

@Entity
public class Notification {

	public static enum Type {
		NEW_BORROW_REQUEST,	//新的借书请求
		ACCEPT_BORROW_REQUEST, //接受请求
		REJECT_BORROW_REQEUST,//请求被拒绝
		CANCLE_BORROW_REQUEST,//请求被取消
		ACCEPT_BORROW_REQUEST_TO_OTHER, // 接受了其他用户的请求
		RETURN_BOOK,	//还书
		SHARING_DELETED,	//分享被取消
		EVALUATE_FINISHED,  //信誉评价完成
	}
	
	@Id
	@GeneratedValue
	private int id;//	Id
	
	@ManyToOne
	private User user;//	拥有者
	
	@Enumerated(EnumType.STRING)
	private Type type;//	通知类型
	
	@Lob
	private String jsonContent;//	JSON 格式的内容
	
	@Column(columnDefinition = "timestamp not null default current_timestamp")
	private Date date;//	创建时间
	
	
	public Notification() {}

	public Notification(User owner, Type type, String jsonContent) {
		this.user = owner;
		this.type = type;
		this.jsonContent = jsonContent;
	}
	
	//setters and getters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getJsonContent() {
		return jsonContent;
	}

	public void setJsonContent(String jsonContent) {
		this.jsonContent = jsonContent;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
}
