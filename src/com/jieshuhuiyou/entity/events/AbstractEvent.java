package com.jieshuhuiyou.entity.events;

import java.util.Date;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.jieshuhuiyou.Config;
import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.Event;
import com.jieshuhuiyou.entity.Event.EventType;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.util.memcachedHelper;

/**
 * 抽象事件类
 * @author Michael
 *
 */
public abstract class AbstractEvent {
	
	
	
	public  String toJSONStr(Content content){
	 return JSON.toJSONString(content);
	}
	
	public Date setTimeline(){
		return new Date();
	}
	public abstract EventType setType();
	
	public abstract String setText();

	public abstract Event toEventEntity();
}
