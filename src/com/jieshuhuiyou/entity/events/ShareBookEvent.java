package com.jieshuhuiyou.entity.events;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;


import com.jieshuhuiyou.Config;
import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.Event;
import com.jieshuhuiyou.entity.Event.EventType;
import com.jieshuhuiyou.entity.Event.HtmlType;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.util.HtmlDeEncoder;
import com.jieshuhuiyou.util.XMLUtil;

public class ShareBookEvent extends AbstractEvent{

	private Book book;
	private User user;
	private XMLUtil xmlUtil;
	
	
	public ShareBookEvent(Book book,User user) {
		this.book = book;
		this.user = user;
	}
	
	public Event toEventEntity(){
		Content content = new Content();
		content.setBook(book);
		content.setHtml_type(HtmlType.A);
		content.setText(setText());
		content.setUser(this.user);
		
		Event event = new Event();
		event.setContent(toJSONStr(content));
		event.setTimeline(setTimeline());
		event.setType(setType());
		event.setUser(user);
		event.setBook(book);
		return event;
	}


	@Override
	public EventType setType() {
		return EventType.SHARE_NEW_BOOK;
	}


	@Override
	public String setText() {
		
	 xmlUtil = XMLUtil.getInstance();
	 xmlUtil.init(Config.EVENT_TEMPLATE_PATH);
	 
	 String str = null;
	try {
		str = URLDecoder.decode(xmlUtil.getNodeTextByAttr("eventText", "type", "shareBookEvent"),"UTF-8");
	} catch (UnsupportedEncodingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	 
	 //替换模板数据
	 str = str.replaceAll("user.id",Integer.toString(user.getId()))
	 		  .replaceAll("user.name",user.getName())
	 		  .replaceAll("book.id",Integer.toString(book.getId()))
	 		  .replaceAll("book.title",book.getTitle());
	 
    return HtmlDeEncoder.HtmlDecoder(str);
	}

	

}
