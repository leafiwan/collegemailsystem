package com.jieshuhuiyou.entity.events;


import com.jieshuhuiyou.entity.Event;
import com.jieshuhuiyou.entity.Event.EventType;
import com.jieshuhuiyou.entity.Event.HtmlType;
import com.jieshuhuiyou.entity.User;

public class ConditionEvent extends AbstractEvent{

	private User user;
	private String condition;
	
	public ConditionEvent(User user,String condition){
		this.user = user;
		this.condition = condition;
	}
	
	@Override
	public EventType setType() {
		return EventType.NEW_CONDITION;
	}

	@Override
	public String setText() {
		 return this.condition;
	}

	@Override
	public Event toEventEntity() {
		Content content = new Content();
		content.setHtml_type(HtmlType.C);
		content.setText(setText());
		content.setUser(user);
		
		Event event = new Event();
		event.setContent(toJSONStr(content));
		event.setTimeline(setTimeline());
		event.setType(setType());
		event.setUser(user);
		return event;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public String getCondition() {
		return condition;
	}

}
