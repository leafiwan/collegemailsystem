package com.jieshuhuiyou.entity.events;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import com.jieshuhuiyou.Config;
import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.Event;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.entity.Event.EventType;
import com.jieshuhuiyou.entity.Event.HtmlType;
import com.jieshuhuiyou.util.HtmlDeEncoder;
import com.jieshuhuiyou.util.XMLUtil;

public class BorrowBookEvent extends AbstractEvent{

	private User borrower;
	private User provider;
	private Book book;
	private XMLUtil xmlUtil;
	
	public BorrowBookEvent(User borrower,User provider,Book book){
		this.borrower = borrower;
		this.provider = provider;
		this.book = book;
	}
	
	
	@Override
	public EventType setType() {
		return EventType.RETURN_BOOK;
	}

	@Override
	public String setText() {
		xmlUtil = XMLUtil.getInstance();
		xmlUtil.init(Config.EVENT_TEMPLATE_PATH);
		 
		 String str = null;
			try {
				str = URLDecoder.decode(xmlUtil.getNodeTextByAttr("eventText", "type", "borrowBookEvent"),"UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			 
			 //替换模板数据
			 str = str.replaceAll("provider.id",Integer.toString(provider.getId()))
			 		  .replaceAll("provider.name",provider.getName())
			 		  .replaceAll("book.id",Integer.toString(book.getId()))
			 		  .replaceAll("book.title",book.getTitle());
			 
		    return HtmlDeEncoder.HtmlDecoder(str);
	}

	@Override
	public Event toEventEntity() {
		Content content = new Content();
		content.setBook(book);
		content.setHtml_type(HtmlType.A);
		content.setText(setText());
		content.setUser(this.borrower);
		
		Event event = new Event();
		event.setContent(toJSONStr(content));
		event.setTimeline(setTimeline());
		event.setType(setType());
		event.setUser(this.borrower);
		event.setBook(book);
		return event;
	}

}
