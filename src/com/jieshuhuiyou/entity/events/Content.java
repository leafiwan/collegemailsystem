package com.jieshuhuiyou.entity.events;

import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.Review;
import com.jieshuhuiyou.entity.Event.HtmlType;
import com.jieshuhuiyou.entity.User;

/**
 * event中的content对象,最终将其转换成json格式，保存
 * @author Administrator
 *
 */
public class Content {

	//feed内容
	private String text;
	//html显示风格A or B
	private HtmlType html_type;
	//关于的书
	private Book book;
	//书评
	private Review review;
	//谁用有该信息
	private User user;
	
	
	public void setText(String text) {
		this.text = text;
	}
	public String getText() {
		return text;
	}
	public HtmlType getHtml_type() {
		return html_type;
	}
	public Book getBook() {
		return book;
	}
	public Review getReview() {
		return review;
	}
	public void setHtml_type(HtmlType html_type) {
		this.html_type = html_type;
	}
	public void setBook(Book book) {
		this.book = book;
	}
	public void setReview(Review review) {
		this.review = review;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public User getUser() {
		return user;
	}
}
