package com.jieshuhuiyou.entity.events;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import com.jieshuhuiyou.Config;
import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.Event;
import com.jieshuhuiyou.entity.Event.EventType;
import com.jieshuhuiyou.entity.Event.HtmlType;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.util.HtmlDeEncoder;
import com.jieshuhuiyou.util.XMLUtil;

public class EvaluateEvent extends AbstractEvent{

	private User borrower;
	private User provider;
	private int rate;
	private Book book;
	private XMLUtil xmlUtil;
	
	public EvaluateEvent(User borrower,User provider,Book book,int rate){
		this.borrower = borrower;
		this.provider = provider;
		this.rate = rate;
		this.book = book;
		
	}
	
	@Override
	public EventType setType() {
		return EventType.EVALUATE_CREDIT;
	}

	@Override
	public String setText() {
		 xmlUtil = XMLUtil.getInstance();
		 xmlUtil.init(Config.EVENT_TEMPLATE_PATH);
		 
		 String str = null;
			try {
				str = URLDecoder.decode(xmlUtil.getNodeTextByAttr("eventText", "type", "evaluateEvent"),"UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			 
			 //替换模板数据
			 str = str.replaceAll("borrower.id", Integer.toString(borrower.getId()))
			 		  .replaceAll("borrower.name", borrower.getName())
			 		  .replaceAll("book.id",Integer.toString(book.getId()))
			 		  .replaceAll("book.title",book.getTitle())
			 		  .replaceAll("rate", Integer.toString(rate));
			 
		    return HtmlDeEncoder.HtmlDecoder(str);
	}

	@Override
	public Event toEventEntity() {
		Content content = new Content();
		content.setBook(book);
		content.setHtml_type(HtmlType.A);
		content.setText(setText());
		content.setUser(this.provider);
		
		Event event = new Event();
		event.setContent(toJSONStr(content));
		event.setTimeline(setTimeline());
		event.setType(setType());
		event.setUser(this.provider);
		event.setBook(book);
		return event;
	}

	public User getBorrower() {
		return borrower;
	}

	public User getProvider() {
		return provider;
	}

	public int getRate() {
		return rate;
	}

	public void setBorrower(User borrower) {
		this.borrower = borrower;
	}

	public void setProvider(User provider) {
		this.provider = provider;
	}

	public void setRate(int rate) {
		this.rate = rate;
	}

	public XMLUtil getXmlUtil() {
		return xmlUtil;
	}

	public void setXmlUtil(XMLUtil xmlUtil) {
		this.xmlUtil = xmlUtil;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

}
