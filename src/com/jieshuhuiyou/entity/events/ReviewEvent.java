package com.jieshuhuiyou.entity.events;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import com.jieshuhuiyou.Config;
import com.jieshuhuiyou.entity.Event;
import com.jieshuhuiyou.entity.Event.EventType;
import com.jieshuhuiyou.entity.Event.HtmlType;
import com.jieshuhuiyou.entity.Review;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.util.HtmlDeEncoder;
import com.jieshuhuiyou.util.XMLUtil;

public class ReviewEvent extends AbstractEvent{

	private User user;
	private Review review;
	private XMLUtil xmlUtil;
	
	public ReviewEvent(User user,Review review){
		this.user = user;
		this.review = review;
	}
	
	@Override
	public EventType setType() {
		return EventType.WRITE_NEW_REVIEW;
	}

	@Override
	public String setText() {
		xmlUtil = XMLUtil.getInstance();
		 xmlUtil.init(Config.EVENT_TEMPLATE_PATH);
		 
		 String str = null;
			try {
				str = URLDecoder.decode(xmlUtil.getNodeTextByAttr("eventText", "type", "writeReviewEvent"),"UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			 
			 //替换模板数据
			 str = str.replaceAll("user.id",Integer.toString(user.getId()))
			 		  .replaceAll("user.name",user.getName())
			 		  .replaceAll("book.id",Integer.toString(review.getBook().getId()))
			 		  .replaceAll("book.title",review.getBook().getTitle());
			 
		    return HtmlDeEncoder.HtmlDecoder(str);
	}

	@Override
	public Event toEventEntity() {
		Content content = new Content();
		content.setBook(review.getBook());
		content.setHtml_type(HtmlType.B);
		content.setText(setText());
		content.setUser(this.user);
		
		//书评内容之要返回一部分即可
		if(this.review.getContent().length() >= Config.REVIEW_FEED_LENGTH)
			this.review.setContent(this.review.getContent().substring(0, Config.REVIEW_FEED_LENGTH) + "...");
		content.setReview(this.review);
		
		Event event = new Event();
		event.setContent(toJSONStr(content));
		event.setTimeline(setTimeline());
		event.setType(setType());
		event.setUser(this.user);
		event.setBook(review.getBook());
		return event;
	}

	public User getUser() {
		return user;
	}

	public Review getReview() {
		return review;
	}

	public XMLUtil getXmlUtil() {
		return xmlUtil;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setReview(Review review) {
		this.review = review;
	}

	public void setXmlUtil(XMLUtil xmlUtil) {
		this.xmlUtil = xmlUtil;
	}

}
