package com.jieshuhuiyou.tag;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.struts2.components.Component;
import org.apache.struts2.views.jsp.ComponentTagSupport;

import com.jieshuhuiyou.tag.component.IsLoginComponent;
import com.opensymphony.xwork2.util.ValueStack;
/**
 * 验证用户是否登录的标签
 * 用户登录则输出标签体中的内容
 * @author psjay
 *
 */
public class IsLoginTag extends ComponentTagSupport {

	private static final long serialVersionUID = 7735955127270918667L;

	@Override
	public Component getBean(ValueStack stack, HttpServletRequest req,
			HttpServletResponse res) {
		return new IsLoginComponent(stack);
	}

	
}
