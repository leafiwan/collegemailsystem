package com.jieshuhuiyou.tag.component;

import java.io.IOException;
import java.io.Writer;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.struts2.components.Component;

import com.opensymphony.xwork2.util.ValueStack;
import com.opensymphony.xwork2.util.logging.Logger;
import com.opensymphony.xwork2.util.logging.LoggerFactory;
// copy from Property
public class PrefixAndSuffixComponent extends Component {
	
	private static final Logger LOG = LoggerFactory.getLogger(PrefixAndSuffixComponent.class);
	
	private String defaultValue;
    private boolean escape = true;
    private boolean escapeJavaScript = false;
	private String prefix = "";
	private String suffix = "";
	private String regExp;
	private String value;
	
	public PrefixAndSuffixComponent(ValueStack stack) {
		super(stack);
	}

	@Override
	public boolean start(Writer writer) {
		boolean result = super.start(writer);

        String actualValue = null;

        if (value == null) {
            value = "top";
        }
        else {
        	value = stripExpressionIfAltSyntax(value);
        }

        actualValue = (String) getStack().findValue(value, String.class, throwExceptionOnELFailure);
        
        try {
            if (actualValue != null) {
                writer.write(prepare(actualValue).replaceAll(regExp, prefix + "$0" + suffix));
            } else if (defaultValue != null) {
                writer.write(prepare(defaultValue));
            }
        } catch (IOException e) {
            LOG.info("Could not print out value '" + value + "'", e);
        }

        return result;
	}

	
	private String prepare(String value) {
    	String result = value;
        if (escape) {
        	result = StringEscapeUtils.escapeHtml(result);
        }
        if (escapeJavaScript) {
        	result = StringEscapeUtils.escapeJavaScript(result);
        }
        return result;
    }
	
	// setters and getters
	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getRegExp() {
		return regExp;
	}

	public void setRegExp(String regExp) {
		this.regExp = regExp;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public boolean isEscape() {
		return escape;
	}

	public void setEscape(boolean escape) {
		this.escape = escape;
	}

	public boolean isEscapeJavaScript() {
		return escapeJavaScript;
	}

	public void setEscapeJavaScript(boolean escapeJavaScript) {
		this.escapeJavaScript = escapeJavaScript;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}
