package com.jieshuhuiyou.tag.component;

import java.io.Writer;

import org.apache.struts2.components.Component;

import com.jieshuhuiyou.validator.LoginValidator;
import com.opensymphony.xwork2.util.ValueStack;

public class IsNotLoginComponent extends Component {

	public IsNotLoginComponent(ValueStack stack) {
		super(stack);
	}
	
	@Override
	public boolean start(Writer writer) {
		return !LoginValidator.validate();
	}
}
