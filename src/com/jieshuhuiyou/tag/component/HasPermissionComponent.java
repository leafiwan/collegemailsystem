package com.jieshuhuiyou.tag.component;

import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.struts2.components.Component;

import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.service.permission.PermissionValidatableAction;
import com.jieshuhuiyou.service.permission.Role;
import com.jieshuhuiyou.service.permission.Validator;
import com.jieshuhuiyou.util.UserUtil;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.ValueStack;
/**
 * 权限验证标签的组件
 * @author PSJay
 *
 */
public class HasPermissionComponent extends Component {
		
	public static final String IS_PASSED = "jieshuhuiyou.permission.isPassed";
	public static final String CACHE = "jieshuhuiyou.permission.cache";
	
	private String user;
	private String subject;
	private Set<Role> allowRoles;
	private Set<Role> disallowRoles;
	
	public HasPermissionComponent(ValueStack stack) {
		super(stack);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean start(Writer writer) {
		
		boolean isPassed = false;
	
		
		User actualUser  = null;
		Object actualSubject = null;
		
		boolean isPermissionValidatable = false;
		Object action = ActionContext.getContext().getActionInvocation().getAction();
		if( action instanceof PermissionValidatableAction) {
			isPermissionValidatable = true;
		}
		
		if(user == null || user == "") {
			if(isPermissionValidatable) {
				PermissionValidatableAction pva = (PermissionValidatableAction)action;
				actualUser = pva.getValidatedUser();
			} else {
				// get current user
				actualUser = UserUtil.getCurrentUser();
			}
		}
		else {
			actualUser = (User) getStack().findValue(user, User.class);
		}

		if(subject == null || subject == "") {
			if(isPermissionValidatable) {
				PermissionValidatableAction pva = (PermissionValidatableAction)action;
				actualSubject = pva.getValidatedSubject();
			}
		}
		else {
			actualSubject = getStack().findValue(subject, Object.class);
		}
		
		Set<Role> roles = null;
		
		// fetch roles from cache
		if(stack.getContext().get(CACHE) != null) {
			Map<User, Map<Object, Set<Role>>> cache 
				= (Map<User, Map<Object, Set<Role>>>) stack.getContext().get(CACHE);
			
			if(cache.get(actualUser) != null 
					&& cache.get(actualUser).get(actualSubject) != null) {
				roles = cache.get(actualUser).get(actualSubject);
			}
			
		}
		
		// fetch roles from action or validator
		if(roles == null) {
			// if action is permission validatable, call action's getRoles() method
			if(isPermissionValidatable) {
				PermissionValidatableAction pva = (PermissionValidatableAction)action;
				
				if(actualUser != null && actualSubject != null 
						&& actualUser.equals(pva.getValidatedUser())
						&& actualSubject.equals(pva.getValidatedSubject())) {
					roles = pva.getRoles();
				} else {
					roles = Validator.fetchRoles(actualUser, actualSubject);
				}
				
			} else {
				// if action is not permission validatable, fetch roles using validator
				roles = Validator.fetchRoles(actualUser, actualSubject);		
			}
			
			
			// put roles into cache
			Map<User, Map<Object, Set<Role>>> cache = null;
				
			if(stack.getContext().get(CACHE) == null) {
				// create cache in context
				cache = new HashMap<User, Map<Object, Set<Role>>>();
				stack.getContext().put(CACHE, cache);
			} else {
				// get cache object from context
				cache = (Map<User, Map<Object, Set<Role>>>) stack.getContext().get(CACHE);
			}
			
			// add new elements to cache
			if(cache.get(actualUser) == null) {
				cache.put(actualUser, new HashMap<Object, Set<Role>>());
			}
			
			cache.get(actualUser).put(actualSubject, roles);
		}

		isPassed = Validator.validate(roles, allowRoles, disallowRoles);
		
		return isPassed;
	}


	public String getUser() {
		return user;
	}


	public void setUser(String user) {
		this.user = user;
	}


	public String getSubject() {
		return subject;
	}


	public void setSubject(String subject) {
		this.subject = subject;
	}


	public Set<Role> getAllowRoles() {
		return allowRoles;
	}


	public void setAllowRoles(Set<Role> allowRoles) {
		this.allowRoles = allowRoles;
	}


	public Set<Role> getDisallowRoles() {
		return disallowRoles;
	}

	public void setDisallowRoles(Set<Role> disallowRoles) {
		this.disallowRoles = disallowRoles;
	}

}
