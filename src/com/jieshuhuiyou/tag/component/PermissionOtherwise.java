package com.jieshuhuiyou.tag.component;

import java.io.Writer;

import org.apache.struts2.components.Component;


import com.opensymphony.xwork2.util.ValueStack;

public class PermissionOtherwise extends Component {

	public PermissionOtherwise(ValueStack stack) {
		super(stack);
	}

	@Override
	public boolean start(Writer writer) {
		
        Boolean result = (Boolean) stack.getContext().get(HasPermissionComponent.IS_PASSED);

        stack.getContext().remove(HasPermissionComponent.IS_PASSED);

        return !((result == null) || (result.booleanValue()));
	}

	
	
}
