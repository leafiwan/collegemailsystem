package com.jieshuhuiyou.tag.component.ui;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.UIBean;
import org.apache.struts2.views.annotations.StrutsTag;
import org.apache.struts2.views.annotations.StrutsTagAttribute;

import com.jieshuhuiyou.infrastructure.Page;
import com.opensymphony.xwork2.util.ValueStack;
/**
 * 翻页工具 UI Bean
 * @author PSJay
 *
 */
@StrutsTag(name = "paginator", description = "generate paginator"
	, tldTagClass = "com.jieshuhuiyou.tag.ui.PaginatorTag")
public class PaginatorBean extends UIBean {
	/**
	 *  URL 参数中,翻页参数名
	 */
	private String paramName;
	
	/**
	 * 一个指向 Page 对象的 OGNL 表达式
	 */
	private String value;
	
	/**
	 *  URL 的基字符串
	 */
	private String base;
	
	/**
	 * 显示的链接数量
	 */
	private int linksCount = 5;
	
	private static final String OGNL_WRAPPER_REG_EXP = "%\\{.+?\\}";
	
	public PaginatorBean(ValueStack stack, HttpServletRequest request,
			HttpServletResponse response) {
		super(stack, request, response);
	}

	@Override
	protected String getDefaultTemplate() {
		return "paginator";
	}
	
	
	@Override
	protected void evaluateExtraParams() {
		super.evaluateExtraParams();
		
		addParameter("linksCount", linksCount);
		
		if(paramName == null) {
			paramName = "start";	// start as the default parameter name
		}
		addParameter("paramName", paramName);
		if(value != null) {
			if(findValue(value, Page.class) != null) {
				addParameter("page", findValue(value, Page.class));
			} else {
				addParameter("page", "NULL");
			}			
		}
		
		// generate URL
		String url = null;
		if(base == null) {
			@SuppressWarnings("unchecked")
			Map<String, Object> request = (Map<String, Object>) findValue("#request");
			@SuppressWarnings("unchecked")
			Map<String, String[]> parameters = (Map<String, String[]>) findValue("#parameters");
			url = request.get("struts.request_uri").toString() + "?";
			parameters.remove(paramName);
			if(parameters.size() != 0) {
				Iterator<Entry<String, String[]>> i = parameters.entrySet().iterator();
				Entry<String, String[]> e = null;
				String[] paramValues = null;
				while(i.hasNext()) {
					e = i.next();
					url += e.getKey() + "=";
					paramValues = e.getValue();
					for(int k = 0; k < paramValues.length - 1; k++) {
						url += paramValues[k] + ",";
					}
					url += paramValues[paramValues.length - 1] + "&";
				}
			}
		} else {
			Pattern pattern = Pattern.compile(OGNL_WRAPPER_REG_EXP);
			Matcher matcher = pattern.matcher(base);
			String currentExp = null;
			Object currentObj = null;
			while(matcher.find()) {
				currentExp = matcher.group();
				currentExp = currentExp.substring(2, currentExp.length() - 1);
				currentObj = findValue(currentExp);
				if(currentObj != null) {
					base = base.replaceFirst(OGNL_WRAPPER_REG_EXP, currentObj.toString());
				}
			}
			
			url = base;
			if(base.matches(".+\\?([^/^&.]+=[^/^&.]+&*)+")) {
				// if base with parameters
				url += "&";
			} else {
				url += "?";
			}
		}
		url += paramName + "=";
		addParameter("url", url);
	}

	//	setters and getters
	public String getParamName() {
		return paramName;
	}

	@StrutsTagAttribute(description = "parameter name in URL", type = "String", defaultValue = "start")
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getValue() {
		return value;
	}
	
	@StrutsTagAttribute(description = "A OGNL expression pointing to a Page object")
	public void setValue(String value) {
		this.value = value;
	}

	public int getLinksCount() {
		return linksCount;
	}

	public void setLinksCount(int linksCount) {
		this.linksCount = linksCount;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

}
