package com.jieshuhuiyou.tag.component;

import java.io.Writer;

import org.apache.struts2.components.Component;

import com.jieshuhuiyou.infrastructure.Page;
import com.opensymphony.xwork2.util.ValueStack;

public class PaginatorComponent extends Component {

	private Page<?> page;
	private String value;
	
	
	
	@Override
	public boolean start(Writer writer) {
		boolean result =  super.start(writer);
		page = (Page<?>) getStack().findValue(value, Page.class); // convert to Page object
		
		
		//FIXME! 具体内容还未写
		
		
		return result;
	}

	

	// setters and getters
	public void setPage(Page<?> page) {
		this.page = page;
	}
	
	public Page<?> getPage() {
		return page;
	}
	
	public PaginatorComponent(ValueStack stack) {
		super(stack);
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
