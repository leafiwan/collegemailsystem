package com.jieshuhuiyou.tag;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.Component;
import org.apache.struts2.views.jsp.ComponentTagSupport;

import com.jieshuhuiyou.tag.component.PaginatorComponent;
import com.opensymphony.xwork2.util.ValueStack;

/**
 * 分页标签
 * @author PSJay
 *
 */
public class PaginatorTag extends ComponentTagSupport {

	private static final long serialVersionUID = -3397620757119747082L;
	
	private String value;
	
	@Override
	public Component getBean(ValueStack stack, HttpServletRequest req,
			HttpServletResponse res) {
		return new PaginatorComponent(stack);
	}
	
	@Override
	protected void populateParams() {
		PaginatorComponent comp = (PaginatorComponent) getComponent();
		comp.setValue(value);
	}
	// setters and getters
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
}
