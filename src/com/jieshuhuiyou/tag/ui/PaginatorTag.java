package com.jieshuhuiyou.tag.ui;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.Component;
import org.apache.struts2.views.jsp.ui.AbstractUITag;

import com.jieshuhuiyou.tag.component.ui.PaginatorBean;
import com.opensymphony.xwork2.util.ValueStack;
/**
 * 翻页工具标签
 * @author PSJay
 *
 */
public class PaginatorTag extends AbstractUITag {

	private static final long serialVersionUID = -2869960136435512234L;
	
	private String paramName;
	private String value;
	private int linksCount;
	private String base;
	
	@Override
	public Component getBean(ValueStack stack, HttpServletRequest req,
			HttpServletResponse res) {
		return new PaginatorBean(stack, req, res);
	}

	@Override
	protected void populateParams() {
		super.populateParams();
		PaginatorBean bean = (PaginatorBean) getComponent();
		
		bean.setParamName(paramName);
		bean.setValue(value);
		if(linksCount != 0) {
			bean.setLinksCount(linksCount);
		}
		if(base != null && !base.trim().equals("")) {
			bean.setBase(base);
		}
	}

	//	setters and getters
	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public int getLinksCount() {
		return linksCount;
	}

	public void setLinksCount(int linksCount) {
		this.linksCount = linksCount;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

}
