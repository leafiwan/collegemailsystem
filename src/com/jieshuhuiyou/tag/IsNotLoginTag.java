package com.jieshuhuiyou.tag;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.Component;
import org.apache.struts2.views.jsp.ComponentTagSupport;

import com.jieshuhuiyou.tag.component.IsNotLoginComponent;
import com.opensymphony.xwork2.util.ValueStack;

public class IsNotLoginTag extends ComponentTagSupport {

	private static final long serialVersionUID = -6849673741522396562L;

	@Override
	public Component getBean(ValueStack stack, HttpServletRequest req,
			HttpServletResponse res) {
		return new IsNotLoginComponent(stack);
	}

}
