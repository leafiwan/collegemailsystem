package com.jieshuhuiyou.tag;


import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.Component;
import org.apache.struts2.views.jsp.ComponentTagSupport;

import com.jieshuhuiyou.service.permission.Role;
import com.jieshuhuiyou.service.permission.RoleParser;
import com.jieshuhuiyou.tag.component.HasPermissionComponent;
import com.opensymphony.xwork2.util.ValueStack;



public class HasPermissionTag extends ComponentTagSupport {

	private static final long serialVersionUID = -2844482537645548808L;
	
	private String user;
	private String subjectType;
	private String subject;
	private String allowRoles;
	private String disallowRoles;
	
	@Override
	public Component getBean(ValueStack stack, HttpServletRequest req,
			HttpServletResponse res) {
		return new HasPermissionComponent(stack);
	}

	@Override
	protected void populateParams() {
		// cast parameters
		
		Set<Role> actualAllowRoles = new HashSet<Role>();
		if(this.allowRoles != null) {
			String[] rolesStr = allowRoles.split(",");
			for(String s : rolesStr) {
				actualAllowRoles.add(RoleParser.parseString(s));
			}
		}
		
		Set<Role> actualDisallowRoles = new HashSet<Role>();
		if(this.disallowRoles != null) {
			String[] rolesStr2 = disallowRoles.split(",");
			for(String s : rolesStr2) {
				actualDisallowRoles.add(RoleParser.parseString(s));
			}
		}
		
		
		
		HasPermissionComponent component = (HasPermissionComponent) getComponent();
		component.setUser(user);
		component.setSubject(subject);
		component.setAllowRoles(actualAllowRoles);
		component.setDisallowRoles(actualDisallowRoles);

	}

	// setters and getters
	public String getSubjectType() {
		return subjectType;
	}

	public void setSubjectType(String subjectType) {
		this.subjectType = subjectType;
	}


	public String getAllowRoles() {
		return allowRoles;
	}

	public void setAllowRoles(String allowRoles) {
		this.allowRoles = allowRoles;
	}
	

	public String getDisallowRoles() {
		return disallowRoles;
	}

	public void setDisallowRoles(String disallowRoles) {
		this.disallowRoles = disallowRoles;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	
}
