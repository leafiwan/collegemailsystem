package com.jieshuhuiyou.tag;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.Component;
import org.apache.struts2.views.jsp.ComponentTagSupport;

import com.jieshuhuiyou.tag.component.PrefixAndSuffixComponent;
import com.opensymphony.xwork2.util.ValueStack;

public class PrefixAndSuffixTag extends ComponentTagSupport {

	
	private static final long serialVersionUID = 4631666861295485558L;
	
	private String defaultValue;
    private boolean escape = true;
    private boolean escapeJavaScript = false;
    private String value;
	private String prefix = "";
	private String suffix = "";
	private String regExp;
	
	
	@Override
	public Component getBean(ValueStack stack, HttpServletRequest req,
			HttpServletResponse res) {
		return new PrefixAndSuffixComponent(stack);
	}
	
	protected void populateParams() {
        super.populateParams();

        PrefixAndSuffixComponent tag = (PrefixAndSuffixComponent) component;
        tag.setDefaultValue(defaultValue);
        tag.setValue(value);
        tag.setEscape(escape);
        tag.setEscapeJavaScript(escapeJavaScript);
        tag.setPrefix(prefix);
        tag.setSuffix(suffix);
        tag.setRegExp(regExp);
    }

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public boolean isEscape() {
		return escape;
	}

	public void setEscape(boolean escape) {
		this.escape = escape;
	}

	public boolean isEscapeJavaScript() {
		return escapeJavaScript;
	}

	public void setEscapeJavaScript(boolean escapeJavaScript) {
		this.escapeJavaScript = escapeJavaScript;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getRegExp() {
		return regExp;
	}

	public void setRegExp(String regExp) {
		this.regExp = regExp;
	}


}
