package com.jieshuhuiyou.action;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.jieshuhuiyou.entity.Message;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.infrastructure.Page;
import com.jieshuhuiyou.service.MessageService;
import com.jieshuhuiyou.service.UserService;
import com.jieshuhuiyou.util.UserUtil;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.util.ValueStack;

import freemarker.template.utility.StringUtil;

@Controller
@Scope("prototype")
public class MessageAction extends ActionSupport implements ModelDriven<Message>,Preparable {

	private static final long serialVersionUID = -4809611005312877841L;
	
	// 操作类型(1--查看收到的私信,2--查看发出的私信)
	private int operationType = 1;
	
	@Resource
	private MessageService messageService;
	@Resource
	private UserService userService;
	
	private Message message;
	private int messageId;
	
	private User sendUser;
	private User receiveUser;
	private int receiveUserId;
	private String title;
	private String content;
	private int preMsgId;
	private Message preMsg;
	
	private Page<Message> messagePage;
	private User currentUser;
	
	// page device
	private int start = 0;
	private int pageSize = 15;
	
	public String create(){
		if(receiveUser == null) {
			ActionContext context = ActionContext.getContext();   
			ValueStack stack = context.getValueStack();
			stack.set("errorMsg", "Error: This User did not Exist!");
			return ERROR;
		}
		return SUCCESS;
	}
	
	public void prepareCreate() {
		receiveUser = userService.getById(receiveUserId);
		if(preMsgId != 0) {
			preMsg = messageService.getById(preMsgId);
			if(preMsg != null) {
				receiveUser = preMsg.getSendUser();
			}
		}
	}
	

	public String send(){
		
		if(receiveUser == null) {
			return Action.ERROR;
		}
		
		message = new Message();
		if(preMsg != null) {
			message.setReplyTimes(preMsg.getReplyTimes() + 1);
			String replyTitle = null;
			if(message.getReplyTimes() == 1) {
				replyTitle = "回复(1):" + preMsg.getTitle();
			}
			else {
				replyTitle = preMsg.getTitle().replaceFirst("回复\\(\\d\\):", "回复(" + message.getReplyTimes() + "):");
			}
			message.setTitle(replyTitle);
		}
		else {
			message.setTitle(title);
		}
		message.setSendUser(sendUser);
		message.setReceiveUser(receiveUser);
		message.setContent(content);
		
		StringUtil.HTMLEnc(title);
		StringUtil.HTMLEnc(content);
		messageService.sendMessage(message);
		return SUCCESS;
	}
	
	public void prepareSend() {
		sendUser = UserUtil.getCurrentUser();
		receiveUser = userService.getById(receiveUserId);
		if(preMsgId != 0) {
			preMsg = messageService.getById(preMsgId);
			if(preMsg != null) {
				receiveUser = preMsg.getSendUser();
			}
		}
	}
	
	public String mine(){
		currentUser = UserUtil.getCurrentUser();
		if(operationType == 1){
			messagePage = messageService.getReceiveMessageListByUser(currentUser , start , pageSize , true);
			return SUCCESS;
		} else if (operationType == 2){
			messagePage = messageService.getSendMessageListByUser(currentUser , start , pageSize , true);
			return SUCCESS;
		} else {
			return ERROR;
		}
	}
	
	/**
	 * 读取一条私信
	 * @return
	 */
	public String view(){
		if(message == null) {
			return Action.NONE;
		}
		// mark as read
		if(!message.getIsRead() && currentUser.equals(message.getReceiveUser())) {
			message.setIsRead(true);
			messageService.update(message);
		}
		return SUCCESS;
	}
	
	public void prepareView(){
		currentUser = UserUtil.getCurrentUser();
		message = messageService.getById(messageId);
	}
	
	// getters and setters ...
	public MessageService getMessageService() {
		return messageService;
	}

	public void setMessageService(MessageService messageService) {
		this.messageService = messageService;
	}

	@Override
	public void prepare() throws Exception {
		
	}

	@Override
	public Message getModel() {
		return message;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}
	
	public User getSendUser() {
		return sendUser;
	}

	public void setSendUser(User sendUser) {
		this.sendUser = sendUser;
	}

	public User getReceiveUser() {
		return receiveUser;
	}

	public void setReceiveUser(User receiveUser) {
		this.receiveUser = receiveUser;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getReceiveUserId() {
		return receiveUserId;
	}

	public void setReceiveUserId(int receiveUserId) {
		this.receiveUserId = receiveUserId;
	}

	public User getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}

	public int getMessageId() {
		return messageId;
	}

	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}

	public int getOperationType() {
		return operationType;
	}

	public void setOperationType(int operationType) {
		this.operationType = operationType;
	}

	public Page<Message> getMessagePage() {
		return messagePage;
	}

	public void setMessagePage(Page<Message> messagePage) {
		this.messagePage = messagePage;
	}
	
	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}
	
	public int getPageSize() {
		return pageSize;
	}
	
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}


	public int getPreMsgId() {
		return preMsgId;
	}

	public void setPreMsgId(int preMsgId) {
		this.preMsgId = preMsgId;
	}

	public Message getPreMsg() {
		return preMsg;
	}

	public void setPreMsg(Message preMsg) {
		this.preMsg = preMsg;
	}
}
