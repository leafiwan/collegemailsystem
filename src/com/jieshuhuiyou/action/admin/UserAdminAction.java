package com.jieshuhuiyou.action.admin;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.jieshuhuiyou.entity.School;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.infrastructure.Page;
import com.jieshuhuiyou.service.SchoolService;
import com.jieshuhuiyou.service.UserService;
import com.jieshuhuiyou.util.MD5er;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

@Controller
@Scope("prototype")
public class UserAdminAction extends ActionSupport implements
		ModelDriven<User>, Preparable {

	private static final long serialVersionUID = -4434345848174595014L;

	// 操作类型(add--1,list--2)
	private int operationType;

	@Resource
	private UserService userService;
	@Resource
	private SchoolService schoolService;

	private Page<User> users;
	private List<School> schools;
	
	private User user;
	private int userId;

	// editor function
	private int start = 0;// 起始页
	private int pageSize = 15;// 每页显示用户数
	// update function
	private String username;
	private String password;
	private String description;
	private School school;
	private String schoolName;
	private String eamil;
	// search function
	private String nameInfo;
	private String emailInfo;
	private String schoolInfo;

	public String view() {
		if (operationType == 1) {
			schools = schoolService.getAllSchools();
			return "add";
		} else if (operationType == 2) {
			users = userService.getUsers(start, pageSize, false);
			return "list";
		} else {
			return ERROR;
		}
	}

	public String add() {
		// 保留的方法, 暂时不用
		return SUCCESS;
	}

	public String editor() {
		schools = schoolService.getAllSchools();
		user = userService.getById(userId);
		return SUCCESS;
	}
	
	public String update() {
		if(!"".equals(password)){// update password if the password is exist
			String salt = user.getPasswordSalt();
			String pwdMD5 = MD5er.md5(password + salt);
			user.setPassword(pwdMD5);
		}
		// update user name and description
		user.setName(username);
		user.setDescription(description);
		// update School
		school = schoolService.getByName(schoolName);
		user.setSchool(school);
		// update
		userService.update(user);
		return SUCCESS;
	}
	
	public void prepareUpdate() {
		user = userService.getById(userId);
	}
	
	public String block() {
		user.setBlock(true);
		userService.update(user);
		return SUCCESS;
	}

	public void prepareBlock() {
		user = userService.getById(userId);
	}
	
	public String release() {
		user.setBlock(false);
		userService.update(user);
		return SUCCESS;
	}

	public void prepareRelease() {
		user = userService.getById(userId);
	}
	
	public String search() {
		users = userService.searchUsers(emailInfo, nameInfo, schoolInfo,
				start, pageSize, true);
		return SUCCESS;
	}

	public void prepareSearch() {
	}

	@Override
	public void prepare() throws Exception {

	}

	@Override
	public User getModel() {
		return null;
	}

	// getters and setters...
	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public int getOperationType() {
		return operationType;
	}

	public void setOperationType(int operationType) {
		this.operationType = operationType;
	}
	
	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public SchoolService getSchoolService() {
		return schoolService;
	}

	public void setSchoolService(SchoolService schoolService) {
		this.schoolService = schoolService;
	}

	public List<School> getSchools() {
		return schools;
	}

	public void setSchools(List<School> schools) {
		this.schools = schools;
	}

	public String getNameInfo() {
		return nameInfo;
	}

	public void setNameInfo(String nameInfo) {
		this.nameInfo = nameInfo;
	}

	public String getEmailInfo() {
		return emailInfo;
	}

	public void setEmailInfo(String emailInfo) {
		this.emailInfo = emailInfo;
	}

	public String getSchoolInfo() {
		return schoolInfo;
	}

	public void setSchoolInfo(String schoolInfo) {
		this.schoolInfo = schoolInfo;
	}

	public Page<User> getUsers() {
		return users;
	}

	public void setUsers(Page<User> users) {
		this.users = users;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getEamil() {
		return eamil;
	}

	public void setEamil(String eamil) {
		this.eamil = eamil;
	}
}
