package com.jieshuhuiyou.action.admin;

import java.io.File;

import javax.annotation.Resource;
import javax.servlet.ServletContext;

import org.apache.struts2.util.ServletContextAware;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.jieshuhuiyou.entity.School;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.service.AdministratorService;
import com.jieshuhuiyou.service.SchoolService;
import com.jieshuhuiyou.service.UserService;
import com.jieshuhuiyou.util.MD5er;
import com.jieshuhuiyou.util.UserUtil;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@Controller
@Scope("prototype")
public class InstallAction extends ActionSupport implements ServletContextAware  {

	private static final long serialVersionUID = 1436830438455003948L;
	
	private String email;
	private String name;
	private String password;
	private String adminPassword;
	private String schoolName;
	
	@Resource
	private SchoolService schoolService;
	
	@Resource
	private UserService userService;
	
	@Resource
	private AdministratorService administratorService;
	
	private ServletContext context;
	
	@Override
	public String execute() throws Exception {
		// check precondition
		String path = context.getRealPath("/");
		
		File lockFile = new File(path + "/WEB-INF/install.lock");
		if(lockFile.exists()) {
			return "installed";
		}
		
		
		// generate default school
		School s = new School();
		s.setName(schoolName);
		s.setAddress("默认学校的地址");
		schoolService.add(s);
		
		// generate user
		User user = new User();
		user.setName(name);
		user.setEmail(email);
		String salt = UserUtil.produceSalt();
		user.setPasswordSalt(salt);
		user.setPassword(MD5er.md5(password + salt));
		user.setSchool(s);
		userService.register(user);
		
		// generate administrator
		administratorService.add(user, adminPassword);
		
		// generate a installcation lock file
		lockFile.createNewFile();
		
		
		return Action.SUCCESS;
	}

	// setters and getters
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAdminPassword() {
		return adminPassword;
	}

	public void setAdminPassword(String adminPassword) {
		this.adminPassword = adminPassword;
	}

	public SchoolService getSchoolService() {
		return schoolService;
	}

	public void setSchoolService(SchoolService schoolService) {
		this.schoolService = schoolService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public AdministratorService getAdministratorService() {
		return administratorService;
	}

	public void setAdministratorService(AdministratorService administratorService) {
		this.administratorService = administratorService;
	}

	@Override
	public void setServletContext(ServletContext context) {
		this.context = context;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
}
