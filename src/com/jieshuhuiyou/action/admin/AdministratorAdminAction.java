package com.jieshuhuiyou.action.admin;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.jieshuhuiyou.Config;
import com.jieshuhuiyou.entity.Administrator;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.infrastructure.Page;
import com.jieshuhuiyou.service.AdministratorService;
import com.jieshuhuiyou.service.UserService;
import com.jieshuhuiyou.util.UserUtil;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

/**
 * 管理员 Action
 * @author psjay
 *
 */
@Controller
@Scope("prototype")
public class AdministratorAdminAction extends ActionSupport implements
		Preparable, ModelDriven<Administrator> {

	private static final long serialVersionUID = 7131871827255320126L;
	
	private Administrator administrator;
	
	private Page<Administrator> administrators;
	
	private int start;
	
	private int pageSize = 10;
	
	private String newPassword;
	
	private int aid;
	
	private int step;
	
	@Resource
	private AdministratorService administratorService;
	
	@Resource
	private UserService userService;
	
	private User user;
	
	private List<User> users;
	
	private int uid;

	/**
	 * 管理员登录
	 * @return
	 */
	public String login() {
		
		if(administratorService.login(user, newPassword)) {
			
			ActionContext context = ActionContext.getContext();
			Map<String, Object> session = context.getSession();
			@SuppressWarnings("unchecked")
			List<String> roleList = (List<String>) session.get(Config.SESSION_KEY_ROLE);
			roleList.add(Config.SESSION_VALUE_ROLE_ADMIN);
			
			return Action.SUCCESS;
		}
		
		return Action.INPUT;
	}
	
	
	public void prepareLogin() {
		user = UserUtil.getCurrentUser();
	}
	
	/**
	 * 列出一页管理员
	 * @return
	 */
	public String list() {
		administrators = administratorService.list(start, pageSize, true);
		return Action.SUCCESS;
	}
	
	/**
	 * 删除一个管理员
	 * @return
	 */
	public String delete() {
		if(administrator != null) {
			administratorService.delete(administrator);
		}
		return Action.SUCCESS;
	}
	
	public void prepareDelete() {
		if(aid != 0) {
			administrator = administratorService.getById(aid);
		}
	}
	
	/**
	 * 修改密码
	 * @return
	 */
	public String changePassword() {
		if(administrator != null) {
			administratorService.changePassword(administrator, newPassword);
		}
		return Action.SUCCESS;
	}
	
	public void prepareChangePassword() {
		if(aid != 0) {
			administrator = administratorService.getById(aid);
		}
	}
	
	/**
	 * 添加管理员
	 * @return
	 */
	public String add() {
		
		if(user == null) {
			return Action.ERROR;
		}	
		
		if(step == 2) {
			administratorService.add(user, newPassword);
			return "step2-success";
		}
		
		return "step1-success";
	}
	
	public void prepareAdd() {
		if(uid != 0) {
			user = userService.getById(uid);
		}
	}
	
	
	public void validateAdd() {
		if(step == 2) {
			if(newPassword == null 
					|| newPassword.trim().length() < 5 
					|| newPassword.trim().length() > 20) {
				
				addFieldError("newPassword", "wrongPwdLength");
				
			}
		}
	}

	@Override
	public void prepare() throws Exception {
		
	}

	@Override
	public Administrator getModel() {
		return administrator;
	}

	public Administrator getAdministrator() {
		return administrator;
	}

	public void setAdministrator(Administrator administrator) {
		this.administrator = administrator;
	}

	public AdministratorService getAdministratorService() {
		return administratorService;
	}

	public void setAdministratorService(AdministratorService administratorService) {
		this.administratorService = administratorService;
	}

	public Page<Administrator> getAdministrators() {
		return administrators;
	}

	public void setAdministrators(Page<Administrator> administrators) {
		this.administrators = administrators;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getPageSize() {
		return pageSize;
	}

	public int getAid() {
		return aid;
	}

	public void setAid(int aid) {
		this.aid = aid;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public int getStep() {
		return step;
	}

	public void setStep(int step) {
		this.step = step;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

}
