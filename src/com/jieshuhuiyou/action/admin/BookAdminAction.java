package com.jieshuhuiyou.action.admin;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.Sharing;
import com.jieshuhuiyou.exceptions.SharingNotAvailableException;
import com.jieshuhuiyou.infrastructure.Page;
import com.jieshuhuiyou.service.BookService;
import com.jieshuhuiyou.service.SharingService;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

@Controller
@Scope("prototype")
public class BookAdminAction extends ActionSupport implements
	ModelDriven<Book>, Preparable  {
	
	private static final long serialVersionUID = -6228718522213057197L;

	// 操作类型(add--1,list--2)
	private int operationType;
	
	@Resource
	private BookService bookService;
	@Resource
	private SharingService sharingService;
	
	private Page<Book> books;
	
	private Book book;
	private int bookId;

	// editor function
	private int start = 0;// 起始用户
	private int pageSize = 15;// 每页显示用户数
	
	// update function
	private String intro;
	private String authorIntro;
	
	// search function
	private String isbnInfo;
	private String nameInfo;
	private String authorInfo;
	private String publisherInfo;
	
	public String view(){
		if (operationType == 1) {
			return "add";
		} else if (operationType == 2) {
			books = bookService.getBooksPage(start , pageSize , false);
			return "list";
		} else {
			return ERROR;
		}
	}
	
	public String search(){
		books = bookService.getByBGQuery(isbnInfo, nameInfo, authorInfo, publisherInfo, start, pageSize, false);
		return SUCCESS;
	}
	
	public String editor(){
		book = bookService.getById(bookId);
		return SUCCESS;
	}
	
	public String update(){
		book.setAuthorIntro(authorIntro);
		book.setIntro(intro);
		bookService.update(book);
		return SUCCESS;
	}
	
	public void prepareUpdate(){
		book = bookService.getById(bookId);
	}
	
	public String delete(){
		List<Sharing> sharings = sharingService.getByBookAndSchool(book , null);
		for(Sharing sharing : sharings){// cancel sharing and unfollowing
			try {
				sharingService.cancel(sharing);
				bookService.unfollow(sharing.getUser() , sharing.getBook());
			} catch (SharingNotAvailableException e) {
				e.printStackTrace();
			}
		}
		bookService.delete(book);
		return SUCCESS;
	}

	public void prepareDelete(){
		book = bookService.getById(bookId);
	}
	
	@Override
	public void prepare() throws Exception {
		
	}

	@Override
	public Book getModel() {
		return null;
	}

	//getters and setters
	public BookService getBookService() {
		return bookService;
	}

	public void setBookService(BookService bookService) {
		this.bookService = bookService;
	}

	public Page<Book> getBooks() {
		return books;
	}

	public void setBooks(Page<Book> books) {
		this.books = books;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getOperationType() {
		return operationType;
	}

	public void setOperationType(int operationType) {
		this.operationType = operationType;
	}

	public String getIsbnInfo() {
		return isbnInfo;
	}

	public void setIsbnInfo(String isbnInfo) {
		this.isbnInfo = isbnInfo;
	}

	public String getNameInfo() {
		return nameInfo;
	}

	public void setNameInfo(String nameInfo) {
		this.nameInfo = nameInfo;
	}

	public String getAuthorInfo() {
		return authorInfo;
	}

	public void setAuthorInfo(String authorInfo) {
		this.authorInfo = authorInfo;
	}

	public String getPublisherInfo() {
		return publisherInfo;
	}

	public void setPublisherInfo(String publisherInfo) {
		this.publisherInfo = publisherInfo;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public String getAuthorIntro() {
		return authorIntro;
	}

	public void setAuthorIntro(String authorIntro) {
		this.authorIntro = authorIntro;
	}

	public SharingService getSharingService() {
		return sharingService;
	}

	public void setSharingService(SharingService sharingService) {
		this.sharingService = sharingService;
	}
	
}
