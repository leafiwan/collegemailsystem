package com.jieshuhuiyou.action.admin;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.jieshuhuiyou.entity.School;
import com.jieshuhuiyou.infrastructure.Page;
import com.jieshuhuiyou.service.SchoolService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.util.ValueStack;

@Controller
@Scope("prototype")
public class SchoolAdminAction extends ActionSupport implements ModelDriven<School>,Preparable {

	private static final long serialVersionUID = -4505998380570543223L;

	@Resource
	private SchoolService schoolService;
	
	private Page<School> schools;

	// editor function
	private int start = 0;// 起始页
	private int pageSize = 15;// 每页显示用户数
	//Entity
	private School school;
	
	private int schoolId;
	private String schoolName;
	private String schoolAddress;
	//操作类型(add--1,update--2,delete--3)
	private int operationType;
	
	@Override
	public String execute(){
		schools = schoolService.getSchoolsPage(start, pageSize , false);
		return SUCCESS;
	}
	
	public String view() {
		if(operationType == 1){
			return "add";
		} else if(operationType == 2){
			return "update";
		} else if(operationType == 3){
			return "delete";
		} else {
			return ERROR;
		}
	}
	
	public void prepareView() {
		schools = schoolService.getSchoolsPage(start, pageSize , false);
	}
	
	public String add(){
		if( schoolService.getByName(school.getName()) != null ) {
			// school already exist
			// return ERROR page
			ActionContext context = ActionContext.getContext();   
			ValueStack stack = context.getValueStack();
			stack.set("errorMsg", "Error: School Already Exist!");
			return ERROR;
		} else {
			schoolService.add(school);
			return SUCCESS;
		}
	}
	
	public void prepareAdd(){
		school = new School();
	}

	public String delete(){
		if( schoolService.getByName(school.getName()) != null ) {
			schoolService.delete(school);
			return SUCCESS;
		} else {
			// school doesn't exist
			// return ERROR page
			ActionContext context = ActionContext.getContext();   
			ValueStack stack = context.getValueStack();
			stack.set("errorMsg", "Error: School Doesn't Exist!");
			return ERROR;
		}
	}
	
	public void prepareDelete(){
		school = new School();
	}
	
	public String update(){
		if( schoolService.getByName(school.getName()) != null ) {
			school.setName(schoolName);
			school.setAddress(schoolAddress);
			schoolService.update(school);
			return SUCCESS;
		} else {
			// school doesn't exist
			// return ERROR page
			ActionContext context = ActionContext.getContext();
			ValueStack stack = context.getValueStack();
			stack.set("errorMsg", "Error: School Doesn't Exist!");
			return ERROR;
		}
	}
	
	public void prepareUpdate(){
		school = schoolService.getById(schoolId);
	}
	
	public String editor(){
		school = schoolService.getById(schoolId);
		return SUCCESS;
	}
	
	//getters and setters ...
	public SchoolService getSchoolService() {
		return schoolService;
	}

	public void setSchoolService(SchoolService schoolService) {
		this.schoolService = schoolService;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}
	
	public Page<School> getSchools() {
		return schools;
	}

	public void setSchools(Page<School> schools) {
		this.schools = schools;
	}
	
	public int getOperationType() {
		return operationType;
	}

	public void setOperationType(int operationType) {
		this.operationType = operationType;
	}

	public int getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(int schoolId) {
		this.schoolId = schoolId;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getSchoolAddress() {
		return schoolAddress;
	}

	public void setSchoolAddress(String schoolAddress) {
		this.schoolAddress = schoolAddress;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	@Override
	public School getModel() {
		return school;
	}

	@Override
	public void prepare() throws Exception {
	}
	
}
