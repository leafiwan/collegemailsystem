package com.jieshuhuiyou.action;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.time.DateUtils;
import org.apache.struts2.StrutsStatics;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.jieshuhuiyou.Config;
import com.jieshuhuiyou.entity.AutoLoginRecord;
import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.Borrow;
import com.jieshuhuiyou.entity.ResetPasswordRecord;
import com.jieshuhuiyou.entity.Review;
import com.jieshuhuiyou.entity.School;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.exceptions.PasswordNotMatchException;
import com.jieshuhuiyou.exceptions.UserAlreadyExistsException;
import com.jieshuhuiyou.mail.DefaultMailSender;
import com.jieshuhuiyou.mail.Mail;
import com.jieshuhuiyou.mail.TemplateHTMLMail;
import com.jieshuhuiyou.mail.template.RegisterSuccessMailTemplate;
import com.jieshuhuiyou.service.AutoLoginRecordService;
import com.jieshuhuiyou.service.BookService;
import com.jieshuhuiyou.service.BorrowService;
import com.jieshuhuiyou.service.ResetPasswordRecordService;
import com.jieshuhuiyou.service.ReviewService;
import com.jieshuhuiyou.service.SchoolService;
import com.jieshuhuiyou.service.UserService;
import com.jieshuhuiyou.util.LevelHelper;
import com.jieshuhuiyou.util.MD5er;
import com.jieshuhuiyou.util.UserUtil;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

import freemarker.template.utility.StringUtil;

@Controller
@Scope("prototype")
public class UserAction extends ActionSupport implements ModelDriven<User>,
		Preparable {


	private static final long serialVersionUID = 2137860077507062846L;

	@Resource
	private UserService userService;
	@Resource
	private BookService bookService;
	@Resource
	private SchoolService schoolService;

	@Resource
	private AutoLoginRecordService autoLoginRecordService;

	@Resource
	private BorrowService borrowService;

	@Resource
	private ResetPasswordRecordService resetPasswordRecordService;

	@Resource
	private ReviewService reviewService;
	
	private User user;

	private String realName;
	private String qqNumber;
	private String cellphone;
	private String selfDes;
	private String password2;
	private String captcha;
	private String newPwd;
	private String originPwd;
	private String userEmail;
	private String key;
	private String action;
	private int school_id;// 传入的school_id
	private School school;
	private List<School> schools;//all schools list
	
	private String referrer;
	private boolean rememberMe;

	private int userId;
	private List<Book> providedBooks;
	private static final int PROVIDED_BOOKS_PAGE_SIZE = 8;
	private List<Borrow> borrows;
	private static final int BORROWS_PAGE_SIZE = 8;
	private List<Review> reviews;
	private static final int REVIEWS_PAGE_SIZE = 5;
	
	private String credit;
	

	/**
	 * 用户注册 Action
	 * 
	 * @return
	 */
	public String register() throws Exception {
		try {
			//set school
			school = schoolService.getById(school_id);
			school.setUsersCount(school.getUsersCount() + 1);
			user.setSchool(school);
			// produce the password salt
			String salt = UserUtil.produceSalt();
			user.setPasswordSalt(salt);
			// md5 password
			user.setPassword(MD5er.md5(user.getPassword() + salt));
			
			user.setName(StringUtil.HTMLEnc(user.getName()));
			
			//user.setDescription(StringUtil.HTMLEnc(user.getDescription()));
			
			userService.register(user);
		} catch (UserAlreadyExistsException e) {
			addFieldError("email", e.getReadbleMsg());
			return Action.INPUT;
		}
		// send a welcome email
		Mail mail = new TemplateHTMLMail(user.getEmail(), new RegisterSuccessMailTemplate(user));	
		DefaultMailSender.getSender().addMail(mail);
		return Action.SUCCESS;
	}
	
	public void prepareRegister() {
		user = new User();
	}

	/**
	 * 用户登录
	 * 
	 * @return
	 */
	public String login() {
		// md5 password
		User u = userService.getByEmail(user.getEmail());
		if(u != null) {
			String salt = u.getPasswordSalt();
			user.setPassword(MD5er.md5(user.getPassword() + salt));
		}
		try {
			user = userService.login(user);
		} catch (PasswordNotMatchException e) {
			addActionError("notMatch");
			return Action.ERROR;
		}
		// FIXME! using a simple method to implement permission module
		// When business services become more complex,
		// should use a better method to implement it, such as RBAC
		ActionContext context = ActionContext.getContext();
		Map<String, Object> session = context.getSession();
		List<String> roleList = new ArrayList<String>();
		roleList.add(Config.SESSION_VALUE_ROLE_USER);
		session.put(Config.SESSION_KEY_ROLE, roleList);
		session.put(Config.SESSION_KEY_USER_ID, user.getId());
		session.put(Config.SESSION_KEY_USER_NAME, user.getName());
		session.put(Config.SESSION_KEY_USER_SMALL_AVATAR, user.getSmallAvatar());
		session.put(Config.SESSION_KEY_SCHOOL, user.getSchool());

		if (rememberMe) {
			HttpServletResponse response = (HttpServletResponse) ActionContext
					.getContext().get(StrutsStatics.HTTP_RESPONSE);
			int userId = user.getId();
			String recordContent = MD5er.md5(user.getEmail()
					+ user.getPassword() + new Date().getTime());
			String content = userId + "|" + recordContent;
			Cookie cookie = new Cookie(Config.COOKIES_KEY_USER_REMEMBER_ME,
					content);
			cookie.setPath("/");
			cookie.setMaxAge(15 * 24 * 60 * 60);
			response.addCookie(cookie);
			AutoLoginRecord record = new AutoLoginRecord();
			record.setUser(user);
			record.setContent(recordContent);
			autoLoginRecordService.saveOrUpdate(record);
		}
		return Action.SUCCESS;
	}

	public void prepareLogin() {
		user = new User();
	}

	/**
	 * 用户退出
	 * 
	 * @return
	 */
	public String logout() {
		ActionContext context = ActionContext.getContext();
		Map<String, Object> session = context.getSession();

		// delete remember me cookie and record if exists
		AutoLoginRecord record = autoLoginRecordService
				.getByUserId((Integer) session.get(Config.SESSION_KEY_USER_ID));
		if (record != null) {
			autoLoginRecordService.delete(record);
		}
		HttpServletRequest request = (HttpServletRequest) ActionContext
				.getContext().get(StrutsStatics.HTTP_REQUEST);
		HttpServletResponse response = (HttpServletResponse) ActionContext
				.getContext().get(StrutsStatics.HTTP_RESPONSE);
		Cookie[] cookies = request.getCookies();
		for (Cookie c : cookies) {
			if (c.getName().equals(Config.COOKIES_KEY_USER_REMEMBER_ME)) {
				c.setMaxAge(0);
				c.setPath("/");
				response.addCookie(c);
			}
		}
		// clear session
		session.remove(Config.SESSION_VALUE_ROLE_USER);
		session.remove(Config.SESSION_KEY_ROLE);
		session.remove(Config.SESSION_KEY_USER_ID);
		session.remove(Config.SESSION_KEY_USER_NAME);
		session.remove(Config.SESSION_KEY_USER_SMALL_AVATAR);
		session.remove(Config.SESSION_KEY_SCHOOL);
		return Action.SUCCESS;
	}

	/**
	 * 设置动作
	 * 
	 * @return
	 */
	public String settings() {
		if (action.equals("profile")) {
			this.settingsProfile();
		} else if (action.equals("password")) {
			this.settingsPassword();
		}
		return Action.SUCCESS;
	}

	public void prepareSettings() {
		int currentUserId = (Integer) ActionContext.getContext().getSession()
				.get(Config.SESSION_KEY_USER_ID);
		user = userService.getById(currentUserId);
	}

	/**
	 * 修改个人资料
	 * 
	 * @return
	 */
	private String settingsProfile() {
		realName = StringUtil.HTMLEnc(realName);
		selfDes = StringUtil.HTMLEnc(selfDes);
		
		user.setName(realName);
		user.setQq(qqNumber);
		user.setPhoneNumber(cellphone);
		user.setDescription(selfDes);
		school = schoolService.getById(school_id);
		user.setSchool(school);
		userService.update(user);
		// refresh http session
		Map<String, Object> session = ActionContext.getContext().getSession();
		session.put(Config.SESSION_KEY_USER_NAME, user.getName());
		session.put("currentSchoolId" , school.getId());
		session.put("currentSchoolName" , school.getName());
		session.put(Config.SESSION_KEY_SCHOOL, school);
		addActionMessage("修改成功！");
		return Action.SUCCESS;
	}

	public void prepareSettingsProfile() {
		int currentUserId = (Integer) ActionContext.getContext().getSession()
				.get(Config.SESSION_KEY_USER_ID);
		user = userService.getById(currentUserId);
	}

	/**
	 * 修改密码
	 * 
	 * @return
	 */
	private String settingsPassword() {
		String salt = user.getPasswordSalt();
		String md5Pwd = MD5er.md5(newPwd + salt);
		user.setPassword(md5Pwd);
		userService.update(user);
		addActionMessage("修改成功！");
		return Action.SUCCESS;
	}

	public void prepareSettingsPassword() {
		int currentUserId = (Integer) ActionContext.getContext().getSession()
				.get(Config.SESSION_KEY_USER_ID);
		user = userService.getById(currentUserId);
	}

	/**
	 * 浏览用户
	 * 
	 * @return
	 */
	public String view() {
		if (user == null) {
			return Action.NONE;
		}
		providedBooks = bookService.getByProvider(user, 0,
				PROVIDED_BOOKS_PAGE_SIZE, true);
		borrows = borrowService.getByBorrower(user, 0, BORROWS_PAGE_SIZE, true);
		reviews = reviewService.getByAuthor(user, 0, REVIEWS_PAGE_SIZE, true);
		
		
		return Action.SUCCESS;
	}


	public void prepareView() {
		user = userService.getById(userId);
		//转换float类型，舍弃后一位
		BigDecimal bd = new BigDecimal(user.getCredit());
		bd = bd.setScale(1, BigDecimal.ROUND_DOWN);
		user.setCredit((float)bd.doubleValue());
	}

	/**
	 * 浏览当前用户 ( set 'user' to current user)
	 * 
	 * @return
	 */
	public String viewCurrent() {
		return Action.SUCCESS;
	}

	public void prepareViewCurrent() {
		int currentUserId = (Integer) ActionContext.getContext().getSession()
				.get(Config.SESSION_KEY_USER_ID);
		user = userService.getById(currentUserId);
		//get all school
		schools = schoolService.getAllSchools();
	}

	/**
	 * 设置头像
	 * 
	 * @return
	 */
	public String settingsAvatar() {
		return Action.SUCCESS;
	}

	public void prepareSettingsAvatar() {
		int currentUserId = (Integer) ActionContext.getContext().getSession()
				.get(Config.SESSION_KEY_USER_ID);
		user = userService.getById(currentUserId);
	}

	public String resetPassword() {
		// apply for reseting
		if (userEmail != null && !(userEmail.trim().equals(""))) {
			if (user != null) {
				resetPasswordRecordService.apply(user);
				return "mailSended";
			} else {
				return "userNotExists";
			}
		}
		if (key != null && !(key.trim().equals(""))) {
			// validate key
			ResetPasswordRecord record = resetPasswordRecordService
					.getByKey(key);
			if (record == null
					|| new Date().after(DateUtils.addDays(
							record.getCreateDate(),
							Config.RESET_PASSWORD_RECORD_EXPIRES))) {
				// delete expired record
				if (record != null) {
					resetPasswordRecordService.delete(record);
				}
				return "wrongKey";
			}
			if (action != null && action.equals("reset") && newPwd != null
					&& password2 != null) {
				// update user's password
				User user = record.getUser();
				user.setPassword(MD5er.md5(newPwd));
				userService.update(user);

				// delete record
				resetPasswordRecordService.delete(record);

				return "done";
			} else if (action == null || !action.equals("resetForm")) {
				return "resetForm";
			}
		}
		return Action.SUCCESS;
	}

	public void prepareResetPassword() {
		if (userEmail != null) {
			user = userService.getByEmail(userEmail);
		}
	}

	// validation of resetPassword
	public void validateResetPassword() {
		if (userEmail != null) {
			userEmail = userEmail.trim();
			if (!userEmail
					.matches("^(([a-zA-Z0-9\\._-]+)@([a-zA-Z0-9-]+)\\.([a-zA-Z]{2,4}))$"))
				addFieldError("userEmail", "邮件地址错误");
		}

		if (action != null && action.equals("reset")) {
			if (newPwd == null || newPwd.trim().equals("")) {
				addFieldError("newPwd", "新密码不能为空");
			} else if (!newPwd.matches("^\\w{5,20}$")) {
				addFieldError("newPwd", "新密码只能为 5-20 位普通字符");
			}
			if (password2 == null || !password2.equals(newPwd)) {
				addFieldError("password2", "两次输入的密码不相同");
			}

		}

	}

	@Override
	public void prepare() throws Exception {
	}

	@Override
	public User getModel() {
		return user;
	}

	// setters and getters
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public String getPassword2() {
		return password2;
	}

	public AutoLoginRecordService getAutoLoginRecordService() {
		return autoLoginRecordService;
	}

	public void setAutoLoginRecordService(
			AutoLoginRecordService autoLoginRecordService) {
		this.autoLoginRecordService = autoLoginRecordService;
	}

	public void setPassword2(String password2) {
		this.password2 = password2;
	}

	public String getCaptcha() {
		return captcha;
	}

	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}

	public BorrowService getBorrowService() {
		return borrowService;
	}

	public void setBorrowService(BorrowService borrowService) {
		this.borrowService = borrowService;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getQqNumber() {
		return qqNumber;
	}

	public void setQqNumber(String qqNumber) {
		this.qqNumber = qqNumber;
	}

	public String getCellphone() {
		return cellphone;
	}

	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}

	public List<Borrow> getBorrows() {
		return borrows;
	}

	public void setBorrows(List<Borrow> borrows) {
		this.borrows = borrows;
	}

	public String getSelfDes() {
		return selfDes;
	}

	public void setSelfDes(String selfDes) {
		this.selfDes = selfDes;
	}

	public String getNewPwd() {
		return newPwd;
	}

	public String getOriginPwd() {
		return originPwd;
	}

	public void setOriginPwd(String originPwd) {
		this.originPwd = originPwd;
	}

	public void setNewPwd(String newPwd) {
		this.newPwd = newPwd;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public List<Book> getProvidedBooks() {
		return providedBooks;
	}

	public void setProvidedBooks(List<Book> providedBooks) {
		this.providedBooks = providedBooks;
	}

	public BookService getBookService() {
		return bookService;
	}

	public void setBookService(BookService bookService) {
		this.bookService = bookService;
	}

	public String getReferrer() {
		return referrer;
	}

	public void setReferrer(String referrer) {
		this.referrer = referrer;
	}

	public boolean isRememberMe() {
		return rememberMe;
	}

	public void setRememberMe(boolean rememberMe) {
		this.rememberMe = rememberMe;
	}

	public ResetPasswordRecordService getResetPasswordRecordService() {
		return resetPasswordRecordService;
	}

	public void setResetPasswordRecordService(
			ResetPasswordRecordService resetPasswordRecordService) {
		this.resetPasswordRecordService = resetPasswordRecordService;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public int getSchool_id() {
		return school_id;
	}

	public void setSchool_id(int school_id) {
		this.school_id = school_id;
	}

	public SchoolService getSchoolService() {
		return schoolService;
	}

	public void setSchoolService(SchoolService schoolService) {
		this.schoolService = schoolService;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public List<School> getSchools() {
		return schools;
	}

	public void setSchools(List<School> schools) {
		this.schools = schools;
	}

	public List<Review> getReviews() {
		return reviews;
	}

	public void setReviews(List<Review> reviews) {
		this.reviews = reviews;
	}

	public ReviewService getReviewService() {
		return reviewService;
	}

	public void setReviewService(ReviewService reviewService) {
		this.reviewService = reviewService;
	}
	
	public String getCredit() {
		return credit;
	}

	public void setCredit(String credit) {
		this.credit = credit;
	}
	
	
}
