package com.jieshuhuiyou.action;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.jieshuhuiyou.entity.School;
import com.jieshuhuiyou.service.SchoolService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.util.ValueStack;

@Controller
@Scope("prototype")
public class RegisterAction extends ActionSupport {

	private static final long serialVersionUID = 4879645977256056760L;
	
	@Resource
	private SchoolService schoolService;
	
	private List<School> schools;
	
	@Override
	public String execute(){
		ActionContext context = ActionContext.getContext();   
		ValueStack stack = context.getValueStack();   
		schools = schoolService.getAllSchools();
		stack.set("schools", schools);
		return SUCCESS;
	}

	//getters and setters...
	public SchoolService getSchoolService() {
		return schoolService;
	}

	public void setSchoolService(SchoolService schoolService) {
		this.schoolService = schoolService;
	}
}
