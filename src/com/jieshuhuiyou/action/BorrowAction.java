package com.jieshuhuiyou.action;


import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.jieshuhuiyou.Config;
import com.jieshuhuiyou.entity.Borrow;
import com.jieshuhuiyou.entity.Event;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.entity.UserFollowRelation;
import com.jieshuhuiyou.entity.events.ReturnBookEvent;
import com.jieshuhuiyou.infrastructure.Page;
import com.jieshuhuiyou.service.BorrowService;
import com.jieshuhuiyou.service.EventService;
import com.jieshuhuiyou.service.UserFollowRelationService;
import com.jieshuhuiyou.service.UserService;
import com.jieshuhuiyou.service.permission.PermissionValidatableAction;
import com.jieshuhuiyou.util.UserUtil;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

@Controller
@Scope("prototype")
public class BorrowAction extends PermissionValidatableAction 
	implements Preparable, ModelDriven<Borrow> {

	private static final long serialVersionUID = 3572655949400894409L;

	@Resource
	private BorrowService borrowService;
	
	@Resource
	private UserService userService;
	
	@Resource
	private EventService eventService;
	
	@Resource
	private UserFollowRelationService userFollowRelationService;
	
	private Borrow borrow;
	
	private List<Borrow> borrows;
	
	private int bid;
	
	private String result;
	
	private User currentUser;
	
	
	/**
	 * list 的类型
	 * 1 借阅的
	 * 2 借出的
	 */
	private int type;
	
	private int start;
	
	private Page<Borrow> page;
	
	private static final int PAGE_SIZE = 5;
	
	/**
	 * 浏览一个借阅
	 * @return
	 */
	public String view() {
		if(borrow == null) {
			return Action.NONE;
		}
		return Action.SUCCESS;
	}
	
	public void prepareView() {
		if(bid == 0) {
			return;
		}
		
		borrow = borrowService.getById(bid);
		
	}
	/**
	 * 还书
	 * @return
	 */
	public String returnBook() {
		if(borrow == null) {
			result = "none";
			return Action.SUCCESS;
		}
		
		borrowService.returnBook(borrow, UserUtil.getCurrentUser());
		
		Event returnBookEvent = new ReturnBookEvent(borrow.getUser(), borrow.getSharing().getUser(), borrow.getSharing().getBook()).toEventEntity();
		eventService.save(returnBookEvent);
		
		result = "success";
		return Action.SUCCESS;
	}
	
	public void prepareReturnBook() {
		if(bid == 0) {
			return;
		}
		borrow = borrowService.getById(bid);
	}
	
	
	/**
	 * 列出我的 borrow
	 * @return
	 */
	public String mine() {		
		
		if(type == 0) {
			borrows = borrowService.getByBorrower(currentUser, start, PAGE_SIZE, true);
			page = new Page<Borrow>(borrows, start, PAGE_SIZE, currentUser.getBorrowedCount());
		} else if( type == 1) {
			borrows = borrowService.getByProvider(currentUser, start, PAGE_SIZE, true);
			page = new Page<Borrow>(borrows, start, PAGE_SIZE, currentUser.getBorrowingCount());
		}
		
		return Action.SUCCESS;
	}
	
	public void prepareMine() {
		currentUser = UserUtil.getCurrentUser();
	}
	
	@Override
	public Borrow getModel() {
		return borrow;
	}

	@Override
	public void prepare() throws Exception {
		
	}

	public int getBid() {
		return bid;
	}

	public void setBid(int bid) {
		this.bid = bid;
	}

	public Borrow getBorrow() {
		return borrow;
	}

	public void setBorrow(Borrow borrow) {
		this.borrow = borrow;
	}

	public BorrowService getBorrowService() {
		return borrowService;
	}

	public void setBorrowService(BorrowService borrowService) {
		this.borrowService = borrowService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public List<Borrow> getBorrows() {
		return borrows;
	}

	public void setBorrows(List<Borrow> borrows) {
		this.borrows = borrows;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public User getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}

	public Page<Borrow> getPage() {
		return page;
	}

	public void setPage(Page<Borrow> page) {
		this.page = page;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public void setEventService(EventService eventService) {
		this.eventService = eventService;
	}

	public EventService getEventService() {
		return eventService;
	}
	
}
