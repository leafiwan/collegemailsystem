package com.jieshuhuiyou.action;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.Event;
import com.jieshuhuiyou.entity.Review;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.entity.comment.ReviewComment;
import com.jieshuhuiyou.entity.events.ReviewEvent;
import com.jieshuhuiyou.infrastructure.Page;
import com.jieshuhuiyou.service.BookService;
import com.jieshuhuiyou.service.CommentService;
import com.jieshuhuiyou.service.EventService;
import com.jieshuhuiyou.service.ReviewService;
import com.jieshuhuiyou.service.permission.PermissionValidatableAction;
import com.jieshuhuiyou.util.UserUtil;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

import freemarker.template.utility.StringUtil;

@Controller
@Scope("prototype")
public class ReviewAction extends PermissionValidatableAction implements
		Preparable, ModelDriven<Review> {

	private static final long serialVersionUID = 6803182221147295041L;

	private Review review;
	
	private Page<Review> page;

	private long rid;
	
	private int bid;
	
	private int start;
	
	private static final int COMMENT_PAGE_SIZE = 25;
	
	private static final int PAGE_SIZE = 10;
	
	private Page<ReviewComment> comments;
	
	private Book book;
	
	private List<Review> reviews;
	
	@Resource
	private ReviewService reviewService;
	
	@Resource
	private BookService bookService;
	
	@Resource
	private CommentService commentService;
	
	@Resource
	private EventService eventService;
	
	/**
	 * 创建书评
	 * @return
	 */
	public String create() {
		if(bid == 0) {
			return Action.NONE;
		}
		Book book =  bookService.getById(bid);
		if(book == null) {
			return Action.NONE;
		}
		
		User author = UserUtil.getCurrentUser();
		
		review.setAuthor(author);
		review.setBook(book);
		
		review.setTitle(StringUtil.HTMLEnc(review.getTitle()));
		review.setContent(StringUtil.HTMLEnc(review.getContent()));
		reviewService.create(review);
		
		
		//创建一个新书评event
		Event reviewEvent = new ReviewEvent(UserUtil.getCurrentUser(), review).toEventEntity();
		eventService.save(reviewEvent);
		
		return Action.SUCCESS;
	}
	
	public void prepareCreate() {
		review = new Review();
	}
	
	/**
	 * 浏览书评
	 * @return
	 */
	public String view() {
		if(review == null) {
			return Action.NONE;
		}
		
		List<ReviewComment> commentsList = commentService.getByReview(review, start, COMMENT_PAGE_SIZE, false);
		comments = new Page<ReviewComment>(commentsList, start, COMMENT_PAGE_SIZE, review.getCommentsCount());
		
		return Action.SUCCESS;
	}
	
	public void prepareView() {
		if(rid != 0) {
			review = reviewService.getById(rid);
		}
	}
	
	/**
	 * 根据书籍列出书评
	 * @return
	 */
	public String listByBook() {
		if(book == null) {
			return Action.NONE;
		}
		
		reviews = reviewService.getByBook(book, start, PAGE_SIZE, true);
		return Action.SUCCESS;
	}
	
	public void prepareListByBook() {
		if(bid == 0) {
			return;
		}
		book = bookService.getById(bid);
	}
	
	
	@Override
	public void prepare() throws Exception {
		
	}

	@Override
	public Review getModel() {
		return review;
	}

	// setters and getters
	public Review getReview() {
		return review;
	}

	public void setReview(Review review) {
		this.review = review;
	}

	public Page<Review> getPage() {
		return page;
	}

	public void setPage(Page<Review> page) {
		this.page = page;
	}

	public long getRid() {
		return rid;
	}

	public void setRid(long rid) {
		this.rid = rid;
	}

	public int getBid() {
		return bid;
	}

	public void setBid(int bid) {
		this.bid = bid;
	}

	public ReviewService getReviewService() {
		return reviewService;
	}

	public void setReviewService(ReviewService reviewService) {
		this.reviewService = reviewService;
	}

	public BookService getBookService() {
		return bookService;
	}

	public void setBookService(BookService bookService) {
		this.bookService = bookService;
	}

	public Page<ReviewComment> getComments() {
		return comments;
	}

	public void setComments(Page<ReviewComment> comments) {
		this.comments = comments;
	}

	public CommentService getCommentService() {
		return commentService;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public List<Review> getReviews() {
		return reviews;
	}

	public void setReviews(List<Review> reviews) {
		this.reviews = reviews;
	}

	public void setEventService(EventService eventService) {
		this.eventService = eventService;
	}

	public EventService getEventService() {
		return eventService;
	}

}
