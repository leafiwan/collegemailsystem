package com.jieshuhuiyou.action;

import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.jieshuhuiyou.Config;
import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.Event;
import com.jieshuhuiyou.entity.Sharing;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.entity.events.AbstractEvent;
import com.jieshuhuiyou.entity.events.ShareBookEvent;
import com.jieshuhuiyou.exceptions.InvalidParameterException;
import com.jieshuhuiyou.exceptions.SharingNotAvailableException;
import com.jieshuhuiyou.infrastructure.Page;
import com.jieshuhuiyou.service.BookService;
import com.jieshuhuiyou.service.BorrowRequestService;
import com.jieshuhuiyou.service.DoubanService;
import com.jieshuhuiyou.service.EventService;
import com.jieshuhuiyou.service.SharingService;
import com.jieshuhuiyou.service.UserService;
import com.jieshuhuiyou.service.permission.PermissionValidatableAction;
import com.jieshuhuiyou.util.UserUtil;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

import freemarker.template.utility.StringUtil;

@Controller
@Scope("prototype")
public class SharingAction extends PermissionValidatableAction 
	implements ModelDriven<Sharing>,  Preparable {

	private static final long serialVersionUID = -5706568462249551093L;
	
	private Book book;
	private String isbn;
	private Sharing sharing;
	private List<Sharing> sharings;
	private int bookId;
	private int sid;
	private String message;
	
	@Resource
	private DoubanService doubanService;
	@Resource
	private BookService bookService;
	@Resource
	private SharingService sharingService;
	@Resource
	private EventService eventService;
	
	@Resource
	private BorrowRequestService borrowRequestService;
	
	@Resource
	private UserService userService;
	
	private String result;
	
	private int start;
	
	private Page<Sharing> page;
	
	private static final int PAGE_SIZE = 10;
	
	private User currentUser = null;
	
	
	
	/**
	 * 增加一个分享
	 * @return
	 * @throws InvalidParameterException
	 */
	public String add() throws InvalidParameterException {
		if(book == null) {
			return Action.NONE;
		}
		// get current User
		Map<String, Object> session = ActionContext.getContext().getSession();
		int userId = (Integer) session.get(Config.SESSION_KEY_USER_ID);
		User currentUser = userService.getById(userId);
		
		sharing.setRequirements(StringUtil.HTMLEnc(sharing.getRequirements()));
		sharingService.save(currentUser, sharing, book);
		
		//添加一个共享event
		Event event = new ShareBookEvent(book,currentUser).toEventEntity();
		eventService.save(event);
		return Action.SUCCESS;
	}
	
	public void prepareAdd() throws Exception {
		//prepare book
		// get by id
		if(bookId != 0) {
			book = bookService.getById(bookId);
		}
		// get book by ISBN
		if (book == null && isbn != null && !isbn.equals("")) {
			book = bookService.getByISBN(isbn); // from database
			if (book == null) { // from douban.com
				book = doubanService.getBookByISBN(isbn);
				// if the book is fetched from douban, skip the validation
				setSkipValidation(true);
			}
			setValidatedSubject(book);
		}
		// new a sharing
		sharing = new Sharing();
	}
	
	@Override
	public void prepare() throws Exception {
		
	}

	/**
	 * 发起借书请求
	 * @return
	 */
	public String request() {
		if(sharing == null) {
			return Action.NONE;
		}
		Map<String, Object> session = ActionContext.getContext().getSession();
		int userId = (Integer) session.get(Config.SESSION_KEY_USER_ID);
		currentUser = userService.getById(userId);
		if(currentUser.getIntegral() > 0){
			borrowRequestService.request(currentUser, sharing, message);
			currentUser.setIntegral(currentUser.getIntegral() - Config.INTEGRAL_DEC_NUM_BRO);
			userService.update(currentUser);
		}

		return Action.SUCCESS;
	}
	
	public void prepareRequest() {
		if(sid != 0) {
			sharing = sharingService.getById(sid);
		}
	}
	
	
	public String cancel() {
		if(sharing == null) {
			this.result = "none";
			return Action.SUCCESS;
		}
		try {
			sharingService.cancel(sharing);
		} catch (SharingNotAvailableException e) {
			this.result = "notAvailable";
			return Action.SUCCESS;
		}
		this.result = "success";
		return Action.SUCCESS;
	}
	
	public void prepareCancel() {
		if(sid != 0) {
			sharing = sharingService.getById(sid);
		}
	}
	
	/**
	 * 列出我的分享
	 * @return
	 */
	public String mine() {
		this.sharings = sharingService.getByUser(currentUser, start, PAGE_SIZE, true);
		page = new Page<Sharing>(sharings, start, PAGE_SIZE, currentUser.getSharingCount());
		return Action.SUCCESS;
	}
	
	public void prepareMine() {
		currentUser = UserUtil.getCurrentUser();
	}
	
	
	
	@Override
	public Sharing getModel() {
		return sharing;
	}

	// setters and getters
	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public DoubanService getDoubanService() {
		return doubanService;
	}

	public void setDoubanService(DoubanService doubanService) {
		this.doubanService = doubanService;
	}

	public Sharing getSharing() {
		return sharing;
	}

	public void setSharing(Sharing sharing) {
		this.sharing = sharing;
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public SharingService getSharingService() {
		return sharingService;
	}

	public void setSharingService(SharingService sharingService) {
		this.sharingService = sharingService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public BookService getBookService() {
		return bookService;
	}

	public void setBookService(BookService bookService) {
		this.bookService = bookService;
	}

	public Page<Sharing> getPage() {
		return page;
	}

	public User getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}

	public void setPage(Page<Sharing> page) {
		this.page = page;
	}

	public int getSid() {
		return sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}

	public BorrowRequestService getBorrowRequestService() {
		return borrowRequestService;
	}

	public void setBorrowRequestService(BorrowRequestService borrowRequestService) {
		this.borrowRequestService = borrowRequestService;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public List<Sharing> getSharings() {
		return sharings;
	}

	public void setSharings(List<Sharing> sharings) {
		this.sharings = sharings;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public void setEventService(EventService eventService) {
		this.eventService = eventService;
	}

	public EventService getEventService() {
		return eventService;
	}
	
}
