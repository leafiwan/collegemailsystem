package com.jieshuhuiyou.action;


import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.mail.Session;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.jieshuhuiyou.Config;
import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.Review;
import com.jieshuhuiyou.entity.School;
import com.jieshuhuiyou.entity.Sharing;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.infrastructure.Page;
import com.jieshuhuiyou.service.BookService;
import com.jieshuhuiyou.service.BorrowService;
import com.jieshuhuiyou.service.DoubanService;
import com.jieshuhuiyou.service.ReviewService;
import com.jieshuhuiyou.service.SharingService;
import com.jieshuhuiyou.service.UserService;
import com.jieshuhuiyou.service.permission.PermissionValidatableAction;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

@Controller
@Scope("prototype")
public class BookAction extends PermissionValidatableAction
	implements ModelDriven<Book>, Preparable {

	private static final long serialVersionUID = -337219809863310328L;

	private Book book;
	
	private String isbn;
	private int bookId;
	
	private Page<Sharing> bookSharings;
	
	@Resource
	private BookService bookService;
	
	@Resource
	private DoubanService doubanService;
	
	@Resource
	private UserService userService;
	
	@Resource
	private SharingService sharingService;
	
	@Resource
	private BorrowService borrowService;
	
	@Resource
	private ReviewService reviewService;
	
	private List<Book> books;
	
	private int pageSize;
	
	private int start;
	
	private int range;	//查找范围

	private Sharing currentSharing;
	
	private User currentUser;
	
	private static final int BORROWS_PAGE_SIZE = 8;
	
	private List<User> borrowers;
	private List<User> providers;
	
	private List<Review> reviews;
	
	/**
	 * 分享书籍
	 * @return
	 */
	public String share() throws Exception {
		if(book == null) {
			return Action.NONE;
		}
		return Action.SUCCESS;
	}
	
	/**
	 * 浏览书籍
	 * @return
	 */
	public String view() {
		if(book == null) {
			return Action.NONE;
		}
		borrowers = userService.getBorrowers(book, 0, BORROWS_PAGE_SIZE, true);
		providers = userService.getProviders(book, 0, BORROWS_PAGE_SIZE, true);
		reviews = reviewService.getByBook(book, 0, 5, true);
		return Action.SUCCESS;
	}
	
	/**
	 * 借阅
	 * @return
	 */
	public String borrow() {
		if(book == null) {
			return Action.NONE;
		}
		if(range == 0) {
			Map<String, Object> session = ActionContext.getContext().getSession();
			School school = (School) session.get(Config.SESSION_KEY_SCHOOL);
			bookSharings = sharingService.getByBookAndSchool(book, school, start, 10, true);
		} else {
			bookSharings = sharingService.getByBook(book, start, 10, true);
		}
		
		return Action.SUCCESS;
	}
	
	public void prepareBorrow() {
		if(bookId != 0) {
			book = bookService.getById(bookId);
			
			Map<String, Object> session = ActionContext.getContext().getSession();
			currentUser = userService.getById((Integer)session.get(Config.SESSION_KEY_USER_ID));
			
		}
	}
	
	/**
	 * 写书评
	 * @return
	 */
	public String review() {
		if(book == null) {
			return Action.NONE;
		}
		return Action.SUCCESS;
	}
	
	public void prepareReview() {
		if(bookId != 0) {
			book = bookService.getById(bookId);
		}
	}
	
	
	@Override
	public void prepare() throws Exception {
		
	}

	public void prepareShare() throws Exception {
		// prepare share by book's id
		if(bookId != 0) {
			book = bookService.getById(bookId);
			return;
		}
		
		// prepare share by book's ISBN
		if(isbn != null && !isbn.equals("")) {
			book = bookService.getByISBN(isbn);
			if(book == null) {
				book = doubanService.getBookByISBN(isbn);
			}
		}
	}
	
	public void prepareView() throws Exception {
		if(bookId != 0) {
			book = bookService.getById(bookId);
		}
	}
	
	@Override
	public Book getModel() {
		return book;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public BookService getBookService() {
		return bookService;
	}

	public void setBookService(BookService bookService) {
		this.bookService = bookService;
	}

	public DoubanService getDoubanService() {
		return doubanService;
	}

	public void setDoubanService(DoubanService doubanService) {
		this.doubanService = doubanService;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public SharingService getSharingService() {
		return sharingService;
	}

	public void setSharingService(SharingService sharingService) {
		this.sharingService = sharingService;
	}

	public Page<Sharing> getBookSharings() {
		return bookSharings;
	}

	public void setBookSharings(Page<Sharing> bookSharings) {
		this.bookSharings = bookSharings;
	}

	public Sharing getCurrentSharing() {
		return currentSharing;
	}

	public void setCurrentSharing(Sharing currentSharing) {
		this.currentSharing = currentSharing;
	}

	public List<Book> getBooks() {
		return books;
	}

	public List<Review> getReviews() {
		return reviews;
	}

	public void setReviews(List<Review> reviews) {
		this.reviews = reviews;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

	public BorrowService getBorrowService() {
		return borrowService;
	}

	public void setBorrowService(BorrowService borrowService) {
		this.borrowService = borrowService;
	}

	public int getPageSize() {
		return pageSize;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public List<User> getBorrowers() {
		return borrowers;
	}

	public void setBorrowers(List<User> borrowers) {
		this.borrowers = borrowers;
	}

	public List<User> getProviders() {
		return providers;
	}

	public void setProviders(List<User> providers) {
		this.providers = providers;
	}

	public int getRange() {
		return range;
	}

	public void setRange(int range) {
		this.range = range;
	}

	public ReviewService getReviewService() {
		return reviewService;
	}

	public void setReviewService(ReviewService reviewService) {
		this.reviewService = reviewService;
	}
	
	public User getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}

}
