package com.jieshuhuiyou.action;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Map;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;

import org.apache.struts2.util.ServletContextAware;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.jieshuhuiyou.Config;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.service.UserService;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.util.logging.Logger;
import com.opensymphony.xwork2.util.logging.LoggerFactory;

@Controller
@Scope("prototype")
public class AvatarAction extends ActionSupport 
	implements ServletContextAware {

	private static final long serialVersionUID = -6387919453593332238L;

	private static final Logger LOG = LoggerFactory.getLogger(AvatarAction.class);
	
	private File avatar;
	private String avatarFileName;
	private String avatarContentType;
	
	private int x1;
	private int y1;
	private int x2;
	private int y2;
	
	ServletContext context;
	
	@Resource
	private UserService userService;
	
	
	public String upload() throws IOException {
		
		// validate aspect ratio
		BufferedImage img = null;
		try {
			img = ImageIO.read(avatar);
		} catch (IOException e1) {
			LOG.error("read file error", e1);
			throw e1;
		}
		
		if(((double)img.getWidth()) / img.getHeight() > Config.USER_AVATAR_LARGE_MAX_ASPECT_RATIO
				|| ((double)img.getHeight()) / img.getWidth() > Config.USER_AVATAR_LARGE_MAX_ASPECT_RATIO) {
						
			addFieldError("avatar", "improperAspectRatio");
			return INPUT;
		}
		
		Map<String, Object> session = ActionContext.getContext().getSession();
		User currentUser = userService.getById((Integer)session.get(Config.SESSION_KEY_USER_ID));
		String contextPath = context.getRealPath("/");
		userService.uploadAvatar(currentUser, avatar, contextPath);
		
		//  refresh session for new avatar
		session.put(Config.SESSION_KEY_USER_SMALL_AVATAR, currentUser.getSmallAvatar());
		return Action.SUCCESS;
	}
	
	public String edit() throws IOException {
		String contextPath = context.getRealPath("/");
		Map<String, Object> session = ActionContext.getContext().getSession();
		User currentUser = userService.getById((Integer)session.get(Config.SESSION_KEY_USER_ID));
		if(currentUser.getSmallAvatar().equals(Config.USER_AVATAR_DEFUALT_SMALL)) {
			return Action.INPUT;
		}
		
		userService.editAvatar(currentUser, contextPath, x1, y1, x2, y2);
		
		return Action.SUCCESS;
	}

	public File getAvatar() {
		
		return avatar;
	}

	
	//setters and getters
	public void setAvatar(File avatar) {
		this.avatar = avatar;
	}

	public String getAvatarFileName() {
		return avatarFileName;
	}

	public void setAvatarFileName(String avatarFileName) {
		this.avatarFileName = avatarFileName;
	}

	public String getAvatarContentType() {
		return avatarContentType;
	}

	public void setAvatarContentType(String avatarContentType) {
		this.avatarContentType = avatarContentType;
	}

	public int getX1() {
		return x1;
	}

	public void setX1(int x1) {
		this.x1 = x1;
	}

	public int getY1() {
		return y1;
	}

	public void setY1(int y1) {
		this.y1 = y1;
	}

	public int getX2() {
		return x2;
	}

	public void setX2(int x2) {
		this.x2 = x2;
	}

	public int getY2() {
		return y2;
	}

	public void setY2(int y2) {
		this.y2 = y2;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	@Override
	public void setServletContext(ServletContext context) {
		this.context = context;
	}
	
}
