package com.jieshuhuiyou.action;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.service.BookService;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@Controller
@Scope("prototype")
public class BookBaseAction extends ActionSupport{

	private static final long serialVersionUID = -2096685412501203116L;

	private int pageSize = 16;
	
	private int totals;
	
	private List<Book> books;

	@Resource
	private BookService bookService;
	
	@Override
	public String execute() throws Exception {
		books = bookService.getBooks(totals, pageSize, true);
		return Action.SUCCESS;
	}

	//	setters and getters
	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

	public BookService getBookService() {
		return bookService;
	}

	public void setBookService(BookService bookService) {
		this.bookService = bookService;
	}
	
	public int getPageSize() {
		return pageSize;
	}

	public int getTotals() {
		return totals;
	}

	public void setTotals(int totals) {
		this.totals = totals;
	}
}
