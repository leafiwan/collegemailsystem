package com.jieshuhuiyou.action;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.jieshuhuiyou.Config;
import com.jieshuhuiyou.entity.BorrowRequest;
import com.jieshuhuiyou.entity.Event;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.entity.UserFollowRelation;
import com.jieshuhuiyou.entity.events.BorrowBookEvent;
import com.jieshuhuiyou.infrastructure.Page;
import com.jieshuhuiyou.service.BorrowRequestService;
import com.jieshuhuiyou.service.EventService;
import com.jieshuhuiyou.service.UserFollowRelationService;
import com.jieshuhuiyou.service.UserService;
import com.jieshuhuiyou.service.permission.PermissionValidatableAction;
import com.jieshuhuiyou.util.UserUtil;
import com.jieshuhuiyou.util.memcachedHelper;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;
@Controller
@Scope("prototype")
public class BorrowRequestAction extends PermissionValidatableAction
	implements Preparable, ModelDriven<BorrowRequest> {

	private static final long serialVersionUID = -6502916327897433158L;
	
	@Resource
	private BorrowRequestService borrowRequestService;
	@Resource
	private UserService userService;
	@Resource
	private EventService eventService;
	@Resource
	private UserFollowRelationService userFollowRelationService;
	
	
	private BorrowRequest request;
	private List<BorrowRequest> requests;
	
	private Page<BorrowRequest> page;
	
	private int rid;
	
	private String result;
	
	private int pageSize = 20;
	
	private int start;
	
	/*
	 * 请求访问的请求类型
	 * 0 为收到的
	 * 1 为发出的
	 * 
	 */
	private int type;
	
	private User currentUser;
	
	
	/**
	 * 列出请求
	 * @return
	 */
	public String mine() {
		if(type == 0) {
			requests = borrowRequestService.getByReciever(currentUser, start, pageSize, true);
			page = new Page<BorrowRequest>(requests, start, pageSize, currentUser.getReceivedBorrowRequestsCount());
		}
		else if(type == 1) {
			requests = borrowRequestService.getByRequester(currentUser, start, pageSize, true);
			page = new Page<BorrowRequest>(requests, start, pageSize, currentUser.getReceivedBorrowRequestsCount());
		}
		return Action.SUCCESS;
	}
	
	public void prepareMine() {
		currentUser = UserUtil.getCurrentUser();
	}
	
	/**
	 * 同意请求
	 * @return
	 */
	public String accept() {
		
		if(request == null) {
			result = "none";
			return Action.NONE;
		}
		
		borrowRequestService.accept(request);
		
		//保存一个借书事件
		Event borrowBookEvent = new BorrowBookEvent(request.getUser(),request.getSharing().getUser(),request.getSharing().getBook()).toEventEntity();
		eventService.save(borrowBookEvent);
		
		//借书成功后，用户互相添加关注
		UserFollowRelation userFollowRelation1 = new UserFollowRelation();
		UserFollowRelation userFollowRelation2 = new UserFollowRelation();
		
		userFollowRelation1.setFollow(request.getUser());
		userFollowRelation1.setFollowed(request.getSharing().getUser());
		userFollowRelationService.save(userFollowRelation1);
		
		userFollowRelation2.setFollow(request.getSharing().getUser());
		userFollowRelation2.setFollowed(request.getUser());
		userFollowRelationService.save(userFollowRelation2);
		
		//更新缓存
		memcachedHelper memcached = memcachedHelper.getInstance();
		memcached.replace(Config.MEMCACHED_KEY_UIDLIST, ((List<Integer>)memcached.get(Config.MEMCACHED_KEY_UIDLIST)).add(request.getUser().getId()));
		
		result = "success";
		return Action.SUCCESS;
	}
	
	public void prepareAccept() {
		request = borrowRequestService.getById(rid);
	}
	
	
	/**
	 * 拒绝请求
	 * @return
	 */
	public String reject() {
		
		borrowRequestService.reject(request);
		
		result = "success";
		return Action.SUCCESS;
	}
	
	public void prepareReject() {
		request = borrowRequestService.getById(rid);
	}
	
	/**
	 * 取消请求
	 * @return
	 */
	public String cancel() {
		
		borrowRequestService.cancle(request);
		
		result = "success";
		return Action.SUCCESS;
	}
	
	public void prepareCancel() {
		request = borrowRequestService.getById(rid);
	}
	
	@Override
	public void prepare() throws Exception {
		
	}

	@Override
	public BorrowRequest getModel() {
		return request;
	}

	public BorrowRequestService getBorrowRequestService() {
		return borrowRequestService;
	}

	public void setBorrowRequestService(BorrowRequestService borrowRequestService) {
		this.borrowRequestService = borrowRequestService;
	}

	public int getRid() {
		return rid;
	}

	public void setRid(int rid) {
		this.rid = rid;
	}

	public BorrowRequest getRequest() {
		return request;
	}

	public void setRequest(BorrowRequest request) {
		this.request = request;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public List<BorrowRequest> getRequests() {
		return requests;
	}

	public void setRequests(List<BorrowRequest> requests) {
		this.requests = requests;
	}

	public User getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public int getPageSize() {
		return pageSize;
	}

	public Page<BorrowRequest> getPage() {
		return page;
	}

	public void setPage(Page<BorrowRequest> page) {
		this.page = page;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public void setEventService(EventService eventService) {
		this.eventService = eventService;
	}

	public EventService getEventService() {
		return eventService;
	}

}
