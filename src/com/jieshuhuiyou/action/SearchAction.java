package com.jieshuhuiyou.action;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.infrastructure.Page;
import com.jieshuhuiyou.service.BookService;
import com.opensymphony.xwork2.ActionSupport;

/**
 * 搜索 action
 * @author Wan Linfeng
 * ------------------
 * edited by PSJay, 2012-1-21
 * add paginator, fix a bug which will occur problems when keyword is null
 * ------------------
 */
@Controller
@Scope("prototype")
public class SearchAction extends ActionSupport{

	private static final long serialVersionUID = -5161964062699828734L;
	
	@Resource
	private BookService bookService;
	//查询信息
	private String keyword;
	//查询结果
	private List<Book> books;
	
	private Page<Book> page;
	
	private int start;
	
	private static final int PAGE_SIZE = 20;
	
	private static final List<Book> EMPTY_LIST = new ArrayList<Book>();
	
	@Override
	public String execute(){
		books = EMPTY_LIST;
		
		if(keyword == null) {
			return SUCCESS;
		}
		
		keyword = keyword.trim();
		
		if(!keyword.equals("")) {
			//第一个参数是查询信息
			books = bookService.getByQuery(keyword, 0, PAGE_SIZE);
			page = new Page<Book>(books, start, PAGE_SIZE
					, bookService.getCountByQuery(keyword));
		}
		return SUCCESS;
	}

	//setters and getters
	public BookService getBookService() {
		return bookService;
	}

	public void setBookService(BookService bookService) {
		this.bookService = bookService;
	}

	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Page<Book> getPage() {
		return page;
	}

	public void setPage(Page<Book> page) {
		this.page = page;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}
	
}
