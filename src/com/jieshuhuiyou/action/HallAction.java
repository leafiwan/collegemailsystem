package com.jieshuhuiyou.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.json.annotations.JSON;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.jieshuhuiyou.Config;
import com.jieshuhuiyou.entity.BookFollowRelation;
import com.jieshuhuiyou.entity.Event;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.entity.UserFollowRelation;
import com.jieshuhuiyou.entity.events.ConditionEvent;
import com.jieshuhuiyou.service.BookFollowRelationService;
import com.jieshuhuiyou.service.BookService;
import com.jieshuhuiyou.service.EventService;
import com.jieshuhuiyou.service.UserFollowRelationService;
import com.jieshuhuiyou.service.UserService;
import com.jieshuhuiyou.util.FileUtil;
import com.jieshuhuiyou.util.HtmlDeEncoder;
import com.jieshuhuiyou.util.JSONUtil;
import com.jieshuhuiyou.util.PathUtil;
import com.jieshuhuiyou.util.StringUtil;
import com.jieshuhuiyou.util.memcachedHelper;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import com.sun.faces.facelets.tag.jsf.html.HtmlDecorator;

@Controller
@Scope("prototype")
public class HallAction extends ActionSupport implements Preparable{
	
	private static final long serialVersionUID = -8964881661646771655L;

	@Resource
	private EventService eventService;
	
	@Resource
	private UserService userService;
	
	@Resource
	private BookService bookService;
	
	@Resource
	private UserFollowRelationService userFollowRelationService;
	
	@Resource
	private BookFollowRelationService bookFollowRelationService;
	
	private List<Event> eventList;
	
	private User currentUser;
	
	private int id;
	
	private int page = 1;
	
	private int pageSize = 10;
	
	private String condition;
	
	private List<String> faces;
	
	private String facesJson;
	
	
	public String mine(){
		return Action.SUCCESS;
	}	
	
	/**
	 * 相应添加状态
	 * @return
	 */
	public String addCondition(){
		condition = HtmlDeEncoder.HtmlEncoder(this.condition);
		condition = StringUtil.replaceAllImg(condition);
		Event conditionEvent = new ConditionEvent(this.currentUser,this.condition).toEventEntity();
		eventService.save(conditionEvent);
		return Action.SUCCESS;
	}
	
	/**
	 * 删除关注
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public void unfollowUser(){
		memcachedHelper memcached = memcachedHelper.getInstance();
		List<Integer> uidList = (List<Integer>)memcached.get(Config.MEMCACHED_KEY_UIDLIST);
		uidList.remove(uidList.indexOf(this.id));
		memcached.replace(Config.MEMCACHED_KEY_UIDLIST, uidList);
		
		UserFollowRelation userFollowRelation = userFollowRelationService.getRelationByFandFedUser(currentUser, userService.getById(this.id));
		userFollowRelationService.delete(userFollowRelation);
	}
	
	/**
	 * 添加关注人
	 */
	@SuppressWarnings("unchecked")
	public void followUser(){
		memcachedHelper memcached = memcachedHelper.getInstance();
		List<Integer> uidList = (List<Integer>)memcached.get(Config.MEMCACHED_KEY_UIDLIST);
		uidList.add(this.id);	
		memcached.replace(Config.MEMCACHED_KEY_UIDLIST,uidList);
		
		UserFollowRelation userFollowRelation = new UserFollowRelation();
		userFollowRelation.setFollow(currentUser);
		userFollowRelation.setFollowed(userService.getById(this.id));
		userFollowRelationService.save(userFollowRelation);
	}
	
	/**
	 * 删除关注书
	 */
	@SuppressWarnings("unchecked")
	public void unfollowBook(){
		memcachedHelper memcached = memcachedHelper.getInstance();
		List<Integer> bookidList = (List<Integer>)memcached.get(Config.MEMCACHED_KEY_BOOKIDLIST);
		bookidList.remove(bookidList.indexOf(this.id));
		memcached.replace(Config.MEMCACHED_KEY_BOOKIDLIST,bookidList);
		
		BookFollowRelation bookFollowRelation = bookFollowRelationService.getByUserAndBook(this.currentUser, bookService.getById(this.id));
		bookFollowRelationService.delete(bookFollowRelation);
		return;
	}
	
	/**
	 * 添加关注书
	 */
	@SuppressWarnings("unchecked")
	public void followBook(){
		memcachedHelper memcached = memcachedHelper.getInstance();
		List<Integer> bookidList = ((List<Integer>)memcached.get(Config.MEMCACHED_KEY_BOOKIDLIST));
		bookidList.add(this.id);
		memcached.replace(Config.MEMCACHED_KEY_BOOKIDLIST,bookidList);
		
		BookFollowRelation bookFollowRelation = new BookFollowRelation();
		bookFollowRelation.setBook(bookService.getById(id));
		bookFollowRelation.setUser(this.currentUser);
		bookFollowRelationService.save(bookFollowRelation);
	}
	
	/**
	 * 异步加载表情
	 * @return
	 */
	public String returnFaces(){
		try {
			this.faces = FileUtil.getAllFilesName(PathUtil.getInstance().getWebRoot() + Config.FACES_DIR_PATH);
			this.faces.add("no.gif");
			this.facesJson = JSONUtil.listToJson(this.faces);
			return Action.SUCCESS;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			return Action.ERROR;
		}
		
	}
	
	public void setEventService(EventService eventService) {
		this.eventService = eventService;
	}

	@JSON(serialize = false)
	public EventService getEventService() {
		return eventService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	@JSON(serialize = false)
	public UserService getUserService() {
		return userService;
	}




	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}



	@JSON(serialize = false)
	public User getCurrentUser() {
		return currentUser;
	}


	@JSON(serialize = false)
	public int getPage() {
		return page;
	}


	@JSON(serialize = false)
	public int getPageSize() {
		return pageSize;
	}




	public void setPage(int page) {
		this.page = page;
	}




	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}




	public void setEventList(List<Event> eventList) {
		this.eventList = eventList;
	}



	
	public List<Event> getEventList() {
		return eventList;
	}





	@Override
	public void prepare() throws Exception {
		ActionContext actionContext = ActionContext.getContext();
		currentUser = userService.getById((Integer) actionContext.getSession().get(Config.SESSION_KEY_USER_ID));
		if(currentUser != null)
		eventList= eventService.getFollowEventByUser(currentUser, page, pageSize);				
	}


	public void setCondition(String condition) {
		this.condition = condition;
	}

	@JSON(serialize = false)
	public String getCondition() {
		return condition;
	}


	public void setId(int id) {
		this.id = id;
	}

	@JSON(serialize = false)
	public int getId() {
		return id;
	}


	public void setUserFollowRelationService(UserFollowRelationService userFollowRelationService) {
		this.userFollowRelationService = userFollowRelationService;
	}


	@JSON(serialize = false)
	public UserFollowRelationService getUserFollowRelationService() {
		return userFollowRelationService;
	}

	public void setFaces(List<String> faces) {
		this.faces = faces;
	}

	@JSON(serialize = false)
	public List<String> getFaces() {
		return faces;
	}

	public void setFacesJson(String facesJson) {
		this.facesJson = facesJson;
	}

	public String getFacesJson() {
		return facesJson;
	}
	
}
