package com.jieshuhuiyou.action;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.jieshuhuiyou.entity.Comment;
import com.jieshuhuiyou.entity.Review;
import com.jieshuhuiyou.entity.comment.ReviewComment;
import com.jieshuhuiyou.service.CommentService;
import com.jieshuhuiyou.service.ReviewService;
import com.jieshuhuiyou.service.permission.PermissionValidatableAction;
import com.jieshuhuiyou.util.UserUtil;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

import freemarker.template.utility.StringUtil;

@Controller
@Scope("prototype")
public class CommentAction extends PermissionValidatableAction implements
		Preparable, ModelDriven<Comment> {

	private static final long serialVersionUID = 7166877616807988468L;

	@Resource
	private CommentService commentService;
	
	@Resource
	private ReviewService reviewService;
	
	private Comment comment;
	
	private int reviewId;
	
	/**
	 * 创建一个评论
	 * @return
	 */
	public String createReviewComment() {
		if(reviewId == 0) {
			return Action.NONE;
		}
		
		Review review = reviewService.getById(reviewId);
		
		if(review == null) {
			return Action.NONE;
		} 
		
		ReviewComment rc = (ReviewComment)comment;
		rc.setReview(review);
		rc.setAuthor(UserUtil.getCurrentUser());
		
		rc.setContent(StringUtil.HTMLEnc(rc.getContent()));
		commentService.createReviewComment(rc);
		
		return Action.SUCCESS;
	}
	
	public void prepareCreateReviewComment() {
		comment = new ReviewComment();
	}
	
	@Override
	public void prepare() throws Exception {
		
	}

	@Override
	public Comment getModel() {
		return comment;
	}

	// setters and getters
	public Comment getComment() {
		return comment;
	}

	public void setComment(Comment comment) {
		this.comment = comment;
	}

	public CommentService getCommentService() {
		return commentService;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	public int getReviewId() {
		return reviewId;
	}

	public void setReviewId(int reviewId) {
		this.reviewId = reviewId;
	}

}
