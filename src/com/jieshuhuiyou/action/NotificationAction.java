package com.jieshuhuiyou.action;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.jieshuhuiyou.entity.Notification;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.infrastructure.Page;
import com.jieshuhuiyou.service.NotificationService;
import com.jieshuhuiyou.service.permission.PermissionValidatableAction;
import com.jieshuhuiyou.util.UserUtil;
import com.jieshuhuiyou.views.NotificationDisplayHelper;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

@Controller
@Scope("prototype")
public class NotificationAction extends PermissionValidatableAction implements
		Preparable, ModelDriven<Notification> {

	private static final long serialVersionUID = 5627178645158307840L;
	
	private Notification notification;
	private List<Notification> notifications;
	private List<NotificationDisplayHelper> displayedNotifications;
	
	private static final int PAGE_SIZE = 10;
	private int start;
	
	private int nid;
	private int[] ids;
	
	private String result;
	
	private Page<NotificationDisplayHelper> page;
	
	@Resource
	private NotificationService notificationService;
	
	
	public String mine() {
		if(!notifications.isEmpty()) {
			displayedNotifications = new ArrayList<NotificationDisplayHelper>();
			for(Notification n : notifications) {
				displayedNotifications.add(new NotificationDisplayHelper(n));
			}
			page = new Page<NotificationDisplayHelper>(displayedNotifications, start, PAGE_SIZE,
					notificationService.getNotificationCount(UserUtil.getCurrentUser()));
		}
		return Action.SUCCESS;
	}
	public void prepareMine() {
		User cu = UserUtil.getCurrentUser();
		notifications = notificationService.getByOwner(cu, start, PAGE_SIZE, true);
	}
	
	@Override
	public void prepare() throws Exception {
	}

	
	public String delete() {
		if(notification != null) {
			notificationService.delete(notification);
			nid = 0;
		}else if(ids != null && ids.length != 0) {
			notificationService.deleteByIdsAndOwner(ids, UserUtil.getCurrentUser());
		}
		result = "success";
		return Action.SUCCESS;
	}
	
	public void prepareDelete() {
		if(nid != 0) {
			notification = notificationService.getById(nid);
		}
	}
	
	
	@Override
	public Notification getModel() {
		return notification;
	}
	public Notification getNotification() {
		return notification;
	}
	public void setNotification(Notification notification) {
		this.notification = notification;
	}
	public List<Notification> getNotifications() {
		return notifications;
	}
	public void setNotifications(List<Notification> notifications) {
		this.notifications = notifications;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public NotificationService getNotificationService() {
		return notificationService;
	}
	public void setNotificationService(NotificationService notificationService) {
		this.notificationService = notificationService;
	}
	public List<NotificationDisplayHelper> getDisplayedNotifications() {
		return displayedNotifications;
	}
	public void setDisplayedNotifications(
			List<NotificationDisplayHelper> displayedNotifications) {
		this.displayedNotifications = displayedNotifications;
	}
	public int getNid() {
		return nid;
	}
	public void setNid(int nid) {
		this.nid = nid;
	}
	public int[] getIds() {
		return ids;
	}
	public void setIds(int[] ids) {
		this.ids = ids;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public Page<NotificationDisplayHelper> getPage() {
		return page;
	}
	public void setPage(Page<NotificationDisplayHelper> page) {
		this.page = page;
	}

}
