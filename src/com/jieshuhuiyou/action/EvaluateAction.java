package com.jieshuhuiyou.action;

import javax.annotation.Resource;

import org.apache.struts2.json.annotations.JSON;

import com.jieshuhuiyou.Config;
import com.jieshuhuiyou.dao.NotificationDao;
import com.jieshuhuiyou.entity.Borrow;
import com.jieshuhuiyou.entity.Event;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.entity.events.EvaluateEvent;
import com.jieshuhuiyou.entity.notifications.EvaluatedNotification;
import com.jieshuhuiyou.service.BookService;
import com.jieshuhuiyou.service.BorrowService;
import com.jieshuhuiyou.service.EventService;
import com.jieshuhuiyou.service.NotificationService;
import com.jieshuhuiyou.service.UserService;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class EvaluateAction extends ActionSupport{
	
	//获取用户id
	private String borrowerId;
	
	private String providerId;
	
	private String bookId;
	
	//获取分数
	private String score;
	
	@Resource
	private BorrowService borrowService;
	
	@Resource
	private UserService userService;
	
	@Resource
	private NotificationService notificationService;
	
	@Resource
	private BookService bookService;
	
	@Resource
	private EventService eventService;
	
	@JSON(serialize = false)  
	public BorrowService getBorrowService() {
		return borrowService;
	}

	public void setBorrowService(BorrowService borrowService) {
		this.borrowService = borrowService;
	}


	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	@Override
	public String execute() throws Exception {
		
		Borrow borrow = borrowService.getById(Integer.parseInt(this.borrowerId));
		borrow.setEvaluated(true);
		borrowService.update(borrow);
		
		User borrower = userService.getById(borrow.getUser().getId());
		
		int nScore = Integer.parseInt(score);
		//设置信誉度
		if(nScore <= Config.CREDIT_PER_STAR * Config.CREDIT_STAR_NUM){
			borrower.setCredit((borrower.getCredit() * borrower.getEvalutedNum() + nScore)/(borrower.getEvalutedNum() + 1));
			borrower.setEvalutedNum(borrower.getEvalutedNum() + 1);
			userService.update(borrower);
			
			EvaluatedNotification evaluatedNodification = new EvaluatedNotification(borrow);
			notificationService.save(evaluatedNodification.toNotificationEntity());
			
			Event evaluateEvent = new EvaluateEvent(borrower, userService.getById(Integer.parseInt(providerId)), bookService.getById(Integer.parseInt(this.bookId)), Integer.parseInt(this.score)).toEventEntity();
			eventService.save(evaluateEvent);
			
		}
		
		
		return Action.SUCCESS;
		
	}

	@JSON(serialize = false)
	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public void setBorrowerId(String borrowerId) {
		this.borrowerId = borrowerId;
	}

	public String getBorrowerId() {
		return borrowerId;
	}

	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}

	public String getProviderId() {
		return providerId;
	}

	public void setBookId(String bookId) {
		this.bookId = bookId;
	}

	public String getBookId() {
		return bookId;
	}

	public void setBookService(BookService bookService) {
		this.bookService = bookService;
	}

	public BookService getBookService() {
		return bookService;
	}

	public void setNotificationService(NotificationService notificationService) {
		this.notificationService = notificationService;
	}

	public NotificationService getNotificationService() {
		return notificationService;
	}
	
	
}
