package com.jieshuhuiyou.action;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;

import com.jieshuhuiyou.Config;
import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.Borrow;
import com.jieshuhuiyou.entity.Notification;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.service.BookService;
import com.jieshuhuiyou.service.BorrowService;
import com.jieshuhuiyou.service.NotificationService;
import com.jieshuhuiyou.service.UserService;
import com.jieshuhuiyou.views.NotificationDisplayHelper;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

@Scope("prototype")
public class HomeAction extends ActionSupport
	implements Preparable {

	private static final long serialVersionUID = -3613170981794682408L;
	
	@Resource
	private UserService userService;
	@Resource
	private BookService bookService;
	@Resource
	private NotificationService notificationService;
	@Resource
	private BorrowService borrowService;
	
	private User currentUser;
	private List<Notification> notifications;
	private List<Book> sharedBooks;
	private List<Book> borrowedBooks;
	private List<NotificationDisplayHelper> displayedNotifications;
	
	private static final int NOTIFICATIONS_COUNT = 6;
	private static final int SHARED_BOOKS_COUNT = 6;
	private static final int BORROWED_BOOKS_COUNT = 6;
	
	
	@Override
	public String execute() throws Exception {
		return Action.SUCCESS;
	}
	
	@Override
	public void prepare() throws Exception {
		// get current user
		Map<String, Object> session = ActionContext.getContext().getSession();
		int currentUserId = (Integer) session.get(Config.SESSION_KEY_USER_ID);
		this.currentUser = userService.getById(currentUserId);
		
		if(currentUser == null) {
			return;
		}
		
		// get notifications
		this.notifications = notificationService.getByOwner(currentUser, 0, NOTIFICATIONS_COUNT, true);
		if(this.notifications != null && !this.notifications.isEmpty()) {
			if(displayedNotifications == null) {
				displayedNotifications = new ArrayList<NotificationDisplayHelper>();
			}
			for(Notification n : notifications) {
				displayedNotifications.add(new NotificationDisplayHelper(n));
			}
		}
		//get books
		this.sharedBooks = bookService.getByProvider(currentUser, 0, SHARED_BOOKS_COUNT, true);
		List<Borrow> borrows = borrowService.getByBorrower(currentUser, 0, BORROWED_BOOKS_COUNT, true);
		this.borrowedBooks = new LinkedList<Book>();
		for(Borrow b : borrows) {
			borrowedBooks.add(b.getSharing().getBook());
		}
		
	}
	
	//	setters and getters
	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public BookService getBookService() {
		return bookService;
	}

	public void setBookService(BookService bookService) {
		this.bookService = bookService;
	}

	public NotificationService getNotificationService() {
		return notificationService;
	}

	public void setNotificationService(NotificationService notificationService) {
		this.notificationService = notificationService;
	}

	public List<Notification> getNotifications() {
		return notifications;
	}

	public void setNotifications(List<Notification> notifications) {
		this.notifications = notifications;
	}

	public List<Book> getSharedBooks() {
		return sharedBooks;
	}

	public void setSharedBooks(List<Book> sharedBooks) {
		this.sharedBooks = sharedBooks;
	}

	public List<Book> getBorrowedBooks() {
		return borrowedBooks;
	}

	public void setBorrowedBooks(List<Book> borrowedBooks) {
		this.borrowedBooks = borrowedBooks;
	}

	public User getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}

	public List<NotificationDisplayHelper> getDisplayedNotifications() {
		return displayedNotifications;
	}

	public void setDisplayedNotifications(
			List<NotificationDisplayHelper> displayedNotifications) {
		this.displayedNotifications = displayedNotifications;
	}

	public BorrowService getBorrowService() {
		return borrowService;
	}

	public void setBorrowService(BorrowService borrowService) {
		this.borrowService = borrowService;
	}

}
