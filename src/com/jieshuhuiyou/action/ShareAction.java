package com.jieshuhuiyou.action;


import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

@Controller
@Scope("prototype")
public class ShareAction extends ActionSupport {

	private static final long serialVersionUID = 453441949060764379L;
	
	private String isbn;
	
	@Override
	public String execute() throws Exception {
		if(isbn != null) {
			return "continue";
		}
		return Action.SUCCESS;
	}
	
	@Override
	public void validate() {
		// validate ISBN
		if(isbn != null) {
			// replace all non-digital char to empty char
			isbn = isbn.replaceAll("\\D", "");
			if(isbn.length() != 10 && isbn.length() != 13) {
				addFieldError("isbn", "wrongISBN");
			}
		}
	}

	// setters and getters
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getIsbn() {
		return isbn;
	}

}
