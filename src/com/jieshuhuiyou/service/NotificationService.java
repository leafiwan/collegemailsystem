package com.jieshuhuiyou.service;

import java.util.List;

import com.jieshuhuiyou.entity.Notification;
import com.jieshuhuiyou.entity.User;

public interface NotificationService {

	/**
	 * 根据拥有者取出通知
	 * @param owner
	 * @param start
	 * @param count
	 * @param desc
	 * @return
	 */
	public List<Notification> getByOwner(User owner, int start, int count, boolean desc);
	
	/**
	 * 删除一个通知
	 * @param notification
	 */
	public void delete(Notification notification);

	/**
	 * 根据 id 数组和拥有者删除通知
	 * @param ids
	 * @param owner
	 */
	public void deleteByIdsAndOwner(int[] ids, User owner);
	
	/**
	 * 根据 id 取出通知
	 * @param id
	 * @return
	 */
	public Notification getById(int id);
	
	/**
	 * 得到某用户的通知数量
	 * @param user
	 * @return 通知数量
	 */
	public int getNotificationCount(User user);
	
	public void save(Notification notification);
}
