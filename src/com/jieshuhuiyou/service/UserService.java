package com.jieshuhuiyou.service;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.exceptions.PasswordNotMatchException;
import com.jieshuhuiyou.exceptions.UserAlreadyExistsException;
import com.jieshuhuiyou.infrastructure.Page;

/**
 * 用户服务接口
 * @author psjay
 *
 */
public interface UserService {

	/**
	 * 用户注册
	 * @param user
	 * @throws UserAlreadyExistsException
	 */
	public void register(User user)
		throws UserAlreadyExistsException;
	
	/**
	 * 用户登录
	 * @param user	需要登录的用户
	 * @return
	 * @throws PasswordNotMatchException
	 */
	public User login(User user) throws PasswordNotMatchException;
	
	/**
	 * 根据 id 取得用户
	 * @param id
	 * @return
	 */
	public User getById(int id);
	
	
	/**
	 * 获取书籍的共享者
	 * @param book 书籍
	 * @param start 
	 * @param count
	 * @param desc 是否倒序排列
	 * @return
	 */
	public List<User> getProviders(Book book, int start, int count, boolean desc);
	
	/**
	 * 获取书籍的借阅者
	 * @param book
	 * @param start
	 * @param count
	 * @param desc 是否倒序排列
	 * @return
	 */
	public List<User> getBorrowers(Book book, int start, int count, boolean desc);
	
	
	/**
	 * 更新一个用户
	 * @param user
	 */
	public void update(User user);
	
	/**
	 * user 上传 头像 avatarFile
	 * @param user 更换头像的用户
	 * @param avatarFile	上传的图片文件
	 */
	public void uploadAvatar(User user, File avatarFile, String contextPath) throws IOException;
	
	/**
	 * 根据 email 获取用户
	 * @param email
	 * @return
	 */
	public User getByEmail(String email);
	
	/**
	 * user 编辑头像
	 * @param user
	 * @param contextPath
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @throws IOException
	 */
	public void editAvatar(User user, String contextPath, int x1, int y1, int x2, int y2) 
		throws IOException;
	
	/**
	 * 得到某个区间内的用户
	 * @param start 起始位置
	 * @param count 最大数
	 * @param desc
	 * @return
	 */
	public Page<User> getUsers(int start , int count , boolean desc);
	
	/**
	 * 根据条件查询user
	 * @param name
	 * @param school
	 * @param start
	 * @param count
	 * @return
	 */
	public Page<User> searchUsers(String email , String name , String school , int start , int count , boolean desc);

	/**
	 * 增加某个用户的积分
	 * @param user
	 * @param integral
	 */
	public void increaseUsersIntegral(User user , int integral);
}
