package com.jieshuhuiyou.service;

import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.entity.UserFollowRelation;

public interface UserFollowRelationService{
	
	/**
	 * 保存一个新的映射
	 * @param userFollowRelation
	 */
	public void save(UserFollowRelation userFollowRelation);
	
	/**
	 * 删除一个映射
	 * @param userFollowRelation
	 */
	public void delete(UserFollowRelation userFollowRelation);
	
	public UserFollowRelation getRelationByFandFedUser(User follow,User followed);
}
