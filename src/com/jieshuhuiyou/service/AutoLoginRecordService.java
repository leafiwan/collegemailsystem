package com.jieshuhuiyou.service;

import com.jieshuhuiyou.entity.AutoLoginRecord;

public interface AutoLoginRecordService {

	/**
	 * 保存或更新
	 * @param record
	 */
	public void saveOrUpdate(AutoLoginRecord record);
	
	/**
	 * 删除
	 * @param record
	 */
	public void delete(AutoLoginRecord record);
	
	
	/**
	 * 根据用户名查找
	 * @param uid
	 * @return
	 */
	public AutoLoginRecord getByUserId(int uid);
	
}
