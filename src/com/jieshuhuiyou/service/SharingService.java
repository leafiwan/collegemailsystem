package com.jieshuhuiyou.service;

import java.util.List;

import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.School;
import com.jieshuhuiyou.entity.Sharing;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.exceptions.SharingNotAvailableException;
import com.jieshuhuiyou.infrastructure.Page;


/**
 * 分享服务类接口
 * @author psjay
 *
 */
public interface SharingService {

	/**
	 * 保存一个 Sharing
	 * @param user	分享的用户
	 * @param sharing
	 * @param book 
	 */
	public void save(User user, Sharing sharing, Book book);
	
	/**
	 * 得到一本书籍的分享
	 * @param book
	 */
	public Page<Sharing> getByBook(Book book, int start, int count, boolean desc);
	
	/**
	 * 根据书籍和学校取得分享
	 * @param book
	 * @param school
	 * @param start
	 * @param count
	 * @param desc
	 * @return
	 */
	public Page<Sharing> getByBookAndSchool(final Book book, final School school
			, final int start, final int count, final boolean desc);
	
	/**
	 * 根据书籍和学校取得分享
	 * @param book
	 * @param school
	 * @param start
	 * @param count
	 * @param desc
	 * @return
	 */
	public List<Sharing> getByBookAndSchool(Book book,School school);
	
	/**
	 * 得到一个分享
	 * @param id	分享的 id
	 * @return
	 */
	public Sharing getById(int id);
	
	
	/**
	 * 取消一个分享
	 * @param sharing
	 */
	public void cancel(Sharing sharing) throws SharingNotAvailableException;
	
	
	/**
	 * 根据共享者和书籍得到一个分享
	 * @param provider
	 * @param book
	 * @return
	 */
	public Sharing getByBookAndProvider(User provider, Book book);
	
	
	/**
	 * 找出 user 的分享
	 * @param user
	 * @param start
	 * @param count
	 * @param desc
	 * @return
	 */
	public List<Sharing> getByUser(User user, int start, int count, boolean desc);
	
	
}
