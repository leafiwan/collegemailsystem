package com.jieshuhuiyou.service;

import java.util.List;

import com.jieshuhuiyou.entity.School;
import com.jieshuhuiyou.infrastructure.Page;

/**
 * 后台学校管理
 * @author WanLinfeng
 *
 */
public interface SchoolService {
	
	/**
	 * 添加一所学校进入数据库
	 * @param school
	 */
	public void add(School school);
	
	/**
	 * 删除一所学校
	 * @param school
	 */
	public void delete(School school);
	
	/**
	 * 更新一所学校的信息
	 * @param school
	 */
	public void update(School school);
	
	/**
	 * 根据学校id得到一所学校
	 * @param id
	 * @return
	 */
	public School getById(int id);

	/**
	 * 根据学校名字得到一所学校
	 * @param name
	 */
	public School getByName(String name);

	/**
	 * 得到数据库中的所有学校
	 * @return
	 */
	public List<School> getAllSchools();
	
	/**
	 * 得到学校的分页
	 * @param start
	 * @param count
	 * @return
	 */
	public Page<School> getSchoolsPage(int start, int count , boolean desc);
}
