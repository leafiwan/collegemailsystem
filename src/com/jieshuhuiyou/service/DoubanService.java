package com.jieshuhuiyou.service;

import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.exceptions.DoubanException;

/**
 * 豆瓣服务接口
 * @author psjay
 *
 */
public interface DoubanService {

	/**
	 * 从豆瓣获取一个书籍对象
	 * @param isbn	书籍的 isbn 编号， 10位或者13位，不包含“-”
	 * @return	获取的书对象，如果不存在，则返回 null
	 */
	public Book getBookByISBN(String isbn) throws DoubanException;
	
	/**
	 * 从豆瓣获取一个书籍对象
	 * @param doubanId	书籍在豆瓣的 subject Id
	 * @return	获取的书对象，如果不存在，则返回 null
	 */
	public Book getBookByDoubanId(String doubanId) throws DoubanException;
}
