package com.jieshuhuiyou.service;

import com.jieshuhuiyou.entity.ResetPasswordRecord;
import com.jieshuhuiyou.entity.User;

public interface ResetPasswordRecordService {

	/**
	 * 申请重置密码
	 * @param user 申请重置密码的用户
	 */
	public void apply(User user);
	
	

	/**
	 * 根据 key 取得记录
	 * @param key
	 * @return
	 */
	public ResetPasswordRecord getByKey(String key);
	
	
	/**
	 * 删除一条记录
	 * @param record
	 */
	public void delete(ResetPasswordRecord record);
	
}
