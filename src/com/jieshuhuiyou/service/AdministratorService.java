package com.jieshuhuiyou.service;

import com.jieshuhuiyou.entity.Administrator;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.infrastructure.Page;

/**
 * 管理员业务类
 * @author psjay
 *
 */
public interface AdministratorService {

	/**
	 * 管理员登录
	 * @param user 当前登录的用户
	 * @param password 输入的密码
	 * @return
	 */
	public boolean login(User user, String password);
	
	
	/**
	 * 得到一页管理员记录
	 * @param start
	 * @param count
	 * @param desc
	 * @return
	 */
	public Page<Administrator> list(int start, int count, boolean desc);
	
	/**
	 * 删除一个管理员
	 * @param administrator
	 */
	public void delete(Administrator administrator);
	
	/**
	 * 添加一个管理员
	 * @param user
	 */
	public void add(User user, String password);
	
	/**
	 * 更改密码
	 * @param administrator
	 */
	public void changePassword(Administrator administrator, String newPassword);
	
	/**
	 * 通过 id 取得管理员
	 * @param id
	 * @return
	 */
	public Administrator getById(int id);
	
}
