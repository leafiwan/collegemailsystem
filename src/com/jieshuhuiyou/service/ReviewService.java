package com.jieshuhuiyou.service;

import java.util.List;

import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.Review;
import com.jieshuhuiyou.entity.User;

/**
 * 书评 Service
 * @author PSJay
 *
 */
public interface ReviewService {

	/**
	 * 创建一个书评
	 */
	public void create(Review review);
	
	/**
	 * 删除一个书评
	 * @param review
	 */
	public void delete(Review review);
	
	/**
	 * 根据 id 取书评
	 * @param id
	 * @return
	 */
	public Review getById(long id);
	
	/**
	 * 根据作者查找书评
	 * @param author
	 * @param start
	 * @param count
	 * @param desc
	 * @return
	 */
	public List<Review> getByAuthor(User author, int start, int count, boolean desc);
	
	
	/**
	 * 根据书籍查找书评
	 * @param book
	 * @param start
	 * @param count
	 * @param desc
	 * @return
	 */
	public List<Review> getByBook(Book book, int start, int count, boolean desc);
	
}
