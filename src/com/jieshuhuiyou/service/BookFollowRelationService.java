package com.jieshuhuiyou.service;

import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.BookFollowRelation;
import com.jieshuhuiyou.entity.User;



public interface BookFollowRelationService {

	public void save(BookFollowRelation bookFollowRelation);
	
	public void delete(BookFollowRelation bookFollowRelation);
	
	public BookFollowRelation getByUserAndBook(User user, Book book);
}
