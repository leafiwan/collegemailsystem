package com.jieshuhuiyou.service;

import java.util.List;

import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.infrastructure.Page;


/**
 * 书籍类服务接口
 * @author psjay
 *
 */
public interface BookService {

	/**
	 * 保存一本新的书籍
	 * @param book
	 */
	public void save(Book book);
	
	/**
	 * 更新一本书
	 * @param book
	 */
	public void update(Book book);
	
	/**
	 * 删除一本书
	 * @param book
	 */
	public void delete(Book book);
	
	/**
	 * 根据 ISBN 查询书籍
	 * @return
	 */
	public Book getByISBN(String isbn);
	
	/**
	 * 根据 id 查询书籍
	 * @param id
	 * @return
	 */
	public Book getById(int id);
	
	/**
	 * get book from dao
	 * @param start	
	 * @param count
	 * @param desc order
	 * @return books
	 */
	public List<Book> getBooks(int start, int count, boolean desc);
	
	/**
	 * get book from dao
	 * @param start	
	 * @param count
	 * @param desc order
	 * @return books
	 */
	public Page<Book> getBooksPage(int start, int count, boolean desc);
	
	/**
	 * 根据 共享者 查找书籍
	 * @param provider
	 * @return
	 */
	public List<Book> getByProvider(User provider, int start, int count, boolean desc);
	
	/**
	 * 根据借阅者查找书籍
	 * @param borrower
	 * @param start
	 * @param count
	 * @param desc
	 * @return
	 */
	public List<Book> getByBorrower(User borrower, int start, int count, boolean desc);
	
	/**
	 * 根据用户搜索查找书籍
	 * @param queryInfo 搜索信息
	 * @param start
	 * @param count
	 * @return
	 */
	public List<Book> getByQuery(String queryInfo, int start, int count);
	
	/**
	 * 后台查询
	 * @param isbnInfo
	 * @param nameInfo
	 * @param authorInfo
	 * @param publisherInfo
	 * @param start
	 * @param count
	 * @param desc
	 * @return
	 */
	public Page<Book> getByBGQuery(String isbnInfo, String nameInfo, String authorInfo, final String publisherInfo,
			 int start, int count, boolean desc);
	
	/**
	 * 用户搜索的结果总数
	 * @param queryInfo 搜索信息
	 * @return
	 */
	public int getCountByQuery(String queryInfo);
	
	/**
	 * 关注一本书
	 * @param user
	 * @param book
	 */
	public void follow(User user, Book book);
	
	/**
	 * 取消关注一本书
	 * @param user
	 * @param book
	 */
	public void unfollow(User user, Book book);
}
