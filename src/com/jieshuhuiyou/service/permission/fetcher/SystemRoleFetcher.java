package com.jieshuhuiyou.service.permission.fetcher;


import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Component;


import com.jieshuhuiyou.Config;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.service.permission.Role;
import com.jieshuhuiyou.service.permission.RoleFetcher;
import com.opensymphony.xwork2.ActionContext;
@Component
public class SystemRoleFetcher<T> implements RoleFetcher<T> {
	
	public static enum SystemRole implements Role{
		GUEST,	//游客
		NORMAL_USER,	//正常用户
		ADMIN,	//管理员
		BLOCKED_USER	//被禁用户
	}
	
	
	@Override
	public Set<Role> fetchRoles(User user, T subject) {
		
		ActionContext ac = ActionContext.getContext();
		Map<String, Object> session = ac.getSession();
		
		Set<Role> roles = new HashSet<Role>();
		
		if(session.get(Config.SESSION_KEY_ROLE) != null) {
			roles.add(SystemRole.NORMAL_USER);
		} else {
			roles.add(SystemRole.GUEST);
		}
	
		return roles;
	}


	@Override
	public Role parseRole(String str) {
		return SystemRole.valueOf(str);
	}

}
