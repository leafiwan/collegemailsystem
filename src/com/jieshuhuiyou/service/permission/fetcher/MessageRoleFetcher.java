package com.jieshuhuiyou.service.permission.fetcher;

import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Component;

import com.jieshuhuiyou.entity.Message;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.service.permission.Role;

@Component
public class MessageRoleFetcher extends SystemRoleFetcher<Message> {
	
	public static enum MessageRole implements Role {
		SENDER,		// 发送者
		RECEIVER,	// 接受者
		STRANGER	// 陌生人
	}
	
	@Override
	public Set<Role> fetchRoles(User user, Message subject) {
		Set<Role> roles = new HashSet<Role>();
		roles.addAll(super.fetchRoles(user, subject));
		if(user == null || subject == null) {
			roles.add(MessageRole.STRANGER);
			return roles;
		}
		if(user.equals(subject.getSendUser())) {//sender
			roles.add(MessageRole.SENDER);
		} else if(user.equals(subject.getReceiveUser())) {//receiver
			roles.add(MessageRole.RECEIVER);
		} else {//stranger
			roles.add(MessageRole.STRANGER);
		}
		return roles;
	}
	
	@Override
	public Role parseRole(String str) {
		return MessageRole.valueOf(str);
	}

}
