package com.jieshuhuiyou.service.permission.fetcher;

import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Component;

import com.jieshuhuiyou.entity.BorrowRequest;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.service.permission.Role;

@Component
public class BorrowRequestRoleFetcher extends SystemRoleFetcher<BorrowRequest>{

	public static enum BorrowRequestRole implements Role{
		REQUESTER,	//请求者
		RECEIVER	//接收者
	}

	@Override
	public Set<Role> fetchRoles(User user, BorrowRequest subject) {
		Set<Role> roles = new HashSet<Role>();
		roles.addAll(super.fetchRoles(user, subject));
		
		if(user != null && subject != null) {
			if(user.equals(subject.getUser())) {
				roles.add(BorrowRequestRole.REQUESTER);
			} else if(user.equals(subject.getSharing().getUser())) {
				roles.add(BorrowRequestRole.RECEIVER);
			}
		}
		
		return roles;
	}

	@Override
	public Role parseRole(String str) {
		return BorrowRequestRole.valueOf(str);
	}
	
}
