package com.jieshuhuiyou.service.permission.fetcher;

import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Component;

import com.jieshuhuiyou.entity.Borrow;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.service.permission.Role;

@Component
public class BorrowRoleFetcher extends SystemRoleFetcher<Borrow> {

	public static enum BorrowRole implements Role {
		PROVIDER,	// 共享者
		BORROWER	// 借阅者
	}

	@Override
	public Set<Role> fetchRoles(User user, Borrow subject) {
		Set<Role> roles = new HashSet<Role>();
		roles.addAll(super.fetchRoles(user, subject));
		if(user == null || subject == null) {
			return roles;
		}
		if(user.equals(subject.getUser())) {
			roles.add(BorrowRole.BORROWER);
		} else if(user.equals(subject.getSharing().getUser())) {
			roles.add(BorrowRole.PROVIDER);
		}
		return roles;
	}

	@Override
	public Role parseRole(String str) {
		return BorrowRole.valueOf(str);
	}
	
}
