package com.jieshuhuiyou.service.permission.fetcher;

import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Component;

import com.jieshuhuiyou.entity.Sharing;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.service.permission.Role;

@Component
public class SharingRoleFetcher extends SystemRoleFetcher<Sharing> {

	public static enum SharingRole implements Role {
		OWNER	//拥有者
	}

	@Override
	public Set<Role> fetchRoles(User user, Sharing subject) {
		Set<Role> roles = new HashSet<Role>();
		roles.addAll(super.fetchRoles(user, subject));
		
		if(user == null || subject == null) {
			return roles;
		}
		
		if(user.equals(subject.getUser())) {
			roles.add(SharingRole.OWNER);
		}
		
		return roles;
	}

	@Override
	public Role parseRole(String str) {
		return SharingRole.valueOf(str);
	}
	
	
	
}
