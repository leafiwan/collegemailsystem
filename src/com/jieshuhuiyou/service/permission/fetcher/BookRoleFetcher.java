package com.jieshuhuiyou.service.permission.fetcher;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;


import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.Borrow;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.service.BookService;
import com.jieshuhuiyou.service.BorrowRequestService;
import com.jieshuhuiyou.service.BorrowService;
import com.jieshuhuiyou.service.SharingService;
import com.jieshuhuiyou.service.permission.Role;
@Component
public class BookRoleFetcher extends SystemRoleFetcher<Book> {

	public static enum BookRole implements Role {
		PROVIDER,	//共享者
		BORROWER,	//借阅着
		REQUESTER,	//请求者
		BORROWINGER	// 正在借阅者
	}
	
	@Resource
	private BookService bookService;
	
	@Resource
	private SharingService sharingService;
	
	@Resource
	private BorrowService borrowService;
	
	@Resource
	private BorrowRequestService borrowRequestService;
	
	
	@Override
	public Set<Role> fetchRoles(User user, Book subject) {

		Set<Role> roles = new HashSet<Role>();
		roles.addAll(super.fetchRoles(user, subject));
		
		if(user == null || subject == null) {
			return roles;
		}
		
		// is provider?
		if(sharingService.getByBookAndProvider(user, subject) != null) {
			roles.add(BookRole.PROVIDER);
		} else {
			
			// has borrowed?
			List<Borrow> borrows = borrowService.getByBorrowerAndBook(user, subject, 0, 1, true);
			if(!borrows.isEmpty()) {
				Borrow borrow = borrows.get(0);
				if(borrow.isReturned()) {
					roles.add(BookRole.BORROWER);				
				}
				else {
					roles.add(BookRole.BORROWINGER);
				}
			}
			//requesting?
			if(borrowRequestService.getByRequesterAndBook(user, subject) != null) {
				roles.add(BookRole.REQUESTER);
			}
		
		}
		
		return roles;
	}

	
	@Override
	public Role parseRole(String str) {
		return BookRole.valueOf(str);
	}
	
	// setters and getters
	public BookService getBookService() {
		return bookService;
	}


	public void setBookService(BookService bookService) {
		this.bookService = bookService;
	}

	public SharingService getSharingService() {
		return sharingService;
	}

	public void setSharingService(SharingService sharingService) {
		this.sharingService = sharingService;
	}

	public BorrowService getBorrowService() {
		return borrowService;
	}

	public void setBorrowService(BorrowService borrowService) {
		this.borrowService = borrowService;
	}

	public BorrowRequestService getBorrowRequestService() {
		return borrowRequestService;
	}

	public void setBorrowRequestService(BorrowRequestService borrowRequestService) {
		this.borrowRequestService = borrowRequestService;
	}

}
