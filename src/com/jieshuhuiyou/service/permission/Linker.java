package com.jieshuhuiyou.service.permission;

/**
 * Linker is a object for linking entity class and role fetcher.
 * @author PSJay
 *
 */
public class Linker {

	private String name;
	
	private Class<?> entityClass;
	
	private RoleFetcher<?> roleFetcher;
	
	public Linker(String name, Class<?> entityClass, RoleFetcher<?> roleFetcher) {
		this.name = name;
		this.entityClass = entityClass;
		this.roleFetcher = roleFetcher;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Linker other = (Linker) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}


	// setters and getters
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Class<?> getEntityClass() {
		return entityClass;
	}

	public void setEntityClass(Class<?> entityClass) {
		this.entityClass = entityClass;
	}

	public RoleFetcher<?> getRoleFetcher() {
		return roleFetcher;
	}

	public void setRoleFetcher(RoleFetcher<?> roleFetcher) {
		this.roleFetcher = roleFetcher;
	}

}
