package com.jieshuhuiyou.service.permission;

import java.security.InvalidParameterException;



/**
 * Role parser
 * @author psjay
 *
 */
public class RoleParser {

	private static PermissionConfig config = PermissionConfig.getInstance();
	
	/**
	 * Parse String to Role object
	 * @param str The string will be parsed. The structure of the string must be something like "LinkerName.RoleName", such as "Book.PROVIDER" and "System.ADMIN"
	 * @return
	 */
	public static Role parseString(String str) {
		int firstPoint = str.indexOf(".");
		String linkerName = str.substring(0, firstPoint).trim();
		String roleName = str.substring(firstPoint+1).trim();
		
		RoleFetcher<?> fetcher = config.getFetcher(linkerName);
		
		if(fetcher == null) {
			throw new InvalidParameterException("link:" + linkerName + " is not exists.");
		}
		
		return fetcher.parseRole(roleName);
		
	}

}
