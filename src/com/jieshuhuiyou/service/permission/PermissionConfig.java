package com.jieshuhuiyou.service.permission;

import java.util.HashSet;
import java.util.Set;

import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.Borrow;
import com.jieshuhuiyou.entity.BorrowRequest;
import com.jieshuhuiyou.entity.Message;
import com.jieshuhuiyou.entity.Sharing;
import com.jieshuhuiyou.service.permission.fetcher.BookRoleFetcher;
import com.jieshuhuiyou.service.permission.fetcher.BorrowRequestRoleFetcher;
import com.jieshuhuiyou.service.permission.fetcher.BorrowRoleFetcher;
import com.jieshuhuiyou.service.permission.fetcher.MessageRoleFetcher;
import com.jieshuhuiyou.service.permission.fetcher.SharingRoleFetcher;
import com.jieshuhuiyou.service.permission.fetcher.SystemRoleFetcher;
import com.jieshuhuiyou.util.BeanHelper;


/**
 * Config class for Permission moudule
 * @author PSJay
 *
 */
public class PermissionConfig {
	
	private Set<Linker> linkers = new HashSet<Linker>();
	
	private RoleFetcher<?> defaultFetcher;
	
	private static PermissionConfig instance = null;
	
	public static PermissionConfig getInstance() {
		if(instance == null) {
			synchronized (PermissionConfig.class) {
				if(instance == null) {
					instance = new PermissionConfig();
				}
			}
		}
		
		return instance;
	}
	
	private PermissionConfig() {
		init();
	}
	
	/**
	 * Get a RoleFetcher object by entity class
	 * @param entityClass
	 * @return corresponding RoleFetcher, if not exists, return null
	 */
	public RoleFetcher<?> getFetcher(Class<?> entityClass) {
		for(Linker l : linkers) {
			if(l.getEntityClass() != null && l.getEntityClass().equals(entityClass)) {
				return l.getRoleFetcher();
			}
		}
		return null;
	}
	
	/**
	 * Get a RoleFetcher by linker's name
	 * @param linkerName
	 * @return corresponding RoleFetcher, if not exists, return null
	 */
	public RoleFetcher<?> getFetcher(String linkerName) {
		for(Linker l : linkers) {
			if(l.getName().equals(linkerName)) {
				return l.getRoleFetcher();
			}
		}
		return null;
	}
	
	/**
	 *  init config object
	 */
	private void init() {
		
		// set default fetcher
		defaultFetcher = new SystemRoleFetcher<Object>();
		
		// set fetcher map
		linkers.add(new Linker("System", null, BeanHelper.getBean("systemRoleFetcher", SystemRoleFetcher.class)));
		linkers.add(new Linker("Book", Book.class, BeanHelper.getBean("bookRoleFetcher", BookRoleFetcher.class)));
		linkers.add(new Linker("BorrowRequest", BorrowRequest.class, BeanHelper.getBean("borrowRequestRoleFetcher", BorrowRequestRoleFetcher.class)));
		linkers.add(new Linker("Sharing", Sharing.class, BeanHelper.getBean("sharingRoleFetcher", SharingRoleFetcher.class)));
		linkers.add(new Linker("Borrow", Borrow.class, BeanHelper.getBean("borrowRoleFetcher", BorrowRoleFetcher.class)));
        linkers.add(new Linker("Message", Message.class, BeanHelper.getBean("messageRoleFetcher", MessageRoleFetcher.class)));
	}
	
	
	// setters and getters
	public RoleFetcher<?> getDefaultFetcher() {
		return defaultFetcher;
	}

	public void setDefaultFetcher(RoleFetcher<?> defaultFetcher) {
		this.defaultFetcher = defaultFetcher;
	}

}
