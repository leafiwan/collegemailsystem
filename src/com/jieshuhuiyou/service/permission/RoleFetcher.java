package com.jieshuhuiyou.service.permission;

import java.util.Set;

import com.jieshuhuiyou.entity.User;
/**
 * Role fetcher
 * fetch roles for user
 * @author psjay
 *
 * @param <T>
 */
public interface RoleFetcher<T> {
	/**
	 * fetch roles
	 * @param user 
	 * @param subject
	 * @return 
	 */
	public Set<Role> fetchRoles(User user, T subject);
	
	/**
	 * parse string to Role
	 * @param str
	 * @return a corresponding Role, if parse fail, return null
	 */
	public Role parseRole(String str);
	
}
