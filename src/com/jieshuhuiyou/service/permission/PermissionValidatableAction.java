package com.jieshuhuiyou.service.permission;

import java.util.Set;

import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.util.UserUtil;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

/**
 * Permission Validatable Action 
 * @author psjay
 *
 * @param <T> entity type
 */
public abstract class PermissionValidatableAction extends ActionSupport {

	private static final long serialVersionUID = 8996787238870433679L;

	private User validatedUser;
	
	private Object validatedSubject;

	private Set<Role> roles;
	
	private boolean skipValidation;
	
	private boolean skipInterceptorUserSetting = false;	// 是否忽略拦截器对验证用户的设置
	
	private boolean skipInterceptorSubjectSetting = false; //是否忽略拦截器对验证实体的设置
	
	public void populateValidatedUser(User validatedUser) {
		if(!skipInterceptorUserSetting) {
			this.validatedUser = validatedUser;
		}
	}
	
	public void populateValidatedSubject(Object validatedSubject) {
		if(!skipInterceptorSubjectSetting) {
			this.validatedSubject = validatedSubject;
		}
	}
	
	public void skipValidation() {
		this.skipValidation = true;
	}
	
	// setters and getters
	public User getValidatedUser() {
		// return current logined user, by default.
		if(validatedUser == null) {
			this.validatedUser =  UserUtil.getCurrentUser();
		}
		
		return validatedUser;
	}

	protected void setValidatedUser(User validatedUser) {
		this.validatedUser = validatedUser;
		skipInterceptorUserSetting = true;
	}

	
	public Object getValidatedSubject() {
		if(validatedSubject == null) {
			// if the action implemented ModelDriven, return the model by default
			if(this instanceof ModelDriven) {
				this.validatedSubject =  ((ModelDriven<?>)this).getModel();
			}
		}
		return validatedSubject;
	}

	protected <T> void setValidatedSubject(T validatedSubject) {
		this.validatedSubject = validatedSubject;
		skipInterceptorSubjectSetting = true;
	}

	public Set<Role> getRoles() {
		if(this.roles == null) {
			this.roles = Validator.fetchRoles(getValidatedUser(), getValidatedSubject());
		}
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public boolean isSkipValidation() {
		return skipValidation;
	}

	public void setSkipValidation(boolean skipValidation) {
		this.skipValidation = skipValidation;
	}
	
}
