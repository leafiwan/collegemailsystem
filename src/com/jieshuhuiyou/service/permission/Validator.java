package com.jieshuhuiyou.service.permission;


import java.util.HashSet;
import java.util.Set;

import com.jieshuhuiyou.entity.User;


/**
 * 角色验证器
 * @author psjay
 *
 */
public class Validator {
	
	private static PermissionConfig config = PermissionConfig.getInstance();
	
	private static final Set<Role> EMPTY_ROLE_SET = new HashSet<Role>();
	
	/**
	 * 验证角色是否被允许（不允许的优先级大于允许）
	 * @param <T> 实体类型
	 * @param user 被验证的用户
	 * @param subject 实体对象
	 * @param allowRoles 允许的角色
	 * @param disallowRoles 不允许的角色
	 * @return
	 */
	public static <T> boolean validate(User user, T subject, Set<? extends Role> allowRoles
			, Set<? extends Role> disallowRoles) {
		

		Set<Role> roles = fetchRoles(user, subject);

		return validate(roles, allowRoles, disallowRoles);
		
	}
	
	/**
	 * 验证角色是否被允许
	 * @param roles 用户的角色列表
	 * @param allowRoles 允许的列表
	 * @param disallowRoles 不允许的列表
	 * @return
	 */
	public static boolean validate(Set<Role> roles, Set<? extends Role> allowRoles
			, Set<? extends Role> disallowRoles) {
		
		assert((allowRoles != null && !allowRoles.isEmpty())
				|| (disallowRoles != null && !disallowRoles.isEmpty())); // can not both be null at the same time
	
		if(disallowRoles != null && !disallowRoles.isEmpty()) {
			for(Role r : disallowRoles) {
				for(Role r1 : roles) {
					if(r.equals(r1)) {
						return false;
					}
				}
			}
			return true;
		} else {
			for(Role r : allowRoles) {
				for(Role r1 : roles) {
					if(r.equals(r1)) {
						return true;
					}
				}
			}
			return false;
		}
		
	}
	
	
	/**
	 * 得到用户在某个实体上的角色列表
	 * @param <T> 实体类型
	 * @param user 用户
	 * @param subject 实体
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> Set<Role> fetchRoles(User user, T subject) {
				
        RoleFetcher<T> rf = null;
        rf = subject == null ? (RoleFetcher<T>)config.getDefaultFetcher() 
        		: (RoleFetcher<T>) config.getFetcher(subject.getClass());

        // if can not get a proper role fetcher, just return a empty set.
        if(rf == null) {
        	return EMPTY_ROLE_SET;
        }
        
		return rf.fetchRoles(user, subject);
		
	}
	
	// setters and getters
	public PermissionConfig getConfig() {
		return config;
	}

	public void setConfig(PermissionConfig config) {
		Validator.config = config;
	}
	

}
