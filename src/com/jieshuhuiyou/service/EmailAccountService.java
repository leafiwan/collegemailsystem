package com.jieshuhuiyou.service;

import com.jieshuhuiyou.entity.EmailAccount;
import com.jieshuhuiyou.infrastructure.Page;

/**
 * Email Account Service
 * @version 0.0.1, 2012-03-27
 * @author psjay
 *
 */

public interface EmailAccountService {

	/**
	 * create a email account
	 * @param email email address
	 * @param password password for the email account
	 */
	public void create(String email, String password);
	
	/**
	 * save or update(when the account is already exsits) a email account
	 * @param account account to be save or update
	 */
	public void saveOrUpdate(EmailAccount account);
	
	/**
	 * delete a email account via email address
	 * @param email the address of the email account to be deleted
	 */
	public void delete(String email);
	
	/**
	 * delete a email account
	 * @param account the email account to be deleted
	 */
	public void delete(EmailAccount account);
	
	/**
	 * get a email account via address
	 * @param address
	 * @return the account or null
	 */
	public EmailAccount getByAddress(String address);
	
	/**
	 * List email accounts
	 * @param start
	 * @param count
	 * @param desc it true, sort by create time DESC
	 * @return
	 */
	public Page<EmailAccount> getAccounts(int start, int count, boolean desc);
	
}
