package com.jieshuhuiyou.service;

import java.util.List;

import com.jieshuhuiyou.entity.Comment;
import com.jieshuhuiyou.entity.Review;
import com.jieshuhuiyou.entity.comment.ReviewComment;

/**
 * 评论业务接口
 * @author psjay
 *
 */
public interface CommentService {
	
	/**
	 * 创建一个书评评论
	 * @param comment
	 */
	public void createReviewComment(ReviewComment comment);
	
	/**
	 * 根据 id 查询评论
	 * @param id
	 * @return
	 */
	public Comment getById(long id);
	
	/**
	 * 根据书评取出评论
	 * @param review 书评
	 * @param start 
	 * @param count
	 * @param desc
	 * @return
	 */
	public List<ReviewComment> getByReview(Review review, int start, int count, boolean desc);
	
}
