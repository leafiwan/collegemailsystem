package com.jieshuhuiyou.service;

import java.util.List;

import com.jieshuhuiyou.entity.Event;
import com.jieshuhuiyou.entity.User;

/**
 * 事件服务类接口
 * @author Michael
 *
 */
public interface EventService {

	public List<Event> getFollowEventByUser(User user,int page,int pageSize);
	
	public void save(Event event);
}
