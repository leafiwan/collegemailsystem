package com.jieshuhuiyou.service;

import java.util.List;

import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.BorrowRequest;
import com.jieshuhuiyou.entity.Sharing;
import com.jieshuhuiyou.entity.User;

public interface BorrowRequestService {

	/**
	 * 保存
	 * @param request
	 */
	public void save(BorrowRequest request);
	
	/**
	 * 删除一个请求
	 * @param request 被删除的请求
	 */
	public void delete(BorrowRequest request);
	
	/**
	 * 用户发出请求
	 * @param user
	 * @param sharing
	 * @param message
	 */
	public void request(User user, Sharing sharing, String message);
	
	
	/**
	 * 根据 id 从数据库中查找
	 * @param id
	 * @return
	 */
	public BorrowRequest getById(int id);

	
	/**
	 * 根据借阅接收者取出请求
	 * @param reciever
	 * @param start
	 * @param count
	 * @param desc
	 * @return
	 */
	public List<BorrowRequest> getByReciever(User reciever, int start, int count, boolean desc);

	
	/**
	 * 根据借阅请求者取出请求
	 * @param requester
	 * @param start
	 * @param count
	 * @param desc
	 * @return
	 */
	public List<BorrowRequest> getByRequester(User requester, int start, int count, boolean desc);

	/**
	 * 根据请求者和书籍取出请求
	 * @param requester 请求者
	 * @param book 书籍
	 * @return 符合条件的借阅请求，没有则返回 null
	 */
	public BorrowRequest getByRequesterAndBook(User requester, Book book);
	
	
	/**
	 * 同意请求
	 * @param request
	 */
	public void accept(BorrowRequest request);
	
	/**
	 * 拒绝一个请求
	 * @param reuqest
	 */
	public void reject(BorrowRequest reuqest);
	
	/**
	 * 取消一个请求
	 * @param request
	 */
	public void cancle(BorrowRequest request);
	
	
}
