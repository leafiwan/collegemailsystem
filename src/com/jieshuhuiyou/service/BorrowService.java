package com.jieshuhuiyou.service;

import java.util.List;

import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.Borrow;
import com.jieshuhuiyou.entity.User;


public interface BorrowService {

	/**
	 * 创建一个借阅
	 * @param borrow 需要被创建的借阅
	 */
	public void create(Borrow borrow);
	
	/**
	 * 根据 id 得到借阅
	 * @param id
	 * @return
	 */
	public Borrow getById(int id);
	
	
	/**
	 * 归还书籍
	 * @param borrow 借阅
	 * @param operator 操作者
	 */
	public void returnBook(Borrow borrow, User operator);
	
	
	/**
	 * 根据借阅者取出借阅
	 * @param borrower
	 * @param start
	 * @param count
	 * @param desc
	 * @return
	 */
	public List<Borrow> getByBorrower(User borrower, int start, int count, boolean desc);
	
	/**
	 * 根据共享者找出借阅
	 * @param provider
	 * @param start
	 * @param count
	 * @return
	 */
	public List<Borrow> getByProvider(User provider, int start, int count, boolean desc);
	
	/**
	 * 根据书籍找出借阅
	 * @param book
	 * @param start
	 * @param count
	 * @return
	 */
	public List<Borrow> getByBook(Book book, int start, int count, boolean desc);
		
	/**
	 * 根据借阅者和书籍取出借阅（可能借阅过多次）
	 * @param borrower
	 * @param book
	 * @param start
	 * @param count
	 * @param desc
	 * @return
	 */
	public List<Borrow> getByBorrowerAndBook(User borrower, Book book, int start, int count, boolean desc);
	
	
	public void update(Borrow borrow);

}
