package com.jieshuhuiyou.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jieshuhuiyou.dao.EmailAccountDao;
import com.jieshuhuiyou.entity.EmailAccount;
import com.jieshuhuiyou.infrastructure.Page;
import com.jieshuhuiyou.service.EmailAccountService;

/**
 * Default implementation of EmailAccountService interface
 * @author psjay
 *
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class EmailAccountServiceImpl implements EmailAccountService {

	@Resource
	private EmailAccountDao emailAccountDao;
	
	@Override
	@Transactional(readOnly = false)
	public void create(String email, String password) {
		EmailAccount account = emailAccountDao.getByAddress(email);
		if(account == null) {
			account = new EmailAccount(email, password);
			emailAccountDao.saveOrUpdate(account);
		}
	}

	@Override
	@Transactional(readOnly = false)
	public void saveOrUpdate(EmailAccount account) {
		emailAccountDao.saveOrUpdate(account);
	}

	@Override
	@Transactional(readOnly = false)
	public void delete(String email) {
		EmailAccount account = emailAccountDao.getByAddress(email);
		if(account != null) {
			emailAccountDao.delete(account);
		}
	}

	@Override
	@Transactional(readOnly = false)
	public void delete(EmailAccount account) {
		emailAccountDao.delete(account);
	}

	@Override
	public EmailAccount getByAddress(String address) {
		return emailAccountDao.getByAddress(address);
	}

	@Override
	public Page<EmailAccount> getAccounts(int start, int count, boolean desc) {
		List<EmailAccount> elements = emailAccountDao.getAccounts(start, count, desc);
		int totalsCount = emailAccountDao.getAccountsCount();
		Page<EmailAccount> page = new Page<EmailAccount>(elements, start, count, totalsCount);
		return page;
	}

	// setters and getters
	public EmailAccountDao getEmailAccountDao() {
		return emailAccountDao;
	}

	public void setEmailAccountDao(EmailAccountDao emailAccountDao) {
		this.emailAccountDao = emailAccountDao;
	}

}
