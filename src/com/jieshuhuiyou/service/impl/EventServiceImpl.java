package com.jieshuhuiyou.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jieshuhuiyou.dao.EventDao;
import com.jieshuhuiyou.entity.Event;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.service.EventService;

@Service
@Transactional
public class EventServiceImpl implements EventService{

	@Resource
	private EventDao eventDao;
	
	@Override
	public List<Event> getFollowEventByUser(User user, int page, int pageSize) {
		return eventDao.getFollowEventByUser(user, (page - 1) * pageSize, page * pageSize);
	}

	public void setEventDao(EventDao eventDao) {
		this.eventDao = eventDao;
	}

	public EventDao getEventDao() {
		return eventDao;
	}

	@Override
	public void save(Event event) {
		eventDao.save(event);
	}
}
