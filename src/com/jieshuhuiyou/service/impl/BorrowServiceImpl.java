package com.jieshuhuiyou.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jieshuhuiyou.Config;
import com.jieshuhuiyou.dao.BorrowDao;
import com.jieshuhuiyou.dao.NotificationDao;
import com.jieshuhuiyou.dao.UserDao;
import com.jieshuhuiyou.dao.impl.UserDaoImpl;
import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.Borrow;
import com.jieshuhuiyou.entity.School;
import com.jieshuhuiyou.entity.Sharing;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.entity.notifications.ReturnBookNotification;
import com.jieshuhuiyou.service.BorrowService;
import com.jieshuhuiyou.service.UserService;


@Service
public class BorrowServiceImpl implements BorrowService {

	@Resource
	private BorrowDao borrowDao;
	
	@Resource
	private UserService userService;
	
	@Resource
	private NotificationDao notificationDao;
	
	@Override
	@Transactional(readOnly = false)
	public void create(Borrow borrow) {
		
		borrow.getUser().setBorrowedCount(borrow.getUser().getBorrowedCount() + 1);
		Sharing sharing = borrow.getSharing();
		sharing.setBorrowedTimes(sharing.getBorrowedTimes() + 1);
		sharing.getUser().setBorrowingCount(sharing.getUser().getBorrowingCount() + 1);
		sharing.getBook().setBorrowedCount(sharing.getBook().getBorrowedCount() + 1);
		sharing.getBook().setInventory(sharing.getBook().getInventory() - 1);
		sharing.setAvalible(false);
		
		borrowDao.save(borrow);
	}
	
	@Override
	public Borrow getById(int id) {
		return borrowDao.getById(id);
	}
	
	@Override
	@Transactional(readOnly = false)
	public void returnBook(Borrow borrow, User operator) {
		
		if(borrow.getReturned()) {
			return;
		}
		
		// update entities
		borrow.setReturnDate(new Date());
		borrow.setReturned(true);
		borrow.getSharing().setAvalible(true);
		borrow.getSharing().getBook().setInventory(borrow.getSharing().getBook().getInventory() + 1);
		borrowDao.saveOrUpdate(borrow);
		
		
		//积分的增减
		User borrower = userService.getById(borrow.getUser().getId());
		User provider = userService.getById(borrow.getSharing().getUser().getId());
		
		//借书扣积分
		int integral = borrower.getIntegral() - Config.INTEGRAL_DEC_NUM_BRO;
		if( integral < 0)
			borrower.setIntegral(0);
		else
			borrower.setIntegral(integral);
		
		provider.setIntegral(provider.getIntegral() + Config.INTEGRAL_INC_NUM_PRO);
		
		
		userService.update(borrower);
		userService.update(provider);
		// -- 
		
		// send notification
		ReturnBookNotification notification = new ReturnBookNotification(borrow, operator);
		notificationDao.save(notification.toNotificationEntity());
	}
	/**
	 * 更新借阅
	 */
	@Override
	public void update(Borrow borrow) {
		 borrowDao.update(borrow);
	}
	
	@Override
	public List<Borrow> getByBorrower(User borrower, int start, int count, boolean desc) {
		return borrowDao.getByBorrows(borrower, start, count, desc);
	}

	@Override
	public List<Borrow> getByProvider(User provider, int start, int count, boolean desc) {
		return borrowDao.getByProvider(provider, start, count, desc);
	}

	@Override
	public List<Borrow> getByBook(Book book, int start, int count, boolean desc) {
		return borrowDao.getByBook(book, start, count, desc);
	}
	
	public List<Borrow> getByBookAndSchool(Book book, School school
			, int start, int count, boolean desc) {
		
		return null;
		
	}
	
	@Override
	public List<Borrow> getByBorrowerAndBook(User borrower, Book book, int start, int count, boolean desc) {
		return borrowDao.getByBorrowerAndBook(borrower, book, start, count, desc);
	}
	
	// setters and getters
	public BorrowDao getBorrowDao() {
		return borrowDao;
	}

	public void setBorrowDao(BorrowDao borrowDao) {
		this.borrowDao = borrowDao;
	}

	public NotificationDao getNotificationDao() {
		return notificationDao;
	}

	public void setNotificationDao(NotificationDao notificationDao) {
		this.notificationDao = notificationDao;
	}
}
