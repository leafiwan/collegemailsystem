package com.jieshuhuiyou.service.impl;


import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jieshuhuiyou.dao.BookDao;
import com.jieshuhuiyou.dao.BorrowRequestDao;
import com.jieshuhuiyou.dao.NotificationDao;
import com.jieshuhuiyou.dao.SharingDao;
import com.jieshuhuiyou.dao.UserDao;
import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.BorrowRequest;
import com.jieshuhuiyou.entity.School;
import com.jieshuhuiyou.entity.Sharing;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.entity.notifications.SharingDeletedNotification;
import com.jieshuhuiyou.exceptions.SharingNotAvailableException;
import com.jieshuhuiyou.infrastructure.Page;
import com.jieshuhuiyou.service.BookService;
import com.jieshuhuiyou.service.BorrowRequestService;
import com.jieshuhuiyou.service.SharingService;



@Service
@Transactional
public class SharingServiceImpl implements SharingService {

	@Resource
	private SharingDao sharingDao;
	
	@Resource
	private BorrowRequestDao borrowRequestDao;
	
	@Resource
	private UserDao userDao;
	
	@Resource
	private BookDao bookDao;
	
	@Resource
	private NotificationDao notificationDao;
	
	@Resource
	private BorrowRequestService borrowRequestService;
	
	@Resource
	private BookService bookService;
	
	@Override
	@Transactional(readOnly = false)
	public void save(User user, Sharing sharing, Book book) {
		sharing.setUser(user);
		sharing.setBook(book);
		
		book.setCount(book.getCount() + 1);
		book.setInventory(book.getInventory() + 1);
		
		user.setSharingCount(user.getSharingCount() + 1);
		
		sharingDao.save(sharing);
		
		// auto-follow
		bookService.follow(user, book);
	}
	
	public Page<Sharing> getByBook(Book book, int start, int count, boolean desc) {
		return sharingDao.getByBook(book, start, count, desc);
	}
	
	public Page<Sharing> getByBookAndSchool(final Book book, final School school
			, final int start, final int count, final boolean desc) {
		return sharingDao.getByBookAndSchool(book, school, start, count, desc);
	}
	
	public Sharing getById(int id) {
		return sharingDao.getById(id);
	}
	
	@Override
	@Transactional(readOnly = false)
	public void cancel(Sharing sharing) throws SharingNotAvailableException {
		
		if(sharing.isCanceled()) {
			return;
		}
		
		if(!sharing.isAvalible()) {
			throw new SharingNotAvailableException("SHaring#" + sharing.getId() + " is not avaliable, cancel failed.",
					"您分享的书籍处于借出状态，不能取消分享");
		}
		User user = sharing.getUser();
		// get related unprocessed borrow request
		List<BorrowRequest> requests = borrowRequestDao.getBySharing(sharing);
		// send notification to requesters
		for(BorrowRequest request : requests) {
			User borrower = request.getUser();
			SharingDeletedNotification n = new SharingDeletedNotification(sharing, borrower);
			notificationDao.save(n.toNotificationEntity());
			
			// delete request
			borrowRequestService.delete(request);
		}
		// update book
		Book book = sharing.getBook();
		book.setInventory(book.getInventory() - 1);
		bookDao.update(book);
		// update user
		user.setSharingCount(user.getSharingCount() - 1);
		userDao.update(user);
		
		//cancel sharing
		sharing.setCanceled(true);
		sharingDao.update(sharing);
	}
	
	@Override
	public List<Sharing> getByBookAndSchool(Book book, School school) {
		return sharingDao.getByBookAndSchool(book, school);
	}
	
	@Override
	public Sharing getByBookAndProvider(User provider, Book book) {
		return sharingDao.getByBookAndProvider(provider, book);
	}
	
	@Override
	public List<Sharing> getByUser(User user, int start, int count, boolean desc) {
		return sharingDao.getByUser(user, start, count, desc);
	}
	
	// setters and getters
	public SharingDao getSharingDao() {
		return sharingDao;
	}

	public void setSharingDao(SharingDao sharingDao) {
		this.sharingDao = sharingDao;
	}

	public BorrowRequestDao getBorrowRequestDao() {
		return borrowRequestDao;
	}

	public void setBorrowRequestDao(BorrowRequestDao borrowRequestDao) {
		this.borrowRequestDao = borrowRequestDao;
	}

	public BookDao getBookDao() {
		return bookDao;
	}

	public void setBookDao(BookDao bookDao) {
		this.bookDao = bookDao;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public NotificationDao getNotificationDao() {
		return notificationDao;
	}

	public void setNotificationDao(NotificationDao notificationDao) {
		this.notificationDao = notificationDao;
	}

	public BorrowRequestService getBorrowRequestService() {
		return borrowRequestService;
	}

	public void setBorrowRequestService(BorrowRequestService borrowRequestService) {
		this.borrowRequestService = borrowRequestService;
	}

	public BookService getBookService() {
		return bookService;
	}

	public void setBookService(BookService bookService) {
		this.bookService = bookService;
	}

}
