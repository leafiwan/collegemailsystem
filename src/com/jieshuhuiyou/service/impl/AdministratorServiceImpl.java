package com.jieshuhuiyou.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jieshuhuiyou.dao.AdministratorDao;
import com.jieshuhuiyou.entity.Administrator;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.infrastructure.Page;
import com.jieshuhuiyou.service.AdministratorService;
import com.jieshuhuiyou.util.MD5er;

@Service
@Transactional
public class AdministratorServiceImpl implements AdministratorService {

	@Resource
	private AdministratorDao administratorDao;
	
	@Override
	public boolean login(User user, String password) {
		Administrator administrator = administratorDao.getByUser(user);
		if(administrator != null) {
			String saltedPwd = MD5er.md5(password + administrator.getPasswordSalt());
			if(saltedPwd.equals(administrator.getPassword())) {
				return true;
			}
		}
		
		return false;
	}
	
	@Override
	public Page<Administrator> list(int start, int count, boolean desc) {
		int totalsCount = administratorDao.getCount();
		List<Administrator> elements = administratorDao.list(start, count, desc);
		Page<Administrator> page = new Page<Administrator>(elements, start, count, totalsCount);
		return page;
	}

	@Override
	@Transactional(readOnly = false)
	public void delete(Administrator administrator) {
		administratorDao.delete(administrator);
	}

	@Override
	@Transactional(readOnly = false)
	public void add(User user, String password) {
		Administrator administrator = new Administrator();
		administrator.setUser(user);
		String salt = MD5er.md5(String.valueOf(new Date().getTime()));
		String saltedPwd = MD5er.md5(password + salt);
		administrator.setPasswordSalt(salt);
		administrator.setPassword(saltedPwd);
		
		// save to database
		administratorDao.save(administrator);
	}

	@Override
	@Transactional(readOnly = false)
	public void changePassword(Administrator administrator, String newPassword) {
		String saltedPwd = MD5er.md5(newPassword + administrator.getPasswordSalt());
		administrator.setPassword(saltedPwd);
		
		// update 
		administratorDao.update(administrator);
	}
	
	@Override
	public Administrator getById(int id) {
		return administratorDao.getById(id);
	}
	
	
	// setters and getters
	public AdministratorDao getAdministratorDao() {
		return administratorDao;
	}

	public void setAdministratorDao(AdministratorDao administratorDao) {
		this.administratorDao = administratorDao;
	}

}
