package com.jieshuhuiyou.service.impl;

import java.util.Date;

import javax.annotation.Resource;

import org.apache.commons.lang.time.DateUtils;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jieshuhuiyou.Config;
import com.jieshuhuiyou.dao.ResetPasswordRecordDao;
import com.jieshuhuiyou.entity.ResetPasswordRecord;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.mail.DefaultMailSender;
import com.jieshuhuiyou.mail.Mail;
import com.jieshuhuiyou.mail.TemplateHTMLMail;
import com.jieshuhuiyou.mail.template.ResetPasswordMailTemplate;
import com.jieshuhuiyou.service.ResetPasswordRecordService;

@Service
@Transactional
public class ResetPasswordRecordServiceImpl implements
		ResetPasswordRecordService {

	@Resource
	private ResetPasswordRecordDao resetPasswordRecordDao;

	
	@Override
	@Transactional(readOnly = false)
	public void apply(User user) {
		ResetPasswordRecord record = resetPasswordRecordDao.getByUser(user);
		if(record == null) {
			// if record is never been created, create one.
			record = new ResetPasswordRecord(user);
			resetPasswordRecordDao.save(record);
		} else if( new Date().after( 
					DateUtils.addDays(record.getCreateDate(), Config.RESET_PASSWORD_RECORD_EXPIRES) ) ) {
			// if record is expired, recreate one
			resetPasswordRecordDao.delete(record);
			record = new ResetPasswordRecord(user);
		} 
		//send a mail to user
		try {
			Mail mail = new TemplateHTMLMail(record.getUser().getEmail(), 
							new ResetPasswordMailTemplate(record));
			DefaultMailSender.getSender().addMail(mail);
		} catch (ResourceNotFoundException e) {		
			e.printStackTrace();
		} catch (ParseErrorException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	@Override
	public ResetPasswordRecord getByKey(String key) {
		return resetPasswordRecordDao.getByKey(key);
	}

	//	setters and getters
	public ResetPasswordRecordDao getResetPasswordRecordDao() {
		return resetPasswordRecordDao;
	}
	
	public void setResetPasswordRecordDao(
			ResetPasswordRecordDao resetPasswordRecordDao) {
		this.resetPasswordRecordDao = resetPasswordRecordDao;
	}

	@Override
	public void delete(ResetPasswordRecord record) {
		resetPasswordRecordDao.delete(record);
	}

}
