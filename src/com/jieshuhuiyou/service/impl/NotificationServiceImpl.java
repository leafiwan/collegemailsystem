package com.jieshuhuiyou.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jieshuhuiyou.dao.NotificationDao;
import com.jieshuhuiyou.entity.Notification;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.service.NotificationService;


@Service
@Transactional
public class NotificationServiceImpl implements NotificationService {

	@Resource
	private NotificationDao notificationDao;
	
	public List<Notification> getByOwner(User owner, int start, int count, boolean desc) {
		return notificationDao.getByOwner(owner, start, count, desc);
	}


	public Notification getById(int id) {
		return notificationDao.getById(id);
	}
	
	public int getNotificationCount(User user) {
		return notificationDao.getNotificationCount(user);
	}
	
	public  void save(Notification notification){
		notificationDao.save(notification);
	}
	
	@Transactional(readOnly = false)
	public void deleteByIdsAndOwner(int[] ids, User owner) {
		notificationDao.deleteByIdsAndOwner(ids, owner);
	}
	
	@Transactional(readOnly = false)
	public void delete(Notification notification) {
		notificationDao.delete(notification);
	}
	
	//	setters and getters
	public NotificationDao getNotificationDao() {
		return notificationDao;
	}

	public void setNotificationDao(NotificationDao notificationDao) {
		this.notificationDao = notificationDao;
	}

}
