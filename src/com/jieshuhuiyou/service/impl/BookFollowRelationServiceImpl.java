package com.jieshuhuiyou.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jieshuhuiyou.dao.BookFollowRelationDao;
import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.BookFollowRelation;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.service.BookFollowRelationService;

@Service
@Transactional
public class BookFollowRelationServiceImpl implements BookFollowRelationService{

	@Resource
	private BookFollowRelationDao bookFollowRelationDao;
	
	@Override
	public void save(BookFollowRelation bookFollowRelation) {
		bookFollowRelationDao.save(bookFollowRelation);
		}

	@Override
	public void delete(BookFollowRelation bookFollowRelation) {
		bookFollowRelationDao.delete(bookFollowRelation);
	}

	@Override
	public BookFollowRelation getByUserAndBook(User user, Book book) {
		return bookFollowRelationDao.getByUserAndBook(user, book);
	}

}
