package com.jieshuhuiyou.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jieshuhuiyou.dao.MessageDao;
import com.jieshuhuiyou.entity.Message;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.infrastructure.Page;
import com.jieshuhuiyou.service.MessageService;

@Service
@Transactional
public class MessageServiceImpl implements MessageService {

	@Resource
	private MessageDao messageDao;
	
	/**
	 * 发送消息(存入数据库)
	 * @param message
	 */
	@Override
	public void sendMessage(Message message){
		messageDao.save(message);
	}

	@Override
	public Page<Message> getReceiveMessageListByUser(User user , int start, int count, boolean desc) {
		List<Message> messageList = messageDao.getReceiveMessageListByUser(user, start, count, desc);
		return new Page<Message>(messageList , start, count, messageDao.getReceiveMessageCount(user));
	}

	@Override
	public Page<Message> getSendMessageListByUser(User user , int start, int count, boolean desc) {
		List<Message> messageList = messageDao.getSendMessageListByUser(user, start, count, desc);
		return new Page<Message>(messageList , start, count, messageDao.getSendMessageCount(user));
	}

	@Override
	public Message getById(int id) {
		return messageDao.getById(id);
	}

	@Override
	public void update(Message message) {
		messageDao.update(message);
	}

}
