package com.jieshuhuiyou.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.jieshuhuiyou.dao.BookDao;
import com.jieshuhuiyou.dao.ReviewDao;
import com.jieshuhuiyou.dao.UserDao;
import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.Review;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.service.ReviewService;

@Service
public class ReviewServiceImpl implements ReviewService {

	@Resource
	private ReviewDao reviewDao;
	
	@Resource
	private UserDao userDao;
	
	@Resource
	private BookDao bookDao;
	
	@Override
	public void create(Review review) {
		User author = review.getAuthor();
		Book book = review.getBook();
		author.setReviewsCount(author.getReviewsCount() + 1);
		book.setReviewsCount(book.getReviewsCount() + 1);
		
		reviewDao.save(review);
	}

	@Override
	public void delete(Review review) {
		User author = review.getAuthor();
		Book book = review.getBook();
		author.setReviewsCount(author.getReviewsCount() - 1);
		book.setReviewsCount(book.getReviewsCount() - 1);
		reviewDao.delete(review);
	}

	@Override
	public Review getById(long id) {
		return reviewDao.getById(id);
	}

	@Override
	public List<Review> getByAuthor(User author, int start, int count,
			boolean desc) {
		return reviewDao.getByAuthor(author, start, count, desc);
	}

	@Override
	public List<Review> getByBook(Book book, int start, int count, boolean desc) {
		return reviewDao.getByBook(book, start, count, desc);
	}

	// setters and getters
	public ReviewDao getReviewDao() {
		return reviewDao;
	}

	public void setReviewDao(ReviewDao reviewDao) {
		this.reviewDao = reviewDao;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public BookDao getBookDao() {
		return bookDao;
	}

	public void setBookDao(BookDao bookDao) {
		this.bookDao = bookDao;
	}

}
