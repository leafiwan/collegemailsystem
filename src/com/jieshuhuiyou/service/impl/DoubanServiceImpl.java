package com.jieshuhuiyou.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.exceptions.DoubanException;
import com.jieshuhuiyou.service.DoubanService;

@Service
public class DoubanServiceImpl implements DoubanService {

	
	private static final String DOUBAN_API_KEY = "04276510145b9c5c0c762f303d12d514";
	
	@SuppressWarnings("unused")
	private static final String DOUBAN_SECRET = "8065eaf7e69ed07f";
	
	private static final String DOUBAN_URL_BASE = "http://api.douban.com/book/";
	
	@Override
	public Book getBookByISBN(String isbn) throws DoubanException {
		try {
			URL url = new URL(DOUBAN_URL_BASE + "subject/isbn/" + isbn + "?apikey=" + DOUBAN_API_KEY
					+ "&alt=json");
			String responseContent = doHttpRequest(url);
			if(responseContent == null) {	//404
				return null;
			}
			return jsonStringToBook(responseContent);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Book getBookByDoubanId(String doubanId) throws DoubanException {
		try {
			URL url = new URL(DOUBAN_URL_BASE + "subject/" + doubanId + "?apikey=" + DOUBAN_API_KEY
					+ "&alt=json");
			String responseContent = doHttpRequest(url);
			if(responseContent == null) {	//404
				return null;
			}
			return jsonStringToBook(responseContent);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	private String doHttpRequest(URL url) throws DoubanException {
	
		HttpURLConnection connection = null;
		BufferedReader bw = null;
		String content = "";
		try {
			connection = (HttpURLConnection) url.openConnection();
			connection.setConnectTimeout(10 * 1000);
			connection.setReadTimeout(8 * 1000);
			connection.connect();
			if(connection.getResponseCode() == HttpURLConnection.HTTP_NOT_FOUND) {
				return null;
			}
			bw = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
			String temp;
			while((temp = bw.readLine()) != null){
				content += temp;
			}
			return content;
		} catch (IOException e) {
			//FIXME: there should be logged
			throw new DoubanException(e.getMessage(), "与豆瓣服务器通信错误");
		}
	}
	
	
	private Book jsonStringToBook(String jsonStr) {
		Book book = new Book();
		JSONObject jsonObject = JSON.parseObject(jsonStr);
		if(!jsonObject.containsKey("title")) {
			return null;
		}
		//set title
		book.setTitle(jsonObject.getJSONObject("title").getString("$t"));
		//set content
		book.setIntro(jsonObject.getJSONObject("summary").getString("$t"));
		//set author (separating with space)
		JSONArray authors = jsonObject.getJSONArray("author");
		String author = null;
		if(authors.size() > 0) {
			author = authors.getJSONObject(0).getJSONObject("name").getString("$t");
			if(authors.size() > 1) {
				for(int i = 1; i < authors.size(); i++) {
					author += " " + authors.getJSONObject(i).getJSONObject("name").getString("$t");
				}
			}
		}
		book.setAuthor(author);
		
		// set rating
		JSONObject ratingObject = jsonObject.getJSONObject("gd:rating");
		if(ratingObject != null) {
			float averageRating = 0;
			String averageStr = ratingObject.getString("@average");
			if(averageStr != null) {
				averageRating = Float.parseFloat(averageStr);
				book.setRate(averageRating);
			}
		}
		
		//FIXME: this implementation of coverting json string to Book object is bad
		// set attributes (isbn10, isbn13, author-intro, price, publisher, publishdate, pages, traslator)
		JSONArray attributes = jsonObject.getJSONArray("db:attribute");
		JSONObject attrItem = null;
		for(int i = 0; i < attributes.size(); i++) {
			attrItem = attributes.getJSONObject(i);
			if(attrItem != null) {
				if(attrItem.getString("@name") != null 
						&& attrItem.getString("@name") .equals("isbn10")) {
					if(attrItem.getString("$t") != null) {
						book.setIsbn10(attrItem.getString("$t"));
					}
				} else if(attrItem.getString("@name") != null 
						&& attrItem.getString("@name") .equals("isbn13")) {
					if(attrItem.getString("$t") != null) {
						book.setIsbn13(attrItem.getString("$t"));
					}
				} else if(attrItem.getString("@name") != null 
						&& attrItem.getString("@name") .equals("author-intro")) {
					if(attrItem.getString("$t") != null) {
						book.setAuthorIntro(attrItem.getString("$t"));
					}
				} else if(attrItem.getString("@name") != null 
						&& attrItem.getString("@name") .equals("price")) {
					if(attrItem.getString("$t") != null) {
						book.setPrice(attrItem.getString("$t"));
					}
				}  else if(attrItem.getString("@name") != null 
						&& attrItem.getString("@name") .equals("publisher")) {
					if(attrItem.getString("$t") != null) {
						book.setPublisher(attrItem.getString("$t"));
					}
				}  else if(attrItem.getString("@name") != null 
						&& attrItem.getString("@name") .equals("pubdate")) {
					if(attrItem.getString("$t") != null) {
						book.setPublishDate(attrItem.getString("$t"));
					}
				} else if(attrItem.getString("@name") != null 
						&& attrItem.getString("@name") .equals("pages")) {
					if(attrItem.getString("$t") != null) {
						String pagesStr = attrItem.getString("$t");
						pagesStr = pagesStr.replaceAll("\\D+", "");	// replace all non-digit chars
						book.setPages(Integer.parseInt(pagesStr));
					}
				}  else if(attrItem.getString("@name") != null 
						&& attrItem.getString("@name") .equals("translator")) {
					if(attrItem.getString("$t") != null) {
						book.setTranslator(attrItem.getString("$t"));
					}
				}
			}
		}
		// set douban id
		String idString = jsonObject.getJSONObject("id").getString("$t");
		String doubanId = idString.replaceFirst("http://api.douban.com/book/subject/", "");
		book.setDoubanId(doubanId);
		//set image
		JSONArray links = jsonObject.getJSONArray("link");
		JSONObject linkItem = null;
		for(int i = 0; i < links.size(); i++) {
			linkItem = links.getJSONObject(i);
			if(linkItem.getString("@rel") != null
					&& linkItem.getString("@rel").equals("image")) {
				String spic = linkItem.getString("@href");
				book.setSmallImg(spic);
				String mpic = spic.replaceFirst("spic", "mpic");
				book.setMiddleImg(mpic);
			}
		}
		
		
		return book;
		
	}
	
}
