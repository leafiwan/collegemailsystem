package com.jieshuhuiyou.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jieshuhuiyou.dao.BookDao;
import com.jieshuhuiyou.dao.BookFollowRelationDao;
import com.jieshuhuiyou.dao.BorrowDao;
import com.jieshuhuiyou.dao.BorrowRequestDao;
import com.jieshuhuiyou.dao.SharingDao;
import com.jieshuhuiyou.dao.UserDao;
import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.BookFollowRelation;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.infrastructure.Page;
import com.jieshuhuiyou.service.BookService;

@Service
@Transactional
public class BookServiceImpl implements BookService {

	@Resource
	private BookDao bookDao;
	
	@Resource
	private SharingDao sharingDao;
	
	@Resource
	private BorrowRequestDao borrowRequestDao;
	
	@Resource
	private BorrowDao borrowDao;
	
	@Resource
	private BookFollowRelationDao bookFollowRelationDao;
	
	@Resource
	private UserDao userDao;
	
	@Override
	@Transactional(readOnly = false)
	public void save(Book book) {
		bookDao.save(book);
	}

	@Override
	public void update(Book book) {
		bookDao.update(book);
	}
	
	@Override
	public void delete(Book book) {
		bookDao.delete(book);
	}
	
	@Override
	public Book getById(int id) {
		return bookDao.getById(id);
	}

	@Override
	public List<Book> getBooks(int start, int count, boolean desc) {
		return bookDao.getBooks(start, count, desc);
	}
	
	@Override
	public Page<Book> getBooksPage(int start, int count, boolean desc) {
		List<Book> bookList = bookDao.getBooks(start, count, desc);
		return new Page<Book>(bookList, start, count, bookDao.getBooksCount());
	}

	@Override
	public List<Book> getByProvider(User provider, int start, int count, boolean desc) {
		return bookDao.getByProvider(provider, start, count, desc);
	}
	
	@Override
	public List<Book> getByBorrower(User borrower, int start, int count,
			boolean desc) {
		return bookDao.getByBorrower(borrower, start, count, desc);
	}

	@Override
	public Book getByISBN(String isbn) {
		if(isbn.length() == 10) {
			return bookDao.getByISBN10(isbn);
		}
		else if(isbn.length() == 13) {
			return bookDao.getByISBN13(isbn);
		}
		else {
			return null;
		}
	}
	
	/**
	 * 根据书名或者ISBN搜索图书
	 * @param queryInfo
	 * @return
	 */
	public List<Book> getByQuery(String queryInfo, int start, int count){
		List<Book> resultList = new ArrayList<Book>();
		Book b = this.getByISBN(queryInfo);
		if(b != null){//搜索ISBN
			resultList.add(b);
			return resultList;
		} else {//搜索书名
			resultList = bookDao.getByQuery(queryInfo, start, count);
			return resultList;
		}
	}
	
	@Override
	public int getCountByQuery(String queryInfo) {
		return bookDao.getCountByQuery(queryInfo);
	}

	@Override
	public Page<Book> getByBGQuery(String isbnInfo,
			String nameInfo, String authorInfo, String publisherInfo,
			int start, int count, boolean desc) {
		List<Book> bookList = new ArrayList<Book>();
		// 判断是否存在ISBN信息
		if(!"".equals(isbnInfo)){
			if(isbnInfo.length() == 10){
				bookList.add(bookDao.getByISBN10(isbnInfo));
			} else if(isbnInfo.length() == 13){
				bookList.add(bookDao.getByISBN13(isbnInfo));
			} else {// 非正常ISBN码
				return null;
			}
		} else {
			bookList = bookDao.getByBGQuery(nameInfo, authorInfo,
					publisherInfo, start, count, desc);
		}
		return new Page<Book>(bookList, start, count, bookDao.getCountByBGQuery(nameInfo, authorInfo, publisherInfo));
	}
	
	@Override
	@Transactional(readOnly = false)
	public void follow(User user, Book book) {
		if(bookFollowRelationDao.hasFollowed(user, book)) {
			return;
		} else {
			// save the relationship
			BookFollowRelation relation = new BookFollowRelation(user, book);
			user.setFollowingBookCount(user.getFollowingBookCount() + 1);
			book.setFollowersCount(book.getFollowersCount() + 1);
			bookFollowRelationDao.save(relation);
		}
	}
	
	@Override
	@Transactional(readOnly = false)
	public void unfollow(User user, Book book) {
		BookFollowRelation relation = bookFollowRelationDao.getByUserAndBook(user, book);
		if(relation == null) {
			return;
		}
		
		bookFollowRelationDao.delete(relation);
		
		user.setFollowingBookCount(user.getFollowingBookCount() - 1);
		userDao.update(user);
		book.setFollowersCount(book.getFollowersCount() - 1);
		bookDao.update(book);
	}
	
	// setters and getters
	public BookDao getBookDao() {
		return bookDao;
	}

	public void setBookDao(BookDao bookDao) {
		this.bookDao = bookDao;
	}
	
	public SharingDao getSharingDao() {
		return sharingDao;
	}

	public void setSharingDao(SharingDao sharingDao) {
		this.sharingDao = sharingDao;
	}

	public BorrowRequestDao getBorrowRequestDao() {
		return borrowRequestDao;
	}

	public void setBorrowRequestDao(BorrowRequestDao borrowRequestDao) {
		this.borrowRequestDao = borrowRequestDao;
	}

	public BorrowDao getBorrowDao() {
		return borrowDao;
	}

	public void setBorrowDao(BorrowDao borrowDao) {
		this.borrowDao = borrowDao;
	}

	public BookFollowRelationDao getBookFollowRelationDao() {
		return bookFollowRelationDao;
	}

	public void setBookFollowRelationDao(BookFollowRelationDao bookFollowRelationDao) {
		this.bookFollowRelationDao = bookFollowRelationDao;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
}
