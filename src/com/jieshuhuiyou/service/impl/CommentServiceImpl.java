package com.jieshuhuiyou.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jieshuhuiyou.dao.CommentDao;
import com.jieshuhuiyou.entity.Comment;
import com.jieshuhuiyou.entity.Review;
import com.jieshuhuiyou.entity.comment.ReviewComment;
import com.jieshuhuiyou.service.CommentService;

@Service
@Transactional
public class CommentServiceImpl implements CommentService {

	@Resource
	private CommentDao commentDao;
	
	@Override
	public void createReviewComment(ReviewComment comment) {
		Review review = comment.getReview();
		review.setCommentsCount(review.getCommentsCount() + 1);
		
		commentDao.save(comment);
	}

	@Override
	public Comment getById(long id) {
		return commentDao.getById(id);
	}

	@Override
	public List<ReviewComment> getByReview(Review review, int start, int count,
			boolean desc) {
		return commentDao.getByReview(review, start, count, desc);
	}
	
	// setters and getters
	public CommentDao getCommentDao() {
		return commentDao;
	}

	public void setCommentDao(CommentDao commentDao) {
		this.commentDao = commentDao;
	}

}
