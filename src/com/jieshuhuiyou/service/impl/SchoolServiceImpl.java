package com.jieshuhuiyou.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.jieshuhuiyou.dao.SchoolDao;
import com.jieshuhuiyou.entity.School;
import com.jieshuhuiyou.infrastructure.Page;
import com.jieshuhuiyou.service.SchoolService;

@Service
public class SchoolServiceImpl implements SchoolService{
	
	@Resource
	private SchoolDao schoolDao;

	@Override
	public void add(School school) {
		schoolDao.save(school);
	}

	@Override
	public void delete(School school) {
		//	取得持久化对象
		school = schoolDao.getByName(school.getName());
		schoolDao.delete(school);
	}
	
	@Override
	public void update(School school) {
		schoolDao.update(school);
	}
	
	@Override
	public School getById(int id) {
		return schoolDao.getById(id);
	}

	@Override
	public School getByName(String name) {
		return schoolDao.getByName(name);
	}
	
	@Override
	public List<School> getAllSchools() {
		return schoolDao.getAllSchools();
	}

	@Override
	public Page<School> getSchoolsPage(int start, int count , boolean desc) {
		return new Page<School>(schoolDao.getAllSchools(start , count , desc) , start , count , schoolDao.getSchoolsCount());
	}
}
