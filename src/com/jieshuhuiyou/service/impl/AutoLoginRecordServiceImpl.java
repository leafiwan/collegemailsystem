package com.jieshuhuiyou.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jieshuhuiyou.dao.AutoLoginRecordDao;
import com.jieshuhuiyou.entity.AutoLoginRecord;
import com.jieshuhuiyou.service.AutoLoginRecordService;

@Service
@Transactional
public class AutoLoginRecordServiceImpl implements AutoLoginRecordService {

	@Resource
	private AutoLoginRecordDao autoLoginRecordDao;
	
	
	@Override
	@Transactional(readOnly = false)
	public void saveOrUpdate(AutoLoginRecord record) {
		autoLoginRecordDao.saveOrUpdate(record);
	}

	@Override
	@Transactional(readOnly = false)
	public void delete(AutoLoginRecord record) {
		autoLoginRecordDao.delete(record);
	}

	@Override
	public AutoLoginRecord getByUserId(int uid) {
		return autoLoginRecordDao.getByUserId(uid);
	}

	//	setters and getters
	public AutoLoginRecordDao getAutoLoginRecordDao() {
		return autoLoginRecordDao;
	}

	public void setAutoLoginRecordDao(AutoLoginRecordDao autoLoginRecordDao) {
		this.autoLoginRecordDao = autoLoginRecordDao;
	}

}
