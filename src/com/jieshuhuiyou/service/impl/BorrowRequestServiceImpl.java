package com.jieshuhuiyou.service.impl;


import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jieshuhuiyou.Config;
import com.jieshuhuiyou.dao.BorrowDao;
import com.jieshuhuiyou.dao.BorrowRequestDao;
import com.jieshuhuiyou.dao.NotificationDao;
import com.jieshuhuiyou.dao.UserDao;
import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.Borrow;
import com.jieshuhuiyou.entity.BorrowRequest;
import com.jieshuhuiyou.entity.Sharing;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.entity.notifications.AcceptBorrowRequestNotification;
import com.jieshuhuiyou.entity.notifications.AcceptBorrowRequestToOtherNotification;
import com.jieshuhuiyou.entity.notifications.CancleBorrowRequestNotification;
import com.jieshuhuiyou.entity.notifications.NewBorrowRequestNotification;
import com.jieshuhuiyou.entity.notifications.RejectBorrowRequestNotification;
import com.jieshuhuiyou.service.BookService;
import com.jieshuhuiyou.service.BorrowRequestService;
import com.jieshuhuiyou.service.BorrowService;
import com.jieshuhuiyou.service.UserService;


@Service
@Transactional(propagation = Propagation.REQUIRED)
public class BorrowRequestServiceImpl implements BorrowRequestService {

	@Resource
	private BorrowRequestDao borrowRequestDao;
	
	@Resource
	private BorrowDao borrowDao;
	
	@Resource
	private NotificationDao notificationDao;
	
	@Resource
	private UserDao userDao;
	
	@Resource
	private BorrowService borrowService;
	
	@Resource
	private BookService bookService;
	
	@Override
	@Transactional(readOnly = false)
	public void save(BorrowRequest request) {
		// update user(by cascade)
		User provider = request.getSharing().getUser();
		User requester = request.getUser();
		provider.setReceivedBorrowRequestsCount(provider.getReceivedBorrowRequestsCount() + 1);
		requester.setSendedBorrowRequestsCount(requester.getSendedBorrowRequestsCount() + 1);
		
		// save
		borrowRequestDao.saveOrUpdate(request);
		
		// auto-follow
		Book book = request.getSharing().getBook();
		bookService.follow(requester, book);
	}

	@Override
	@Transactional(readOnly = false)
	public void delete(BorrowRequest request) {
		// delete request
		borrowRequestDao.delete(request);
		
		// update users
		User requester = request.getUser();
		User provider = request.getSharing().getUser();
		requester.setSendedBorrowRequestsCount(requester.getSendedBorrowRequestsCount() - 1);
		provider.setReceivedBorrowRequestsCount(provider.getReceivedBorrowRequestsCount() - 1);
		userDao.update(provider);
		userDao.update(requester);
	}
	
	@Override
	@Transactional(readOnly = false)
	public void request(User user, Sharing sharing, String message) {
		
		//必须有积分才能提出请求
		if(user.getIntegral() > 0){
			
		BorrowRequest request = new BorrowRequest(user, sharing, message);
		save(request);
				
		// send a notification to provider
		NewBorrowRequestNotification n = new NewBorrowRequestNotification(request);
		notificationDao.save(n.toNotificationEntity());
		}
	}
	
	
	@Override
	@Transactional(readOnly = false)
	public void accept(BorrowRequest request) {
		
		// create a new borrow
		Borrow borrow = new Borrow(request.getUser(), request.getSharing());
		
		borrowService.create(borrow);
		
		// send a notification to borrower
		AcceptBorrowRequestNotification noti = new AcceptBorrowRequestNotification(borrow);
		notificationDao.save(noti.toNotificationEntity());
		
		// delete request
		delete(request);
		
		// send a notification to other users who request the same sharing
		// and delete the request
		List<BorrowRequest> requests = borrowRequestDao.getBySharing(request.getSharing());
		for(BorrowRequest r : requests) {
			AcceptBorrowRequestToOtherNotification n = new AcceptBorrowRequestToOtherNotification(r);
			notificationDao.save(n.toNotificationEntity());
			// delete request
			delete(r);
		}
		
	}
	
	@Override
	public BorrowRequest getByRequesterAndBook(User requester, Book book) {
		return borrowRequestDao.getByRequesterAndBook(requester, book);
	}

	@Override
	public List<BorrowRequest> getByReciever(User reciever, int start,
			int count, boolean desc) {
		
		return borrowRequestDao.getByReciever(reciever, start, count, desc);
	}


	@Override
	public List<BorrowRequest> getByRequester(User requester, int start,
			int count, boolean desc) {
		
		return borrowRequestDao.getByRequester(requester, start, count, desc);
	}
	
	@Override
	@Transactional(readOnly = false)
	public void reject(BorrowRequest request) {
		
		////把用户在提交借阅时的分数补回来
		User borrower= request.getUser();
		
		borrower.setIntegral(borrower.getIntegral() + Config.INTEGRAL_DEC_NUM_BRO);
		

		// send a notification to borrower
		RejectBorrowRequestNotification n = new RejectBorrowRequestNotification(request);
		notificationDao.save(n.toNotificationEntity());
		
		// delete request
		delete(request);
	}
	
	@Override
	@Transactional(readOnly = false)
	public void cancle(BorrowRequest request) {
		//把用户在提交借阅时的分数补回来
		User borrower= request.getUser();
		
		borrower.setIntegral(borrower.getIntegral() + Config.INTEGRAL_DEC_NUM_BRO);
		
		// send a notification to provider
		CancleBorrowRequestNotification n = new CancleBorrowRequestNotification(request);
		notificationDao.save(n.toNotificationEntity());
		
		// delete request
		delete(request);
	}
	
	@Override
	public BorrowRequest getById(int id) {
		return borrowRequestDao.getById(id);
	}

	
	//	setters and getters
	public BorrowRequestDao getBorrowRequestDao() {
		return borrowRequestDao;
	}

	public void setBorrowRequestDao(BorrowRequestDao borrowRequestDao) {
		this.borrowRequestDao = borrowRequestDao;
	}

	public NotificationDao getNotificationDao() {
		return notificationDao;
	}

	public void setNotificationDao(NotificationDao notificationDao) {
		this.notificationDao = notificationDao;
	}

	public BorrowDao getBorrowDao() {
		return borrowDao;
	}

	public void setBorrowDao(BorrowDao borrowDao) {
		this.borrowDao = borrowDao;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public BorrowService getBorrowService() {
		return borrowService;
	}

	public void setBorrowService(BorrowService borrowService) {
		this.borrowService = borrowService;
	}

	public BookService getBookService() {
		return bookService;
	}

	public void setBookService(BookService bookService) {
		this.bookService = bookService;
	}

}
