package com.jieshuhuiyou.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jieshuhuiyou.dao.UserFollowRelationDao;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.entity.UserFollowRelation;
import com.jieshuhuiyou.service.UserFollowRelationService;

@Service
@Transactional
public class UserFollowRelationServiceImpl implements UserFollowRelationService{

	@Resource
	private UserFollowRelationDao userFollowRelationDao;
	
	@Override
	public void save(UserFollowRelation userFollowRelation) {
		userFollowRelationDao.save(userFollowRelation);
	}

	@Override
	public void delete(UserFollowRelation userFollowRelation) {
		userFollowRelationDao.delete(userFollowRelation);
	}

	public void setUserFollowRelationDao(UserFollowRelationDao userFollowRelationDao) {
		this.userFollowRelationDao = userFollowRelationDao;
	}

	public UserFollowRelationDao getUserFollowRelationDao() {
		return userFollowRelationDao;
	}

	@Override
	public UserFollowRelation getRelationByFandFedUser(User follow, User followed) {
		return userFollowRelationDao.getRelationByFandFedUser(follow, followed);
	}

}
