package com.jieshuhuiyou.service.impl;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.imageio.ImageIO;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jieshuhuiyou.Config;
import com.jieshuhuiyou.dao.SchoolDao;
import com.jieshuhuiyou.dao.UserDao;
import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.School;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.exceptions.PasswordNotMatchException;
import com.jieshuhuiyou.exceptions.UserAlreadyExistsException;
import com.jieshuhuiyou.infrastructure.Page;
import com.jieshuhuiyou.service.UserService;
import com.jieshuhuiyou.util.ImageUtil;
import com.jieshuhuiyou.util.MD5er;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Resource
	private UserDao userDao;
	
	@Resource
	private SchoolDao schoolDao;
	
	@Override
	@Transactional(readOnly = false)
	public void register(User user) throws UserAlreadyExistsException {
		User u = userDao.getByEmail(user.getEmail());
		if( u != null ) {// user already exist
			throw new UserAlreadyExistsException("email:" + user.getEmail() + "has been used.",
					"邮箱：" + user.getEmail() + "已经注册过帐号了。");
		} else {
			// save default avatar
			user.setSmallAvatar(Config.USER_AVATAR_DEFUALT_SMALL);
			user.setLargeAvatar(Config.USER_AVATAR_DEFUALT_LARGE);
			School school = user.getSchool();
			school.setUsersCount(school.getUsersCount() + 1);
			userDao.save(user);
			schoolDao.update(school);
		}
	}

	@Override
	public User login(User user) throws PasswordNotMatchException {
		User u = userDao.getByEmail(user.getEmail());
		if(u == null 
				|| !user.getPassword().equals(u.getPassword())) {
			throw new PasswordNotMatchException("wrong password", "用户名或密码错误");
		} else {
			return u;
		}
	}
	
	@Override
	public User getById(int id) {
		return userDao.getById(id);
	}

	@Override
	public User getByEmail(String email) {
		return userDao.getByEmail(email);
	}
	
	@Override
	@Transactional(readOnly = false)
	public void update(User user) {
		userDao.update(user);
	}

	@Override
	public void increaseUsersIntegral(User user, int integral) {
		integral = user.getIntegral() + integral;
		user.setIntegral(integral);
		userDao.update(user);
	}
	
	@Override
	@Transactional(readOnly = false)
	public void uploadAvatar(User user, File avatarFile, String contextPath) throws IOException {
		
		BufferedImage img = null;
		try {
			img = ImageIO.read(avatarFile);
		} catch (IOException e) {
			throw e;
		}
		
		// scale image
		int width = Config.USER_AVATAR_LARGE_MAX_WIDTH;
		int height = (int) (((double)width) / img.getWidth() * img.getHeight());
		
		int minLength = width < height ? width : height;
		
		int sx1 = (width - minLength) / 2;
		int sy1 = (height - minLength) / 2;
		int sx2 = sx1 + minLength;
		int sy2 = sy1 + minLength;
		
		// generate filename
		String targetName = MD5er.md5(user.getSignUpDate().toString());
		// always jpg
		targetName += ".jpg";
		File target = new File(contextPath + Config.USER_AVATAR_LARGE_PATH, targetName);
		File smallTarget = new File(contextPath + Config.USER_AVATAR_SMALL_PATH, targetName);
		// save to file
		BufferedImage bimg = ImageUtil.imageToBufferedImage(ImageUtil.scale(img, width, height));
		try {
			ImageUtil.saveToFile(bimg, target);
			// cut the center area of that large avatar as the small one
			ImageUtil.cutImageFile(target, smallTarget, sx1, sy1, sx2, sy2);
			// resize the small one
			ImageUtil.scaleImageFile(smallTarget, smallTarget, Config.USER_AVATAR_SMALL_SIZE, Config.USER_AVATAR_SMALL_SIZE);
		} catch (IOException e) {
			throw e;
		}
		// update user
		user.setSmallAvatar(Config.USER_AVATAR_SMALL_PATH + targetName);
		user.setLargeAvatar(Config.USER_AVATAR_LARGE_PATH + targetName);
		userDao.update(user);
	}
	
	/**
	 * 编辑头像，裁剪并保存
	 * @param user
	 * @param contextPath
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 */
	@Override
	@Transactional(readOnly = false)
	public void editAvatar(User user, String contextPath, int x1, int y1, int x2, int y2) 
		throws IOException {
		
		File largeAvatar = new File(contextPath + user.getLargeAvatar());
		File smallAvatar = new File(contextPath + user.getSmallAvatar());
		
		ImageUtil.cutImageFile(largeAvatar, smallAvatar, x1, y1, x2, y2);
		ImageUtil.scaleImageFile(smallAvatar, smallAvatar, Config.USER_AVATAR_SMALL_SIZE, Config.USER_AVATAR_SMALL_SIZE);
	}
	
	@Override
	public List<User> getProviders(Book book, int start, int count, boolean desc) {
		return userDao.getProviders(book, start, count, desc);
	}

	@Override
	public List<User> getBorrowers(Book book, int start, int count, boolean desc) {
		return userDao.getBorrowers(book, start, count, desc);
	}

	@Override
	public Page<User> getUsers(int start, int count, boolean desc) {
		List<User> usersList = userDao.getUsers(start, count, desc);
		return new Page<User>(usersList, start, count, userDao.getUsersCount());
	}

	@Override
	public Page<User> searchUsers(String email , String name, String school,
			int start, int count , boolean desc) {
		List<User> usersList = new ArrayList<User>();
		if (!"".equals(email)) {
			User user = userDao.getByEmail(email);
			if(null != user){
				usersList.add(user);
			}
		} else {
			usersList = userDao.searchUsers(name, school, start, count, desc);
		}
		return new Page<User>(usersList, start, count, userDao.getUsersCountByConditions(name, school));
	}
	
	//setters and getters
	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public SchoolDao getSchoolDao() {
		return schoolDao;
	}

	public void setSchoolDao(SchoolDao schoolDao) {
		this.schoolDao = schoolDao;
	}
}
