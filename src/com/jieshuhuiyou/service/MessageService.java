package com.jieshuhuiyou.service;

import com.jieshuhuiyou.entity.Message;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.infrastructure.Page;

public interface MessageService {

	/**
	 * 发送消息
	 * @param message 等待发送的消息
	 */
	public void sendMessage(Message message);
	
	/**
	 * 获取某用户收到的所有的私信
	 * @param user
	 * @return
	 */
	public Page<Message> getReceiveMessageListByUser(User user , int start, int count, boolean desc);
	
	/**
	 * 获取某用户发出的所有的私信
	 * @param user
	 * @return
	 */
	public Page<Message> getSendMessageListByUser(User user , int start, int count, boolean desc);
	
	/**
	 * 根据ID获取某条私信
	 * @param id
	 * @return
	 */
	public Message getById(int id);
	
	/**
	 * 更新某条记录在数据中
	 * @param message
	 */
	public void update(Message message);
	
}
