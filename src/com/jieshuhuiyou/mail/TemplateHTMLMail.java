package com.jieshuhuiyou.mail;


import com.jieshuhuiyou.mail.template.MailTemplate;

public class TemplateHTMLMail implements Mail {

	private MailTemplate template;
	private String receiverEmail;
	
	public TemplateHTMLMail(String receiverEmail, MailTemplate template) {
		this.receiverEmail = receiverEmail;
		this.template = template;
	}
	
	@Override
	public String getReceiverEmail() {
		return receiverEmail;
	}

	@Override
	public String getSubject() {
		return template.getMailSubject();
	}

	@Override
	public String getContent() {
		return template.getMailContent();
	}

}
