package com.jieshuhuiyou.mail;

import java.util.LinkedList;
import java.util.Queue;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

public class DefaultMailSender implements MailSender {

	private final DefaultAuthenticator authenticator 
			= new DefaultAuthenticator(MailConfig.getUserName(), MailConfig.getPassword());
	
	private Queue<Mail> mailQueue = new LinkedList<Mail>();
	
	private Runnable sendThread;
	
	private volatile static DefaultMailSender instance = null;
	
	private final int MAX_MAIL_COUNT = 150;
	
	private DefaultMailSender() {
		sendThread = new Runnable() {
			
			@Override
			public void run() {
				while(true) {
					
					synchronized (DefaultMailSender.this) {
						
						while(mailQueue.isEmpty()) {
							try {
								DefaultMailSender.this.wait();
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
						
						Mail mail = mailQueue.poll();
						if(mail != null) {
							HtmlEmail email = new HtmlEmail();
							email.setHostName(MailConfig.getSmtpHost());
							email.setSmtpPort(MailConfig.getSmtpPort());
							email.setAuthenticator(authenticator);
							email.setCharset("utf-8");
							//email.setTLS(true);
							try {
								email.setFrom(MailConfig.getUserName(), MailConfig.getName());
								email.addTo(mail.getReceiverEmail());
								email.setSubject(mail.getSubject());
								email.setHtmlMsg(mail.getContent());
								email.send();
							} catch (EmailException e) {
								e.printStackTrace();
							}
						}
					}

				}
			}
		};
		
		new Thread(sendThread).start();
	}
	
	public static DefaultMailSender getSender() {
		if(instance == null) {
			synchronized(DefaultMailSender.class) {
				if(instance == null) {
					instance = new DefaultMailSender();
				}
			}
		}
		return instance;
	}
	
	@Override
	public synchronized void addMail(Mail mail) {
		while(mailQueue.size() == MAX_MAIL_COUNT) {
			try {
				this.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		mailQueue.add(mail);
		this.notify();
	}
	
	@Override
	public void destory() {
		
	}

}
