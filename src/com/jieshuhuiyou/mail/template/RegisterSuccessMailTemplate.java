package com.jieshuhuiyou.mail.template;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;

import com.jieshuhuiyou.entity.User;

public class RegisterSuccessMailTemplate extends VelocityMailTemplate {

	private User user;
	
	private static final String SUBJECT_TEMPLATE_STRING = "$user.name , 感谢您注册借书会友";
	
	private static final String CONTENT_TEMPLATE_FILENAME = "RegisterSuccessMailTemplate.html";
	
	public RegisterSuccessMailTemplate(User user) 
			throws ResourceNotFoundException, ParseErrorException, Exception {
		super(SUBJECT_TEMPLATE_STRING, CONTENT_TEMPLATE_FILENAME);
		this.user = user;
	}

	@Override
	public VelocityContext prepareContext() {
		VelocityContext ctx = new VelocityContext();
		ctx.put("user", user);
		return ctx;
	}
	
}
