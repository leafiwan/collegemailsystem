package com.jieshuhuiyou.mail.template;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Properties;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;


public abstract class VelocityMailTemplate implements MailTemplate {

	private String templateFileName;
	
	private VelocityContext context;

	private String subjectTemplate;
	
	private Template template;
	
	private static boolean inited = false;
	
	protected VelocityMailTemplate(String subjectTemplate, String templateFileName) throws ResourceNotFoundException
			, ParseErrorException, Exception {
		this.subjectTemplate = subjectTemplate;
		this.templateFileName = templateFileName;
		
		if(!inited) {
			Properties props = new Properties();
		    props.put(Velocity.INPUT_ENCODING, "utf-8");
			init(props);
		}
	
	}
	
	public static void init(Properties props) throws Exception {
		if(inited) {
			return;
		}
		if(props == null) {
			Velocity.init();
		} else {
			Velocity.init(props);
		}
		inited = true;
	}
	
	
	public abstract VelocityContext prepareContext();
	
	private String renderContent() throws ResourceNotFoundException, ParseErrorException, Exception  {
		if(context == null) {
			context = prepareContext();
		}
		template = Velocity.getTemplate(templateFileName);
		StringWriter sw = new StringWriter();
		template.merge(context, sw);
		return sw.toString();
	}
	
	private String renderSubject() throws ParseErrorException, MethodInvocationException, ResourceNotFoundException, IOException {
		if(context == null) {
			context = prepareContext();
		}
		StringWriter sw = new StringWriter();
		Velocity.evaluate(context, sw, "mystring", subjectTemplate);
		return sw.toString();
	}
	
	
	@Override
	public String getMailContent() {
		String s =  null;
		try {
			s = renderContent();
		} catch (ResourceNotFoundException e) {
			e.printStackTrace();
		} catch (ParseErrorException e) {
			e.printStackTrace();
		} catch (MethodInvocationException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return s;
	}

	@Override
	public String getMailSubject() {
		String s = null;
		try {
			s = renderSubject();
		} catch (ParseErrorException e) {
			e.printStackTrace();
		} catch (MethodInvocationException e) {
			e.printStackTrace();
		} catch (ResourceNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return s;
	}

	// setters and getters
	public String getTemplateFileName() {
		return templateFileName;
	}

	public void setTemplateFileName(String templateFileName) {
		this.templateFileName = templateFileName;
	}

	public VelocityContext getContext() {
		return context;
	}

	public void setContext(VelocityContext context) {
		this.context = context;
	}

	public String getSubjectTemplate() {
		return subjectTemplate;
	}

	public void setSubjectTemplate(String subjectTemplate) {
		this.subjectTemplate = subjectTemplate;
	}
	
}