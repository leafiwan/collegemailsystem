package com.jieshuhuiyou.mail.template;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;

import com.jieshuhuiyou.Config;
import com.jieshuhuiyou.entity.ResetPasswordRecord;

public class ResetPasswordMailTemplate extends VelocityMailTemplate {

	private static final String TEMPLATE_FILE_NAME = "ResetPasswordMailTemplate.html";
	private static final String SUBJECT_TEMPLATE = "$record.user.name ，请按照邮件提示重置密码";
	
	private ResetPasswordRecord record;
	
	
	public ResetPasswordMailTemplate(ResetPasswordRecord record) 
			throws ResourceNotFoundException,
			ParseErrorException, Exception {
		super(SUBJECT_TEMPLATE, TEMPLATE_FILE_NAME);
		this.record = record;
	}

	@Override
	public VelocityContext prepareContext() {
		VelocityContext ctx = new VelocityContext();
		ctx.put("record", record);
		ctx.put("domainName", Config.DOMAIN);
		return ctx;
	}

	
	public ResetPasswordRecord getRecord() {
		return record;
	}

	public void setRecord(ResetPasswordRecord record) {
		this.record = record;
	}
}
