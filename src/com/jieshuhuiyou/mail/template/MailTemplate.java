package com.jieshuhuiyou.mail.template;

public interface MailTemplate {

	/**
	 * 取得邮件的内容
	 * @return
	 */
	public String getMailContent();
	
	/**
	 * 取得邮件的主题
	 * @return
	 */
	public String getMailSubject();
	
}
