package com.jieshuhuiyou.mail;

import java.util.Properties;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.velocity.app.Velocity;

import com.jieshuhuiyou.mail.template.VelocityMailTemplate;

public class VelocityTemplateMailConfigListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		
	}

	@Override
	public void contextInitialized(ServletContextEvent event) {
		Properties props = new Properties();
		props.put(Velocity.INPUT_ENCODING, "utf-8");
		props.put("resource.loader", "webapp");
		props.put("webapp.resource.loader.class", "org.apache.velocity.tools.view.WebappResourceLoader");
		props.put("webapp.resource.loader.path", MailConfig.getTemplatePath());
		Velocity.setApplicationAttribute("javax.servlet.ServletContext", event.getServletContext());
		try {
			VelocityMailTemplate.init(props);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
