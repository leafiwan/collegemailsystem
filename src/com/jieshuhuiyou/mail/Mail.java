package com.jieshuhuiyou.mail;


public interface Mail {

	public String getReceiverEmail();
	
	public String getSubject();
	
	public String getContent();
	
}
