package com.jieshuhuiyou.mail;

public final class MailConfig {

	private static String templatePath = "/WEB-INF/mailTemplates/";
	
	private static String smtpHost = "smtp.exmail.qq.com";
	
	private static int smtpPort = 25;
	
	private static String userName = "no-reply@psjay.com";
	
	private static String name = "蚂蚁书屋";
	
	private static String password = "psjaytest123";

	
	// setters and getters
	public static String getTemplatePath() {
		return templatePath;
	}

	public static void setTemplatePath(String templatePath) {
		MailConfig.templatePath = templatePath;
	}

	public static String getSmtpHost() {
		return smtpHost;
	}

	public static void setSmtpHost(String smtpHost) {
		MailConfig.smtpHost = smtpHost;
	}

	public static int getSmtpPort() {
		return smtpPort;
	}

	public static void setSmtpPort(int smtpPort) {
		MailConfig.smtpPort = smtpPort;
	}

	public static String getUserName() {
		return userName;
	}

	public static void setUserName(String userName) {
		MailConfig.userName = userName;
	}

	public static String getName() {
		return name;
	}

	public static void setName(String name) {
		MailConfig.name = name;
	}

	public static String getPassword() {
		return password;
	}

	public static void setPassword(String password) {
		MailConfig.password = password;
	}
	
}
