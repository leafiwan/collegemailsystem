package com.jieshuhuiyou.exceptions;

/**
 * 密码不匹配异常
 * @author psjay
 *
 */
public class PasswordNotMatchException extends AbstractBusinessException {

	private static final long serialVersionUID = -2114118261313302965L;
	
	private static final int errorCode = 102;
	
	public PasswordNotMatchException(String msg) {
		super(errorCode);
		this.msg = msg;
	}

	public PasswordNotMatchException(String msg, String readableMsg) {
		super(errorCode);
		this.msg = msg;
		this.readableMsg = readableMsg;
	}
	
}
