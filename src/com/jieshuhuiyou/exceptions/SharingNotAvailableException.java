package com.jieshuhuiyou.exceptions;

public class SharingNotAvailableException extends AbstractBusinessException {

	private static final long serialVersionUID = -7745628477314630110L;
	
	private static final int ERORR_CODE = 501;
	
	public SharingNotAvailableException(String msg) {
		super(ERORR_CODE);
		this.msg = msg;
	}
	
	public SharingNotAvailableException(String msg, String readableMsg) {
		super(ERORR_CODE);
		this.msg = msg;
		this.readableMsg = readableMsg;
	}

}
