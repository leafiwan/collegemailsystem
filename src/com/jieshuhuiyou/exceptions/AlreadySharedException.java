package com.jieshuhuiyou.exceptions;

public class AlreadySharedException extends AbstractBusinessException {

	private static final long serialVersionUID = -2294991616674109669L;
	
	public static final int ERROR_CODE = 201;
	
	
	public AlreadySharedException(String msg) {
		super(ERROR_CODE);
		this.msg = msg;
	}
	
	public AlreadySharedException(String msg, String readableMsg) {
		super(ERROR_CODE);
		this.msg = msg;
		this.readableMsg = readableMsg;
	}
	
}
