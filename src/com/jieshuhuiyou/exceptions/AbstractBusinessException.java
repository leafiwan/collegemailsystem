package com.jieshuhuiyou.exceptions;

/**
 * 业务逻辑异常抽象类
 * @author psjay
 *
 */
public abstract class AbstractBusinessException extends Exception {

	private static final long serialVersionUID = -5070391002266705985L;
	
	/**
	 *  errorCode is a global-scope flag which describes what the kind of the exception is
	 *  generally, it's a three-digit number.
	 *  the first digit indicates the exception's category:
	 *  1 is user category, all user exceptions' errorCode are begin with '1'
	 *  2 is book category,
	 *  3 is douban category,
	 *  4 is other category, such as InvalidParameterException
	 *  5 is sharing category
	 *  
	 *  All business exceptions' error code is here:
	 *  101: UserAlreadyExistsException;
	 *  102: PasswordNotMatchException;
	 *  201: AlreadySharedException;
	 *  301: DoubanException;
	 *  401: InvalidParameterException
	 *  501：SharingNotAvailableException
	 */
	protected final int errorCode;
	protected String msg;
	protected String readableMsg;
	
	public AbstractBusinessException(int errorCode) {
		this.errorCode = errorCode;
	}
	
	
	// setters and getters
	public int getErrorCode() {
		return errorCode;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getReadbleMsg() {
		return readableMsg;
	}
	public void setReadbleMsg(String readableMsg) {
		this.readableMsg = readableMsg;
	}
	
}
