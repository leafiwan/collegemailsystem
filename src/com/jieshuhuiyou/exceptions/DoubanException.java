package com.jieshuhuiyou.exceptions;

public class DoubanException extends AbstractBusinessException {


	private static final long serialVersionUID = -2818762215345444918L;
	
	public static final int ERROR_CODE = 301;
	
	
	public DoubanException(String msg) {
		super(ERROR_CODE);
		this.msg = msg;
	}
	
	public DoubanException(String msg, String readableMsg) {
		super(ERROR_CODE);
		this.msg = msg;
		this.readableMsg = readableMsg;
	}
	
	
}
