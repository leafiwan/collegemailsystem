package com.jieshuhuiyou.exceptions;

/**
 * 错误的参数异常
 * @author psjay
 *
 */

public class InvalidParameterException extends AbstractBusinessException {

	private static final long serialVersionUID = 3229817450129347636L;
	
	private static final int ERROR_CODE = 401;
	
	public InvalidParameterException(String msg) {
		super(ERROR_CODE);
		this.msg = msg;
	}
	
	public InvalidParameterException(String msg, String readableMsg) {
		super(ERROR_CODE);
		this.msg = msg;
		this.readableMsg = readableMsg;
	}
	
}
