package com.jieshuhuiyou.exceptions;

/**
 * 用户已存在异常
 * @author psjay
 *
 */
public class UserAlreadyExistsException 
	extends AbstractBusinessException {

	private static final long serialVersionUID = -3051729116657966765L;
	
	public final static int errorCode = 101;
	
	public UserAlreadyExistsException(String msg){
		super(errorCode);
		this.msg = msg;
	}
	
	public UserAlreadyExistsException(String msg, String readableMsg) {
		super(errorCode);
		this.msg = msg;
		this.readableMsg = readableMsg;
	}
	
}
