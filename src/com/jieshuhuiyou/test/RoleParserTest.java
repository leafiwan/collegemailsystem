package com.jieshuhuiyou.test;

import junit.framework.TestCase;

import org.junit.Test;

import com.jieshuhuiyou.service.permission.RoleParser;
import com.jieshuhuiyou.service.permission.fetcher.BookRoleFetcher.BookRole;
import com.jieshuhuiyou.service.permission.fetcher.SystemRoleFetcher.SystemRole;

public class RoleParserTest extends TestCase{

	@Test
	public void testRoleParser() {
		assertEquals(BookRole.PROVIDER, RoleParser.parseString("Book.PROVIDER"));
		assertEquals(SystemRole.ADMIN, RoleParser.parseString("System.ADMIN"));
	}
	
	
}
