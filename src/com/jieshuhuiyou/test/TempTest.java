package com.jieshuhuiyou.test;

import junit.framework.TestCase;

import org.junit.Test;

import com.jieshuhuiyou.service.permission.fetcher.BookRoleFetcher.BookRole;


public class TempTest extends TestCase {

	@SuppressWarnings("unchecked")
	@Test
	public void testClass() throws ClassNotFoundException {
		
		@SuppressWarnings("rawtypes")
		Class<? extends Enum> role = (Class<? extends Enum>) Class.forName("com.jieshuhuiyou.service.permission.fetcher.BookRoleFetcher$BookRole");
		
		assertEquals(BookRole.PROVIDER, Enum.valueOf(role, "PROVIDER"));
		
	}
	
	
}
