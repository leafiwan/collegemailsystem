package com.jieshuhuiyou.test.permission;

import java.util.Set;

import javax.annotation.Resource;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.service.BookService;
import com.jieshuhuiyou.service.UserService;
import com.jieshuhuiyou.service.permission.Role;
import com.jieshuhuiyou.service.permission.fetcher.BookRoleFetcher;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"../dao/testContext.xml"})
public class PermissionTest extends TestCase {

	@Resource
	private BookRoleFetcher bf;

	@Resource
	private UserService us;
	
	@Resource
	private BookService bs;
	
	
	@Test
	public void simpleTest() {
		User u = us.getById(1);
		Book b = bs.getById(1);
		Set<Role> roles = bf.fetchRoles(u, b);
		assertTrue(!roles.isEmpty());
	}
	
	
	
	// setters and getters
	public BookRoleFetcher getBf() {
		return bf;
	}

	public void setBf(BookRoleFetcher bf) {
		this.bf = bf;
	}



	public UserService getUs() {
		return us;
	}



	public void setUs(UserService us) {
		this.us = us;
	}



	public BookService getBs() {
		return bs;
	}



	public void setBs(BookService bs) {
		this.bs = bs;
	}
	
	
	
}
