package com.jieshuhuiyou.test.dao;


import java.util.HashSet;
import java.util.Set;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jieshuhuiyou.dao.BookDao;
import com.jieshuhuiyou.dao.UserDao;
import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.service.permission.Validator;
import com.jieshuhuiyou.service.permission.fetcher.BookRoleFetcher.BookRole;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"testContext.xml"})
public class UserDaoTestCase extends TestCase {
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private BookDao bookDao;
	
	@Test
	public void testSimple() {
		User user = userDao.getByEmail("me@psjay.com");
		Book book = bookDao.getById(1);
		
		
		
		Set<BookRole> ar2 = new HashSet<BookRole>();
		ar2.add(BookRole.PROVIDER);
		
		assertTrue(Validator.validate(user, book, ar2, null));
		
	}
	
	
	
	// setters and getters
	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}



	public BookDao getBookDao() {
		return bookDao;
	}



	public void setBookDao(BookDao bookDao) {
		this.bookDao = bookDao;
	}
}
