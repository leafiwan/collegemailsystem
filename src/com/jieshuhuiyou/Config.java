package com.jieshuhuiyou;

import org.hibernate.validator.util.GetClassLoader;

/**
 * 配置类
 * @author psjay
 *
 */
public final class Config {
	
	/**
	 * 主域名
	 */
	public static final String DOMAIN = "mayishuwu.openpk.org";
	
	/**
	 * HTTP Session 中保存 ROLE 的关键字
	 */
	public static final String SESSION_KEY_ROLE = "ROLE";
	
	/**
	 * HTTP Session 中保存当前用户所在学校的关键字
	 */
	public static final String SESSION_KEY_SCHOOL = "SCHOOL";
	
	/**
	 * 	HTTP Session 中普通用户角色的值
	 */
	public static final String SESSION_VALUE_ROLE_USER = "USER";
	
	/**
	 * HTTP Session 中管理员角色值
	 */
	public static final String SESSION_VALUE_ROLE_ADMIN = "ADMIN";
	
	/**
	 * HTTP Session 中当前用户的 id key
	 */
	public static final String SESSION_KEY_USER_ID = "USER_ID";
	
	/**
	 * HTTP session 中当前用户的名字
	 */
	public static final String SESSION_KEY_USER_NAME = "USER_NAME";
	
	/**
	 * HTTP sessoin 中当前用户的小头像地址
	 */
	public static final String SESSION_KEY_USER_SMALL_AVATAR = "USER_SMALL_AVATAR";
	
	/**
	 * 权限不够的 action 返回值
	 */
	public static final String PERMISSION_FORBIDDEN = "forbidden";
	
	/**
	 * 用户大头像最大的宽度
	 */
	public static final int USER_AVATAR_LARGE_MAX_WIDTH = 120;
	
	/**
	 * 用户大头像最大比例
	 */
	public static final int USER_AVATAR_LARGE_MAX_ASPECT_RATIO = 2;
	
	/**
	 * 用户大头像目录
	 */
	public static final String USER_AVATAR_LARGE_PATH = "/upload/avatar/large/";
	
	/**
	 * 用户小头像目录
	 */
	public static final String USER_AVATAR_SMALL_PATH = "/upload/avatar/small/";
	
	/**
	 * 用户小头像尺寸
	 */
	public static final int USER_AVATAR_SMALL_SIZE = 48;
	
	/**
	 * 用户默认小头像
	 */
	public static final String USER_AVATAR_DEFUALT_SMALL = "/upload/avatar/default-s.jpg";
	
	/**
	 * 用户默认大头像
	 */
	public static final String USER_AVATAR_DEFUALT_LARGE = "/upload/avatar/default-l.jpg";
	
	/**
	 * 用户自动登录 key
	 */
	public static final String COOKIES_KEY_USER_REMEMBER_ME = "remember_me";
	
	/**
	 * 用户重置密码记录的过期时间（天）
	 */
	public static final int RESET_PASSWORD_RECORD_EXPIRES = 15;
	
    /**
     * 完成还书后还书方所减少的积分
     */
	public static final int  INTEGRAL_DEC_NUM_BRO = 4;
	
	
	/**
     * 完成还书后分享方所获得的积分
     */
	public static final int  INTEGRAL_INC_NUM_PRO = 6;
	
	/**
	 * 每个星星代表的分值
	 */
	public static final int CREDIT_PER_STAR = 2;
	
	/**
	 * 反馈信誉度时星星的总数
	 */
	public static final int CREDIT_STAR_NUM = 5;
	
	/**
	 * 相对于WebRoot的feeds内容模板路径
	 */
	public static final String EVENT_TEMPLATE_PATH = "WEB-INF/eventTemplates/eventTemplate.xml";
	
	/**
	 * 表情文件夹
	 */
	public static final String FACES_DIR_PATH = "images/face/";
	
	/**
	 * 用户积分初始值
	 */
	public static final int INTEGRAL_DEFAULT_NUM = 50;
	
	/**
	 * 首页书评的截取长度
	 */
	public static final int REVIEW_FEED_LENGTH = 100;
	
	/**
	 * 缓存中保存关注人id的KEY
	 */
	public static final String MEMCACHED_KEY_UIDLIST = "uidList";
	
	/**
	 * 缓存中保存关注书的id的KEY
	 */
	public static final String MEMCACHED_KEY_BOOKIDLIST = "bookidList";
}
