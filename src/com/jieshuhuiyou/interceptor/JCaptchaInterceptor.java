package com.jieshuhuiyou.interceptor;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.StrutsStatics;

import com.octo.captcha.module.servlet.image.SimpleImageCaptchaServlet;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class JCaptchaInterceptor extends AbstractInterceptor {

	private static final long serialVersionUID = 5658372875678605082L;

	@Override
	public String intercept(ActionInvocation ai) throws Exception {
		
		ActionContext context = ai.getInvocationContext();
		HttpServletRequest request = (HttpServletRequest) context.get(StrutsStatics.HTTP_REQUEST);
		String userCaptchaResponse = request.getParameter("captcha");
		boolean captchaPassed = SimpleImageCaptchaServlet.validateResponse(request, userCaptchaResponse);
		if(captchaPassed){
			return ai.invoke();
		}else{
			if(ai.getAction() instanceof ActionSupport) {
				ActionSupport action = (ActionSupport) ai.getAction();
				action.addFieldError("captcha", "验证码错误");
			}
			return Action.INPUT;
		}
		
	}

}


