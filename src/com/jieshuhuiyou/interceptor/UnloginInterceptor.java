package com.jieshuhuiyou.interceptor;

import java.util.List;
import java.util.Map;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class UnloginInterceptor extends AbstractInterceptor {

	private static final long serialVersionUID = -3820016373290796382L;

	@SuppressWarnings("unchecked")
	@Override
	public String intercept(ActionInvocation ai) throws Exception {
		Map<String, Object> session = ai.getInvocationContext().getSession();
		List<String> roles = (List<String>) session.get(com.jieshuhuiyou.Config.SESSION_KEY_ROLE);
		if(roles != null && !roles.isEmpty()) {
			return "unlogin";
		}
		return ai.invoke();
	}

}
