package com.jieshuhuiyou.interceptor;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jieshuhuiyou.Config;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.service.permission.PermissionValidatableAction;
import com.jieshuhuiyou.service.permission.Role;
import com.jieshuhuiyou.service.permission.RoleParser;
import com.jieshuhuiyou.service.permission.Validator;
import com.jieshuhuiyou.util.UserUtil;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

/**
 * 权限验证拦截器
 * @author psjay
 *
 */
public class PermissionInterceptor extends AbstractInterceptor {

	private static final long serialVersionUID = -3136125501446582857L;

	private Set<Role> allowRoles = new HashSet<Role>();
	private Set<Role> disallowRoles = new HashSet<Role>();
	private Map<Role, MessageAndResult> messageAndResult;
	private String user;
	private String subject;
	
	@Override
	public String intercept(ActionInvocation ai) throws Exception {
		Action action = (Action) ai.getAction();
		
		User validatedUser = null;
		Object validatedSubject = null;
		Set<Role> roles = null;
		
		// get validated user and subject
		if(user != null && !user.equals("")) {
			validatedUser = (User) ai.getStack().findValue(user, User.class);
		}
		
		if(subject != null && !subject.equals("")) {
			validatedSubject = ai.getStack().findValue(subject);
		}
		
		
		// get current user by default
		if(validatedUser == null) {
			validatedUser = UserUtil.getCurrentUser();
		}
		
		// get model by default if action is model driven
		if(validatedSubject == null && ai.getAction() instanceof ModelDriven<?>) {
			validatedSubject = ((ModelDriven<?>)ai.getAction()).getModel();
		}
		
		if(action instanceof PermissionValidatableAction) {
			
			PermissionValidatableAction pva = (PermissionValidatableAction)action;
			
			// check the validation is skipped or not
			if(pva.isSkipValidation()) {
				return ai.invoke();
			}
			
			// set validated user and subject
			pva.populateValidatedUser(validatedUser);
			pva.populateValidatedSubject(validatedSubject);
			
			roles = pva.getRoles();
		} else {
			roles = Validator.fetchRoles(validatedUser, validatedSubject);
		}
		
		boolean pass = Validator.validate(roles, allowRoles, disallowRoles);
		if(pass) {
			return ai.invoke();
		} else {
			if(messageAndResult == null || messageAndResult.isEmpty()) {
				return Config.PERMISSION_FORBIDDEN;
			}
			
			for(Role r : roles) {
				for(Role dr : disallowRoles) {
					if(r.equals(dr)) {
						MessageAndResult mar = messageAndResult.get(r);
						if(mar != null) {
							String msg = mar.getMessage();
							String result = mar.getResult();
							if(msg != null) {
								if(ai.getAction() instanceof ActionSupport) {
									ActionSupport as = (ActionSupport) ai.getAction();
									as.addActionError(msg); // add action error.
								}
							}
							if(result != null) {
								return result;	
							}
						}
					} 
				}
			}
			return Config.PERMISSION_FORBIDDEN;	// forbidden as the default result
		}
	}

	//	setters and getters
	public Set<Role> getAllowRoles() {
		return allowRoles;
	}

	public void setAllowRoles(String allowRolesStr) {
		String[] temp = allowRolesStr.split(",");
		for(String s: temp) {
			this.allowRoles.add(RoleParser.parseString(s.trim()));
		}
	}

	public Set<Role> getDisallowRoles() {
		return disallowRoles;
	}

	public void setDisallowRoles(String disallowRolesStr) {
		String[] temp = disallowRolesStr.split(",");
		if(messageAndResult == null) {
			messageAndResult = new HashMap<Role, MessageAndResult>();
		}
		for(String s: temp) {
			this.disallowRoles.add(RoleParser.parseString(s.trim()));
		}
	}

	public Map<Role, MessageAndResult> getMessageAndResult() {
		return messageAndResult;
	}
	
	
	/**
	 * Convert JSON string to messageAndResult map
	 * @param messageAndResultStr is something like : [{role: "Book.Provider", msg: "you've shared this book", result: "input"}]
	 */
	public void setMessageAndResult(String messageAndResultStr) {
		JSONArray array = JSONArray.parseArray(messageAndResultStr);
		Iterator<?> i = array.iterator();
		JSONObject obj = null;
		while(i.hasNext()) {
			obj = (JSONObject) i.next();
			this.messageAndResult.put(RoleParser.parseString(obj.getString("role"))
					, new MessageAndResult(obj.getString("msg"), obj.getString("result")));
		}
	}


	private static class MessageAndResult {
		
		private String message;
		private String result;
		
		public MessageAndResult(String msg, String result) {
			this.message = msg;
			this.result = result;
		}
		
		// setters and getters
		public String getMessage() {
			return message;
		}
		public String getResult() {
			return result;
		}

	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
		if(this.user != null) {
			this.user = this.user.trim();
		}
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
		if(this.subject != null) {
			this.subject = this.subject.trim();
		}
	}
	
}
