package com.jieshuhuiyou.interceptor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.StrutsStatics;

import com.jieshuhuiyou.Config;
import com.jieshuhuiyou.dao.AutoLoginRecordDao;
import com.jieshuhuiyou.dao.UserDao;
import com.jieshuhuiyou.entity.AutoLoginRecord;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.util.MD5er;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class AutoLoginInterceptor extends AbstractInterceptor {

	private static final long serialVersionUID = -7686073693456906442L;

	@Resource
	private AutoLoginRecordDao autoLoginRecordDao;
	@Resource
	private UserDao userDao;
	
	@Override
	public String intercept(ActionInvocation ai) throws Exception {
		
		HttpServletRequest request = (HttpServletRequest) ai.getInvocationContext()
				.get(StrutsStatics.HTTP_REQUEST);
		HttpServletResponse response = (HttpServletResponse) ai.getInvocationContext()
				.get(StrutsStatics.HTTP_RESPONSE);
		ActionContext context = ActionContext.getContext();
		Map<String, Object> session = context.getSession();
		
		if(session.get(Config.SESSION_KEY_USER_ID) != null) {
			return ai.invoke();
		}
		
		Cookie[] cookies = request.getCookies();
		if(cookies != null) {
			for(Cookie c : cookies) {
				if(c.getName().equals(Config.COOKIES_KEY_USER_REMEMBER_ME)) {
					
					String[] values = c.getValue().split("\\|");
					if(values.length == 2) {
						int userId = Integer.valueOf(values[0].trim());
						String content = values[1].trim();
						
						AutoLoginRecord record = autoLoginRecordDao.getByUserId(userId);
						if(record == null) {	//delete cookie
							c.setMaxAge(0);
							c.setPath("/");
							response.addCookie(c);
						} else {
							if(content.equals(record.getContent())) {
								// auto login
								User user = userDao.getById(userId);
								if(user != null) {									
									List<String> roleList = new ArrayList<String>();
									roleList.add(Config.SESSION_VALUE_ROLE_USER);
									session.put(Config.SESSION_KEY_ROLE, roleList);
									session.put(Config.SESSION_KEY_USER_ID, user.getId());
									session.put(Config.SESSION_KEY_USER_NAME, user.getName());
									session.put(Config.SESSION_KEY_USER_SMALL_AVATAR, user.getSmallAvatar());
									// update cookie
									String newContent = String.valueOf(userId) + "|";
									String recordContent = MD5er.md5(user.getEmail() + user.getPassword() + new Date().getTime());
									newContent += recordContent;
									c.setValue(newContent);
									c.setPath("/");
									c.setMaxAge(15 * 24 * 60 * 60);
									response.addCookie(c);
									// update record
									record.setContent(recordContent);
									autoLoginRecordDao.saveOrUpdate(record);
								} else {
									autoLoginRecordDao.delete(record);
								}
							}
							else {
								c.setMaxAge(0);
								c.setPath("/");
								response.addCookie(c);
								autoLoginRecordDao.delete(record);
							}
						}
					}
					
				}
			}
		}
		
		
		return ai.invoke();
	}

	public AutoLoginRecordDao getAutoLoginRecordDao() {
		return autoLoginRecordDao;
	}

	public void setAutoLoginRecordDao(AutoLoginRecordDao autoLoginRecordDao) {
		this.autoLoginRecordDao = autoLoginRecordDao;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

}
