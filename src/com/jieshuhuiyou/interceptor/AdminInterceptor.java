package com.jieshuhuiyou.interceptor;

import java.util.List;
import java.util.Map;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class AdminInterceptor extends AbstractInterceptor {

	private static final long serialVersionUID = 7890734962072852472L;

	@Override
	public String intercept(ActionInvocation ai) throws Exception {
		Map<String, Object> session = ai.getInvocationContext().getSession();
		@SuppressWarnings("unchecked")
		List<String> roles = (List<String>) session.get(com.jieshuhuiyou.Config.SESSION_KEY_ROLE);
		if(roles == null || roles.isEmpty() 
				|| !roles.contains(com.jieshuhuiyou.Config.SESSION_VALUE_ROLE_ADMIN)) {			
			return "admin_login";
		}
		return ai.invoke();
	}

}
