package com.jieshuhuiyou.interceptor;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class LoginInterceptor extends AbstractInterceptor {

	private static final long serialVersionUID = -4060353468042606456L;
	private static final String REF_KEY = "jieshuhuiyouReferrer";
	
	
	@SuppressWarnings("unchecked")
	@Override
	public String intercept(ActionInvocation ai) throws Exception {
		Map<String, Object> session = ai.getInvocationContext().getSession();
		List<String> roles = (List<String>) session.get(com.jieshuhuiyou.Config.SESSION_KEY_ROLE);
		if(roles == null || roles.isEmpty()) {
			// generate current url
			HttpServletRequest request = ServletActionContext.getRequest();
			String url = request.getRequestURI();
			if(request.getQueryString() != null) {
				url += "?" + request.getQueryString();
			}
			// set into ValueStack
			ai.getInvocationContext().put(REF_KEY, url);
			return Action.LOGIN;
		}
		return ai.invoke();
	}

}
