package com.jieshuhuiyou.views;

import com.alibaba.fastjson.JSON;
import com.jieshuhuiyou.entity.Notification;
import com.jieshuhuiyou.entity.notifications.AcceptBorrowRequestNotification;
import com.jieshuhuiyou.entity.notifications.AcceptBorrowRequestToOtherNotification;
import com.jieshuhuiyou.entity.notifications.CancleBorrowRequestNotification;
import com.jieshuhuiyou.entity.notifications.EvaluatedNotification;
import com.jieshuhuiyou.entity.notifications.NewBorrowRequestNotification;
import com.jieshuhuiyou.entity.notifications.RejectBorrowRequestNotification;
import com.jieshuhuiyou.entity.notifications.ReturnBookNotification;
import com.jieshuhuiyou.entity.notifications.SharingDeletedNotification;

public class NotificationDisplayHelper {

	private Notification notification;
	
	private String html;
	
	private static final String EMPTY_STRING = "";
	
	public NotificationDisplayHelper(Notification notification) {
		this.notification = notification;
		html = toHTML();
	}
	
	public String toHTML() {
		switch(notification.getType()) {
		case NEW_BORROW_REQUEST:
			return newBorrowRequestNotificationToHTML();
		case ACCEPT_BORROW_REQUEST:
			return acceptBorrowRequestNotificationToHTML();
		case REJECT_BORROW_REQEUST:
			return rejectBorrowRequestNotificationToHTML();
		case CANCLE_BORROW_REQUEST:
			return cancelBorrowRequestNotificationToHTML();
		case ACCEPT_BORROW_REQUEST_TO_OTHER:
			return acceptBorrowRequestToOtherNotificationToHTML();
		case RETURN_BOOK:
			return returnBookNotificationToHTML();
		case SHARING_DELETED:
			return sharingDeletedNotificationToHTML();
		case EVALUATE_FINISHED:
			return evaluatedNotificationToHTML();
		}
		return EMPTY_STRING;
	}
	
	private String newBorrowRequestNotificationToHTML() {
	
		NewBorrowRequestNotification n = JSON.parseObject(notification.getJsonContent()
				, NewBorrowRequestNotification.class);
		
		
		String html = "<a target=\"_blank\" _nid=\"" + notification.getId() + "\" class=\"notification\" href=\"/borrowRequest/mine\">" + n.getRequest().getUser().getName() + "想向我借阅《" + n.getRequest().getSharing().getBook().getTitle() + "》</a>"; 
		
		return html;
	}
	
	
	private String acceptBorrowRequestNotificationToHTML() {
		
		AcceptBorrowRequestNotification n = JSON.parseObject(notification.getJsonContent()
				, AcceptBorrowRequestNotification.class);
		
		String html = "<a target=\"_blank\" _nid=\"" + notification.getId() + "\" class=\"notification\" href=\"/borrow/mine\">" 
			+ n.getBorrow().getSharing().getUser().getName() + "同意了我借阅《" 
			+ n.getBorrow().getSharing().getBook().getTitle() + "》的请求</a>";
		return html;
	}
	
	private String returnBookNotificationToHTML() {
		ReturnBookNotification n = JSON.parseObject(notification.getJsonContent()
				, ReturnBookNotification.class);

		String html = null;
		if(notification.getUser().getId() == n.getBorrow().getSharing().getUser().getId()) {
			html = "<a target=\"_blank\" _nid=\"" + notification.getId() + "\" href=\"/borrow/mine?type=1\" class=\"notification\">" 
				+ n.getBorrow().getUser().getName() +"已经将《" + n.getBorrow().getSharing().getBook().getTitle() + "》还给了我</a>";
		}
		else {
			html = "<a target=\"_blank\" _nid=\"" + notification.getId() + "\" href=\"/borrow/mine\" class=\"notification\">" 
				+ n.getBorrow().getSharing().getUser().getName() +"已经确认我将《" + n.getBorrow().getSharing().getBook().getTitle() + "》还给了他</a>";
		}
		return html;
	}
	
	
	private String rejectBorrowRequestNotificationToHTML() {
		RejectBorrowRequestNotification n = JSON.parseObject(notification.getJsonContent()
				, RejectBorrowRequestNotification.class);
		int bookId = n.getRequest().getSharing().getBook().getId();
		String bookTitle = n.getRequest().getSharing().getBook().getTitle();
		String providerName = n.getRequest().getSharing().getUser().getName();
		String html = "<a target=\"_blank\" _nid=\"" + notification.getId() + "\" class=\"notification\" href=\"/book/" + bookId + "\">"
			+ providerName + "拒绝了我借阅《"
			+ bookTitle + "》的请求</a>";
		return html;
	}
	
	
	private String cancelBorrowRequestNotificationToHTML() {
		CancleBorrowRequestNotification n = JSON.parseObject(notification.getJsonContent()
				, CancleBorrowRequestNotification.class);
		int bookId = n.getRequest().getSharing().getBook().getId();
		String borrowerName = n.getRequest().getUser().getName();
		String bookTitle = n.getRequest().getSharing().getBook().getTitle();
		String html = "<a target=\"_blank\" _nid=\"" + notification.getId() + "\" class=\"notification\" href=\"/book/" + bookId + "\">"
			+ borrowerName + "取消了向我借阅《"
			+ bookTitle + "》的请求</a>";
		return html;
	}
	
	private String acceptBorrowRequestToOtherNotificationToHTML() {
		AcceptBorrowRequestToOtherNotification n = JSON.parseObject(notification.getJsonContent()
				, AcceptBorrowRequestToOtherNotification.class);
		
		int bookId = n.getRequest().getSharing().getBook().getId();
		String bookTitle = n.getRequest().getSharing().getBook().getTitle();
		String providerName = n.getRequest().getSharing().getUser().getName();
		String html = "<a target=\"_blank\" _nid=\"" + notification.getId() + "\" class=\"notification\" href=\"/book/" + bookId + "\">"
			+ providerName + "将《" + bookTitle + "》借给了其他用户，我的请求自动被取消</a>";
		return html;
	}
	
	private String sharingDeletedNotificationToHTML() {
		
		SharingDeletedNotification n = JSON.parseObject(notification.getJsonContent()
				, SharingDeletedNotification.class);
		
		int bookId = n.getSharing().getBook().getId();
		String bookTitle = n.getSharing().getBook().getTitle();
		String providerName = n.getSharing().getUser().getName();
		String html = "<a target=\"_blank\" _nid=\"" + notification.getId() + "\" class=\"notification\" href=\"/book/" + bookId + "\">"
			+ providerName + "取消了《" + bookTitle + "》的分享，我的请求自动被删除</a>";
		return html;
	}
	
	private String evaluatedNotificationToHTML(){
		EvaluatedNotification n = JSON.parseObject(notification.getJsonContent()
				, EvaluatedNotification.class);
	return n.toHTML();
	}
	
	//	setters and getters
	public Notification getNotification() {
		return notification;
	}

	public void setNotification(Notification notification) {
		this.notification = notification;
	}

	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}
	
}
