package com.jieshuhuiyou.util;

/**
 * 目录工具类
 * @author Michael
 *
 */
public class PathUtil {    
   
   
	private final static PathUtil pathUtil = new PathUtil();
	
	public static PathUtil getInstance(){
		return pathUtil;
	}
	
    public  String getWebClassesPath() {    
         String path = getClass().getProtectionDomain().getCodeSource()    
                 .getLocation().getPath().replaceAll("%20", " ");    
        return path;    
   
     }    
   
    public String getWebInfPath() throws IllegalAccessException {    
         String path = getWebClassesPath();    
        if (path.indexOf("WEB-INF") > 0) {    
             path = path.substring(0, path.indexOf("WEB-INF") + 8);    
         } else {    
            throw new IllegalAccessException("获取路劲出错");    
         }    
        return path;    
     }    
   
    public String getWebRoot() throws IllegalAccessException {    
         String path = getWebClassesPath();    
        if (path.indexOf("WEB-INF") > 0) {    
             path = path.substring(0, path.indexOf("WEB-INF/classes"));    
         } else {    
            throw new IllegalAccessException("·获取路劲出错");    
         }    
        return path;    
     }    
}   
