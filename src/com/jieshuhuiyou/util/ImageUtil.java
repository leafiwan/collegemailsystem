package com.jieshuhuiyou.util;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

public class ImageUtil {

	/**
	 * 裁剪图片
	 * @param src
	 * @param width
	 * @param height
	 * @return
	 */
	public static Image scale(Image src, int width, int height) {
		Image target = src.getScaledInstance(width, height, Image.SCALE_SMOOTH);
		return target;
	}
	
	/**
	 * 
	 * @param input
	 * @param output
	 * @param width
	 * @param height
	 * @throws IOException
	 */
	public static void scaleImageFile(File input, File output, int width, int height) throws IOException {
		BufferedImage bi = ImageIO.read(input);
		saveToFile(imageToBufferedImage(scale(bi, width, height)), output);
	}
	
	/**
	 * Image转换为BufferedImage
	 * @param img 被转换的Image对象
	 * @return 转换好的BufferedImage对象
	 */
	public static BufferedImage imageToBufferedImage(Image img) {
		BufferedImage bi = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_RGB);
		Graphics g = bi.getGraphics();
		g.drawImage(img, 0, 0, null);
		g.dispose();
		return bi;
	} 
	
	public static void saveToFile(BufferedImage img, File file) throws IOException {
		ImageIO.write(img, "JPEG", file);
	}
	
	public static void cutImageFile (File input, File output, int x1, int y1, int x2, int y2) 
		throws IOException {
		
		FileInputStream fis = null;
		ImageInputStream iis = null;
		
		try {
			fis = new FileInputStream(input);
			Iterator<ImageReader> it = ImageIO.getImageReadersByFormatName("jpg");
			ImageReader reader = it.next();
			iis = ImageIO.createImageInputStream(fis);
			reader.setInput(iis, true);
			ImageReadParam param = reader.getDefaultReadParam();
			Rectangle rect = new  Rectangle(x1, y1, x2 - x1, y2 - y1);
			param.setSourceRegion(rect); 
			BufferedImage bi  =  reader.read( 0 ,param);
			ImageIO.write(bi, "jpg", output);
		} catch (IOException e) {
				e.printStackTrace();
		} finally {
			if( fis != null) {
				fis.close();
			}
			if( iis != null) {
				iis.close();
			}
		}
	}

}
