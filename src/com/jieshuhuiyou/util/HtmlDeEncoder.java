package com.jieshuhuiyou.util;
/**
 * HTML编码类
 * @author Michael
 *
 */
public class HtmlDeEncoder {	
	
	public static String HtmlDecoder(String html){
		return html.replaceAll("&lt;", "<")
					.replaceAll("&quot;", "\"")
					.replaceAll("&gt;", ">");
	}
	
	public static String HtmlEncoder(String html){
		return html.replaceAll("<","&lt;")
					.replaceAll("\"","&quot;")
					.replaceAll(">","&gt;");
	}
}
