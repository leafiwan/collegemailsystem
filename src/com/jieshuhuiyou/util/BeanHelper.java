package com.jieshuhuiyou.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
/**
 * Bean Helper
 * @author PSJay
 *
 */
@Component
public class BeanHelper implements ApplicationContextAware {
	/**
	 * Spring's applicationContext
	 */
	private static ApplicationContext applicationContext;

	/**
	 * get a bean by name
	 * @param name bean's name
	 * @return
	 */
	public static Object getBean(String name) {
		return applicationContext.getBean(name);
	}
	/**
	 * get a bean by type
	 * @param <T>
	 * @param beanCls
	 * @return
	 */
	public static <T> T getBean(Class<T> beanCls) {
		return applicationContext.getBean(beanCls);
	}
	/**
	 * get a bean by name and type
	 * @param <T>
	 * @param name
	 * @param beanCls
	 * @return
	 */
	public static <T> T getBean(String name, Class<T> beanCls) {
		return applicationContext.getBean(name, beanCls);
	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		BeanHelper.applicationContext = applicationContext;
	}

}
