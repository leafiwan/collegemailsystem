package com.jieshuhuiyou.util;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

/**
 * XML工具类
 * @author Michael
 *
 */
public class XMLUtil {
	
	private static final XMLUtil xmlUtil = new XMLUtil();
	
	private Document doc;
	
	private String rootPath;
	
	public static XMLUtil getInstance(){
		return xmlUtil;
	}
	
	//初始化
	public void init(String filename)
	{	
	 	try {
			rootPath = PathUtil.getInstance().getWebRoot();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		doc = load(rootPath + filename);
	}
	
	//载入
	public Document load(String filename) 
	   { 
	      Document document = null; 
	      try  
	      {  
	          SAXReader saxReader = new SAXReader(); 
	          document = saxReader.read(new File(filename)); 
	      } 
	      catch (Exception ex){ 
	          ex.printStackTrace(); 
	      }   
	      return document; 
	   }
	
	
	//获取拥有特定属性的特定节点 若有相同的则返回第一个
	public String getNodeTextByAttr(String nodeName,String attrName,String attrValue){
		Element root = doc.getRootElement();
		@SuppressWarnings("unchecked")
		List<Element> childs = root.elements(nodeName);
		Iterator<Element> i = childs.iterator();
		while(i.hasNext()){
			Element child = (Element)i.next();
			if(child.attribute(attrName).getText().equals(attrValue)){
				return child.getText();
			}
		}
		return null;
	}
	
	

	public Document getDoc() {
		return doc;
	}


	public void setDoc(Document doc) {
		this.doc = doc;
	}
	
	
}
	
	
	
