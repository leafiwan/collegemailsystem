package com.jieshuhuiyou.util;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.jieshuhuiyou.Config;

/**
 * 字符串工具类
 * @author WanLinfeng  Michael
 *
 */
public class StringUtil {
	
	/**
	 * 将字符串缩短为一半，结果字符串i号位置的值是原字符串i号位置和i+1号位置和的一半
	 * @param str
	 * @return
	 */
	public static String narrowStringToHalf(String str){
		byte[] bytesOfStr = str.getBytes();
		int n = bytesOfStr.length;
		byte[] resultArr = new byte[n/2];
		for(int i = 0,j = 0 ; i < n ; i+=2 , j++){
			if((i+1) < n){
				resultArr[j] = (byte) ((bytesOfStr[i] + bytesOfStr[i+1])/2);
			} else {
				resultArr[j] = bytesOfStr[i];
			}
		}
		String result = new String(resultArr);
		return result;
	}
	
	/**
	 * 替换所有可用的图片
	 * @param str
	 * @return
	 */
	public static String replaceAllImg(String str){
		//匹配出表情图片
		String reg = "\\@.*?@";
		String fileName = "";
		Pattern p = Pattern.compile(reg);
		Matcher m = p.matcher(str);
		while(m.find()){
			fileName = m.group().replaceAll("@", "") + ".gif";
			try {
				if(new File(PathUtil.getInstance().getWebRoot() + Config.FACES_DIR_PATH + fileName).exists()){
					str = str.replaceAll(m.group(), "<img src='" + Config.FACES_DIR_PATH + fileName + "'/>");
				}
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return str;
	}
}
