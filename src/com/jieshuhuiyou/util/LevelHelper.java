package com.jieshuhuiyou.util;
import java.util.HashMap;
import java.util.Map;
/**
 * 暂时无用，等以后用到QQ等级制度后，才用
 * @author Administrator
 *
 */
public class LevelHelper {
     
	static public Map<String, Integer> getCreditLevelArray(int score){
		//跨度
		int increase = 20;
		//信誉度初始值
		int nScore = 0;
		//跨度的增长值
		int increaseI = 10;
		//星星数   初始为1
		int starNum = 1;
		//月亮数
		int moonNum = 0;
		//太阳数
		int sunNum = 0;
		//转换比例，即4个星星为1个月亮
		int rate = 4;
		
		Map<String, Integer> levelMap = new HashMap<String, Integer>();
		
		while(true){
			nScore += increase;
			if(nScore <= score){
				starNum ++;
				increase += increaseI;
				
				moonNum = moonNum + (int)(starNum / rate);
				starNum = starNum % rate;
				sunNum = sunNum + (int)(moonNum / rate);
				moonNum = moonNum % rate;
				
			}else{
				break;
			}
		}
		
		levelMap.put("star", starNum);
		levelMap.put("moon", moonNum);
		levelMap.put("sun", sunNum);
		levelMap.put("score",score);
		
		return levelMap;
	}
}
