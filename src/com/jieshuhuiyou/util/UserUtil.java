package com.jieshuhuiyou.util;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.jieshuhuiyou.Config;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.service.UserService;
import com.opensymphony.xwork2.ActionContext;


/**
 * 用户工具类
 * @author psjay
 *
 */
@Service
public class UserUtil {

	
	private static UserService userService;

	/**
	 * 产生salt
	 * @return 产生的salt字符串
	 */
	public static String produceSalt() {
		String currentTime = "" + System.currentTimeMillis();
		String salt = MD5er.md5(currentTime);
		salt = StringUtil.narrowStringToHalf(salt);
		return salt;
	}

	/**
	 * 得到当前登录的用户
	 * @return
	 */
	public static User getCurrentUser() {
		int id = getCurrentUserId();
		if(id != 0) {
			return userService.getById(id);
		}
		return null;
	}
	
	/**
	 * 得到当前登录用户的 id
	 * @return 当前登录用户的 id，否则返回 0
	 */
	public static int getCurrentUserId() {
		int id;
		Map<String, Object> session = ActionContext.getContext().getSession();
		if(session.get(Config.SESSION_KEY_USER_ID) != null) {
			id = (Integer) session.get(Config.SESSION_KEY_USER_ID);
			return id;
		}
		return 0;
	}
	
	// setters and getters
	public UserService getUserService() {
		return userService;
	}
	
	@Resource
	public void setUserService(UserService userService) {
		UserUtil.userService = userService;
	}

}
