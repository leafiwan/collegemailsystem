package com.jieshuhuiyou.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 文件操作工具类
 * @author Michael
 *
 */
public class FileUtil {
	
	/**
	 * 得到特定目录下的所有文件名
	 * @param dirPath
	 * @return
	 */
	public static List<String> getAllFilesName(String dirPath){
		List<String> filesName = new ArrayList<String>();
		File dir = new File(dirPath);
		File[] files = dir.listFiles();
		
		for(int i = 0;i < files.length;i++){
			filesName.add(files[i].getName());
		}
		filesName.add("no.gif");
		return filesName;
	}
}
