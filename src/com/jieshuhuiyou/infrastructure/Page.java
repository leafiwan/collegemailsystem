package com.jieshuhuiyou.infrastructure;

import java.util.List;


/**
 * 分页类
 * @author PSJay
 *
 * @param <T> 元素类型
 */
public class Page<T> {
	
	private List<T> elements;	//当前页的所有元素
	private int start;	//开始元素
	private int pageSize;	//页面尺寸
	private int currentPage;	//当前页
	private int elementsCount;	// 总元素数
	private int pagesCount;	//总页面数
	
	public Page(List<T> elements, int start, int pageSize, int elementsCount) {
		this.elements = elements;
		this.start = start;
		this.pageSize = pageSize;
		this.elementsCount = elementsCount;
		
		// calculate current page
		this.currentPage = start / pageSize + 1;
		// calculate pagesCount
		this.pagesCount = (elementsCount - 1) / pageSize + 1;
	}
	
	// setters and getters
	public List<T> getElements() {
		return elements;
	}
	public void setElements(List<T> elements) {
		this.elements = elements;
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	public int getElementsCount() {
		return elementsCount;
	}
	public void setElementsCount(int elementsCount) {
		this.elementsCount = elementsCount;
	}
	public int getPagesCount() {
		return pagesCount;
	}
	public void setPagesCount(int pagesCount) {
		this.pagesCount = pagesCount;
	}
	
}
