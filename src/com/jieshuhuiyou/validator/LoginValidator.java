package com.jieshuhuiyou.validator;

import java.util.List;
import java.util.Map;

import com.opensymphony.xwork2.ActionContext;


public class LoginValidator {
	@SuppressWarnings("unchecked")
	public static boolean validate() {
		Map<String, Object> session = ActionContext.getContext().getSession();
		List<String> roles = (List<String>) session.get(com.jieshuhuiyou.Config.SESSION_KEY_ROLE);
		if(roles == null || roles.isEmpty()) {
			return false;
		}
		return true;
	}
}
