package com.jieshuhuiyou.dao;

import java.util.List;

import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.BookFollowRelation;
import com.jieshuhuiyou.entity.User;

/**
 * 书籍关注数据访问接口
 * @author psjay
 *
 */
public interface BookFollowRelationDao extends Dao<BookFollowRelation> {

	/**
	 * 判断一个用户是否关注了某本书
	 * @return
	 */
	public boolean hasFollowed(User user, Book book);
	
	/**
	 * 根据用户和书籍取出关注关系
	 * @param user
	 * @param book
	 * @return
	 */
	public BookFollowRelation getByUserAndBook(User user, Book book);
	
	/**
	 * 得到关注书的id列表
	 */
	public List<Integer> getFollowBookIdByUser(User user);
	
}
