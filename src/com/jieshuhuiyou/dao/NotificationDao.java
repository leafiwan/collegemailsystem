package com.jieshuhuiyou.dao;

import java.util.List;

import com.jieshuhuiyou.entity.Notification;
import com.jieshuhuiyou.entity.User;

public interface NotificationDao extends Dao<Notification>{

	/**
	 * 根据拥有者获取通知
	 * @param owner
	 * @return
	 */
	public List<Notification> getByOwner(User owner, int start, int count, boolean desc);
	
	/**
	 * 根据 id 和 拥有者删除通知
	 * @param ids
	 * @param owner
	 */
	public void deleteByIdsAndOwner(int[] ids, User owner);
	
	/**
	 * 根据 id 取出通知
	 * @param id
	 * @return
	 */
	public Notification getById(int id);
	

	/**
	 * 得到某用户的通知数量
	 * @param user
	 * @return 通知数量
	 */
	public int getNotificationCount(User user);
	
}
