package com.jieshuhuiyou.dao;

import java.util.List;

import com.jieshuhuiyou.entity.Administrator;
import com.jieshuhuiyou.entity.User;
/**
 * 管理员数据访问类
 * @author psjay
 *
 */
public interface AdministratorDao extends Dao<Administrator> {

	/**
	 * 列出管理员
	 * @param start	 第一个索引
	 * @param count 列出的数量
	 * @param desc 是否按 id 倒序
	 * @return
	 */
	public List<Administrator> list(int start, int count, boolean desc);
	
	/**
	 * 得到管理员的总数
	 * @return
	 */
	public int getCount();
	
	/**
	 * 根据用户取出管理员
	 * @param user
	 * @return
	 */
	public Administrator getByUser(User user);
	
	/**
	 * 根据 id 取出管理员
	 * @param id
	 * @return
	 */
	public Administrator getById(int id);
	
}
