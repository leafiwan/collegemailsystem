package com.jieshuhuiyou.dao;

import java.util.List;

import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.User;
/**
 * 用户数据访问接口
 * @author psjay
 */
public interface UserDao extends Dao<User>{
	
	/**
	 * 从数据库中获取一个用户对象
	 * @param id
	 * @return
	 */
	public User getById(int id);
	
	/**
	 * 从数据库中获取一个对象
	 * @param email
	 * @return
	 */
	public User getByEmail(String email);
	
	/**
	 * 判断 User 是否存在
	 * @param user
	 * @return
	 */
	public boolean isExists(User user);
	
	/**
	 * get book's providers
	 * @param book
	 * @param start
	 * @param count
	 * @return
	 */
	public List<User> getProviders(Book book, int start, int count, boolean desc);
	
	/**
	 * 获取书籍的借阅者
	 * @param book
	 * @param start
	 * @param count
	 * @param desc
	 * @return
	 */
	public List<User> getBorrowers(Book book, int start, int count, boolean desc);
	
	/**
	 * 获取所有用户
	 * @param start 起始id
	 * @param count 最大获得数
	 * @param desc 是否降序
	 * @return
	 */
	public List<User> getUsers(int start, int count, boolean desc);
	
	/**
	 * 根据条件查询user
	 * @param name
	 * @param school
	 * @param start
	 * @param count
	 * @return
	 */
	public List<User> searchUsers(String name , String school , int start , int count , boolean desc);

	/**
	 * 根据条件查找用户
	 * @param name
	 * @param school
	 * @return
	 */
	public int getUsersCountByConditions(String name , String school);
	
	/**
	 * 根据条件查找用户
	 * @param name
	 * @param school
	 * @return
	 */
	public int getUsersCount();
}
