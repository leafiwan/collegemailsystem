package com.jieshuhuiyou.dao;

import java.util.List;

import com.jieshuhuiyou.entity.Message;
import com.jieshuhuiyou.entity.User;

public interface MessageDao extends Dao<Message> {

	/**
	 * 取出某用户收到的所有的私信
	 * @param user
	 * @return
	 */
	public List<Message> getReceiveMessageListByUser(User user ,int start ,int count ,boolean desc);
	
	/**
	 * 取出某用户收到的所有的私信的数量
	 * @param user
	 * @return
	 */
	public int getReceiveMessageCount(User user);
	
	/**
	 * 取出某用户发送的所有的私信
	 * @param user
	 * @return
	 */
	public List<Message> getSendMessageListByUser(User user , int start, int count, boolean desc);

	/**
	 * 取出某用户发送的所有的私信的数量
	 * @param user
	 * @return
	 */
	public int getSendMessageCount(User user);
	
	/**
	 * 通过ID获得私信
	 * @param id
	 * @return
	 */
	public Message getById(int id);
}
