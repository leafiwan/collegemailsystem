package com.jieshuhuiyou.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;

import com.jieshuhuiyou.dao.AdministratorDao;
import com.jieshuhuiyou.entity.Administrator;
import com.jieshuhuiyou.entity.User;

@Repository
public class AdministratorDaoImpl extends AbstractDao<Administrator> 
	implements AdministratorDao{

	@Override
	public List<Administrator> list(final int start, final int count, final boolean desc) {
		@SuppressWarnings("unchecked")
		List<Administrator> results = getHibernateTemplate().executeFind(new HibernateCallback<List<Administrator>>() {
			@Override
			public List<Administrator> doInHibernate(Session session)
					throws HibernateException, SQLException {
				
				String hql ="FROM Administrator a";
				if(desc) {
					hql += " ORDER BY a.id DESC";
				}
				
				Query query = session.createQuery(hql);
				query.setFirstResult(start);
				query.setMaxResults(count);
				
				return query.list();
			}
			
		});
		return results;
	}

	@Override
	public int getCount() {
		String hql ="SELECT COUNT(*) FROM Administrator a";
		List<?> result = getHibernateTemplate().find(hql);
		if(!result.isEmpty()) {
			return Integer.parseInt(result.get(0).toString());
		}
		return 0;
	}

	@Override
	public Administrator getByUser(User user) {
		String hql = "FROM Administrator a WHERE a.user = ?";
		@SuppressWarnings("unchecked")
		List<Administrator> result = getHibernateTemplate().find(hql, user);
		if(!result.isEmpty()) {
			return result.get(0);
		}
		return null;
	}

	@Override
	public Administrator getById(int id) {
		return getHibernateTemplate().get(Administrator.class, id);
	}

	
}
