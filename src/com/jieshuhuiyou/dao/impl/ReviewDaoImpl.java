package com.jieshuhuiyou.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;

import com.jieshuhuiyou.dao.ReviewDao;
import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.Review;
import com.jieshuhuiyou.entity.User;

@Repository
public class ReviewDaoImpl extends AbstractDao<Review> implements ReviewDao {

	@Override
	public Review getById(long id) {
		return getHibernateTemplate().get(Review.class, id);
	}

	@Override
	public List<Review> getByAuthor(final User author, final int start, final int count,
			final boolean desc) {
		@SuppressWarnings("unchecked")
		List<Review> reviews = getHibernateTemplate().executeFind(new HibernateCallback<List<Review>>() {

			@Override
			public List<Review> doInHibernate(
					Session session) throws HibernateException, SQLException {
				
				String hql = "FROM Review r WHERE r.author = :author";
				if(desc) {
					hql += " ORDER BY r.id DESC";
				}
				Query query = session.createQuery(hql);
				query.setParameter("author", author);
				query.setFirstResult(start);
				query.setMaxResults(count);
				
				return query.list();
			}
			
		});
		return reviews;
	}

	@Override
	public List<Review> getByBook(final Book book, final int start,
			final int count, final boolean desc) {
		@SuppressWarnings("unchecked")
		List<Review> reviews = getHibernateTemplate().executeFind(new HibernateCallback<List<Review>>() {

			@Override
			public List<Review> doInHibernate(
					Session session) throws HibernateException, SQLException {
				
				String hql = "FROM Review r WHERE r.book = :book";
				if(desc) {
					hql += " ORDER BY r.id DESC";
				}
				Query query = session.createQuery(hql);
				query.setParameter("book", book);
				query.setFirstResult(start);
				query.setMaxResults(count);
				
				return query.list();
			}
			
		});
		return reviews;
	}

}
