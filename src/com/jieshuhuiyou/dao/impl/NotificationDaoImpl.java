package com.jieshuhuiyou.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;

import com.jieshuhuiyou.dao.NotificationDao;
import com.jieshuhuiyou.entity.Notification;
import com.jieshuhuiyou.entity.User;

@Repository
public class NotificationDaoImpl extends AbstractDao<Notification> implements NotificationDao {
	
	public List<Notification> getByOwner(final User owner, final int start, final int count,final boolean desc) {
		@SuppressWarnings("unchecked")
		List<Notification> result = getHibernateTemplate().executeFind(new HibernateCallback<List<Notification>>() {
			@Override
			public List<Notification> doInHibernate(Session session)
					throws HibernateException, SQLException {
				
					String hql = "FROM Notification n WHERE n.user = :owner ORDER BY n.id";
					if(desc) {
						hql += " DESC";
					}
					Query query = session.createQuery(hql);
					query.setFirstResult(start);
					query.setMaxResults(count);
					query.setParameter("owner", owner);
					return query.list();
			}
			
		});
		return result;
	}
	
	public int getNotificationCount(User user) {
		String hql = "SELECT COUNT(*) FROM Notification n WHERE n.user = ?";
		List<?> result = getHibernateTemplate().find(hql, user);
		if(!result.isEmpty()) {
			return Integer.parseInt(result.get(0).toString());
		}
		return 0;
	}
	
	public Notification getById(int id) {
		return getHibernateTemplate().get(Notification.class, id);
	}
	
	
	public void deleteByIdsAndOwner(final int[] ids, final User owner) {
		getHibernateTemplate().execute(new HibernateCallback<Void>() {

			@Override
			public Void doInHibernate(Session session) throws HibernateException,
					SQLException {
				
				String hql = "DELETE FROM Notification n WHERE n.id IN (:ids) AND n.user = :owner";
				Query q = session.createQuery(hql);
				List<Integer> idsList = new ArrayList<Integer>();
				for(int id : ids) {
					idsList.add(id);
				}
				q.setParameterList("ids", idsList);
				q.setParameter("owner", owner);
				
				q.executeUpdate();
				return null;
			}
			
		});
	}
	
}
