package com.jieshuhuiyou.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.jieshuhuiyou.dao.AutoLoginRecordDao;
import com.jieshuhuiyou.entity.AutoLoginRecord;

@Repository
public class AutoLoginRecordDaoImpl extends AbstractDao<AutoLoginRecord>
		implements AutoLoginRecordDao {

	@Override
	public void saveOrUpdate(AutoLoginRecord record) {
		getHibernateTemplate().saveOrUpdate(record);
	}

	@Override
	public AutoLoginRecord getByUserId(int userId) {
		String hql = "FROM AutoLoginRecord r WHERE r.user.id = ?";
		@SuppressWarnings("unchecked")
		List<AutoLoginRecord> results = getHibernateTemplate().find(hql, userId);
		if(!results.isEmpty()) {
			return results.get(0);
		}
		return null;
	}
	
	public void delete(AutoLoginRecord record) {
		getHibernateTemplate().delete(record);
	}

}
