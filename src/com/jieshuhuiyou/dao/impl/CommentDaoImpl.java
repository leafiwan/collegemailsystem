package com.jieshuhuiyou.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;

import com.jieshuhuiyou.dao.CommentDao;
import com.jieshuhuiyou.entity.Comment;
import com.jieshuhuiyou.entity.Review;
import com.jieshuhuiyou.entity.comment.ReviewComment;

@Repository
public class CommentDaoImpl extends AbstractDao<Comment> implements CommentDao {

	@Override
	public Comment getById(long id) {
		return getHibernateTemplate().get(Comment.class, id);
	}

	@Override
	public List<ReviewComment> getByReview(final Review review, final int start, final int count,
			final boolean desc) {
		
		
		@SuppressWarnings("unchecked")
		List<ReviewComment> reviewComments = getHibernateTemplate().executeFind(new HibernateCallback<List<ReviewComment>>() {

			@Override
			public List<ReviewComment> doInHibernate(Session session)
					throws HibernateException, SQLException {
				
				String queryString = "FROM ReviewComment rc WHERE rc.review = :review";
				if(desc) {
					queryString += " ORDER BY rc.id DESC";
				}
				
				Query query = session.createQuery(queryString);
				query.setParameter("review", review);
				query.setFirstResult(start);
				query.setMaxResults(count);
				
				return query.list();
			}
		
		});
		
		return reviewComments;
	}

}
