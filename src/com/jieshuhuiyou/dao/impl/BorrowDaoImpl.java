package com.jieshuhuiyou.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;

import com.jieshuhuiyou.dao.BorrowDao;
import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.Borrow;
import com.jieshuhuiyou.entity.User;

@Repository
public class BorrowDaoImpl extends AbstractDao<Borrow> implements BorrowDao {
	
	@Override
	public Borrow getById(int id) {
		return getHibernateTemplate().get(Borrow.class, id);
	}

	@Override
	public List<Borrow> getByBorrows(final User borrower, final int start, final int count, final boolean desc) {
		@SuppressWarnings("unchecked")
		List<Borrow> results = getHibernateTemplate().executeFind(new HibernateCallback<List<Borrow>>() {

			@Override
			public List<Borrow> doInHibernate(Session session)
					throws HibernateException, SQLException {
				
				String hql = "FROM Borrow b WHERE b.user = :borrower ORDER BY b.returned, b.id";
				if(desc) {
					hql += " DESC";
				}
				Query q = session.createQuery(hql);
				q.setParameter("borrower", borrower);
				q.setFirstResult(start);
				q.setMaxResults(count);
				return q.list();
			}
		
		});
		return results;
	}


	@Override
	public List<Borrow> getByProvider(final User provider, final int start, final int count,final boolean desc) {
		@SuppressWarnings("unchecked")
		List<Borrow> results = getHibernateTemplate().executeFind(new HibernateCallback<List<Borrow>>() {

			@Override
			public List<Borrow> doInHibernate(Session session)
					throws HibernateException, SQLException {
				
				String hql = "FROM Borrow b WHERE b.sharing.user = :provider ORDER BY b.returned, b.id";
				if(desc) {
					hql += " DESC";
				}
				Query q = session.createQuery(hql);
				q.setParameter("provider", provider);
				q.setFirstResult(start);
				q.setMaxResults(count);
				return q.list();
			}
		
		});
		return results;
	}
	
	
	@Override
	public List<Borrow> getByBook(final Book book, final int start, final int count, final boolean desc) {
		@SuppressWarnings("unchecked")
		List<Borrow> result = getHibernateTemplate().executeFind(new HibernateCallback<List<Borrow>>() {

			@Override
			public List<Borrow> doInHibernate(Session session)
					throws HibernateException, SQLException {
				
				String hql = "FROM Borrow b WHERE b.sharing.book = :book ORDER BY b.id";
				if(desc) {
					hql += " DESC";
				}
				Query query = session.createQuery(hql);
				query.setParameter("book", book);
				query.setFirstResult(start);
				query.setMaxResults(count);
				
				return query.list();
			}
			
		});
		
		return result;
	}
	
	
	@Override
	public List<Borrow> getByBorrowerAndBook(final User borrower,final  Book book,final  int start
			,final  int count,final  boolean desc) {
		@SuppressWarnings("unchecked")
		List<Borrow> borrows = getHibernateTemplate().executeFind(new HibernateCallback<List<Borrow>>() {

			@Override
			public List<Borrow> doInHibernate(Session session)
					throws HibernateException, SQLException {
				
				String hql = "FROM Borrow b WHERE b.user = :borrower AND b.sharing.book = :book ORDER BY b.borrowDate";
				if(desc) {
					hql += " DESC";
				}
				Query q = session.createQuery(hql).setFirstResult(start)
					.setMaxResults(count)
					.setParameter("borrower", borrower)
					.setParameter("book", book);
				
				return q.list();
			}
		
			
		});
		
		return borrows;
	}
	
	
}
