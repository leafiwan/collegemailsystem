package com.jieshuhuiyou.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;

import com.jieshuhuiyou.dao.UserFollowRelationDao;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.entity.UserFollowRelation;

/**
 * 关注数据层接口
 * @author Michael
 *
 */
@Repository
public class UserFollowRelationDaoImpl extends AbstractDao<UserFollowRelation> implements UserFollowRelationDao{

	@Override
	public ArrayList<User> getFollowUserByUser(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserFollowRelationDao getRelationById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserFollowRelation getRelationByFandFedUser(User follow,
			User followed) {
		String hql = "from UserFollowRelation u where u.follow = ? and u.followed = ?";
		UserFollowRelation userFollowRelation =  (UserFollowRelation) getHibernateTemplate().find(hql, follow,followed).get(0);
		return userFollowRelation;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> getFollowUserIdByUser(final User user) {
		final String hql = "SELECT followed.id FROM UserFollowRelation u WHERE u.follow.id = :currentUserId " ;
		List<Integer> uidList = getHibernateTemplate().executeFind(new HibernateCallback<List<Integer>>() {

			@Override
			public List<Integer> doInHibernate(Session arg0)
					throws HibernateException, SQLException {
				
				Query query =  arg0.createQuery(hql);
				query.setParameter("currentUserId", user.getId());
				
				//无论是谁自己都要关注自己
				List<Integer> temp = query.list();
				temp.add(user.getId());
				
				
				return temp;
			}
		});
		return uidList;
	}




}
