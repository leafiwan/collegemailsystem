package com.jieshuhuiyou.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;


import com.jieshuhuiyou.Config;
import com.jieshuhuiyou.dao.BookFollowRelationDao;
import com.jieshuhuiyou.dao.EventDao;
import com.jieshuhuiyou.dao.UserFollowRelationDao;
import com.jieshuhuiyou.entity.Event;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.util.memcachedHelper;

@Repository
public class EventDaoImpl extends AbstractDao<Event> implements EventDao{

	//获取我关注的人的id列表
	//private String HQL_1 = "SELECT followed.id FROM UserFollowRelation u WHERE u.follow.id = :currentUserId " ;
	//获取我关注的人的事件
	private String HQL_2 = "FROM Event e WHERE  e.book.id in (:bookidList) or e.user.id in (:uidList) ORDER BY  e.id DESC";
	//获取我关注的书的id列表
	//private String HQL_3 = "SELECT b.book.id FROM BookFollowRelation b WHERE b.user.id = :currentUserId";
	
	@Resource
	private UserFollowRelationDao userFollowRelationDao;
	
	@Resource
	private BookFollowRelationDao bookFollowRelationDao;
	
	
	@Override
	public Event getById(int id) {
		return getHibernateTemplate().get(Event.class, id);
	}

	@Override
	public List<Event> getFollowEventByUser(final User user,final int start,final int end) {
		@SuppressWarnings({ "unchecked" })
		List<Event> eventList = getHibernateTemplate().executeFind(new HibernateCallback<List<Event>>() {

			@Override
			public List<Event> doInHibernate(Session arg0)
					throws HibernateException, SQLException {
				
				//首先要获取缓存中的收听列表
				memcachedHelper memcached = memcachedHelper.getInstance();
				List<Integer> temp ;
				Query query= null;
				
				//先试图从memcached缓存中获取关注的人的id列表
				/*query =  arg0.createQuery(HQL_1);
				query.setParameter("currentUserId", user.getId());
				
				//无论是谁自己都要关注自己
				temp = query.list();
				temp.add(user.getId());*/
				
				
				
				if(memcached.get(Config.MEMCACHED_KEY_UIDLIST) == null){
					temp = userFollowRelationDao.getFollowUserIdByUser(user);
					memcached.add(Config.MEMCACHED_KEY_UIDLIST, (List<Integer>)temp);
				}else{
					//memcached.replace(Config.MEMCACHED_KEY_UIDLIST, temp);
				}
	
				//先视图从memcached缓存中获取我关注的书的id列表
				/*query =  arg0.createQuery(HQL_3);
				query.setParameter("currentUserId", user.getId());
				//初始化-1否则会出错
				temp = query.list();
				temp.add(-1);*/
				
				
				
				if(memcached.get(Config.MEMCACHED_KEY_BOOKIDLIST) == null){
					temp = bookFollowRelationDao.getFollowBookIdByUser(user);
					memcached.add(Config.MEMCACHED_KEY_BOOKIDLIST, (List<Integer>)temp);
				}else{
					//memcached.replace(Config.MEMCACHED_KEY_BOOKIDLIST, temp);
				}
				
				query = arg0.createQuery(HQL_2);
				query.setParameterList("uidList",(List<Integer>)memcached.get(Config.MEMCACHED_KEY_UIDLIST));
				query.setParameterList("bookidList", (List<Integer>)memcached.get(Config.MEMCACHED_KEY_BOOKIDLIST));
				
				query.setFirstResult(start);
				query.setMaxResults(end);
				
				return query.list();
				
			}
			
		});
		return eventList;
	}

}
