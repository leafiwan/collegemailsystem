package com.jieshuhuiyou.dao.impl;


import javax.annotation.Resource;

import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.jieshuhuiyou.dao.Dao;


/**
 * 抽象数据访问类
 * @author psjay
 *
 */
public abstract class AbstractDao<T> extends HibernateDaoSupport implements Dao<T>{
	
	// inject hibernate's session factory
	@Resource
	public void initSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	
	//basic methods implementations
	@Override
	public void save(T entity) {
		getHibernateTemplate().save(entity);
	}
	
	@Override
	public void delete(T entity) {
		getHibernateTemplate().delete(entity);
	}
	
	@Override
	public void update(T entity) {
		getHibernateTemplate().update(entity);
	}
	
	@Override
	public void saveOrUpdate(T entity) {
		getHibernateTemplate().saveOrUpdate(entity);
	}
}
