package com.jieshuhuiyou.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;

import com.jieshuhuiyou.dao.SchoolDao;
import com.jieshuhuiyou.entity.School;

@Repository
public class SchoolDaoImpl extends AbstractDao<School> implements SchoolDao {

	@Override
	public School getById(int id) {
		return getHibernateTemplate().get(School.class, id);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public School getByName(String name) {
		String hql = "FROM School s WHERE s.name = ?";
		List<School> schools =  getHibernateTemplate().find(hql, name);
		if(schools.isEmpty()) {
			return null;
		}
		return schools.get(0);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<School> getAllSchools() {
		List<School> resultList = getHibernateTemplate().executeFind(new HibernateCallback<List<School>>() {
			@Override
			public List<School> doInHibernate(Session session)
					throws HibernateException, SQLException {
				String hql = "FROM School";
				Query q = session.createQuery(hql);
				List<School> results = q.list();
				return results;
			}
		});
		return resultList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<School> getAllSchools(final int start , final int count , final boolean desc) {
		List<School> resultList = getHibernateTemplate().executeFind(new HibernateCallback<List<School>>() {
			@Override
			public List<School> doInHibernate(Session session)
					throws HibernateException, SQLException {
				String hql = "FROM School s";
				if(desc) {
					hql = hql + "ORDER BY s.id DESC";
				}
				Query q = session.createQuery(hql).setFirstResult(start).setMaxResults(count);
				List<School> results = q.list();
				return results;
			}
		});
		return resultList;
	}

	@Override
	public int getSchoolsCount() {
		String hql = "SELECT COUNT(*) FROM School s";
		return Integer.parseInt(getHibernateTemplate().find(hql).get(0).toString());
	}
}
