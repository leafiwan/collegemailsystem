package com.jieshuhuiyou.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;

import com.jieshuhuiyou.dao.BorrowRequestDao;
import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.BorrowRequest;
import com.jieshuhuiyou.entity.Sharing;
import com.jieshuhuiyou.entity.User;

@Repository
public class BorrowRequestDaoImpl extends AbstractDao<BorrowRequest> implements BorrowRequestDao {

	@Override
	public BorrowRequest getById(int id) {
		return getHibernateTemplate().get(BorrowRequest.class, id);
	}
	
	@Override
	public List<BorrowRequest> getBySharing(final Sharing sharing) {
		@SuppressWarnings("unchecked")
		List<BorrowRequest> request = getHibernateTemplate().executeFind(new HibernateCallback<List<BorrowRequest>>() {

			@Override
			public List<BorrowRequest> doInHibernate(Session session)
					throws HibernateException, SQLException {
				String hql = "FROM BorrowRequest br WHERE br.sharing = :sharing";
				Query query = session.createQuery(hql);
				query.setParameter("sharing", sharing);
				return query.list();
			}
			
		});
		return request;
	}

	@Override
	public List<BorrowRequest> getByRequester(final User requester, final int start,
			final int count, final boolean desc) {
		
		@SuppressWarnings({"unchecked" })
		List<BorrowRequest> requests = getHibernateTemplate().executeFind(new HibernateCallback<List<BorrowRequest>>() {

			@Override
			public List<BorrowRequest> doInHibernate(Session session)
					throws HibernateException, SQLException {
				
				String hql = "FROM BorrowRequest br WHERE br.user = :requester";
				if(desc) {
					hql += " ORDER BY br.id DESC";
				}
				
				Query q = session.createQuery(hql).setParameter("requester", requester)
					.setFirstResult(start)
					.setMaxResults(count);
				
				
				return q.list();
			}	
			
		});
		
		return requests;
	}

	@Override
	public List<BorrowRequest> getByReciever(final User reciever, final int start,
			final int count, final boolean desc) {
		@SuppressWarnings({"unchecked" })
		List<BorrowRequest> requests = getHibernateTemplate().executeFind(new HibernateCallback<List<BorrowRequest>>() {

			@Override
			public List<BorrowRequest> doInHibernate(Session session)
					throws HibernateException, SQLException {
				
				String hql = "FROM BorrowRequest br WHERE br.sharing.user = :reciever";
				if(desc) {
					hql += " ORDER BY br.id DESC";
				}
				
				Query q = session.createQuery(hql).setParameter("reciever", reciever)
					.setFirstResult(start)
					.setMaxResults(count);
				
				
				return q.list();
			}	
			
		});
		
		return requests;
	}
	
	@Override
	public BorrowRequest getByRequesterAndSharing(User requester, Sharing sharing) {
		String hql = "FROM BorrowRequest br WHERE br.user = ? AND br.sharing = ?";
		@SuppressWarnings("unchecked")
		List<BorrowRequest> results = getHibernateTemplate().find(hql, requester, sharing);
		if(results.isEmpty()) {
			return null;
		}
		return results.get(0);
	}
	
	
	@Override
	public BorrowRequest getByRequesterAndBook(User requester, Book book) {
		String hql = "FROM BorrowRequest br WHERE br.user = ? AND br.sharing.book = ?";
		@SuppressWarnings("unchecked")
		List<BorrowRequest> results = getHibernateTemplate().find(hql, requester, book);
		if(results.isEmpty()) {
			return null;
		}
		return results.get(0);
	}
	
}
