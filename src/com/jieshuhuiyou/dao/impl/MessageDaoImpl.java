package com.jieshuhuiyou.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;

import com.jieshuhuiyou.dao.MessageDao;
import com.jieshuhuiyou.entity.Message;
import com.jieshuhuiyou.entity.User;

/**
 * 私信DAO
 * @author WanLinFeng
 *
 */
@Repository
public class MessageDaoImpl extends AbstractDao<Message> implements MessageDao {

	@Override
	public List<Message> getReceiveMessageListByUser(final User user , final int start,
			final int count, final boolean desc) {
		@SuppressWarnings("unchecked")
		List<Message> messageList = getHibernateTemplate().execute(new HibernateCallback<List<Message>>() {

			@Override
			public List<Message> doInHibernate(Session session)
					throws HibernateException, SQLException {
				String hql = "SELECT DISTINCT m FROM Message m WHERE m.receiveUser=:user ORDER BY m.date";
				if (desc) {
					hql += " DESC";
				}
				Query q = session.createQuery(hql)
						.setParameter("user", user)
						.setFirstResult(start).setMaxResults(count);
				List<Message> messageList = q.list();
				return messageList;
			}
		});
		return messageList;
	}

	@Override
	public List<Message> getSendMessageListByUser(final User user , final int start,
			final int count, final boolean desc) {
		@SuppressWarnings("unchecked")
		List<Message> messageList = getHibernateTemplate().execute(new HibernateCallback<List<Message>>() {

			@Override
			public List<Message> doInHibernate(Session session)
					throws HibernateException, SQLException {
				String hql = "SELECT DISTINCT m FROM Message m WHERE m.sendUser=:user ORDER BY m.date";
				if (desc) {
					hql += " DESC";
				}
				Query q = session.createQuery(hql)
						.setParameter("user", user)
						.setFirstResult(start).setMaxResults(count);
				List<Message> messageList = q.list();
				return messageList;
			}
		});
		return messageList;
	}

	@Override
	public Message getById(int id) {
		return getHibernateTemplate().get(Message.class, id);
	}

	@Override
	public int getReceiveMessageCount(User user) {
		String hql = "SELECT count(*) FROM Message m WHERE receiveUser=:receiveUser";
		Session session = getSession();
		Query q = session.createQuery(hql);
		q.setParameter("receiveUser", user);
		Integer totalCount = Integer.parseInt(q.list().iterator().next().toString());
		return totalCount;
	}

	@Override
	public int getSendMessageCount(User user) {
		String hql = "SELECT count(*) FROM Message m WHERE m.sendUser=:sendUser";
		Session session = getSession();
		Query q = session.createQuery(hql);
		q.setParameter("sendUser", user);
		Integer totalCount = Integer.parseInt(q.list().iterator().next().toString());
		return totalCount;
	}
	
}
