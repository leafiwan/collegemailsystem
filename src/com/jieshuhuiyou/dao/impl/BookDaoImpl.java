package com.jieshuhuiyou.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;

import com.jieshuhuiyou.dao.BookDao;
import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.User;

@Repository
public class BookDaoImpl extends AbstractDao<Book>
	implements BookDao {

	@Override
	public Book getByISBN10(String isbn10) {
		String hql = "FROM Book b WHERE b.isbn10 = ?";
		@SuppressWarnings("unchecked")
		List<Book> books = getHibernateTemplate().find(hql, isbn10);
		if(books != null && !books.isEmpty()) {
			return books.get(0);
		}
		return null;
	}

	@Override
	public Book getByISBN13(String isbn13) {
		String hql = "FROM Book b WHERE b.isbn13 = ?";
		@SuppressWarnings("unchecked")
		List<Book> books = getHibernateTemplate().find(hql, isbn13);
		if(books != null && !books.isEmpty()) {
			return books.get(0);
		}
		return null;
	}

	@Override
	public Book getById(int id) {
		return getHibernateTemplate().get(Book.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<Book> getBooks(final int start, final int count, final boolean desc) {
		List<Book> result = getHibernateTemplate().executeFind(new HibernateCallback<List<Book>>() {
			@Override
			public List<Book> doInHibernate(Session session) throws HibernateException,
					SQLException {
				String hql = "FROM Book b ";
				if(desc) {
					hql = hql + "ORDER BY b.id DESC";
				}
				Query query = session.createQuery(hql);
				query.setFirstResult(start);
				query.setMaxResults(count);
				return query.list();
			}
		});	
		return result;		
	}
	
	@SuppressWarnings("unchecked")
	public List<Book> getByProvider(final User provider, final int start, final int count, final boolean desc) {
		List<Book> result = getHibernateTemplate().executeFind(new HibernateCallback<List<Book>>() {
			@Override
			public List<Book> doInHibernate(Session session) throws HibernateException,
					SQLException {
				String hql = "SELECT b FROM Book b, Sharing s WHERE s.user = :provider AND s.book = b ";
				if(desc) {
					hql = hql + "ORDER BY b.id DESC";
				}
				Query query = session.createQuery(hql);
				query.setParameter("provider", provider);
				query.setFirstResult(start);
				query.setMaxResults(count);
				return query.list();
			}
		});	
		return result;
	}

	@Override
	public List<Book> getByBorrower(final User borrower,final  int start,final  int count,
			final boolean desc) {
		
		@SuppressWarnings("unchecked")
		List<Book> books = getHibernateTemplate().executeFind(new HibernateCallback<List<Book>>() {
			
			@Override
			public List<Book> doInHibernate(Session session)
					throws HibernateException, SQLException {
				
				String hql = "SELECT DISTINCT b, br.borrowDate FROM Book, Borrow br WHERE b = br.sharing.book AND br.user = :borrower ORDER BY br.borrowDate";
				if(desc) {
					hql += " DESC";
				}
				Query q = session.createQuery(hql).setParameter("borrower", borrower)
					.setFirstResult(start)
					.setMaxResults(count);
				Iterator<Object[]> i = q.list().iterator();
				List<Book> results = new ArrayList<Book>();
				while(i.hasNext()) {
					results.add((Book)i.next()[0]);
				}
				return results;
			}
		});
		return books;
	}

	/**
	 * 根据书名(简单模糊)搜索图书
	 * @param queryInfo
	 * @return
	 */
	public List<Book> getByQuery(final String queryInfo, final int start,
			final int count) {
		@SuppressWarnings("unchecked")
		List<Book> result = getHibernateTemplate().executeFind(
			new HibernateCallback<List<Book>>() {
				@Override
				public List<Book> doInHibernate(Session session) throws HibernateException, SQLException {
					String hql = "FROM Book b WHERE b.title LIKE '%" + queryInfo + "%'";
					Query query = session.createQuery(hql);
					query.setFirstResult(start);
					query.setMaxResults(count);
					return query.list();
				}
			});
		return result;
	}
	
	/**
	 * 后台查询
	 * @param isbnInfo
	 * @param nameInfo
	 * @param authorInfo
	 * @param publisherInfo
	 * @param start
	 * @param count
	 * @param desc
	 * @return
	 */
	@Override
	public List<Book> getByBGQuery(final String nameInfo,
			final String authorInfo, final String publisherInfo,
			final int start, final int count, final boolean desc) {
		@SuppressWarnings("unchecked")
		List<Book> result = getHibernateTemplate().executeFind(
			new HibernateCallback<List<Book>>() {
				@Override
				public List<Book> doInHibernate(Session session) throws HibernateException, SQLException {
					String hql = "SELECT DISTINCT b FROM Book b WHERE ";
					Query query;
					if(!"".equals(nameInfo)){
						hql = hql + "b.title LIKE ?";
						if(!"".equals(authorInfo)){
							hql = hql + " AND b.author=:authorInfo";
							if(!"".equals(publisherInfo)){// all information is exist
								hql = hql + " AND b.publisherInfo=:publisherInfo";
								query = session.createQuery(hql);
								query.setString(0, "%" + nameInfo + "%").setParameter("authorInfo", authorInfo).setParameter("publisherInfo", publisherInfo);
							} else {// nameInfo and authorInfo exist
								query = session.createQuery(hql);
								query.setString(0, "%" + nameInfo + "%").setParameter("authorInfo", authorInfo);								
							}
						} else {
							if(!"".equals(publisherInfo)){// nameInfo and publisherInfo exist
								hql = hql + " AND b.publisherInfo=:publisherInfo";
								query = session.createQuery(hql);
								query.setString(0, "%" + nameInfo + "%").setParameter("publisherInfo", publisherInfo);
							} else {// only has nameInfo
								query = session.createQuery(hql);
								query.setString(0, "%" + nameInfo + "%");								
							}
						}
					} else {
						if(!"".equals(authorInfo)){
							hql = hql + "b.author=:authorInfo";
							if(!"".equals(publisherInfo)){// authorInfo and publisherInfo exist
								hql = hql + " AND b.publisherInfo=:publisherInfo";
								query = session.createQuery(hql);
								query.setParameter("authorInfo", authorInfo).setParameter("publisherInfo", publisherInfo);
							} else {// authorInfo exist
								query = session.createQuery(hql);
								query.setParameter("authorInfo", authorInfo);								
							}
						} else {
							if(!"".equals(publisherInfo)){// only publisherInfo exist
								hql = hql + "b.publisherInfo=:publisherInfo";
								query = session.createQuery(hql);
								query.setParameter("publisherInfo", publisherInfo);
							} else {// there is none information
								return null;
							}
						}
					}
					if(desc){
						hql += " DESC";
					}
					query.setFirstResult(start);
					query.setMaxResults(count);
					return query.list();
				}
			});
		return result;
	}
	/**
	 * 用户搜索的结果总数
	 * @param queryInfo 搜索信息
	 * @return
	 */
	@Override
	public int getCountByQuery(String queryInfo) {
		String hql = "SELECT COUNT(*) FROM Book b WHERE b.title LIKE '%" + queryInfo + "%'";
		return Integer.parseInt(getHibernateTemplate().find(hql).get(0).toString());
	}

	@Override
	public int getBooksCount() {
		String hql = "SELECT COUNT(*) FROM Book b";
		return Integer.parseInt(getHibernateTemplate().find(hql).get(0).toString());
	}

	@Override
	public int getCountByBGQuery(String nameInfo, String authorInfo,
			String publisherInfo) {
		Session session = getSession();
		String hql = "SELECT count(*) FROM Book b WHERE ";
		Query query;
		if(!"".equals(nameInfo)){
			hql = hql + "b.title LIKE ?";
			if(!"".equals(authorInfo)){
				hql = hql + " AND b.author=:authorInfo";
				if(!"".equals(publisherInfo)){// all information is exist
					hql = hql + " AND b.publisherInfo=:publisherInfo";
					query = session.createQuery(hql);
					query.setString(0, "%" + nameInfo + "%").setParameter("authorInfo", authorInfo).setParameter("publisherInfo", publisherInfo);
				} else {// nameInfo and authorInfo exist
					query = session.createQuery(hql);
					query.setString(0, "%" + nameInfo + "%").setParameter("authorInfo", authorInfo);								
				}
			} else {
				if(!"".equals(publisherInfo)){// nameInfo and publisherInfo exist
					hql = hql + " AND b.publisherInfo=:publisherInfo";
					query = session.createQuery(hql);
					query.setString(0, "%" + nameInfo + "%").setParameter("publisherInfo", publisherInfo);
				} else {// only has nameInfo
					query = session.createQuery(hql);
					query.setString(0, "%" + nameInfo + "%");								
				}
			}
		} else {
			if(!"".equals(authorInfo)){
				hql = hql + "b.author=:authorInfo";
				if(!"".equals(publisherInfo)){// authorInfo and publisherInfo exist
					hql = hql + " AND b.publisherInfo=:publisherInfo";
					query = session.createQuery(hql);
					query.setParameter("authorInfo", authorInfo).setParameter("publisherInfo", publisherInfo);
				} else {// authorInfo exist
					query = session.createQuery(hql);
					query.setParameter("authorInfo", authorInfo);								
				}
			} else {
				if(!"".equals(publisherInfo)){// only publisherInfo exist
					hql = hql + "b.publisherInfo=:publisherInfo";
					query = session.createQuery(hql);
					query.setParameter("publisherInfo", publisherInfo);
				} else {// there is none information
					return 0;
				}
			}
		}
		Integer totalCount = Integer.parseInt(query.list().iterator().next().toString());
		return totalCount;
	}
	
}
