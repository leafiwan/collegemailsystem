package com.jieshuhuiyou.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.jieshuhuiyou.dao.ResetPasswordRecordDao;
import com.jieshuhuiyou.entity.ResetPasswordRecord;
import com.jieshuhuiyou.entity.User;

@Repository
public class ResetPasswordRecordDaoImpl extends AbstractDao<ResetPasswordRecord> implements
		ResetPasswordRecordDao {

	@Override
	public ResetPasswordRecord getByUser(User user) {
		String hql = "FROM ResetPasswordRecord rpr WHERE rpr.user = ? ORDER BY rpr.id DESC";
		@SuppressWarnings("unchecked")
		List<ResetPasswordRecord> rprs = getHibernateTemplate().find(hql, user);
		if(rprs.isEmpty()) {
			return null;
		}
		return rprs.get(0);
	}


	@Override
	public ResetPasswordRecord getByKey(String key) {
		String hql = "FROM ResetPasswordRecord rpr WHERE rpr.resetKey = ? ORDER BY rpr.id DESC";
		@SuppressWarnings("unchecked")
		List<ResetPasswordRecord> rprs = getHibernateTemplate().find(hql, key);
		if(rprs.isEmpty()) {
			return null;
		}
		return rprs.get(0);
	}
	
}
