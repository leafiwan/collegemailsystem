package com.jieshuhuiyou.dao.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;

import com.jieshuhuiyou.dao.UserDao;
import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.User;

/**
 * 用户访问实现类
 * 
 * @author psjay
 * 
 */
@Repository
public class UserDaoImpl extends AbstractDao<User> implements UserDao {

	@Override
	public User getById(int id) {
		return getHibernateTemplate().get(User.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public User getByEmail(String email) {
		String hql = "FROM User u WHERE u.email = ?";
		List<User> users = getHibernateTemplate().find(hql, email);
		if (users.isEmpty()) {
			return null;
		}
		return users.get(0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean isExists(User user) {
		String hql = "FROM User u WHERE u = ?";
		List<User> users = getHibernateTemplate().find(hql, user);
		if (users.isEmpty()) {
			return false;
		}
		return true;
	}

	public List<User> getProviders(final Book book, final int start,
			final int count, final boolean desc) {

		@SuppressWarnings("unchecked")
		List<User> users = getHibernateTemplate().executeFind(
				new HibernateCallback<List<User>>() {
					@Override
					public List<User> doInHibernate(Session session)
							throws HibernateException, SQLException {

						String hql = "SELECT DISTINCT u, s.shareDate FROM User u, Sharing s WHERE u = s.user AND s.book = :book AND s.canceled = false ORDER BY s.shareDate";
						if (desc) {
							hql += " DESC";
						}

						Query q = session.createQuery(hql)
								.setParameter("book", book)
								.setFirstResult(start).setMaxResults(count);
						Iterator<Object[]> results = q.list().iterator();

						List<User> users = new ArrayList<User>();

						while (results.hasNext()) {
							Object[] item = results.next();
							users.add((User) item[0]);
						}
						return users;
					}
				});
		return users;
	}

	public List<User> getBorrowers(final Book book, final int start,
			final int count, final boolean desc) {
		@SuppressWarnings("unchecked")
		List<User> users = getHibernateTemplate().executeFind(
				new HibernateCallback<List<User>>() {
					@Override
					public List<User> doInHibernate(Session session)
							throws HibernateException, SQLException {
						String hql = "SELECT DISTINCT u, b.borrowDate FROM User u, Borrow b WHERE b.user = u AND b.sharing.book = :book ORDER BY b.borrowDate";
						if (desc) {
							hql += " DESC";
						}
						Query q = session.createQuery(hql)
								.setParameter("book", book)
								.setFirstResult(start).setMaxResults(count);
						List<User> results = new ArrayList<User>();
						Iterator<Object[]> i = q.list().iterator();
						while (i.hasNext()) {
							results.add((User) i.next()[0]);
						}
						return results;
					}
				});
		return users;
	}

	public List<User> getUsers(final int start, final int count,
			final boolean desc) {

		@SuppressWarnings("unchecked")
		List<User> users = getHibernateTemplate().executeFind(
				new HibernateCallback<List<User>>() {
					@Override
					public List<User> doInHibernate(Session session)
							throws HibernateException, SQLException {

						String hql = "SELECT DISTINCT u FROM User u ORDER BY u.id";
						if (desc) {
							hql += " DESC";
						}

						Query q = session.createQuery(hql)
								.setFirstResult(start).setMaxResults(count);
						Iterator<Object> results = q.list().iterator();
						List<User> users = new ArrayList<User>();

						while (results.hasNext()) {
							Object item = results.next();
							users.add((User) item);
						}
						return users;
					}
				});
		return users;
	}

	@Override
	public List<User> searchUsers(final String name, final String school,
			final int start, final int count, final boolean desc) {

		@SuppressWarnings("unchecked")
		List<User> users = getHibernateTemplate().executeFind(
				new HibernateCallback<List<User>>() {
					@Override
					public List<User> doInHibernate(Session session)
							throws HibernateException, SQLException {
						List<User> users = new ArrayList<User>();

						String hql = "";
						Query q;
						if (!"".equals(name)) {
							if (!"".equals(school)) {
								hql = "SELECT DISTINCT u FROM User u WHERE u.name LIKE ? AND u.school.name=:school ORDER BY u.signUpDate";
								q = session.createQuery(hql).setString(0, "%" + name + "%").setParameter("school", school);
							} else {
								hql = "SELECT DISTINCT u FROM User u WHERE u.name LIKE ? ORDER BY u.signUpDate";
								q = session.createQuery(hql).setString(0, "%" + name + "%");
							}
						} else {
							if (!"".equals(school)) {
								hql = "SELECT DISTINCT u FROM User u WHERE u.school.name=:school ORDER BY u.signUpDate";
								q = session.createQuery(hql).setParameter("school", school);
							} else {
								return null;// None result
							}
						}

						if (desc) {
							hql += " DESC";
						}

						q.setFirstResult(start).setMaxResults(count);

						Iterator<Object> results = q.list().iterator();

						while (results.hasNext()) {
							Object item = results.next();
							users.add((User) item);
						}
						return users;
					}
				});
		return users;
	}
	
	@Override
	public int getUsersCount(){
		String hql = "SELECT count(*) FROM User u";
		Session session = getSession();
		Query q = session.createQuery(hql);
		Integer totalCount = Integer.parseInt(q.list().iterator().next().toString());
		return totalCount;
	}

	@Override
	public int getUsersCountByConditions(String name, String school) {
		String hql = "";
		Query q;
		Session session = getSession();
		if (!"".equals(name)) {
			if (!"".equals(school)) {
				hql = "SELECT DISTINCT count(*) FROM User u WHERE u.name LIKE ? AND u.school.name=:school";
				q = session.createQuery(hql).setString(0, "%" + name + "%").setParameter("school", school);
			} else {
				hql = "SELECT DISTINCT count(*) FROM User u WHERE u.name LIKE ?";
				q = session.createQuery(hql).setString(0, "%" + name + "%");
			}
		} else {
			if (!"".equals(school)) {
				hql = "SELECT DISTINCT count(*) FROM User u WHERE u.school.name=:school";
				q = session.createQuery(hql).setParameter("school", school);
			} else {
				return 0;// None result
			}
		}
		Integer totalCount = Integer.parseInt(q.list().iterator().next().toString());
		return totalCount;
	}
}
