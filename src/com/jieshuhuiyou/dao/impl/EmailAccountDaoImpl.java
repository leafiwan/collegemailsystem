package com.jieshuhuiyou.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;

import com.jieshuhuiyou.dao.EmailAccountDao;
import com.jieshuhuiyou.entity.EmailAccount;

@Repository
public class EmailAccountDaoImpl extends AbstractDao<EmailAccount> implements
		EmailAccountDao {

	@Override
	public EmailAccount getByAddress(String address) {
		String hql = "FROM EmailAccount ea where ea.email = ?";
		@SuppressWarnings("unchecked")
		List<EmailAccount> result = getHibernateTemplate().find(hql, address);
		if(!result.isEmpty()) {
			return result.get(0);
		}
		return null;
	}

	@Override
	public List<EmailAccount> getAccounts(final int start, final int count
			, final boolean desc) {
		
		@SuppressWarnings("unchecked")
		List<EmailAccount> result = getHibernateTemplate().executeFind(new HibernateCallback<List<EmailAccount>>() {

			@Override
			public List<EmailAccount> doInHibernate(Session session)
					throws HibernateException, SQLException {
				
				String hql = "FROM EmailAccount ea ORDER BY ea.createDate";
				if(desc) {
					hql +=" DESC";
				}
				
				Query query = session.createQuery(hql);
				query.setFirstResult(start);
				query.setMaxResults(count);
				
				return query.list();
			}
		
		});
		
		return result;
	}
	
	@Override
	public int getAccountsCount() {
		String hql = "SELECT COUNT(*) FROM EmailAccount ea";
		String countStr = getHibernateTemplate().find(hql).get(0).toString();
		return Integer.parseInt(countStr);
	}

}
