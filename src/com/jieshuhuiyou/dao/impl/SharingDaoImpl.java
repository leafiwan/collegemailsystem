package com.jieshuhuiyou.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;

import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.School;
import com.jieshuhuiyou.entity.Sharing;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.infrastructure.Page;

/**
 * 分享数据访问实现类
 * @author psjay
 *
 */
@Repository
public class SharingDaoImpl extends AbstractDao<Sharing> implements
		com.jieshuhuiyou.dao.SharingDao {
	
	public Page<Sharing> getByBook(final Book book,final int start,final int count,final boolean desc) {
		return getByBookAndSchool(book, null, start, count, desc);
	}

	@Override
	public List<Sharing> getByBookAndSchool(final Book book, final School school) {
		@SuppressWarnings("unchecked")
		List<Sharing> sharings = getHibernateTemplate().executeFind(new HibernateCallback<List<Sharing>>() {

			@Override
			public List<Sharing> doInHibernate(Session session)
					throws HibernateException, SQLException {
				String hql = "FROM Sharing s WHERE s.book = :book AND s.canceled = false";
				if(school != null) {
					hql += " AND s.user.school = :school";
				}
				Query query = session.createQuery(hql);
				query.setParameter("book", book);
				if(school != null) 
					query.setParameter("school", school);
				return query.list();
			}
			
		});
		return sharings;
	}
	
	@SuppressWarnings("unchecked")
	public Page<Sharing> getByBookAndSchool(final Book book, final School school
			, final int start, final int count, final boolean desc) {
		
		List<Sharing> sharings = getHibernateTemplate().executeFind(new HibernateCallback<List<Sharing>>() {

			@Override
			public List<Sharing> doInHibernate(Session session)
					throws HibernateException, SQLException {
				String hql = "FROM Sharing s WHERE s.book = :book AND s.canceled = false";
				if(school != null) {
					hql += " AND s.user.school = :school";
				}
				if(desc) {
					hql += " ORDER BY s.id DESC";
				}
				Query query = session.createQuery(hql);
				query.setParameter("book", book);
				if(school != null) 
					query.setParameter("school", school);
				query.setFirstResult(start);
				query.setMaxResults(count);
				return query.list();
			}
			
		});
		int totals = 0;
		List<?> result = null;
		String hql = "SELECT COUNT(*) FROM Sharing s WHERE s.book = ? AND s.canceled = false";
		if(school != null) {
			hql += " AND s.user.school = ?";
			result = getHibernateTemplate().find(hql, book, school);
		} else {
			result = getHibernateTemplate().find(hql, book);
		}
		
		if(!result.isEmpty()) {
			totals = Integer.parseInt(result.get(0).toString());
		}
		
		// create a page
		Page<Sharing> page = new Page<Sharing>(sharings, start, count, totals);
		return page;
	}
	
	public Sharing get(User user, Book book) {
		String hql = "FROM Sharing s WHERE s.user = ? AND s.book = ? AND s.canceled = false";
		@SuppressWarnings("unchecked")
		List<Sharing> rs = getHibernateTemplate().find(hql, user, book);
		if(rs.isEmpty()) {
			return null;
		}
		return rs.get(0);
	}
	
	public Sharing getByBookAndProvider(User provider, Book book) {
		String hql = "FROM Sharing s WHERE s.user = ? AND s.book = ? AND s.canceled = false ORDER BY s.id DESC";
		@SuppressWarnings("unchecked")
		List<Sharing> results = getHibernateTemplate().find(hql, provider, book);
		if(!results.isEmpty()) {
			return results.get(0);
		}
		return null;
	}
	
	
	public List<Sharing> getByUser(final User user, final int start, final int count, final boolean desc) {
		@SuppressWarnings("unchecked")
		List<Sharing> results = getHibernateTemplate().executeFind(new HibernateCallback<List<Sharing>>() {

			@Override
			public List<Sharing> doInHibernate(Session session)
					throws HibernateException, SQLException {
				String hql = "FROM Sharing s WHERE s.user = :user AND s.canceled = false ORDER BY s.id";
				if(desc) {
					hql += " DESC";
				}
				Query query = session.createQuery(hql);
				query.setParameter("user", user);
				query.setFirstResult(start);
				query.setMaxResults(count);
				return query.list();
			}
			
		});
		return results;
	}
	
	
	public Sharing getById(int id) {
		return getHibernateTemplate().get(Sharing.class, id);
	}

}
