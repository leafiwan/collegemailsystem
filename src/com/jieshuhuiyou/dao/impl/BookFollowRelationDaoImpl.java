package com.jieshuhuiyou.dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.stereotype.Repository;

import com.jieshuhuiyou.dao.BookFollowRelationDao;
import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.BookFollowRelation;
import com.jieshuhuiyou.entity.User;

@Repository
public class BookFollowRelationDaoImpl extends AbstractDao<BookFollowRelation> implements
		BookFollowRelationDao {

	@Override
	public boolean hasFollowed(User user, Book book) {
		BookFollowRelation relation = this.getByUserAndBook(user, book);
		if(relation != null) {
			return true;
		}
		return false;
	}

	@Override
	public BookFollowRelation getByUserAndBook(User user, Book book) {
		String hql = "FROM BookFollowRelation bfr WHERE bfr.user = ? AND bfr.book = ?";
		@SuppressWarnings("unchecked")
		List<BookFollowRelation> result = getHibernateTemplate().find(hql, user, book);
		if(!result.isEmpty()) {
			return result.get(0);
		}
		return null;
	}

	@Override
	public List<Integer> getFollowBookIdByUser(final User user) {
		final String hql = "SELECT b.book.id FROM BookFollowRelation b WHERE b.user.id = :currentUserId";
		List<Integer> bookidList = getHibernateTemplate().executeFind(new HibernateCallback<List<Integer>>() {

			@Override
			public List<Integer> doInHibernate(Session arg0)
					throws HibernateException, SQLException {
				Query query =  arg0.createQuery(hql);
				query.setParameter("currentUserId", user.getId());
				
				//要初始化里面为-1 否则会出错
				List<Integer> temp = query.list();
				temp.add(-1);	
				
				return temp;
			}
		});
		return bookidList;
	}
	
}
