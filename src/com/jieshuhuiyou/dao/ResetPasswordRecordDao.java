package com.jieshuhuiyou.dao;

import com.jieshuhuiyou.entity.ResetPasswordRecord;
import com.jieshuhuiyou.entity.User;

public interface ResetPasswordRecordDao extends Dao<ResetPasswordRecord>{

	/**
	 * 根据用户获取实例
	 * @param user
	 * @return
	 */
	public ResetPasswordRecord getByUser(User user);
	
	/**
	 * 根据 key 得到一条记录
	 * @param key
	 */
	public ResetPasswordRecord getByKey(String key);
	
}
