/**
 * 事件类数据层接口
 * @author Michael
 */

package com.jieshuhuiyou.dao;



import java.util.List;

import com.jieshuhuiyou.entity.Event;
import com.jieshuhuiyou.entity.User;

public interface EventDao extends Dao<Event>{
	
	/**
	 * 通过id查询事件
	 * @param id
	 * @return
	 */
	public Event getById(int id);
	
	/**
	 * 通过用户查询该用户应该收听到的事件
	 * @param user
	 * @return
	 */
	public List<Event> getFollowEventByUser(User user,int start,int end);

}
