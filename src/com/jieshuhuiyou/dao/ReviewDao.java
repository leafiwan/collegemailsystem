package com.jieshuhuiyou.dao;

import java.util.List;

import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.Review;
import com.jieshuhuiyou.entity.User;

/**
 * 书评 DAO
 * @author PSJay
 *
 */
public interface ReviewDao extends Dao<Review>{
	
	/**
	 * 根据 ID 取书评
	 * @param id
	 * @return
	 */
	public Review getById(long id);
	
	/**
	 * 根据作者取书评
	 * @param author
	 * @param start
	 * @param count
	 * @param desc
	 * @return
	 */
	public List<Review> getByAuthor(User author, int start, int count, boolean desc);
	
	/**
	 * 根据书籍取书评
	 * @param book
	 * @param start
	 * @param count
	 * @param desc
	 * @return
	 */
	public List<Review> getByBook(Book book, int start, int count, boolean desc);
	
}
