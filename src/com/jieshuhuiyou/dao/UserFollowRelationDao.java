/**
 * 用户关注映射类数据层接口
 * @author Michael
 */
package com.jieshuhuiyou.dao;

import java.util.ArrayList;
import java.util.List;

import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.entity.UserFollowRelation;

public interface UserFollowRelationDao extends Dao<UserFollowRelation>{

	/**
	 * 通过Id查询关注映射
	 * @param id
	 * @return
	 */
	public UserFollowRelationDao getRelationById(int id);
	
	/**
	 * 通过用户查询该用户所关注人的列表
	 * @param user
	 * @return
	 */
	public ArrayList<User> getFollowUserByUser(User user);
	
	/**
	 * 通过关注的用户和被关注的用户得到实体
	 * @param follow
	 * @param followed
	 * @return
	 */
	public UserFollowRelation getRelationByFandFedUser(User follow,User followed);
	
	/**
	 * 通过用户查询该用户所关注人的ID列表
	 */
	public List<Integer> getFollowUserIdByUser(User user);
}
