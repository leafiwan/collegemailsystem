package com.jieshuhuiyou.dao;

import java.util.List;

import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.School;
import com.jieshuhuiyou.entity.Sharing;
import com.jieshuhuiyou.entity.User;
import com.jieshuhuiyou.infrastructure.Page;

/**
 * 分享数据访问类
 * @author psjay
 *
 */
public interface SharingDao extends Dao<Sharing>{

	/**
	 * 取出一本书的分享
	 * @param book
	 * @param start
	 * @param count
	 * @param desc
	 * @return
	 */
	public Page<Sharing> getByBook(Book book, int start, int count, boolean desc);

	/**
	 * 取出一本书的分享
	 * @param book
	 * @param start
	 * @param count
	 * @param desc
	 * @return
	 */
	public List<Sharing> getByBookAndSchool(Book book,School school);
	
	/**
	 * 根据书籍和学校取出分享
	 * @param book
	 * @param school
	 * @param start
	 * @param count
	 * @param desc
	 * @return
	 */
	public Page<Sharing> getByBookAndSchool(Book book, School school, int start, int count, boolean desc);
	
	/**
	 * 得到一个分享
	 * @param id
	 * @return
	 */
	public Sharing getById(int id);
	
	
	/**
	 * 从数据库得到一个分享
	 * @param user
	 * @param book
	 */
	public Sharing get(User user, Book book);
	
	
	/**
	 * 根据分享者和书籍得到一个分享
	 * @param provider
	 * @param book
	 * @return
	 */
	public Sharing getByBookAndProvider(User provider, Book book);
	
	
	/**
	 * 找出 user 的分享
	 * @param user
	 * @param start
	 * @param count
	 * @return
	 */
	public List<Sharing> getByUser(User user, int start, int count, boolean desc);
	
}
