package com.jieshuhuiyou.dao;

import java.util.List;

import com.jieshuhuiyou.entity.EmailAccount;

/**
 * Email Account Data Access interface
 * @author psjay
 *
 */
public interface EmailAccountDao extends Dao<EmailAccount> {

	/**
	 * Get by email address
	 * @param address
	 * @return
	 */
	public EmailAccount getByAddress(String address);
	
	/**
	 * get emails accounts
	 * @param start
	 * @param count
	 * @param desc it true, sort by create time DESC
	 * @return
	 */
	public List<EmailAccount> getAccounts(int start, int count, boolean desc);
	
	/**
	 * get the count of all accounts
	 * @return
	 */
	public int getAccountsCount();
	
}
