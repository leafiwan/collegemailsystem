package com.jieshuhuiyou.dao;

import java.util.List;

import com.jieshuhuiyou.entity.Comment;
import com.jieshuhuiyou.entity.Review;
import com.jieshuhuiyou.entity.comment.ReviewComment;

/**
 * 评论数据访问接口
 * @author psjay
 *
 */
public interface CommentDao extends Dao<Comment>{

	/**
	 * 根据 id 取出评论
	 * @param id 评论的 id
	 * @return 相应的评论
	 */
	public Comment getById(long id);
	
	/**
	 * 根据书评查找评论
	 * @param review 书评
	 * @param start 第一个符合条件的评论索引
	 * @param count 取书评的最大数量
	 * @param desc 是否按 id 倒序排列
	 * @return
	 */
	public List<ReviewComment> getByReview(Review review, int start, int count, boolean desc);
	
	
}
