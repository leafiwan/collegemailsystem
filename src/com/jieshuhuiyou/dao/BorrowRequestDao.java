package com.jieshuhuiyou.dao;

import java.util.List;

import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.BorrowRequest;
import com.jieshuhuiyou.entity.Sharing;
import com.jieshuhuiyou.entity.User;


public interface BorrowRequestDao extends Dao<BorrowRequest>{

	/**
	 * 从数据库中按 id 取出请求
	 * @param id
	 * @return
	 */
	public BorrowRequest getById(int id);

	/**
	 * 根据分享取出请求
	 * @param sharing
	 */
	public List<BorrowRequest> getBySharing(Sharing sharing);
	
	
	/**
	 * 根据请求者取出借阅请求
	 * @param requester 请求者
	 * @param start 第一个请求的索引，用于分页
	 * @param count 取出的数量，用于分页
	 * @param desc 是否倒序
	 * @return 符合条件的借阅，如果没有，则返回一个空 List
	 */
	public List<BorrowRequest> getByRequester(User requester, int start, int count, boolean desc);
	
	/**
	 * 根据请求接收者取出借阅请求
	 * @param reciever 请求接收者
	 * @param start 第一个请求的序列，用于分页
	 * @param count 取出的数量，用于分页
	 * @param desc 是否倒序
	 * @return 符合条件的借阅，如果没有，则返回一个空 List
	 */
	public List<BorrowRequest> getByReciever(User reciever, int start, int count, boolean desc);
	
	
	/**
	 * 取出请求者对某个共享的借阅请求
	 * @param requester 请求者
	 * @param sharing 共享
	 * @return 符合条件的借阅请求，不然返回 null
	 */
	public BorrowRequest getByRequesterAndSharing(User requester, Sharing sharing);
	

	/**
	 * 取出请求者对某本书籍的借阅请求
	 * @param requester 请求者
	 * @param book 书籍
	 * @return 符合条件的请求，不然返回 null
	 */
	public BorrowRequest getByRequesterAndBook(User requester, Book book);
	
}
