package com.jieshuhuiyou.dao;

import com.jieshuhuiyou.entity.AutoLoginRecord;

public interface AutoLoginRecordDao extends Dao<AutoLoginRecord> {

	/**
	 * 根据用户 id 取出
	 * @param userId
	 * @return
	 */
	public AutoLoginRecord getByUserId(int userId);
	
	
}
