package com.jieshuhuiyou.dao;

import java.util.List;

import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.User;

/**
 * 书籍数据访问接口
 * @author psjay
 *
 */
public interface BookDao extends Dao<Book> {

	/**
	 * 根据 10 位 ISBN 查询书籍
	 * @param isbn10
	 * @return
	 */
	public Book getByISBN10(String isbn10);
	
	/**
	 * 根据 13 位 ISBN 查询书籍
	 * @param isbn13
	 * @return
	 */
	public Book getByISBN13(String isbn13);
	
	/**
	 * 根据 id 查询书籍
	 * @param id
	 * @return
	 */
	public Book getById(int id);
	
	/**
	 * get books from database
	 * @param start
	 * @param count
	 * @param desc
	 * @return
	 */
	public List<Book> getBooks(int start, int count, boolean desc);
	
	/**
	 * get count of books
	 * @return
	 */
	public int getBooksCount();
	
	/**
	 * get books by provider
	 * @param provider who provider the book
	 * @param start
	 * @param count
	 * @param desc
	 * @return
	 */
	public List<Book> getByProvider(User provider, int start, int count, boolean desc);
	
	/**
	 * 取出借阅的书
	 * @param borrower 借阅者
	 * @param start 
	 * @param count
	 * @param desc
	 * @return
	 */
	public List<Book> getByBorrower(User borrower, int start, int count, boolean desc);

	/**
	 * 根据用户搜索查找书籍
	 * @param queryInfo 搜索信息
	 * @param start
	 * @param count
	 * @return
	 */
	public List<Book> getByQuery(String queryInfo, int start, int count);
	
	/**
	 * 用户搜索的结果总数
	 * @param queryInfo 搜索信息
	 * @return
	 */
	public int getCountByQuery(String queryInfo);
	
	/**
	 * 后台用户书籍查询
	 * @param isbnInfo
	 * @param nameInfo
	 * @param authorInfo
	 * @param publisherInfo
	 * @param start
	 * @param count
	 * @param desc
	 * @return
	 */
	public List<Book> getByBGQuery(String nameInfo, String authorInfo, String publisherInfo,
			int start, int count, boolean desc);
	
	/**
	 * 得到符合条件的后台查询的book个数
	 * @param nameInfo
	 * @param authorInfo
	 * @param publisherInfo
	 * @return
	 */
	public int getCountByBGQuery(String nameInfo, String authorInfo, String publisherInfo);
}
