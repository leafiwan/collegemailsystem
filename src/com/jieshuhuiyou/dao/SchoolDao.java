package com.jieshuhuiyou.dao;

import java.util.List;

import com.jieshuhuiyou.entity.School;

public interface SchoolDao extends Dao<School>{
	
	/**
	 * 根据id得到学校对象
	 * @param id
	 * @return
	 */
	public School getById(int id);
	
	/**
	 * 根据学校名字得到学校对象
	 * @return
	 */
	public School getByName(String name);

	/**
	 * 得到数据库中的所有学校
	 * @return
	 */
	public List<School> getAllSchools();
	
	/**
	 * 得到数据库中的所有学校
	 * @return
	 */
	public List<School> getAllSchools(int start , int count , boolean desc);
	
	/**
	 * 得到所有学校的数量
	 * @return
	 */
	public int getSchoolsCount();
}
