package com.jieshuhuiyou.dao;

import java.util.List;

import com.jieshuhuiyou.entity.Book;
import com.jieshuhuiyou.entity.Borrow;
import com.jieshuhuiyou.entity.User;

public interface BorrowDao extends Dao<Borrow> {
	
	/**
	 * 根据 id 获取借阅
	 * @param id
	 * @return
	 */
	public Borrow getById(int id);

	/**
	 * 根据借阅者找出借阅
	 * @param borrower
	 * @param start
	 * @param count
	 * @param desc
	 * @return
	 */
	public List<Borrow> getByBorrows(User borrower, int start, int count, boolean desc);
	
	
	/**
	 * 根据共享者找出借阅
	 * @param provider
	 * @param start
	 * @param count
	 * @param desc
	 * @return
	 */
	public List<Borrow> getByProvider(User provider, int start, int count, boolean desc);
	
	
	/**
	 * 根据书籍找出借阅记录
	 * @param book
	 * @param start
	 * @param count
	 * @param desc
	 * @return
	 */
	public List<Borrow> getByBook(Book book, int start, int count, boolean desc);
	
	/**
	 * 根据借阅者和书籍找出借阅记录
	 * @param borrower
	 * @param book
	 * @param start
	 * @param count
	 * @param desc
	 * @return
	 */
	public List<Borrow> getByBorrowerAndBook(User borrower, Book book, int start
			, int count, boolean desc);
	
	
}
