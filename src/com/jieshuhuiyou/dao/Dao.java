package com.jieshuhuiyou.dao;

/**
 * Data access object interface
 * declare few basic data access methods
 * @author psjay
 * 
 * @param <T> entity class
 */
public interface Dao<T> {
	
	public void save(T entity);
	
	public void delete(T entity);
	
	public void update(T entity);
	
	public void saveOrUpdate(T entity);
	
}
