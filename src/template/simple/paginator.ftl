<#if parameters.page != 'NULL' && parameters.page.pagesCount gt 1 >
	<div class="paginator">
		<ul>
			<#if parameters.page.currentPage gt 1>
				<li class="first"><a href="${parameters.url + '0'}">最前</a></li>
				<li class="pre"><a href="${parameters.url + (parameters.page.currentPage - 2) * parameters.page.pageSize}">&lt;&lt;前</a></li>
			<#else>
				<li class="pre">&lt;&lt;前</li>
			</#if>
			<#if parameters.page.pagesCount lte parameters.linksCount >
				<#list 1..parameters.page.pagesCount as i>
					<#if i == parameters.page.currentPage>
						<li class="current">${i}</li>
					<#else>
						<li><a href="${parameters.url + (i-1)*parameters.page.pageSize}">${i}</a></li>
					</#if>
				</#list>
			<#else>
				<#if parameters.page.currentPage - (parameters.linksCount / 2) lte 0>
					<#list 1..parameters.linksCount as i>
						<#if i == parameters.page.currentPage>
							<li class="current">${i}</li>
						<#else>
							<li><a href="${parameters.url + (i-1)*parameters.page.pageSize}">${i}</a></li>
						</#if>
					</#list>
				<#elseif parameters.page.currentPage + (parameters.linksCount / 2) gte parameters.page.pagesCount>
					<#list parameters.page.pagesCount - parameters.linksCount+1..parameters.page.pagesCount as i>
						<#if i == parameters.page.currentPage>
							<li class="current">${i}</li>
						<#else>
							<li><a href="${parameters.url + (i-1)*parameters.page.pageSize}">${i}</a></li>
						</#if>
					</#list>	
				<#else>
					<#list parameters.page.currentPage-(parameters.linksCount/2)+1..parameters.page.currentPage-1 as i>
						<li><a href="${parameters.url + (i-1)*parameters.page.pageSize}">${i}</a></li>
					</#list>
					<li class="current">${parameters.page.currentPage}</li>
					<#list parameters.page.currentPage+1..parameters.page.currentPage+(parameters.linksCount/2) as i>
						<li><a href="${parameters.url + (i-1)*parameters.page.pageSize}">${i}</a></li>
					</#list>
				</#if>
			</#if>
			<#if parameters.page.currentPage lt parameters.page.pagesCount>
				<li class="nxt"><a href="${parameters.url + (parameters.page.currentPage)*parameters.page.pageSize}">后&gt;&gt;</a></li>
				<li class="last"><a href="${parameters.url + (parameters.page.pagesCount-1)*parameters.page.pageSize}">最后</a></li>
			<#else>
				<li class="nxt">后&gt;&gt;</li>
			</#if>	
		</ul>
	</div>
</#if>
