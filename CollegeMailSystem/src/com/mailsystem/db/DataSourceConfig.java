package com.mailsystem.db;

public class DataSourceConfig {

	private String type = "";			//数据库类型
	private String name = "";			//连接池名
	private String driver = "";			//数据库驱动
	private String url = "";			//数据库url
	private String username = "";		//用户名
	private String password = "";		//密码
	private int maxConn = 0;			//最大连接数
	
	// constructor
	public DataSourceConfig(){
		
	}

	public DataSourceConfig(String type, String name, String driver,
			String url, String username, String password, int maxConn) {
		this.type = type;
		this.name = name;
		this.driver = driver;
		this.url = url;
		this.username = username;
		this.password = password;
		this.maxConn = maxConn;
	}

	// getters and setters...
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getMaxConn() {
		return maxConn;
	}

	public void setMaxConn(int maxConn) {
		this.maxConn = maxConn;
	}
	
}
