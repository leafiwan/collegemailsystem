package com.mailsystem.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Iterator;

public class DBConnectionPool {

	private Connection conn = null;
	private int insUsed = 0; // 正在使用的连接数
	private ArrayList<Connection> freeConnections = new ArrayList<Connection>(); // 空闲连接数容器
	private int minConn; // 最小连接数
	private int maxConn; // 最大连接数
	private String name; // 连接池名字
	private String username; // 用户名
	private String password; // 密码
	private String url; // 连接地址
	private String driver; // 驱动

	// constructor
	public DBConnectionPool(String name, String driver, String url,
			String username, String password, int maxConn) {
		this.name = name;
		this.driver = driver;
		this.url = url;
		this.username = username;
		this.password = password;
		this.maxConn = maxConn;
	}
	
	/**
	 * 用完释放连接
	 * @param conn
	 */
	public synchronized void freeConnection(Connection conn){
		this.freeConnections.add(conn);
		this.insUsed--;
	}
	
	/**
	 * 根据timeout获得连接
	 * @param timeout
	 * @return
	 */
	public synchronized Connection getConnection(long timeout){
		Connection conn = null;
		if(this.freeConnections.size() > 0){
			//有空闲连接
			conn = this.freeConnections.get(0);
			if(conn == null){
				conn = getConnection(timeout);
			}
		} else {
			conn = newConnection();//新建连接
		}
		if(this.maxConn == 0 || this.maxConn < this.insUsed){
			//达到最大连接数，或者暂时不能获得连接
			conn = null;
		}
		if(conn != null){
			this.insUsed++;
		}
		return conn;
	}
	
	/**
	 * 得到一个连接
	 * @return
	 */
	public synchronized Connection getConnection(){
		Connection conn = null;
		if(this.freeConnections.size() > 0){
			conn = this.freeConnections.remove(0);
			if(conn == null){//继续获得连接
				conn = getConnection();
			}
		} else {
			conn = newConnection();
		}
		if(this.maxConn == 0 || this.maxConn < this.insUsed){
			//达到最大连接数，或者暂时不能获得连接
			conn = null;
		}
		if(conn != null){
			this.insUsed++;
		}
		return conn;
	}
	
	/**
	 * 释放所有连接
	 */
	public synchronized void release(){
		Iterator<Connection> allConns = this.freeConnections.iterator();
		while(allConns.hasNext()){
			Connection conn = allConns.next();
			try{
				conn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		this.freeConnections.clear();
	}
	
	/**
	 * 创建新俩接
	 * @return
	 */
	private Connection newConnection(){
		Connection conn = null;
		try {
			Class.forName(driver);
			conn = DriverManager.getConnection(url , username , password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}
	
	// getters and setters...
	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}

	public int getInsUsed() {
		return insUsed;
	}

	public void setInsUsed(int insUsed) {
		this.insUsed = insUsed;
	}

	public ArrayList<Connection> getFreeConnections() {
		return freeConnections;
	}

	public void setFreeConnections(ArrayList<Connection> freeConnections) {
		this.freeConnections = freeConnections;
	}

	public int getMinConn() {
		return minConn;
	}

	public void setMinConn(int minConn) {
		this.minConn = minConn;
	}

	public int getMaxConn() {
		return maxConn;
	}

	public void setMaxConn(int maxConn) {
		this.maxConn = maxConn;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}
}
