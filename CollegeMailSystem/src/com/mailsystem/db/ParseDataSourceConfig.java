package com.mailsystem.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

public class ParseDataSourceConfig {

	// constructor
	public ParseDataSourceConfig(){
		
	}
	
	/**
	 * 读取xml配置文件
	 * @param path
	 * @return
	 */
	public Vector<DataSourceConfig> readConfigInfo(String path){
		//得到路径
		String rPath = this.getClass().getResource("").getPath().substring(1)+path;
		Vector<DataSourceConfig> dsConfig = null;
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(rPath);
			dsConfig = new Vector<DataSourceConfig>();
			SAXBuilder builder = new SAXBuilder();
			Document doc = builder.build(fis);
			Element root = doc.getRootElement();
			@SuppressWarnings("unchecked")
			List<Element> pools = root.getChildren();
			Element pool = null;
			Iterator<Element> allPool = pools.iterator();
			while(allPool.hasNext()){
				pool = allPool.next();
				DataSourceConfig dsc = new DataSourceConfig();
				dsc.setType(pool.getChildText("type"));
				dsc.setName(pool.getChildText("name"));
				dsc.setDriver(pool.getChildText("driver"));
				dsc.setUrl(pool.getChildText("url"));
				dsc.setUsername(pool.getChildText("username"));
				dsc.setPassword(pool.getChildText("password"));
				dsc.setMaxConn(Integer.parseInt(pool.getChildText("maxconn")));
				dsConfig.add(dsc);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return dsConfig;
	}
}
