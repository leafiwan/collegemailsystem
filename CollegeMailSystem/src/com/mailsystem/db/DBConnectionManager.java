package com.mailsystem.db;

import java.sql.Connection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

import com.sun.org.apache.xalan.internal.xsltc.runtime.Hashtable;

public class DBConnectionManager {
	private static DBConnectionManager instance; // 单例模式
//	private static int clients; // 客户端连接数
	private Vector<DataSourceConfig> drivers = new Vector<DataSourceConfig>(); // 驱动信息
	private Hashtable pools = new Hashtable(); // 连接池

	private DBConnectionManager() {
		this.init();
	}

	/**
	 * 得到当前单例
	 * 
	 * @return
	 */
	public static synchronized DBConnectionManager getInstance() {
		if (instance == null) {
			instance = new DBConnectionManager();
		}
		return instance;
	}

	/**
	 * 释放连接
	 * 
	 * @param name
	 * @param conn
	 */
	public void freeConnection(String name, Connection conn) {
		// 根据关键名得到连接池
		DBConnectionPool pool = (DBConnectionPool) pools.get(name);
		if (pool != null) {
			pool.freeConnection(conn);
		}
	}

	/**
	 * 根据连接池的名字得到一个连接
	 * 
	 * @param name
	 * @return
	 */
	public Connection getConnection(String name) {
		DBConnectionPool pool = null;
		Connection conn = null;
		// 从名字中获取连接池
		pool = (DBConnectionPool) pools.get(name);
		// 从选定的连接池中获得连接
		conn = pool.getConnection();
		return conn;
	}

	/**
	 * 根据连接池的名字得到一个连接
	 * 
	 * @param name
	 * @param timeout
	 * @return
	 */
	public Connection getConnection(String name, long timeout) {
		DBConnectionPool pool = null;
		Connection conn = null;
		// 从名字中获取连接池
		pool = (DBConnectionPool) pools.get(name);
		// 从选定的连接池中获得连接
		conn = pool.getConnection(timeout);
		return conn;
	}

	/**
	 * 释放连接
	 */
	public synchronized void release() {
		@SuppressWarnings("unchecked")
		Enumeration<DBConnectionPool> allPools = pools.elements();
		while (allPools.hasMoreElements()) {
			DBConnectionPool pool = (DBConnectionPool) allPools.nextElement();
			if (pool != null) {
				pool.release();
			}
		}
	}

	/**
	 * 创建连接池
	 * 
	 * @param dsb
	 */
	private void createPools(DataSourceConfig dsc) {
		DBConnectionPool dbPool = new DBConnectionPool(dsc.getName(),
				dsc.getDriver(), dsc.getUrl(), dsc.getUsername(),
				dsc.getPassword(), dsc.getMaxConn());
		pools.put(dsc.getName(), dbPool);
	}

	/**
	 * 初始化创建连接池
	 */
	private void init() {
		// 加载驱动
		this.loadDrivers();
		Iterator<DataSourceConfig> allDriver = drivers.iterator();
		while (allDriver.hasNext()) {
			this.createPools(allDriver.next());
		}
	}

	private void loadDrivers() {
		ParseDataSourceConfig pd = new ParseDataSourceConfig();
		// 读取数据库配置文件
		drivers = pd.readConfigInfo("db-config.xml");
	}
}
