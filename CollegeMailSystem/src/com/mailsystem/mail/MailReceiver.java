package com.mailsystem.mail;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMultipart;

import com.mailsystem.bean.Mail;

public class MailReceiver {

	private String receiverName = "";
	private String receiverPassword = "";

	public MailReceiver(String receiverName, String receiverPassword) {
		this.receiverName = receiverName;
		this.receiverPassword = receiverPassword;
	}

	public List<Mail> receive() {
		List<Mail> mailList = new ArrayList<Mail>();
		// 获取会话
		Session session = Session.getDefaultInstance(MailConfig.PROPS, null);
		// 获取Store对象，使用POP3协议，也可能使用IMAP协议
		try {
			Store store = session.getStore("pop3");
			// 连接到邮件服务器
			store.connect(MailConfig.SMTP_HOST, receiverName, receiverPassword);
			// 获取该用户Folder对象，并以只读方式打开
			Folder folder = store.getFolder("inbox");
			folder.open(Folder.READ_WRITE);
			// 检索所有邮件，按需填充
			Message message[] = folder.getMessages();
			for (int i = 0; i < message.length; i++) {
				// 打印出每个邮件的发件人和主题
				Mail mail = new Mail();
				mail.setSubject(message[i].getSubject());
				MimeMultipart mmp = (MimeMultipart) message[i].getContent();
				// 正文
				String content = "";
				// 附件
				String fileAttachment = "";
				int fileCount = 0;
				for (int j = 0; j < mmp.getCount(); j++) {
					BodyPart mbp = mmp.getBodyPart(j);
					if (mbp.isMimeType("text/plain")) {
						// 超文本
						content += mbp.getContent().toString();
					} else {
						// 附件
						String fileName = mbp.getFileName();
						InputStream in = mbp.getInputStream();
						BufferedInputStream bin = new BufferedInputStream(in);
						byte[] cont = new byte[bin.available()];
						bin.read(cont);
						BufferedOutputStream writer = new BufferedOutputStream(
								new FileOutputStream(MailConfig.DOWNLOAD_PATH + fileName));
						writer.write(cont);
						writer.flush();
						writer.close();
						fileCount++;
						if(fileCount > 1){
							fileAttachment += "|";
						}
						fileAttachment += fileName;
					}
				}
				mail.setContent(content);
				mail.setFileAttachment(fileAttachment);
				mail.setFileCount(fileCount);
				mail.setSenderAddress(message[i].getFrom()[0].toString());
				mail.setReceiverAddress(message[i].getAllRecipients()[0]
						.toString());
				mail.setSendDate(message[i].getSentDate());
				mailList.add(mail);
				// 读完以后在邮件服务器删除邮件
				message[i].setFlag(Flags.Flag.DELETED, true);
//				message[i].saveChanges();
			}
			folder.close(true);
			store.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mailList;
	}
}
