package com.mailsystem.mail;

import java.util.StringTokenizer;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.URLName;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.mailsystem.bean.Mail;

public class MailSender {

	private String senderName = "";
	private String senderPassword = "";

	public MailSender(String senderName, String senderPassword) {
		this.senderName = senderName;
		this.senderPassword = senderPassword;
	}

	public boolean send(Mail mail) {
		// 配置邮件接收地址
		InternetAddress[] receiveAddress = new InternetAddress[1];
		try {
			receiveAddress[0] = new InternetAddress(mail.getReceiverAddress());
		} catch (AddressException e) {
			e.printStackTrace();
		}
		// smtp认证，获取Session
		SmtpAuth sa = new SmtpAuth();
		sa.setUserinfo(senderName, senderPassword);
		Session session = Session.getInstance(MailConfig.PROPS, sa);
		session.setPasswordAuthentication(new URLName(MailConfig.SMTP_HOST),
				sa.getPasswordAuthentication());
		// 构建邮件体
		MimeMessage sendMess = new MimeMessage(session);
		MimeBodyPart mbp = new MimeBodyPart();
		MimeMultipart mmp = new MimeMultipart();
		try {
			// 邮件文本内容
			mbp.setContent(mail.getContent(), "text/plain; charset=utf-8");
			mmp.addBodyPart(mbp);
			// 附件处理
			String fileAttachment = mail.getFileAttachment();
			if (fileAttachment != null && fileAttachment != "") {
				StringTokenizer token = new StringTokenizer(fileAttachment, "|");
				while (token.hasMoreTokens()) {
					String fileName = token.nextToken();
					DataSource source = new FileDataSource(
							MailConfig.UPLOAD_PATH + fileName);
					String name = source.getName();
					mbp = new MimeBodyPart();
					mbp.setDataHandler(new DataHandler(source));
					mbp.setFileName(name);
					mmp.addBodyPart(mbp);
				}
			}
			// 邮件整体
			sendMess.setSubject(mail.getSubject());
			sendMess.setContent(mmp);
			// 发送邮件
			sendMess.setFrom(new InternetAddress(senderName));
			sendMess.setRecipients(Message.RecipientType.TO, receiveAddress);
			Transport.send(sendMess);
			System.out.println("发送成功");
			return true;
		} catch (MessagingException ex) {
			ex.printStackTrace();
		}
		return false;
	}
}
