package com.mailsystem.mail;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.mailsystem.bean.DBUser;
import com.mailsystem.db.DBConnectionManager;
import com.mailsystem.util.DigestUtil;

public class MailUser {

	private static String connName = "mailuser";

	public static boolean addUserByDB(DBUser user) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = DBConnectionManager.getInstance().getConnection(connName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		String sql = "";
		try {
			String pwd = DigestUtil.digestString(user.getPassword(),
					user.getPwdAlgorithm());
			sql = "INSERT INTO users VALUES(?,?,?,0,NULL,0,NULL)";
			ps = conn.prepareStatement(sql);
			ps.setString(1, user.getUsername());
			ps.setString(2, pwd);
			ps.setString(3, user.getPwdAlgorithm());
			ps.execute();
			ps.close();
			DBConnectionManager.getInstance().freeConnection(connName, conn);
			System.out.println("用户添加成功");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static void main(String[] args) {
		// 连接数据库
		String driverName = "com.mysql.jdbc.Driver";
		String dbURL = "jdbc:mysql://localhost/mailsystem";
		String userName = "root";
		String userPwd = "8238715";
		Connection conn = null;
		try {
			Class.forName(driverName);
			conn = DriverManager.getConnection(dbURL, userName, userPwd);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// 构造并执行SQL语句，关键在于DigestUtil.digestString("881213", "SHA")，实现对密码的SHA加密
		// 注：用户信息的后四个属性需要使用（'SHA',0,NULL,0,null）此四个默认值，若用错，则新建用户可能不能使用
		String sql = "";
		try {
			sql = "insert into users values('hello','"
					+ DigestUtil.digestString("hello", "SHA")
					+ "','SHA',0,NULL,0,null)";
			conn.createStatement().executeUpdate(sql);
			conn.close();
			System.out.println("用户添加成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
