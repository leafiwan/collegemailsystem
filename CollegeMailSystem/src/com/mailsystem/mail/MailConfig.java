package com.mailsystem.mail;

import java.util.Properties;

public class MailConfig {

	public static String SMTP_HOST = "localhost";
	public static String UPLOAD_PATH = "D:\\Tomcat\\lib\\apache-tomcat-6.0.35\\webapps\\CollegeMailSystem\\upload\\";
	public static String DOWNLOAD_PATH = "D:\\Tomcat\\lib\\apache-tomcat-6.0.35\\webapps\\CollegeMailSystem\\download\\";

	public static int SMTP_PORT = 25;
	// 配置服务器属性
	public static Properties PROPS = new Properties();
	
	static {
		PROPS.put("mail.smtp.host", "localhost"); // smtp服务器
		PROPS.put("mail.smtp.auth", "true"); // 是否smtp认证
		PROPS.put("mail.smtp.port", "25"); // 设置smtp端口
		PROPS.put("mail.transport.protocol", "smtp"); // 发邮件协议
		PROPS.put("mail.store.protocol", "pop3"); // 收邮件协议
	}

}
