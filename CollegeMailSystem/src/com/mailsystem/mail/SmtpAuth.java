package com.mailsystem.mail;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 *  STMP协议身份验证类
 * @author WanLinFeng
 *
 */
public class SmtpAuth extends Authenticator{
	
    private String user, password;  
    
    /** 
     *  
     * @param getuser 
     * @param getpassword 
     */  
    public void setUserinfo(String getuser, String getpassword) {  
        user = getuser;  
        password = getpassword;  
    }  
    protected PasswordAuthentication getPasswordAuthentication() {  
        return new PasswordAuthentication(user, password);  
    }  
    public String getPassword() {  
        return password;  
    }  
    public void setPassword(String password) {  
        this.password = password;  
    }  
    public String getUser() {  
        return user;  
    }  
    public void setUser(String user) {  
        this.user = user;  
    }  
}
