package com.mailsystem.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DBUtil {
	
	public static Connection conn = null;
	
	private static String url = "jdbc:sqlserver://localhost:1433;DatabaseName=CollegeMailSystem";
	private static String username = "sa";
	private static String password = "8238715";
	private static String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
	
	static{
		try {
			Class.forName(driver);
			conn = DriverManager.getConnection(url , username , password);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	//close
	public static void close( Connection conn, PreparedStatement ps , ResultSet rs ) throws Exception {
		if( rs != null ){
			rs.close();
			rs = null;
		}
		if( ps != null ){
			ps.close();
			ps = null;
		}
		if( conn != null ){
			conn.close();
			conn = null;
		}
	}
	
//
//	private static DataSource source;
//	
//	public static DataSource getDataSource() throws Exception{
//
//		Context context = new InitialContext();
//		source = (DataSource)context.lookup("java:comp/env/jndi/sqlserver");
//		return source;
//	}
//	
//	static{
//		//init
//		try {
//			source = getDataSource();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
//	
//	public static Connection getConnection() throws Exception{
//		return source.getConnection();
//	}
	
}
