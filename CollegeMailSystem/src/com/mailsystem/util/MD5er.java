package com.mailsystem.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 密码MD5加密累
 * @author WanLinFeng
 *
 */
public class MD5er {

	public static String md5(String str) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] encode = md.digest(str.getBytes());

			StringBuffer buffer = new StringBuffer();
			for (int i = 0; i < encode.length; i++) {
				buffer.append(String.format("%02x", encode[i]));
			}
			return buffer.toString();
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

}
