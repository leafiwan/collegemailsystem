package com.mailsystem.bean;

/**
 * Mail Box Entity
 * 
 * @author WanLinFeng
 * 
 */
public class MailBox {

	private int id;
	private String username;// the name which login in
	private String address;
	private String password;
	private String name; // the name of mailbox's owner
	private int type; // 1-- Personal User; 2-- Agency User; 3-- Admin User
	private int roleID; //

	// constructor
	public MailBox() {

	}

	public MailBox(String address, String password) {
		this.address = address;
		this.password = password;
	}

	public MailBox(String address, String password, String username, int type , String name) {
		this.address = address;
		this.password = password;
		this.username = username;
		this.type = type;
		this.name = name;
	}

	public MailBox(int id, String address, String password, String username,
			int type, int roleID , String name) {
		this.id = id;
		this.address = address;
		this.password = password;
		this.username =username;
		this.type = type;
		this.roleID = roleID;
		this.name = name;
	}

	// getters and setters ...
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getRoleID() {
		return roleID;
	}

	public void setRoleID(int roleID) {
		this.roleID = roleID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
