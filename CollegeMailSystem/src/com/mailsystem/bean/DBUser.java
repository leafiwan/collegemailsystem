package com.mailsystem.bean;

/**
 * 数据库用户
 * @author WanLinFeng
 *
 */
public class DBUser {

	private String username;
	private String password;
	private String pwdAlgorithm = "SHA";
	private int useForwarding = 0;
	private String forwardDestination = null;
	private int useAlias = 0;
	private String alias = null;
	public DBUser(String username, String password) {
		this.username = username;
		this.password = password;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPwdAlgorithm() {
		return pwdAlgorithm;
	}
	public void setPwdAlgorithm(String pwdAlgorithm) {
		this.pwdAlgorithm = pwdAlgorithm;
	}
	public int getUseForwarding() {
		return useForwarding;
	}
	public void setUseForwarding(int useForwarding) {
		this.useForwarding = useForwarding;
	}
	public String getForwardDestination() {
		return forwardDestination;
	}
	public void setForwardDestination(String forwardDestination) {
		this.forwardDestination = forwardDestination;
	}
	public int getUseAlias() {
		return useAlias;
	}
	public void setUseAlias(int useAlias) {
		this.useAlias = useAlias;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
}
