package com.mailsystem.bean;

/**
 * 个人用户类
 * 
 * @author WanLinFeng
 * 
 */
public class User {

	private int id;
	private String name;
	private String role;
	private int dept;
	private int major;
	private String title;
	private int mailBoxID; // 用户关联邮箱id号
	private int classesID;
	private String grade;

	// constructor
	public User(String name, String role, int dept, int major , String title , int classesID , String grade) {
		this.name = name;
		this.role = role;
		this.dept = dept;
		this.major = major;
		this.title = title;
		this.classesID = classesID;
		this.grade = grade;
	}
	
	public User(int id , String name, String role, int dept, int major , int classesID , String grade) {
		this.id = id;
		this.name = name;
		this.role = role;
		this.dept = dept;
		this.major = major;
		this.classesID = classesID;
		this.grade = grade;
	}

	public User(int id, String name, String role, int dept, int major , String title,
			int mailBoxID, int classesID, String grade) {
		this.id = id;
		this.name = name;
		this.role = role;
		this.dept = dept;
		this.major = major;
		this.title = title;
		this.mailBoxID = mailBoxID;
		this.classesID = classesID;
		this.grade = grade;
	}

	public User() {

	}

	// getters and setters...
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getMailBoxID() {
		return mailBoxID;
	}

	public void setMailBoxID(int mailBoxID) {
		this.mailBoxID = mailBoxID;
	}

	public int getClassesID() {
		return classesID;
	}

	public void setClassesID(int classesID) {
		this.classesID = classesID;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public int getDept() {
		return dept;
	}

	public void setDept(int dept) {
		this.dept = dept;
	}

	public int getMajor() {
		return major;
	}

	public void setMajor(int major) {
		this.major = major;
	}
}
