package com.mailsystem.bean;

/**
 * 单位用户类
 * @author WanLinFeng
 * 
 */
public class Agency {

	private int id;
	private String name;
	private int mailBoxID;	// 用户关联邮箱id号
	private int type;		// 机构类型：1--班级机构；2--社团机构；3--年级机构；4--系机构；5--院机构；6--校机构
	
	public static final int CLASSES = 1;
	public static final int LEAGUE = 2;
	public static final int GRADE = 3;
	public static final int DEPART = 4;
	public static final int COLLEGE = 5;
	public static final int SCHOOL = 6;

	// constructor
	public Agency(String name) {
		this.name = name;
	}

	public Agency(String name, int type) {
		this.name = name;
		this.type = type;
	}

	public Agency(int id, String name, int mailBoxID, int type) {
		this.id = id;
		this.name = name;
		this.mailBoxID = mailBoxID;
		this.type = type;
	}

	// getters and setters...
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMailBoxID() {
		return mailBoxID;
	}

	public void setMailBoxID(int mailBoxID) {
		this.mailBoxID = mailBoxID;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
}
