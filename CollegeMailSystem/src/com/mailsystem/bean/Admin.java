package com.mailsystem.bean;

/**
 * 管理员用户类
 * @author WanLinFeng
 *
 */
public class Admin {
	
	private int id;
	private String name;
	private int mailBoxID;	//用户关联邮箱id号
	
	// constructor
	public Admin(String name) {
		this.name = name;
	}	
	
	public Admin(int id, String name, int mailBoxID) {
		this.id = id;
		this.name = name;
		this.mailBoxID = mailBoxID;
	}

	public Admin(){
		
	}
	
	// getters and setters...
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getMailBoxID() {
		return mailBoxID;
	}
	public void setMailBoxID(int mailBoxID) {
		this.mailBoxID = mailBoxID;
	}
}
