package com.mailsystem.bean;

import java.util.Date;

/**
 * Mail Entity
 * 
 * @author WanLinFeng
 * 
 */
public class Mail {

	private int id;
	private String subject;
	private String content;
	private int count = 1; // 默认发送给1一个人
	private String senderAddress;
	private String receiverAddress;
	private String fileAttachment = null;
	private int fileCount = 0;
	private Date sendDate;
	private String intro; // the introduce of content

	// constructor
	public Mail() {

	}

	public Mail(String subject, String content, int count,
			String senderAddress, String receiverAddress) {
		this.subject = subject;
		this.content = content;
		this.count = count;
		this.senderAddress = senderAddress;
		this.receiverAddress = receiverAddress;
	}

	public Mail(int id, String subject, String content, int count,
			String senderAddress, String receiverAddress) {
		this.id = id;
		this.subject = subject;
		this.content = content;
		this.count = count;
		this.senderAddress = senderAddress;
		this.receiverAddress = receiverAddress;
	}

	public Mail(int id, String subject, String content, int count,
			String senderAddress, String receiverAddress,
			String fileAttachment, Date sendDate, String intro , int fileCount) {
		this.id = id;
		this.subject = subject;
		this.content = content;
		this.count = count;
		this.senderAddress = senderAddress;
		this.receiverAddress = receiverAddress;
		this.fileAttachment = fileAttachment;
		this.sendDate = sendDate;
		this.intro = intro;
		this.fileCount = fileCount;
	}

	// getters and setters...
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getSenderAddress() {
		return senderAddress;
	}

	public void setSenderAddress(String senderAddress) {
		this.senderAddress = senderAddress;
	}

	public String getReceiverAddress() {
		return receiverAddress;
	}

	public void setReceiverAddress(String receiverAddress) {
		this.receiverAddress = receiverAddress;
	}

	public String getFileAttachment() {
		return fileAttachment;
	}

	public void setFileAttachment(String fileAttachment) {
		this.fileAttachment = fileAttachment;
	}

	public Date getSendDate() {
		return sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public int getFileCount() {
		return fileCount;
	}

	public void setFileCount(int fileCount) {
		this.fileCount = fileCount;
	}

}
