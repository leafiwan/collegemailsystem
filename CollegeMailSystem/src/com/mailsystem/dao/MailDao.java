package com.mailsystem.dao;

import java.util.List;

import com.mailsystem.bean.Mail;
import com.mailsystem.bean.MailBox;
import com.mailsystem.common.Page;
import com.mailsystem.common.ReceiveView;

public interface MailDao extends Dao<Mail>{
	
	/**
	 * 得到某个人的所有邮件
	 * @param mailBox
	 * @param page
	 * @return
	 */
	public List<ReceiveView> getMails(MailBox mailBox , Page page);

	/**
	 * 根据邮箱得到它所收的所有邮件
	 * @param mailBox
	 * @return
	 */
	public int getMailsCount(MailBox mailBox);
	
	/**
	 * 保存一封邮件
	 * @param Mail
	 * @param mailBox: 发件人邮箱
	 * @return
	 */
	public int save(Mail mail , MailBox mailBox);
	
	/**
	 * 根据ID得到一封邮件
	 * @param request
	 * @param response
	 */
	public Mail getMailById(int id);
}
