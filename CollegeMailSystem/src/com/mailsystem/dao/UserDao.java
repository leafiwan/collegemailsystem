package com.mailsystem.dao;

import java.util.List;

import com.mailsystem.bean.MailBox;
import com.mailsystem.bean.User;

public interface UserDao extends Dao<User>{
	
	public int save(User user , MailBox mailBox);
	
	public User getUserById(int id);
	
	public List<MailBox> getContacts(User user , MailBox mailBox);
	
	public int updateInfo(User user);
}
