package com.mailsystem.dao;

import java.util.List;

import com.mailsystem.bean.Agency;
import com.mailsystem.bean.MailBox;
import com.mailsystem.common.Page;

public interface AgencyDao extends Dao<Agency>{
	
	public int save(Agency agency , MailBox mailBox);

	/**
	 * 根据机构类型取出某页面的机构
	 * @param mailBox
	 * @param page
	 * @param type: 机构的类型
	 * @return
	 */
	public List<Agency> getAgencyListByType(MailBox mailBox , Page page , int type);

	/**
	 *
	 * @param parentAgencyId
	 * @param type: 机构的类型
	 * @return
	 */
	public List<Agency> getAgencyListByType(int parentAgencyId , int type);
	
	/**
	 *
	 * @param type: 机构的类型
	 * @return
	 */
	public List<Agency> getAgencyListByType(int type);
	
	/**
	 * 得到联系人
	 * @param agency
	 * @param mailBox
	 * @return
	 */
	public List<MailBox> getContacts(Agency agency , MailBox mailBox);
	
	/**
	 * 根据ID得到Agency对象
	 * @param id
	 * @return
	 */
	public Agency getAgencyById(int id);
}
