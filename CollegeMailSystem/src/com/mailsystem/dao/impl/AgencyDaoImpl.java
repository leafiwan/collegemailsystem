package com.mailsystem.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mailsystem.bean.Agency;
import com.mailsystem.bean.MailBox;
import com.mailsystem.common.Page;
import com.mailsystem.dao.AgencyDao;
import com.mailsystem.db.DBConnectionManager;
import com.mailsystem.util.DBUtil;
import com.mailsystem.util.MD5er;

public class AgencyDaoImpl implements AgencyDao {

	private PreparedStatement ps = null;
	private ResultSet rs = null;
	private Connection conn = null;
	private String connName = "MailSystem";

	@Override
	public int save(Agency agency, MailBox mailBox) {
		String sql = "{call p_InsertAgency(?,?,?,?,?,?)}";
		int flag = 1;
		CallableStatement cs = null;
		try {
			String pwd = MD5er.md5(mailBox.getPassword());
			conn = DBConnectionManager.getInstance().getConnection(connName);
			cs = conn.prepareCall(sql);
			cs.setString(1, mailBox.getUsername());
			cs.setString(2 , mailBox.getAddress());
			cs.setString(3 , pwd);
			cs.setInt(4 , mailBox.getType());
			cs.setString(5 , mailBox.getName());
			cs.setInt(6 , agency.getType());
			cs.execute();
		} catch (SQLException e) {
			flag = 0;
			e.printStackTrace();
		} finally {
			//close the connection and others
			try {
				cs.close();
				DBConnectionManager.getInstance().freeConnection(connName, conn);
				DBUtil.close(null , ps, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return flag;
	}
	
	@Override
	public int delete(int id) {
		String sql = "DELETE FROM t_agency WHERE a_id=?";
		int flag = 0;
		try {
			conn = DBConnectionManager.getInstance().getConnection(connName);
			ps = conn.prepareStatement(sql);
			ps.setInt(1 , id);
			flag = ps.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//close the connection and others
			try {
				DBConnectionManager.getInstance().freeConnection(connName, conn);
				DBUtil.close(null , ps, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return flag;
	}

	@Override
	//FIXME!
	public List<Agency> getAgencyListByType(MailBox mailBox, Page page, int type) {
		String sql = "SELECT * FROM t_agency WHERE a_type=?";
		List<Agency> list = null;
		try {
			conn = DBConnectionManager.getInstance().getConnection(connName);
			ps = conn.prepareStatement(sql);
			ps.setInt(1 , type);
			rs = ps.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//close the connection and others
			try {
				DBConnectionManager.getInstance().freeConnection(connName, conn);
				DBUtil.close(null , ps, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return list;
	}

	@Override
	public List<Agency> getAgencyListByType(int parentAgencyId, int type) {
		String sql = "SELECT * FROM t_agency a LEFT JOIN t_A_A t ON a.a_id=t.subagency_id WHERE t.supagency_id=? AND a.a_type=?";
		List<Agency> list = new ArrayList<Agency>();
		try {
			conn = DBConnectionManager.getInstance().getConnection(connName);
			ps = conn.prepareStatement(sql);
			ps.setInt(1 , parentAgencyId);
			ps.setInt(2 , type);
			rs = ps.executeQuery();
			while(rs.next()){
				int id = rs.getInt("a_id");
				String name = rs.getString("a_name");
				int b_id = rs.getInt("b_id");
				int a_type = rs.getInt("a_type");
				list.add(new Agency(id , name , b_id , a_type));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//close the connection and others
			try {
				DBConnectionManager.getInstance().freeConnection(connName, conn);
				DBUtil.close(null , ps, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return list;
	}

	@Override
	public List<Agency> getAgencyListByType(int type) {
		String sql = "SELECT * FROM t_agency a LEFT JOIN t_A_A t ON a.a_id=t.subagency_id WHERE a.a_type=?";
		List<Agency> list = new ArrayList<Agency>();
		try {
			conn = DBConnectionManager.getInstance().getConnection(connName);
			ps = conn.prepareStatement(sql);
			ps.setInt(1 , type);
			rs = ps.executeQuery();
			while(rs.next()){
				int id = rs.getInt("a_id");
				String name = rs.getString("a_name");
				int b_id = rs.getInt("b_id");
				int a_type = rs.getInt("a_type");
				list.add(new Agency(id , name , b_id , a_type));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//close the connection and others
			try {
				DBConnectionManager.getInstance().freeConnection(connName, conn);
				DBUtil.close(null , ps, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return list;
	}

	@Override
	public List<MailBox> getContacts(Agency agency, MailBox mailBox) {
		String sql1 = "SELECT DISTINCT * FROM t_agency a , t_A_A , t_box b WHERE supagency_id=? AND a.a_id=subagency_id AND b.b_id=a.b_id";
		String sql2 = "SELECT * FROM t_user u , t_U_A t , t_box b WHERE t.a_id=? AND u.u_id=t.u_id AND u.b_id=b.b_id";
		List<MailBox> contacts = new ArrayList<MailBox>();
		try {
			conn = DBConnectionManager.getInstance().getConnection(connName);
			// get sql1 result
			ps = conn.prepareStatement(sql1);
			ps.setInt(1 , agency.getId());
			rs = ps.executeQuery();
			while(rs.next()){
				String address = rs.getString("b_address");
				String name = rs.getString("b_name");
				MailBox box = new MailBox();
				box.setAddress(address);
				box.setName(name);
				contacts.add(box);
			}
			// get sql2 result
			ps = conn.prepareStatement(sql2);
			ps.setInt(1 , agency.getId());
			rs = ps.executeQuery();
			while(rs.next()){
				String address = rs.getString("b_address");
				String name = rs.getString("b_name");
				MailBox box = new MailBox();
				box.setAddress(address);
				box.setName(name);
				contacts.add(box);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//close the connection and others
			try {
				DBConnectionManager.getInstance().freeConnection(connName, conn);
				DBUtil.close(null , ps, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return contacts;
	}

	@Override
	public Agency getAgencyById(int id) {
		String sql = "SELECT * FROM t_agency WHERE a_id=?";
		Agency agency = null;
		try {
			conn = DBConnectionManager.getInstance().getConnection(connName);
			ps = conn.prepareStatement(sql);
			ps.setInt(1 , id);
			rs = ps.executeQuery();
			if(rs.next()){
				int agencyID = rs.getInt("a_id");
				String name = rs.getString("a_name");
				int mailID = rs.getInt("b_id");
				int type = rs.getInt("a_type");
				agency = new Agency(agencyID , name , mailID , type);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//close the connection and others
			try {
				DBConnectionManager.getInstance().freeConnection(connName, conn);
				DBUtil.close(null , ps, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return agency;
	}

}
