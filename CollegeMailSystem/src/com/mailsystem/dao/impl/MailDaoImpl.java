package com.mailsystem.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mailsystem.bean.Mail;
import com.mailsystem.bean.MailBox;
import com.mailsystem.common.Page;
import com.mailsystem.common.ReceiveView;
import com.mailsystem.dao.MailDao;
import com.mailsystem.db.DBConnectionManager;
import com.mailsystem.util.DBUtil;

public class MailDaoImpl implements MailDao {

	private PreparedStatement ps = null;
	private ResultSet rs = null;
	private Connection conn = null;
	private String connName = "MailSystem";

	@Override
	public int delete(int id) {
		int flag = 0;
		String sql = "DELETE FROM t_mail WHERE m_id=?";
		try {
			conn = DBConnectionManager.getInstance().getConnection(connName);
			ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			boolean suc = ps.execute();
			while (suc) {
				flag = 1;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// close the connection and others
			try {
				DBConnectionManager.getInstance()
						.freeConnection(connName, conn);
				DBUtil.close(null, ps, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return flag;
	}

	@Override
	public List<ReceiveView> getMails(MailBox mailBox, Page page) {
		List<ReceiveView> list = new ArrayList<ReceiveView>();
		String sql = "SELECT top "
				+ Page.ROW_PER_PAGE
				+ " * FROM v_receivemailbox WHERE m_receiver_address=? AND m_id NOT IN"
				+ "(SELECT top "
				+ (page.getCurrentPage() - 1)
				* Page.ROW_PER_PAGE
				+ " m_id FROM v_receivemailbox WHERE m_receiver_address=? ORDER BY m_id) ORDER BY m_id DESC";
		try {
			conn = DBConnectionManager.getInstance().getConnection(connName);
			ps = conn.prepareStatement(sql);
			ps.setString(1, mailBox.getAddress());
			ps.setString(2, mailBox.getAddress());
			rs = ps.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("m_id");
				String sendName = rs.getString("b_name");
				String title = rs.getString("m_title");
				String intro = rs.getString("m_intro");
				Date date = rs.getDate("m_send_date");
				list.add(new ReceiveView(id, sendName, title, intro, date));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// close the connection and others
			try {
				DBConnectionManager.getInstance()
						.freeConnection(connName, conn);
				DBUtil.close(null, ps, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return list;
	}

	@Override
	public int getMailsCount(MailBox mailBox) {
		int count = 0;
		String sql = "SELECT count(*) FROM v_receivemailbox WHERE m_receiver_address=?";
		try {
			conn = DBConnectionManager.getInstance().getConnection(connName);
			ps = conn.prepareStatement(sql);
			ps.setString(1, mailBox.getAddress());
			rs = ps.executeQuery();
			while (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// close the connection and others
			try {
				DBConnectionManager.getInstance()
						.freeConnection(connName, conn);
				DBUtil.close(null, ps, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return count;
	}

	@Override
	public int save(Mail mail, MailBox mailBox) {
		int flag = 0;
		String sql = "INSERT INTO t_mail (m_title , m_content , m_count , m_file_attachment , m_sender_address , m_receiver_address , m_intro , m_send_date , m_file_count) VALUES(?,?,?,?,?,?,?,?,?);";
		try {
			conn = DBConnectionManager.getInstance().getConnection(connName);
			ps = conn.prepareStatement(sql);
			ps.setString(1, mail.getSubject());
			ps.setString(2, mail.getContent());
			ps.setInt(3, mail.getCount());
			ps.setString(4, mail.getFileAttachment());
			ps.setString(5, mail.getSenderAddress());
			ps.setString(6, mail.getReceiverAddress());
			ps.setString(7, mail.getIntro());
			System.out.println("发送时间是："
					+ new Timestamp(mail.getSendDate().getTime()));
			// FIXME!
			// ps.setTimestamp(8 , new
			// Timestamp(mail.getSendDate().getTime()));//不行？？？？
			ps.setDate(8, new java.sql.Date(mail.getSendDate().getTime()));
			ps.setInt(9, mail.getFileCount());
			boolean suc = ps.execute();
			if (suc) {
				flag = 1;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// close the connection and others
			try {
				DBConnectionManager.getInstance()
						.freeConnection(connName, conn);
				DBUtil.close(null, ps, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return flag;
	}

	@Override
	public Mail getMailById(int id) {
		String sql = "SELECT * FROM t_mail WHERE m_id=?";
		Mail mail = null;
		try {
			conn = DBConnectionManager.getInstance().getConnection(connName);
			ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			while (rs.next()) {
				String subject = rs.getString("m_title");
				String content = rs.getString("m_content");
				int count = rs.getInt("m_count");
				String fileAttachment = rs.getString("m_file_attachment");
				String senderAddress = rs.getString("m_sender_address");
				String receiverAddress = rs.getString("m_receiver_address");
				Date sendDate = rs.getDate("m_send_date");
				String intro = rs.getString("m_intro");
				int fileCount = rs.getInt("m_file_count");
				mail = new Mail(id, subject, content, count, senderAddress,
						receiverAddress, fileAttachment, sendDate, intro,
						fileCount);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// close the connection and others
			try {
				DBConnectionManager.getInstance()
						.freeConnection(connName, conn);
				DBUtil.close(null, ps, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return mail;
	}
}
