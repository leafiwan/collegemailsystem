package com.mailsystem.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mailsystem.bean.MailBox;
import com.mailsystem.bean.User;
import com.mailsystem.dao.UserDao;
import com.mailsystem.db.DBConnectionManager;
import com.mailsystem.util.DBUtil;
import com.mailsystem.util.MD5er;

public class UserDaoImpl implements UserDao {

	private PreparedStatement ps = null;
	private ResultSet rs = null;
	private Connection conn = null;
	private String connName = "MailSystem";

	@Override
	public int save(User user, MailBox mailBox) {
		String sql = "{call p_InsertUser(?,?,?,?,?,?,?,?,?,?,?)}";
		int flag = 1;
		CallableStatement cs = null;
		try {
			String pwd = MD5er.md5(mailBox.getPassword());
			conn = DBConnectionManager.getInstance().getConnection(connName);
			cs = conn.prepareCall(sql);
			cs.setString(1, mailBox.getUsername());
			cs.setString(2, mailBox.getAddress());
			cs.setString(3, pwd);
			cs.setInt(4, mailBox.getType());
			cs.setString(5, user.getName());
			cs.setString(6, user.getRole());
			cs.setInt(7, user.getDept());
			cs.setInt(8, user.getMajor());
			cs.setString(9, user.getTitle());
			cs.setInt(10, user.getClassesID());
			cs.setString(11 , user.getGrade());
			cs.execute();
		} catch (SQLException e) {
			flag = 0;
			e.printStackTrace();
		} finally {
			// close the connection and others
			try {
				cs.close();
				DBConnectionManager.getInstance()
						.freeConnection(connName, conn);
				DBUtil.close(null, ps, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return flag;
	}

	@Override
	public int delete(int id) {
		String sql = "DELETE FROM t_user WHERE u_id=?";
		int flag = 0;
		try {
			conn = DBConnectionManager.getInstance().getConnection(connName);
			ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			flag = ps.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// close the connection and others
			try {
				DBConnectionManager.getInstance()
						.freeConnection(connName, conn);
				DBUtil.close(null, ps, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return flag;
	}

	@Override
	public List<MailBox> getContacts(User user, MailBox mailBox) {
		String sql = "SELECT * FROM t_user u LEFT JOIN t_box b ON u.b_id=b.b_id WHERE u_classes=? OR u_grade=?";
		List<MailBox> contacts = new ArrayList<MailBox>();
		try {
			conn = DBConnectionManager.getInstance().getConnection(connName);
			ps = conn.prepareStatement(sql);
			ps.setInt(1, user.getClassesID());
			ps.setString(2, user.getGrade());
			rs = ps.executeQuery();
			while (rs.next()) {
				String address = rs.getString("b_address");
				String name = rs.getString("b_name");
				if (!address.equals(mailBox.getAddress())) {
					MailBox box = new MailBox();
					box.setAddress(address);
					box.setName(name);
					contacts.add(box);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// close the connection and others
			try {
				DBConnectionManager.getInstance()
						.freeConnection(connName, conn);
				DBUtil.close(null, ps, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return contacts;
	}

	@Override
	public User getUserById(int id) {
		String sql = "SELECT * FROM t_user WHERE u_id=?";
		User user = null;
		try {
			conn = DBConnectionManager.getInstance().getConnection(connName);
			ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			if (rs.next()) {
				int userID = rs.getInt("u_id");
				String name = rs.getString("u_name");
				String role = rs.getString("u_role");
				int dept = rs.getInt("u_dept");
				int major = rs.getInt("u_major");
				String title = rs.getString("u_title");
				int mailID = rs.getInt("b_id");
				int classes = rs.getInt("u_classes");
				String grade = rs.getString("u_grade");
				user = new User(userID, name, role, dept, major , title, mailID,
						classes, grade);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// close the connection and others
			try {
				DBConnectionManager.getInstance()
						.freeConnection(connName, conn);
				DBUtil.close(null, ps, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return user;
	}

	@Override
	public int updateInfo(User user) {
		int flag = 0;
		String sql = "UPDATE t_user SET u_name=?, u_dept=?, u_major=?, u_classes=?, u_grade=?, u_role=? WHERE u_id=?";
		try {
			conn = DBConnectionManager.getInstance().getConnection(connName);
			ps = conn.prepareStatement(sql);
			ps.setString(1 , user.getName());
			ps.setInt(2 , user.getDept());
			ps.setInt(3 , user.getMajor());
			ps.setInt(4 , user.getClassesID());
			ps.setString(5 , user.getGrade());
			ps.setString(6 , user.getRole());
			ps.setInt(7 , user.getId());
			flag = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// close the connection and others
			try {
				DBConnectionManager.getInstance()
						.freeConnection(connName, conn);
				DBUtil.close(null, ps, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return flag;
	}

}
