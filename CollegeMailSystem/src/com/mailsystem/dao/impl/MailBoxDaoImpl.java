package com.mailsystem.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mailsystem.bean.MailBox;
import com.mailsystem.dao.MailBoxDao;
import com.mailsystem.db.DBConnectionManager;
import com.mailsystem.util.DBUtil;
import com.mailsystem.util.MD5er;

public class MailBoxDaoImpl implements MailBoxDao{
	
	private PreparedStatement ps = null;
	private ResultSet rs = null;
	private Connection conn = null;
	private String connName = "MailSystem";

	@Override
	public int delete(int id) {
		String sql = "DELETE FROM t_box WHERE b_id=?";
		int flag = 0;
		try {
			conn = DBConnectionManager.getInstance().getConnection(connName);
			ps = conn.prepareStatement(sql);
			ps.setInt(1 , id);
			flag = ps.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//close the connection and others
			try {
				DBConnectionManager.getInstance().freeConnection(connName, conn);
				DBUtil.close(null , ps, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return flag;
	}

	@Override
	public MailBox isMailBoxExist(MailBox mailBox) {
		// query sql
		String sql = "SELECT * FROM t_box WHERE b_address=? AND b_password=?";
		try {
			conn = DBConnectionManager.getInstance().getConnection(connName);
			ps = conn.prepareStatement(sql);
			//encrypt password
			String pwd = MD5er.md5(mailBox.getPassword());
			System.out.println("现在的密码是： " + pwd);
			//add parameter
			ps.setString( 1 , mailBox.getAddress());
			ps.setString( 2 , pwd);
			//get result
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				int id = rs.getInt("b_id");
				String address = rs.getString("b_address");
				String username = rs.getString("b_username");
				String name = rs.getString("b_name");
				int type = rs.getInt("b_type");
				int r_id = rs.getInt("r_id");
				return new MailBox(id , address , mailBox.getPassword() , username , type , r_id , name);
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			//close the connection and others
			try {
				DBConnectionManager.getInstance().freeConnection(connName, conn);
				DBUtil.close(null , ps, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public String getNameByID(int id) {
		String sql = "SELECT b_name FROM t_box WHERE b_id=?";
		String name = null;
		try {
			conn = DBConnectionManager.getInstance().getConnection(connName);
			ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			if(rs.next()){
				name = rs.getString("b_name");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//close the connection and others
			try {
				DBConnectionManager.getInstance().freeConnection(connName, conn);
				DBUtil.close(null , ps, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return name;
	}

	@Override
	public int updatePassword(MailBox mailBox) {
		int flag = 0;
		String sql = "UPDATE t_box SET b_password=? WHERE b_address=?";
		try {
			conn = DBConnectionManager.getInstance().getConnection(connName);
			ps = conn.prepareStatement(sql);
			//encrypt password
			String pwd = MD5er.md5(mailBox.getPassword());
			//add parameter
			ps.setString( 1 , pwd);
			ps.setString( 2 , mailBox.getAddress());
			flag = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			//close the connection and others
			try {
				DBConnectionManager.getInstance().freeConnection(connName, conn);
				DBUtil.close(null , ps, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return flag;
	}

}