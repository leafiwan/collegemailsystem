package com.mailsystem.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mailsystem.bean.Admin;
import com.mailsystem.bean.MailBox;
import com.mailsystem.dao.AdminDao;
import com.mailsystem.db.DBConnectionManager;
import com.mailsystem.util.DBUtil;
import com.mailsystem.util.MD5er;

public class AdminDaoImpl implements AdminDao {

	private PreparedStatement ps = null;
	private ResultSet rs = null;
	private Connection conn = null;
	private String connName = "MailSystem";

	@Override
	public int save(Admin admin, MailBox mailBox) {
		String sql = "{call p_InsertAdmin(?,?,?,?,?)}";
		int flag = 1;
		CallableStatement cs = null;
		try {
			String pwd = MD5er.md5(mailBox.getPassword());
			conn = DBConnectionManager.getInstance().getConnection(connName);
			cs = conn.prepareCall(sql);
			cs.setString(1, mailBox.getUsername());
			cs.setString(2 , mailBox.getAddress());
			cs.setString(3 , pwd);
			cs.setInt(4 , mailBox.getType());
			cs.setString(5 , mailBox.getName());
			cs.execute();
		} catch (SQLException e) {
			flag = 0;
			e.printStackTrace();
		} finally {
			//close the connection and others
			try {
				DBConnectionManager.getInstance().freeConnection(connName, conn);
				cs.close();
				DBUtil.close(null , ps, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return flag;
	}
	
	@Override
	public int delete(int id) {
		String sql = "DELETE FROM t_admin WHERE a_id=?";
		int flag = 0;
		try {
			conn = DBConnectionManager.getInstance().getConnection(connName);
			ps = conn.prepareStatement(sql);
			ps.setInt(1 , id);
			flag = ps.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//close the connection and others
			try {
				DBConnectionManager.getInstance().freeConnection(connName, conn);
				DBUtil.close(null , ps, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return flag;
	}

	/**
	 * 
	 * @param admin
	 * @param mailBox
	 * @return
	 */
	@Override
	public List<MailBox> getContacts(Admin admin, MailBox mailBox) {
		String sql = "SELECT * FROM t_box";
		List<MailBox> contacts = new ArrayList<MailBox>();
		try {
			conn = DBConnectionManager.getInstance().getConnection(connName);
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()){
				String address = rs.getString("b_address");
				String name = rs.getString("b_name");
				if(mailBox.getAddress() != address){
					//添加
					MailBox box = new MailBox();
					box.setAddress(address);
					box.setName(name);
					contacts.add(box);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//close the connection and others
			try {
				DBConnectionManager.getInstance().freeConnection(connName, conn);
				DBUtil.close(null , ps, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return contacts;
	}

	@Override
	public Admin getAdminById(int id) {
		String sql = "SELECT * FROM t_admin WHERE a_id=?";
		Admin admin = null;
		try {
			conn = DBConnectionManager.getInstance().getConnection(connName);
			ps = conn.prepareStatement(sql);
			ps.setInt(1 , id);
			rs = ps.executeQuery();
			if(rs.next()){
				int adminID = rs.getInt("a_id");
				String name = rs.getString("a_name");
				int mailID = rs.getInt("b_id");
				admin = new Admin(adminID , name , mailID);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//close the connection and others
			try {
				DBConnectionManager.getInstance().freeConnection(connName, conn);
				DBUtil.close(null , ps, rs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return admin;
	}

}
