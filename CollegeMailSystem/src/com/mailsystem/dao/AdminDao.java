package com.mailsystem.dao;

import java.util.List;

import com.mailsystem.bean.Admin;
import com.mailsystem.bean.MailBox;

public interface AdminDao extends Dao<Admin>{
	
	public int save(Admin admin , MailBox mailBox);
	
	public List<MailBox> getContacts(Admin admin , MailBox mailBox);
	
	public Admin getAdminById(int id);
}
