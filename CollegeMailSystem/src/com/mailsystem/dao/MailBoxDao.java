package com.mailsystem.dao;

import com.mailsystem.bean.MailBox;

public interface MailBoxDao extends Dao<MailBox> {
	
	/**
	 * 登录时判断该邮箱是否存在且密码是否正确
	 * @param mailBox
	 * @return
	 */
	public MailBox isMailBoxExist(MailBox mailBox);
	
	/**
	 * 根据邮箱id得到用户名
	 * @param id
	 * @return
	 */
	public String getNameByID(int id);
	
	/**
	 * 更新密码
	 * @param mailBox
	 * @return
	 */
	public int updatePassword(MailBox mailBox);
}
