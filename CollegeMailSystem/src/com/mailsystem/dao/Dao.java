package com.mailsystem.dao;

public interface Dao<T> {
	
	/**
	 * 根据id在数据库中删除一条记录
	 * @param id
	 * @return
	 */
	public int delete(int id);
}
