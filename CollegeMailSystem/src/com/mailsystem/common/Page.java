package com.mailsystem.common;


/**
 * 分页类
 * @author WanLinFeng
 *
 */
public class Page {
	
	private int currentPage = 1;	//
	public static int ROW_PER_PAGE = 5;	//
	private int maxPage;			//
	private int rowsCount;			//
	
	// constructor	
	public Page(int currentPage){
		this.currentPage = currentPage;
	}
	
	public Page(){
		
	}
	
	// getters and setters
	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	public int getMaxPage() {
		return maxPage;
	}
	public void setMaxPage(int maxPage) {
		this.maxPage = maxPage;
	}
	public int getRowsCount() {
		return rowsCount;
	}
	public void setRowsCount(int rowsCount) {
		this.rowsCount = rowsCount;
	}
}
