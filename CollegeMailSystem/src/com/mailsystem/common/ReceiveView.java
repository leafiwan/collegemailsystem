package com.mailsystem.common;

import java.util.Date;

/**
 * the entity of receive box's view
 * 
 * @author WanLinFeng
 * 
 */
public class ReceiveView {

	private int id;
	private String sendName;
	private String subject;
	private String intro;
	private Date date;

	// constructor
	public ReceiveView(int id , String sendName, String subject, String intro , Date date) {
		this.id = id;
		this.sendName = sendName;
		this.subject = subject;
		this.intro = intro;
		this.date = date;
	}

	public ReceiveView() {

	}

	// getters and setters...
	public String getSendName() {
		return sendName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setSendName(String sendName) {
		this.sendName = sendName;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}
}
