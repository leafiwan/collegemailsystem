package com.mailsystem.servlet;

import java.io.IOException;
import java.lang.reflect.Method;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mailsystem.action.MailAction;
import com.mailsystem.action.impl.MailActionImpl;

/**
 * 
 * @author WanLinFeng
 *
 */
public class MailServlet extends HttpServlet {

	private static final long serialVersionUID = 3005728529472529090L;
	private MailAction mailAction;

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
		mailAction = new MailActionImpl();
	}



	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		String method = request.getParameter("method");
		System.out.println("Method: " + method);
		// reflect 
		Class<? extends MailAction> c = mailAction.getClass();
		try {
			// get the method of the mail action
			Method m = c.getMethod(method, new Class[]{HttpServletRequest.class, HttpServletResponse.class});
			// invoke the method
			m.invoke(mailAction, new Object[]{request , response});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doGet(request, response);
	}
	
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
	}
}
