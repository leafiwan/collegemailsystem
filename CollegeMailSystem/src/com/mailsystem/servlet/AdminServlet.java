package com.mailsystem.servlet;

import java.io.IOException;
import java.lang.reflect.Method;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mailsystem.action.AdminAction;
import com.mailsystem.action.impl.AdminActionImpl;

public class AdminServlet extends HttpServlet {

	private static final long serialVersionUID = 384990823085185067L;

	private AdminAction adminAction;

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
		adminAction = new AdminActionImpl();
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		// get method
		String method = request.getParameter("method");
		// reflect 
		Class<? extends AdminAction> c = adminAction.getClass();
		try {
			// get the method of the mail action
			Method m = c.getMethod(method, new Class[]{HttpServletRequest.class, HttpServletResponse.class});
			// invoke the method
			m.invoke(adminAction, new Object[]{request , response});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doGet(request, response);
	}

}
