package com.mailsystem.servlet.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author WanLinFeng
 * 
 */
public class MailBoxFilter implements Filter {

	// �ַ����
	private String encoding;

	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1,
			FilterChain arg2) throws IOException, ServletException {
		// change the type
		HttpServletRequest request = (HttpServletRequest) arg0;
		HttpServletResponse response = (HttpServletResponse) arg1;
		// set character encoding
		request.setCharacterEncoding(encoding);
		response.setCharacterEncoding(encoding);
		// get user's request page
		String url = request.getRequestURI();
		url = url.substring(url.lastIndexOf("/") + 1);
		if ("login.jsp".equals(url) || "mailBox.do".equals(url)) {
			//
			arg2.doFilter(request, response);
		} else {
			if (request.getSession().getAttribute("curMailBox") != null) {
				//
				arg2.doFilter(request, response);
			} else {
				//
				request.setAttribute("msg", "");
				request.getRequestDispatcher("login.jsp").forward(request,
						response);
			}
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		this.encoding = arg0.getInitParameter("encoding");
	}

}
