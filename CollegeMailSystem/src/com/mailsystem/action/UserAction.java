package com.mailsystem.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface UserAction {

	/**
	 * 将一个个人用户存入数据库
	 * @param request
	 * @param response
	 */
	public void save(HttpServletRequest request , HttpServletResponse response);
	
	/**
	 * 显示用户具体信息
	 * @param request
	 * @param response
	 */
	public void showInfo(HttpServletRequest request , HttpServletResponse response);

	public void updateInfo(HttpServletRequest request , HttpServletResponse response);
}
