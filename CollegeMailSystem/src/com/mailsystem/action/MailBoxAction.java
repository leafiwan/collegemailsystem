package com.mailsystem.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface MailBoxAction {
	
	/**
	 * 判断该邮箱是否存在
	 * @param request
	 * @param response
	 * @return
	 */
	public boolean isMailBoxExist(HttpServletRequest request , HttpServletResponse response);

	/**
	 * 更新密码
	 * @param request
	 * @param response
	 */
	public void updatePassword(HttpServletRequest request , HttpServletResponse response);

	/**
	 * 得到联系人
	 * @param request
	 * @param response
	 */
	public void getContacts(HttpServletRequest request , HttpServletResponse response);
}
