package com.mailsystem.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface AdminAction {

	/**
	 * 将一个管理员用户存入数据库
	 * @param request
	 * @param response
	 */
	public void save(HttpServletRequest request , HttpServletResponse response);
	
	public void getInfo(HttpServletRequest request , HttpServletResponse response);
}
