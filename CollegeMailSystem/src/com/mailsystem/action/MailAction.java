package com.mailsystem.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface MailAction {
	
	/**
	 * 
	 * @param request
	 * @param response
	 */
	public void getMails(HttpServletRequest request , HttpServletResponse response);
	
	/**
	 * 
	 * @param request
	 * @param response
	 */
	public void sendMail(HttpServletRequest request , HttpServletResponse response);

	/**
	 * 删除一封邮件
	 * @param request
	 * @param response
	 */
	public void deleteMail(HttpServletRequest request , HttpServletResponse response);
	
	/**
	 * 根据ID得到一封邮件
	 * @param request
	 * @param response
	 */
	public void getMailById(HttpServletRequest request , HttpServletResponse response);

	/**
	 * 下载文件
	 * @param request
	 * @param response
	 */
	public void download(HttpServletRequest request, HttpServletResponse response);
}
