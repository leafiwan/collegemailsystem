package com.mailsystem.action.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mailsystem.action.MailBoxAction;
import com.mailsystem.bean.Admin;
import com.mailsystem.bean.Agency;
import com.mailsystem.bean.MailBox;
import com.mailsystem.bean.User;
import com.mailsystem.service.AdminService;
import com.mailsystem.service.AgencyService;
import com.mailsystem.service.MailBoxService;
import com.mailsystem.service.UserService;
import com.mailsystem.service.impl.AdminServiceImpl;
import com.mailsystem.service.impl.AgencyServiceImpl;
import com.mailsystem.service.impl.MailBoxServiceImpl;
import com.mailsystem.service.impl.UserServiceImpl;

public class MailBoxActionImpl implements MailBoxAction {

	private MailBoxService mailBoxService;
	private UserService userService;
	private AgencyService agencyService;
	private AdminService adminService;
	
	public MailBoxActionImpl(){
		this.mailBoxService = new MailBoxServiceImpl();
		this.userService = new UserServiceImpl();
		this.adminService = new AdminServiceImpl();
		this.agencyService = new AgencyServiceImpl();
	}

	@Override
	public boolean isMailBoxExist(HttpServletRequest request,
			HttpServletResponse response) {
		// get mail-address and password
		String address = request.getParameter("address");
		String password = request.getParameter("password");
		MailBox mailBox = new MailBox(address , password);
		mailBox = mailBoxService.isMailBoxExist(mailBox);
		try{
			if(mailBox != null){// address and password is correct
				// save the bean in session
				request.getSession().setAttribute("name" , mailBox.getName());
				request.getSession().setAttribute("type" , mailBox.getType());
				request.getSession().setAttribute("curAddress" , mailBox.getAddress());
				request.getSession().setAttribute("curBoxID" , mailBox.getId());
				request.getSession().setAttribute("curID" , mailBox.getRoleID());
				request.getSession().setAttribute("curMailBox" , mailBox);
				request.getRequestDispatcher("mainframe.html").forward(request, response);
			} else {
				request.setAttribute("msg", "邮箱或者密码错误！");
				request.getRequestDispatcher("login.jsp").forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public void updatePassword(HttpServletRequest request,
			HttpServletResponse response) {
		String address = (String)request.getSession().getAttribute("curAddress");
		String oldPassword = request.getParameter("old_pwd");
		String newPassword = request.getParameter("new_pwd");
		MailBox mailBox = new MailBox(address , newPassword);
		int flag = mailBoxService.updatePassword(mailBox, oldPassword);
		try{
			if(flag > 0){// update successful
				// save the bean in session
				request.setAttribute("msg", "update password successed!");
				request.getRequestDispatcher("updatepwd.jsp").forward(request, response);
			} else {
				request.setAttribute("msg", "update password failed!");
				request.getRequestDispatcher("updatepwd.jsp").forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void getContacts(HttpServletRequest request,
			HttpServletResponse response) {
		MailBox mailBox = (MailBox) request.getSession().getAttribute("curMailBox");
		List<MailBox> contacts = new ArrayList<MailBox>();
		//判断用户类型
		if(mailBox.getType() == 1){
			//普通用户
			User user = userService.getUserById(mailBox.getRoleID());
			contacts = userService.getContacts(user, mailBox);
		} else if(mailBox.getType() == 2){
			//机构用户
			Agency agency = agencyService.getAgencyById(mailBox.getRoleID());
			contacts = agencyService.getContacts(agency, mailBox);
		} else if(mailBox.getType() == 3){
			Admin admin = adminService.getAdminById(mailBox.getRoleID());
			contacts = adminService.getContacts(admin, mailBox);
		} else {
			request.setAttribute("msg", "找不到联系人，角色出错了");
		}
		// redirect
		try {
			request.setAttribute("contacts" , contacts);
			request.getRequestDispatcher("mailsender.jsp").forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
