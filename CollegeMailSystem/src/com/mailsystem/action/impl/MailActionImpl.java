package com.mailsystem.action.impl;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mailsystem.action.MailAction;
import com.mailsystem.bean.Mail;
import com.mailsystem.bean.MailBox;
import com.mailsystem.common.MailFileUpload;
import com.mailsystem.common.Page;
import com.mailsystem.common.ReceiveView;
import com.mailsystem.mail.MailConfig;
import com.mailsystem.mail.MailReceiver;
import com.mailsystem.mail.MailSender;
import com.mailsystem.service.MailService;
import com.mailsystem.service.impl.MailServiceImpl;

public class MailActionImpl implements MailAction {

	private MailService mailService;

	public MailActionImpl() {
		mailService = new MailServiceImpl();
	}

	@Override
	public void getMails(HttpServletRequest request,
			HttpServletResponse response) {
		// get current mailbox
		MailBox curMailBox = (MailBox) request.getSession().getAttribute(
				"curMailBox");
		// get recent message from mail server and save it
		MailReceiver receiver = new MailReceiver(curMailBox.getUsername(),
				curMailBox.getPassword());
		List<Mail> serverMails = receiver.receive();
		for (int i = 0; i < serverMails.size(); i++) {
			mailService.produceIntro(serverMails.get(i));
			mailService.save(serverMails.get(i), curMailBox);
		}
		// get curPage
		int jumpPage = Integer.parseInt(request.getParameter("jumpPage"));
		Page curPage = new Page(jumpPage);
		// the count of mails
		int mailsCount = mailService.getMailsCount(curMailBox);
		int pageCount = (mailsCount - 1) / Page.ROW_PER_PAGE + 1;
		request.setAttribute("mailsCount", mailsCount);
		request.setAttribute("pageCount", pageCount);
		request.setAttribute("curPage", jumpPage);
		// get all mails
		List<ReceiveView> receiveMails = mailService.getMails(curMailBox,
				curPage);
		// save it in request
		request.setAttribute("receiveMails", receiveMails);
		try {
			request.getRequestDispatcher("receivemailbox.jsp").forward(request,
					response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void sendMail(HttpServletRequest request,
			HttpServletResponse response) {
		MailFileUpload uploader = new MailFileUpload();
		request = uploader.upload(request, response);
		// get current mailbox
		MailBox curMailBox = (MailBox) request.getSession().getAttribute(
				"curMailBox");
		MailSender sender = new MailSender(curMailBox.getUsername(),
				curMailBox.getPassword());
		// create a mail sender information
		Mail mail = new Mail();
		mail.setSenderAddress(curMailBox.getAddress());
		mail.setSubject((String) request.getAttribute("subject"));
		mail.setContent((String) request.getAttribute("content"));
		String allAdd = (String) request.getAttribute("address");
		System.out.println("addAdd is: " + allAdd);
		// prepared for multiple send
		StringTokenizer addToken = new StringTokenizer(allAdd , ";");
		while(addToken.hasMoreTokens()){
			String address = addToken.nextToken();
			if(address != ""){
				System.out.println("Address is: " + address);
				// send the mail if the email address is not null
				mail.setReceiverAddress(address);
				int fileCount = (Integer) request.getAttribute("fileCount");
				if (fileCount > 0) {
					String fileAttachment = "";
					for (int i = 0; i < fileCount; i++) {
						if (i > 0) {
							fileAttachment += "|";// 分隔符
						}
						fileAttachment += request.getAttribute("fileAttachment" + i);
					}
					mail.setFileAttachment(fileAttachment);
				}
				sender.send(mail);
			}
		}
//		mail.setReceiverAddress((String) request.getAttribute("address"));
		try {
			request.getRequestDispatcher("sendsuccess.jsp").forward(request,
					response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteMail(HttpServletRequest request,
			HttpServletResponse response) {
		int id = Integer.parseInt(request.getParameter("id"));
		MailBox curMailBox = (MailBox) request.getSession().getAttribute(
				"curMailBox");
		int count = mailService.getMailsCount(curMailBox);
		int curPage = Integer.parseInt(request.getParameter("curPage"));
		if (count <= Page.ROW_PER_PAGE * (curPage - 1)) {
			curPage--;
		}
		mailService.delete(id);
		try {
			request.setAttribute("msg", "成功删除");
			request.getRequestDispatcher(
					"mail.do?method=getMails&jumpPage=" + curPage).forward(
					request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void getMailById(HttpServletRequest request,
			HttpServletResponse response) {
		int id = Integer.parseInt(request.getParameter("id"));
		Mail mail = mailService.getMailById(id);
		List<String> fileList = mailService.getFileList(mail);
		String senderName = request.getParameter("sender");
		try {
			request.setAttribute("senderName", senderName);
			request.setAttribute("fileList" , fileList);
			request.setAttribute("mail", mail);
			request.getRequestDispatcher("mailcontent.jsp").forward(request,
					response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("static-access")
	public void download(HttpServletRequest request,
			HttpServletResponse response) {
		String fileName = request.getParameter("fileName");
		String filePath = MailConfig.DOWNLOAD_PATH + fileName;
		response.reset();
		response.setContentType("APPLICATION/OCTET-STREAM");
		try {
			// 转码
			fileName = response.encodeURL(new String(fileName.getBytes(),
					"UTF-8"));
			response.setHeader("Content-Disposition", "attachment; filename=\""
					+ fileName + "\"");
			ServletOutputStream out = response.getOutputStream();
			InputStream inStream = new FileInputStream(filePath);
			byte[] b = new byte[1024];
			int len;
			while ((len = inStream.read(b)) > 0)
				out.write(b, 0, len);
			response.setStatus(response.SC_OK);
			response.flushBuffer();
			out.close();
			inStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
