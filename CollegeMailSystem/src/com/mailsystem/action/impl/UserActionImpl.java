package com.mailsystem.action.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mailsystem.action.UserAction;
import com.mailsystem.bean.MailBox;
import com.mailsystem.bean.User;
import com.mailsystem.service.UserService;
import com.mailsystem.service.impl.UserServiceImpl;

public class UserActionImpl implements UserAction {

	private UserService userService;

	public UserActionImpl() {
		userService = new UserServiceImpl();
	}

	@Override
	public void save(HttpServletRequest request,
			HttpServletResponse response) {
		String username = request.getParameter("username");
		String address = request.getParameter("address");
		String name = request.getParameter("name");
		String password = request.getParameter("pwd");
		int dept = Integer.parseInt(request.getParameter("college"));
		int major = Integer.parseInt(request.getParameter("depart"));
		int classes = Integer.parseInt(request.getParameter("classes"));
		String grade = request.getParameter("grade");
		String title = request.getParameter("title");
		String role = request.getParameter("role");
		int type = Integer.parseInt(request.getParameter("type"));
		User user = new User(username, role, dept,  major ,title , classes , grade);
		MailBox mailBox = new MailBox(address, password, username, type , name);
		int flag = userService.save(user, mailBox);
		try {
			if (flag > 0) {
				// save success
				request.setAttribute("msg", "save successed!");
				request.getRequestDispatcher("adduser.jsp").forward(request,
						response);
			} else {
				// save failed
				request.setAttribute("msg", "save failed!");
				request.getRequestDispatcher("adduser.jsp").forward(request,
						response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	//FIXME
	public void showInfo(HttpServletRequest request,
			HttpServletResponse response) {
	}

	@Override
	public void updateInfo(HttpServletRequest request,
			HttpServletResponse response) {
		MailBox mailBox = (MailBox) request.getSession().getAttribute("curMailBox");
		int id = mailBox.getRoleID();
		System.out.println("调用了");
		String name = request.getParameter("username");
		int dept = Integer.parseInt(request.getParameter("college"));
		int major = Integer.parseInt(request.getParameter("depart"));
		int classes = Integer.parseInt(request.getParameter("classes"));
		String grade = request.getParameter("grade");
		String role = request.getParameter("role");
		User user = new User(id , name , role , dept , major , classes , grade);
		int flag = userService.updateInfo(user);
		try {
			if (flag > 0) {
				// update success
				request.setAttribute("msg", "update successed!");
				request.getRequestDispatcher("updateinfo.jsp").forward(request,
						response);
			} else {
				// update failed
				request.setAttribute("msg", "update failed!");
				request.getRequestDispatcher("updateinfo.jsp").forward(request,
						response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
