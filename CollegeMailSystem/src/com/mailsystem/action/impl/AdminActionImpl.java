package com.mailsystem.action.impl;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.mailsystem.action.AdminAction;
import com.mailsystem.bean.Admin;
import com.mailsystem.bean.Agency;
import com.mailsystem.bean.MailBox;
import com.mailsystem.service.AdminService;
import com.mailsystem.service.AgencyService;
import com.mailsystem.service.impl.AdminServiceImpl;
import com.mailsystem.service.impl.AgencyServiceImpl;

public class AdminActionImpl implements AdminAction {
	
	private AdminService adminService;
	private AgencyService agencyService;
	
	public AdminActionImpl(){
		this.adminService = new AdminServiceImpl();
		this.agencyService = new AgencyServiceImpl();
	}

	@Override
	public void save(HttpServletRequest request, HttpServletResponse response) {
		String username = request.getParameter("username");
		String address = request.getParameter("address");
		String password = request.getParameter("pwd");
		String name = request.getParameter("name");
		int type = Integer.parseInt(request.getParameter("type"));
		Admin admin = new Admin(username);
		MailBox mailBox = new MailBox(address, password, username, type , name);
		int flag = adminService.save(admin, mailBox);
		try {
			if (flag > 0) {
				// save success
				request.setAttribute("msg", "save successed!");
				request.getRequestDispatcher("adduser.jsp").forward(request,
						response);
			} else {
				// save failed
				request.setAttribute("msg", "save failed!");
				request.getRequestDispatcher("adduser.jsp").forward(request,
						response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void getInfo(HttpServletRequest request, HttpServletResponse response) {
		int oper = Integer.parseInt(request.getParameter("oper"));
		List<Agency> colleges = new ArrayList<Agency>();
		List<Agency> departs = new ArrayList<Agency>();
		List<Agency> league = new ArrayList<Agency>();
		if (oper == 0) {
			// 得到所有学院
			int school = Integer.parseInt(request.getParameter("school"));
			colleges = agencyService
					.getAgencyListByType(school, Agency.COLLEGE);
			league = agencyService.getAgencyListByType(Agency.LEAGUE);
			departs = agencyService.getAgencyListByType(Agency.DEPART);
			System.out.println("有多少系？" + departs.size());
			request.getSession().setAttribute("colleges", colleges);
			request.getSession().setAttribute("league", league);
			request.getSession().setAttribute("departs", departs);
		} else if (oper == 1) {
			// 得到所有系
			int college = Integer.parseInt(request.getParameter("college"));
			departs = agencyService.getAgencyListByType(college, Agency.DEPART);
			System.out.println("DEPARTS: " + departs.size());
			JSONArray depts = JSONArray.fromObject(departs);
			JSONObject dps = new JSONObject();
			dps.put("depts" , depts);
			PrintWriter out;
			try {// 变成JSON对象在前台输出
				out = response.getWriter();
				out.print(dps);
				out.flush();
				out.close();
				return;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			request.getRequestDispatcher("adduser.jsp").forward(request,
					response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
