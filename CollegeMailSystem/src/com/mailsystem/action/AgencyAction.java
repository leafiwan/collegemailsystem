package com.mailsystem.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface AgencyAction {

	/**
	 * 将一个单位用户存入数据库
	 * @param request
	 * @param response
	 */
	public void save(HttpServletRequest request , HttpServletResponse response);
	
	/**
	 * 显示机构信息
	 * @param request
	 * @param response
	 */
	public void showInfo(HttpServletRequest request , HttpServletResponse response);
	
}
