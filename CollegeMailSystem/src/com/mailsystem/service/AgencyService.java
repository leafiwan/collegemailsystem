package com.mailsystem.service;

import java.util.List;

import com.mailsystem.bean.Agency;
import com.mailsystem.bean.MailBox;

public interface AgencyService {

	/**
	 * 将一个单位用户及其邮箱存入数据库
	 * @param user
	 * @param mailBox
	 * @return
	 */
	public int save(Agency agency , MailBox mailBox);

	/**
	 * 
	 * @param parentAgencyId
	 * @param type: 机构的类型
	 * @return
	 */
	public List<Agency> getAgencyListByType(int parentAgencyId , int type);
	
	/**
	 * 
	 * @param type: 机构的类型
	 * @return
	 */
	public List<Agency> getAgencyListByType(int type);
	
	public List<MailBox> getContacts(Agency agency , MailBox mailBox);
	
	public Agency getAgencyById(int id);
}
