package com.mailsystem.service;

import com.mailsystem.bean.MailBox;

public interface MailBoxService {
	
	/**
	 * 
	 * @param mailBox
	 * @return
	 */
	public MailBox isMailBoxExist(MailBox mailBox);

	/**
	 * 更新密码
	 * @param mailBox
	 * @return
	 */
	public int updatePassword(MailBox mailBox , String oldPassword);
}
