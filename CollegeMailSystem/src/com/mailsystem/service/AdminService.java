package com.mailsystem.service;

import java.util.List;

import com.mailsystem.bean.Admin;
import com.mailsystem.bean.MailBox;

public interface AdminService {

	/**
	 * 将一个管理员用户及其邮箱存入数据库
	 * @param user
	 * @param mailBox
	 * @return
	 */
	public int save(Admin admin , MailBox mailBox);
	
	public List<MailBox> getContacts(Admin admin, MailBox mailBox);

	public Admin getAdminById(int id);
}
