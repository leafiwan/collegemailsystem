package com.mailsystem.service;

import java.util.List;

import com.mailsystem.bean.MailBox;
import com.mailsystem.bean.User;

public interface UserService {

	/**
	 * 将一个个人用户及其邮箱存入数据库
	 * @param user
	 * @param mailBox
	 * @return
	 */
	public int save(User user , MailBox mailBox);
	
	/**
	 * 根据用户ID得到用户对象
	 * @param id
	 * @return
	 */
	public User getUserById(int id);
	
	public List<MailBox> getContacts(User user , MailBox mailBox);
	
	public int updateInfo(User user);
}
