package com.mailsystem.service.impl;

import java.util.List;

import com.mailsystem.bean.DBUser;
import com.mailsystem.bean.MailBox;
import com.mailsystem.bean.User;
import com.mailsystem.dao.UserDao;
import com.mailsystem.dao.impl.UserDaoImpl;
import com.mailsystem.mail.MailUser;
import com.mailsystem.service.UserService;

public class UserServiceImpl implements UserService {
	
	private UserDao userDao;
	
	public UserServiceImpl(){
		this.userDao = new UserDaoImpl();
	}

	@Override
	public int save(User user, MailBox mailBox) {
		// save user in mail server database
		DBUser dbUser = new DBUser(mailBox.getUsername() , mailBox.getPassword());
		if(MailUser.addUserByDB(dbUser)){
			// if save it in mail server database then save user and mailbox
			return userDao.save(user, mailBox);
		} else {
			return 0;
		}
	}

	@Override
	public List<MailBox> getContacts(User user, MailBox mailBox) {
		return userDao.getContacts(user, mailBox);
	}

	@Override
	public User getUserById(int id) {
		return userDao.getUserById(id);
	}

	@Override
	public int updateInfo(User user) {
		return userDao.updateInfo(user);
	}

}
