package com.mailsystem.service.impl;

import com.mailsystem.bean.MailBox;
import com.mailsystem.dao.MailBoxDao;
import com.mailsystem.dao.impl.MailBoxDaoImpl;
import com.mailsystem.service.MailBoxService;

public class MailBoxServiceImpl implements MailBoxService{

	private MailBoxDao mailBoxDao;
	
	public MailBoxServiceImpl(){
		mailBoxDao = new MailBoxDaoImpl();
	}
	
	@Override
	public MailBox isMailBoxExist(MailBox mailBox) {
		return mailBoxDao.isMailBoxExist(mailBox);
	}

	@Override
	public int updatePassword(MailBox mailBox, String oldPassword) {
		MailBox oldBox = new MailBox(mailBox.getAddress() , oldPassword);
		if(mailBoxDao.isMailBoxExist(oldBox) == null){
			//旧密码输入错误，或者不存在该用户 
			return 0;
		} else {
			return mailBoxDao.updatePassword(mailBox);
		}
	}

}
