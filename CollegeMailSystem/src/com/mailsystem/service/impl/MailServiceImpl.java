package com.mailsystem.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import com.mailsystem.bean.Mail;
import com.mailsystem.bean.MailBox;
import com.mailsystem.common.Page;
import com.mailsystem.common.ReceiveView;
import com.mailsystem.dao.MailDao;
import com.mailsystem.dao.impl.MailDaoImpl;
import com.mailsystem.service.MailService;

public class MailServiceImpl implements MailService {

	private MailDao mailDao;

	public MailServiceImpl() {
		mailDao = new MailDaoImpl();
	}

	@Override
	public List<ReceiveView> getMails(MailBox mailBox, Page page) {
		return mailDao.getMails(mailBox, page);
	}

	@Override
	public int getMailsCount(MailBox mailBox) {
		return mailDao.getMailsCount(mailBox);
	}

	@Override
	public int save(Mail mail, MailBox mailBox) {
		return mailDao.save(mail, mailBox);
	}

	@Override
	public void produceIntro(Mail mail) {
		String content = mail.getContent();
		String intro = "";
		if (content.length() >= 13) {
			intro = content.substring(0, 10) + "...";
			mail.setIntro(intro);
		} else {
			intro = content;
			mail.setIntro(intro);
		}
	}

	@Override
	public int delete(int id) {
		return mailDao.delete(id);
	}

	@Override
	public Mail getMailById(int id) {
		return mailDao.getMailById(id);
	}

	@Override
	public List<String> getFileList(Mail mail) {
		List<String> fileList = null;
		String files = mail.getFileAttachment();
		if (files != null && files != "") {//有附件
			fileList = new ArrayList<String>();
			StringTokenizer token = new StringTokenizer(
					mail.getFileAttachment(), "|");
			while (token.hasMoreTokens()) {
				fileList.add(token.nextToken());
			}
			return fileList;
		} else {//没附件
			return fileList;
		}
	}

}
