package com.mailsystem.service.impl;

import java.util.List;

import com.mailsystem.bean.Admin;
import com.mailsystem.bean.DBUser;
import com.mailsystem.bean.MailBox;
import com.mailsystem.dao.AdminDao;
import com.mailsystem.dao.impl.AdminDaoImpl;
import com.mailsystem.mail.MailUser;
import com.mailsystem.service.AdminService;

public class AdminServiceImpl implements AdminService {
	
	private AdminDao adminDao;
	
	public AdminServiceImpl(){
		this.adminDao = new AdminDaoImpl();
	}

	@Override
	public int save(Admin admin, MailBox mailBox) {
		// save user in mail server database
		DBUser dbUser = new DBUser(mailBox.getUsername() , mailBox.getPassword());
		if(MailUser.addUserByDB(dbUser)){
			// if save it in mail server database then save user and mailbox
			return adminDao.save(admin, mailBox);
		} else {
			return 0;
		}
	}

	@Override
	public List<MailBox> getContacts(Admin admin, MailBox mailBox) {
		return adminDao.getContacts(admin, mailBox);
	}

	@Override
	public Admin getAdminById(int id) {
		return adminDao.getAdminById(id);
	}

}
