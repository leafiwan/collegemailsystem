package com.mailsystem.service.impl;

import java.util.List;

import com.mailsystem.bean.Agency;
import com.mailsystem.bean.DBUser;
import com.mailsystem.bean.MailBox;
import com.mailsystem.dao.AgencyDao;
import com.mailsystem.dao.impl.AgencyDaoImpl;
import com.mailsystem.mail.MailUser;
import com.mailsystem.service.AgencyService;

public class AgencyServiceImpl implements AgencyService {
	
	private AgencyDao agencyDao;
	
	public AgencyServiceImpl(){
		this.agencyDao = new AgencyDaoImpl();
	}

	@Override
	public int save(Agency agency, MailBox mailBox) {
		// save user in mail server database
		DBUser dbUser = new DBUser(mailBox.getUsername() , mailBox.getPassword());
		if(MailUser.addUserByDB(dbUser)){
			// if save it in mail server database then save user and mailbox
			return agencyDao.save(agency, mailBox);
		} else {
			return 0;
		}
	}

	@Override
	public List<Agency> getAgencyListByType(int parentAgencyId, int type) {
		return agencyDao.getAgencyListByType(parentAgencyId, type);
	}

	@Override
	public List<Agency> getAgencyListByType(int type) {
		return agencyDao.getAgencyListByType(type);
	}

	@Override
	public List<MailBox> getContacts(Agency agency, MailBox mailBox) {
		return agencyDao.getContacts(agency, mailBox);
	}

	@Override
	public Agency getAgencyById(int id) {
		return agencyDao.getAgencyById(id);
	}

}
