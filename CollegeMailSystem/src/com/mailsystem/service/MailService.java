package com.mailsystem.service;

import java.util.List;

import com.mailsystem.bean.Mail;
import com.mailsystem.bean.MailBox;
import com.mailsystem.common.Page;
import com.mailsystem.common.ReceiveView;

public interface MailService {
	
	/**
	 * 
	 * @param mailBox
	 * @param page
	 * @return
	 */
	public List<ReceiveView> getMails(MailBox mailBox , Page page);
	
	/**
	 * 
	 * @param mailBox
	 * @return
	 */
	public int getMailsCount(MailBox mailBox);
	
	/**
	 * save a mail
	 * @param mail
	 * @param mailBox: the mailbox who send this mail
	 * @return
	 */
	public int save(Mail mail , MailBox mailBox);
	
	/**
	 * delete a mail
	 * @param id
	 * @return
	 */
	public int delete(int id);
	
	/**
	 * 生成邮件的介绍信息
	 * @param mail
	 */
	public void produceIntro(Mail mail);
	
	/**
	 * 根据ID得到一封邮件
	 * @param request
	 * @param response
	 */
	public Mail getMailById(int id);
	
	/**
	 * 得到该邮件附属文件的列单
	 * @param mail
	 * @return
	 */
	public List<String> getFileList(Mail mail);
}
