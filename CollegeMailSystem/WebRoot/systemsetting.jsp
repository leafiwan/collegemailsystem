<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'systemsetting.jsp' starting page</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<style type="text/css">
label {
	color: rgb(6, 128, 67);
}

a {
	color: rgb(6, 128, 67);
	text-decoration: none;
}

a:HOVER {
	color: rgb(248, 147, 29);
}

input {
	box-shadow: 0 0 4px rgb(174, 221, 129);
}

.button {
	width: 100px;
	height: 26px;
	background-color: rgb(38, 157, 128);
	color: #FFF;
	font-family: "微软雅黑";
	font-size: 14px;
	font-weight: 600;
}

.button:HOVER {
	background: rgb(248, 147, 29);
}

select {
	box-shadow: 0 0 8px rgb(174, 221, 129);
	width: 50px;
	text-align: right;
}

</style>
</head>

<body>
	<center>
		<h3>系统设定修改</h3>
		<form action="setting.do?method=changeRowPerPage">
			<label>设置收件箱每页显示的邮件数: </label> <select id="rowPerPage"
				name="rowPerPage">
				<option>5</option>
				<option>10</option>
				<option>15</option>
				<option>20</option>
			</select>
			<input class="button" type="submit" value="提交修改" />
		</form>
		<a target="home" href="home.jsp" >退出</a>
	</center>
</body>
</html>
