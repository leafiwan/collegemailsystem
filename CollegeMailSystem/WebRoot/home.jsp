<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'home.jsp' starting page</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">

<link rel="stylesheet" href="css/type.css">
<style type="text/css">
#welcome {
	margin: 2em;
}

p {
	font-size: 16px;
	color: rgb(6, 128, 67);
	font-weight: 600;
}

a {
	text-decoration: none;
	color: rgb(107,194,53);
}


a:HOVER {
	color: rgb(248, 147, 29);
}
</style>
</head>

<body>
	<div id="welcome">

		<p>
			欢迎您，<a href="#">${sessionScope.name}(${sessionScope.curMailBox.address
				})</a>
		</p>
	</div>
</body>
</html>
