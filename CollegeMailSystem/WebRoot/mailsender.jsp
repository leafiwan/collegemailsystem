<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'mailsender.jsp' starting page</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<script type="text/javascript">
	var fileAttachment = 0;
	var addressCount = 0;
	function addRow() {
		var mailTable = document.getElementById("mailTable");
		//添加一行
		var newTr = mailTable.insertRow(3);
		//添加两列
		var newTd0 = newTr.insertCell(0);
		var newTd1 = newTr.insertCell(1);
		//设置列内容和属性
		newTd0.innerHTML = "<label>添加附件:</label>";
		newTd1.innerHTML = "<input type='file' id=fileAttachment" + fileAttachment + " name=fileAttachment" + fileAttachment + " title='选择文件' size='70' />";
		fileAttachment++;
	}
	//添加联系人
	function addAddress(option) {
		var inputAddress = document.getElementById("address");
		var txt = option.text;
		var begin = txt.lastIndexOf("(") + 1;
		var end = txt.lastIndexOf(")");
		var address = txt.substring(begin, end) + ";";
		if (addressCount > 0) {
			var arr = inputAddress.value.split(";");
			var newAddress = "";
			var exist = false;
			for ( var i = 0; i < arr.length; i++) {
				if (arr[i] != "") {
					arr[i] += ";";
					if (arr[i] == address) {
						exist = true;
					} else {
						newAddress += arr[i];
					}
				}
			}
			if (exist) {
				inputAddress.value = newAddress;
				addressCount--;
			} else {
				inputAddress.value += address;
			}
		} else {
			inputAddress.value += address;
		}
		addressCount++;
	}
</script>
<style type="text/css">
body {
	color: rgb(6, 128, 67);
}

#left_sender {
	width: 70%;
	padding-left: 5px;
	padding-top: 10px;
	float: left;
}

#left_sender input,textarea {
	border: rgb(38, 157, 128) 1px solid;
	box-shadow: 0 0 4px rgb(174, 221, 129);
}

#left_sender .button {
	width: 60px;
	height: 24px;
	background-color: rgb(38, 157, 128);
	color: #FFF;
	font-family: "微软雅黑";
	font-size: 14px;
	font-weight: 400;
}

#left_sender .button:HOVER {
	background: rgb(248, 147, 29);
}

#conList {
	width: 28%;
	float: right;
	text-align: center;
}

#conList h3 {
	
}

#conList select {
	height: 360px;
	border: rgb(38, 157, 128) 1px solid;
	box-shadow: 0 0 4px rgb(174, 221, 129);
}
#conList option {
	margin: 2px;
	font-size: 14px;
}
</style>
</head>

<body>

	<div id="left_sender">
		<form action="mail.do?method=sendMail" enctype="multipart/form-data"
			method="post" id="send_form">
			<table id="mailTable">
				<tr>
					<td><b>收件人邮箱:</b>
					</td>
					<td><input type="text" id="address" name="address" size="80" />
					</td>
				</tr>
				<tr>
					<td><b>主题:</b>
					</td>
					<td><input type="text" id="subject" name="subject" size="80" />
					</td>
				</tr>
				<tr>
					<td><b>添加附件:</b>
					</td>
					<td><input type="button" id="upload" name="upload"
						title="添加附件" onclick="addRow()" class="button" value="添加" />
					</td>
				</tr>
				<tr>
					<td colspan="2"><textarea id="content" name="content"
							rows="20" cols="80"></textarea>
					</td>
				</tr>
				<tr>
					<td></td>
					<td align="right"><input class="button" type="submit"
						value="发送" />
					</td>
				</tr>
			</table>
		</form>
		<c:if test="${!empty (msg)}">
			<p style="color:red;" class="errorMsg">${msg}</p>
		</c:if>
	</div>
	<div id="conList">
		<h3>联系人名单</h3>
		<select multiple="multiple">
			<c:forEach var="item" items="${contacts }">
				<option onclick="addAddress(this)">${item.name
					}(${item.address })</option>
			</c:forEach>
		</select>
	</div>
</body>
</html>
