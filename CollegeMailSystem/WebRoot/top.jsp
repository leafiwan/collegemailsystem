<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="<%=basePath%>" />

<title>My JSP 'top.jsp' starting page</title>

<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
<meta http-equiv="description" content="This is my page" />
<style type="text/css">
body {
	color: rgb(174, 221, 129);
	font-family: "Microsoft Yahei", "微软雅黑", Tahoma, Arial, Helvetica,
		STHeiti, "Lucida Grande", "Lucida Sans";
	margin: 0px;
}

a {
	color: rgb(6, 128, 67);
	text-decoration: none;
}

a:HOVER {
	color: rgb(107, 194, 53);
}

#title {
	vertical-align: bottom;
	position: absolute;
	bottom: 5px;
	font-weight: bolder;
	color: rgb(174, 221, 129);
	font-size: 30px;
	padding-left: 24px;
	padding-bottom: 4px;
	text-shadow: -1px -1px 0 #fff,1px 1px 0 #333,1px 1px 0 #444;
}

#back {
	float: right;
	vertical-align: bottom;
	font-size: 14px;
	margin-right: 5px;
	padding-top: 3px;
}

.clr {
	clear: both;
}
</style>
</head>

<body>
	<div id="main">
		<div
			style="height:74px; text-align: left; font-size: 25px; position:relative; padding-left: 5px">
			<span id="title">高校邮件系统</span>
			<div id="back">
				<a href="systemsetting.jsp" target="home">系统设置</a> &nbsp;&nbsp; <a href="login.jsp" type="redict" target="mainFrame">退出</a>
			</div>
			<div class="clr"></div>
		</div>
		<div style="height:2px; background-color:rgb(6,128,67);"></div>
	</div>
</body>
</html>
