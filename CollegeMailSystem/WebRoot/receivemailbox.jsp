<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'receiveMailBox.jsp' starting page</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<style type="text/css">
a {
	text-decoration: none;
	color: rgb(6, 128, 67);
}

a:HOVER {
	color: rgb(248, 147, 29);
}

td {
	text-align: center;
}

.wrapper {
	width: 800px;
}

.head {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	color: blue;
	background: #ddd;
	color: rgb(6, 128, 67);
}

.head th {
	
}

#receiveBox {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	width: 100%;
	border-collapse: collapse;
	box-shadow: 0 0 8px rgb(174, 221, 129);
}

#receiveBox td,#receiveBox th {
	font-size: 1em;
	border: 1px solid #98bf21;
	padding: 3px 7px 2px 7px;
}

#receiveBox th {
	font-size: 1.1em;
	text-align: center;
	padding-top: 5px;
	padding-bottom: 4px;
	background-color: #A7C942;
	color: #ffffff;
}

#pageInfo {
	margin-right: 200px;
}

#pageChoose {
	margin-left: 160px;
}
</style>

<script type=text/javascript>
	function initRowColor() {
		var table = document.getElementById("receiveBox");
		for ( var i = 1; i < table.rows.length; i++)

		{
			table.rows[i].style.backgroundColor = (i % 2 == 0 ? '#ded'
					: '#FFFFFF');
			table.rows[i].onmouseout = function() {
				this.style.backgroundColor = (this.rowIndex % 2 == 0 ? '#ded'
						: '#FFFFFF');
			};
			table.rows[i].onmousemove = function() {
				this.style.backgroundColor = 'rgb(174,221,129)';
			};

		}
	}
</script>
</head>

<body onload="initRowColor()">
	<center>
		<div class="wrapper">
			<h2>收件箱</h2>
			<table id="receiveBox" border="0px">
				<tr class="head">
					<th width="60px">标记</th>
					<th width="100px">发件人</th>
					<th width="100px">标题</th>
					<th width="240px">内容</th>
					<th width="100px">发送时间</th>
					<th width="100px">操作</th>
				</tr>
				<c:forEach varStatus="i" var="mail" items="${receiveMails }">
					<tr class="">
						<td><input type="checkbox" name="mark" /></td>
						<td><a
							href="mail.do?method=getMailById&id=${mail.id }&sender=${mail.sendName
								}">${mail.sendName }</a></td>
						<td><a href="mail.do?method=getMailById&id=${mail.id }&sender=${mail.sendName
								}">${mail.subject }</a></td>
						<td><a href="mail.do?method=getMailById&id=${mail.id }&sender=${mail.sendName
								}">${mail.intro }</a></td>
						<td>${mail.date }</td>
						<td><a
							href="mail.do?method=deleteMail&id=${mail.id }&curPage=${curPage }">删除</a>
						</td>
					</tr>
				</c:forEach>
			</table>
			<br /> <span id="pageInfo">当前是第<b
				style="color:rgb(6 , 128 , 67);">${curPage }</b>页，共<b
				style="color: rgb(6 , 128 , 67);">${pageCount }</b>页</span> <span
				id="pageChoose"><a href="mail.do?method=getMails&jumpPage=1">第一页</a>&nbsp;
				<a href="mail.do?method=getMails&jumpPage=${curPage>1?curPage-1:1 }">上一页</a>&nbsp;
				<a
				href="mail.do?method=getMails&jumpPage=${curPage<pageCount?curPage+1:pageCount }">下一页</a>&nbsp;
				<a href="mail.do?method=getMails&jumpPage=${pageCount }">最后一页</a> </span>
		</div>
	</center>
</body>
</html>
