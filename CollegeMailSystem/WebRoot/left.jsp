<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'left.jsp' starting page</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/type.css">
<link rel="stylesheet" href="themes/base/jquery.ui.all.css">
<script src="js/jquery-1.5.1.js"></script>
<script src="ui/jquery.ui.core.js"></script>
<script src="ui/jquery.ui.widget.js"></script>
<script src="ui/jquery.ui.accordion.js"></script>
<script>
	$(document).ready(function() {
		$("#accordion").accordion();
	});
</script>
<style type="text/css">
a {
	text-decoration: none;
	color: rgb(38 , 157 ,128);
}

#chooseBu {
	color: rgb(38 , 157 ,128);
}
</style>

</head>

<body>
	<div id="accordion">
		<h4 id="chooseBu">
			<a href="#" target="home">发送邮件</a>
		</h4>
		<div style="padding-top: 2px">
			<a href="mailBox.do?method=getContacts" target="home">发送邮件</a><br />
			<c:if test="${sessionScope.type==3 || sessionScope.type==2}">
				<a href="mutimailsender.jsp" target="home">群发邮件</a>
				<br />
			</c:if>
		</div>
		<h4 id="chooseBu">
			<a href="#" target="home">收件箱</a>
		</h4>
		<div style="padding-top: 2px">
			<a href="mail.do?method=getMails&jumpPage=1" target="home">查看所有信件</a><br />
		</div>
		<h4>
			<a href="#" target="home">修改资料</a>
		</h4>
		<div style="padding-top: 2px">
			<a href="updatepwd.jsp" target="home">修改密码</a><br /> <a
				href="agency.do?method=showInfo&oper=0&school=4" target="home">修改资料</a><br />
		</div>
		<c:if test="${sessionScope.type==3}">
			<h4>
				<a href="#" target="home">添加用户</a>
			</h4>
			<div style="padding-top: 2px">
				<a href="admin.do?method=getInfo&oper=0&school=4" target="home">添加用户</a><br />
			</div>
		</c:if>
		<c:if test="${sessionScope.type==3}">
			<h4>
				<a href="#" target="home">管理用户</a>
			</h4>
			<div style="padding-top: 2px">
				<a href="#" target="home">个人用户</a><br /> <a href="#" target="home">单位用户</a><br />
			</div>
		</c:if>
	</div>
</body>
</html>
