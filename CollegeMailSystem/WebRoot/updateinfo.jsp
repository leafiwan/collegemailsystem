<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'updateinfo.jsp' starting page</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<script>
	var school = 4;//湖南师范大学
	var college = "";
	var depts = "";

	//创始ajax对像
	function createXmlHttp() {
		var xmlHttp;
		if (window.XMLHttpRequest) {
			xmlHttp = new XMLHttpRequest();//非ie
		} else if (window.ActiveXObject) {//IE
			try {
				xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
		}
		return xmlHttp;
	}

	//得到信息
	function getInfo() {
		//获取ajax对像
		var xmlHttp = createXmlHttp();
		//服务端的路径
		var url = "agency.do?";
		//获得学院
		college = document.getElementById("college").value;
		if (college != "") {
			//补全url
			url += "method=showInfo&oper=1&college=" + college;
		}
		//open
		xmlHttp.open("get", url, true);
		//监听器 ，回调函数
		xmlHttp.onreadystatechange = function() {
			if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
				//获取服务端的返回值  
				depts = xmlHttp.responseText;
				alert(depts);
			}
		}
		//send
		xmlHttp.send();
	}

	function submitForm() {
		var updateForm = document.getElementById("updateForm");
		updateForm.submit();
	}
</script>
<style type="text/css">
body {
	margin: 0px;
}

#main {
	text-align: center;
}

table {
	color: rgb(6, 128, 67);
	font-weight: 500;
	left: 38%;
	position: absolute;
}

table td {
	width: 120px;
	height: 30px;
}

table select,input {
	width: 180px;
	box-shadow: 0 0 4px rgb(174, 221, 129);
}

table button {
	width: 120px;
	height: 28px;
	background-color: rgb(38, 157, 128);
	color: #FFF;
	font-family: "微软雅黑";
	font-size: 14px;
	font-weight: 400;
}

table button:HOVER {
	background-color: rgb(248, 147, 29);
}
</style>

</head>

<body>
	<div id="main">
		<h3>修改资料</h3>
		<c:if test="${sessionScope.type==1}">
			<form action="user.do" method="post" id="updateForm">
				<input type="hidden" name="method" value="updateInfo" />
				<table>
					<tr>
						<td>用户姓名：</td>
						<td><input type="text" id="username" name="username"
							value="${sessionScope.curMailBox.name }">
						</td>
					</tr>
					<tr>
						<td>学院:</td>
						<td><select id="college" name="college" onchange="getInfo()">
								<c:forEach var="i" items="${colleges }">
									<option value="${i.id }">${i.name }</option>
								</c:forEach>
						</select>
						</td>
					</tr>
					<tr>
						<td>系：</td><td><select id="depart" name="depart">
								<c:forEach var="item" items="${departs }">
									<option value="${item.id }">${item.name }</option>
								</c:forEach>
						</select></td>
					</tr>
					<tr>
						<td>班级:</td>
						<td><select id="classes" name="classes">
								<option value="1">01班</option>
								<option value="2">02班</option>
								<option value="3">03班</option>
								<option value="4">04班</option>
								<option value="5">05班</option>
								<option value="6">06班</option>
								<option value="7">07班</option>
								<option value="8">08班</option>
								<option value="9">09班</option>
								<option value="10">10班</option>
								<option value="11">11班</option>
								<option value="12">12班</option>
								<option value="13">13班</option>
								<option value="14">14班</option>
								<option value="15">15班</option>
								<option value="16">16班</option>
						</select>
						</td>
					</tr>
					<tr>
						<td>年级:</td>
						<td><select id="grade" name="grade">
								<option>06级</option>
								<option>07级</option>
								<option>08级</option>
								<option>09级</option>
								<option>10级</option>
								<option>11级</option>
								<option>12级</option>
								<option>13级</option>
								<option>14级</option>
								<option>15级</option>
								<option>16级</option>
								<option>17级</option>
								<option>18级</option>
								<option>19级</option>
						</select>
						</td>
					</tr>
					<tr>
						<td>用户角色:</td>
						<td><select id="role" name="role">
							<option>学生</option>
							<option>老师</option>
							<option>行政人员</option>
						</select>
						</td>
					</tr>
					<tr>
						<td><input type="hidden" name="method" value="updateInfo" />
						</td>
						<td><button id="submitBu" onclick="updateForm()">修改用户资料</button>
						</td>
					</tr>
				</table>
			</form>
		</c:if>
	</div>
</body>
</html>
