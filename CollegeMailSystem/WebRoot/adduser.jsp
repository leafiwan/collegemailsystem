<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'adduser.jsp' starting page</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<script type="text/javascript">
	function changeType() {
		var type = document.getElementById("user_type").value;
		document.getElementById("type").value = document
				.getElementById("user_type").selectedIndex + 1;
		if (type == "个人用户") {
			document.getElementById("infoForm").action = "user.do";
			document.getElementById("d1").style.display = "inherit";
			document.getElementById("d2").style.display = "none";
			document.getElementById("d3").style.display = "none";
		} else if (type == "单位用户") {
			document.getElementById("infoForm").action = "agency.do";
			document.getElementById("d1").style.display = "none";
			document.getElementById("d2").style.display = "inherit";
			document.getElementById("d4").style.display = "none";
		} else if (type == "管理员用户") {
			document.getElementById("infoForm").action = "admin.do";
			document.getElementById("d1").style.display = "none";
			document.getElementById("d2").style.display = "none";
			document.getElementById("d3").style.display = "inherit";
		}
	}
	function submitForm() {
		document.getElementById("infoForm").submit();
	}
	// 改变机构类型时，修改提交表单
	function changeAgency() {
		document.getElementById("agType").value = document
				.getElementById("agency_type").selectedIndex + 1;
	}
	//名字不能为空
	function validateName(value) {

		//获取img  span
		var img1 = document.getElementById("img1");
		var span1 = document.getElementById("span1");
		if (value.trim().length < 1) {//用户名不能为空
			img1.src = "./images/invalid.gif";
			img1.style.display = "";
			span1.style.display = "";
			//修改层的innerHTML属性
			span1.innerHTML = "<font color='red'>用户名不能为空!</font>";
			return false;
		} else {

			// alert( "验证成功" );
			img1.src = "./images/valid.gif";
			img1.style.display = "";
			span1.style.display = "";

			span1.innerHTML = "<font color='blue'>用户名验证成功!</font>";

			return true;
		}
	}

	//验证密码是否一致
	function password(value) {
		var span3 = document.getElementById("span3");
		//var img3 = document.getElementById( "img3" );
		if (document.getElementById("pwd").value != document
				.getElementById("pwdcfm").value) {
			// img3.src="./images/invalid.gif";
			// img3.style.display = "";
			span3.style.display = "";
			span3.innerHTML = "<font color='red'>两次密码输入不相同，请重新输入！</font>";
			return false;
		} else {
			span3.style.display = "";
			span3.innerHTML = "<font color='blue'>两次输入相同。</font>";
		}
		return true;
	}
	//显示密码
	function showpwd() {
		if (document.getElementById("pwd").type == "password") {
			document.getElementById("pwd").type = "text";
			document.getElementById("pwd").value = document
					.getElementById("pwd").value;
			document.getElementById("pwdcfm").type = "text";
			document.getElementById("pwdcfm").value = document
					.getElementById("pwdcfm").value;
			return true;
		} else {
			document.getElementById("pwd").type = "password";
			document.getElementById("pwdcfm").type = "password";
			return true;
		}
	}
	function getMailBox() {
		document.getElementById("address").value = document
				.getElementById("username").value
				+ "@test.com";
	}
</script>
<style type="text/css">
body {
	margin: 0px;
	text-align: center;
}

#d1 {
	display: inherit;
}

label {
	position: fixed;
	left: 400px;
	width: 100px;
	text-align: right;
	color: rgb(6, 128, 67);
}

form input,select {
	position: relative;
	left: 20px;
	width: 140px;
	box-shadow: 0 0 8px rgb(174, 221, 129);
}

.tips {
	position: fixed;
	left: 700px;
}

button {
	width: 120px;
	height: 24px;
	background-color: rgb(38, 157, 128);
	color: #FFF;
	font-family: "微软雅黑";
	font-size: 14px;
	font-weight: 400;
}

button:HOVER {
	background-color: rgb(248, 147, 29);
}
</style>
</head>

<body>
	<h2>添加用户</h2>
	<form id="infoForm" action="user.do" method="post">
		<input type="hidden" id="method" name="method" value="save" /> <select
			id="user_type" onchange="changeType()">
			<option>个人用户</option>
			<option>单位用户</option>
			<option>管理员用户</option>
		</select> <br /> <input type="hidden" id="type" name="type" value="1" /> <span
			class="info"><label>账号:</label> <input type="text"
			id="username" name="username" onkeyup="getMailBox()"
			onblur="validateName(this.value)" /><span class="tips"> <img
				id="img1" style="display:none"></img> <span id="span1"
				style="display:none"></span> </span> </span> <br /> <span class="info"><label>邮箱号:</label>
			<input readonly="readonly" type="text" id="address" name="address" /><span
			class="tips"></span> </span><br /> <span class="info"><label>用户名:</label>
			<input type="text" id="name" name="name" /><span class="tips"></span>
		</span> <br /> <span class="info"><label>密码:</label> <input
			type="password" id="pwd" name="pwd" /><span class="tips"></span> </span> <br />
		<span class="info"><label>确认密码:</label> <input type="password"
			id="pwdcfm" name="pwdcfm" onblur="password(this.value)" /><span
			class="tips"> <span id="span3" style="display:none;"> </span>
		</span> </span> <br />
		<div id="d1">
			<label>学院:</label><select id="college" name="college"
				onchange="getInfo()">
				<c:forEach var="i" items="${colleges }">
					<option value="${i.id }">${i.name }</option>
				</c:forEach>
			</select> <br /> <label>系:</label><select id="depart" name="depart">
				<c:forEach var="item" items="${departs }">
					<option value="${item.id }">${item.name }</option>
				</c:forEach>
			</select><br /> <label>班级: </label><select id="classes" name="classes">
				<option value="1">01班</option>
				<option value="2">02班</option>
				<option value="3">03班</option>
				<option value="4">04班</option>
				<option value="5">05班</option>
				<option value="6">06班</option>
				<option value="7">07班</option>
				<option value="8">08班</option>
				<option value="9">09班</option>
				<option value="10">10班</option>
				<option value="11">11班</option>
				<option value="12">12班</option>
				<option value="13">13班</option>
				<option value="14">14班</option>
				<option value="15">15班</option>
				<option value="16">16班</option>
			</select><br /> <label>年级:</label><select id="grade" name="grade">
				<option>06级</option>
				<option>07级</option>
				<option>08级</option>
				<option>09级</option>
				<option>10级</option>
				<option>11级</option>
				<option>12级</option>
				<option>13级</option>
				<option>14级</option>
				<option>15级</option>
				<option>16级</option>
				<option>17级</option>
				<option>18级</option>
				<option>19级</option>
			</select><br /> <label>职称:</label><input type="text" id="title" name="title" /><br />
			<label>角色:</label><input type="text" id="role" name="role" /><br />
		</div>
		<div id="d2" style="display: none;">
			<input type="hidden" id="agType" name="agType" value="1" /> <select
				id="agency_type" name="agency_type" onchange="changeAgency()">
				<option>班级机构</option>
				<option>社团机构</option>
				<option>年级机构</option>
				<option>系机构</option>
				<option>学院机构</option>
				<option>学校机构</option>
			</select>
		</div>
		<div id="d3" style="display: none;"></div>
		<span><label>显示密码: </label><input type="checkbox" id="show_pwd"
			name="show_pwd" onclick="showpwd()" />
		<button id="submitBu" onclick="submitForm()">添加用户</button></span>
	</form>
	<c:if test="${!empty (msg)}">
		<p style="color:red;" class="errorMsg">${msg}</p>
	</c:if>
</body>
</html>
