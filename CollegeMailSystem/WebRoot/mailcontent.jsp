<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'mailcontent.jsp' starting page</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">

<style type="text/css">
#intro {
	background: #efe;
	color: rgb(6, 128, 67);
	height: auto;
	padding-bottom: 20px;
}

#intro a {
	text-decoration: none;
	color: rgb(179,214,110);
}

#intro a:HOVER {
	color: rgb(248, 147, 29);
}

#subject {
	color: rgb(6, 128, 67);
	background: #efe;
	padding-top: 10px;
}

#subject p {
	margin-top: 8px;
	margin-left: 25px;
	font-size: 12px;
	display: inline;
	color: #000;
	margin-left: 25px;
}

#subject h3 {
	margin-left: 1em;
	margin-top: 0px;
	font-size: 20px;
	font-weight: 600;
}

#from {
	margin-top: 10px;
	width: 100%;
}

label {
	color: #000;
	margin-left: 25px;
	font-size: 12px;
	font-size: 12px;
}

#content {
	text-align: left;
	width: 800px;
}

pre {
	text-indent: 2em;
}

#fileAttach label {
	text-align: left;
}

#fileAttach a {
	text-decoration: none;
	color: rgb(179,214,110);
}

#fileAttach a:HOVER {
	color: rgb(248, 147, 29);
}
</style>

</head>

<body>
	<div class="wrapper">
		<div id="intro">
			<div id="subject">
				<h3>${mail.subject }</h3>
			</div>
			<span id="from"><label>发件人：</label><a href="#">${senderName
					}(${mail.senderAddress })</a> </span><br /> <span id="sendTime"><label>发送于：</label>${mail.sendDate
				}</span><br /> <span id="fileSize"><label>附件：</label>${fn:length(fileList)
				}个</span><br /> <span id="receiver"><label>收件人：</label>${curMailBox.name
				}(${mail.receiverAddress })</span><br />
		</div>
		<center>
			<div id="content">
				<pre>${mail.content }</pre>
			</div>
			<c:if test="${!empty(fileList) }">
				<div id="fileAttach">
					<label>附件：</label><br />
					<c:forEach var="f" items="${fileList }">
						<a title="下载" href="mail.do?method=download&fileName=${f }">${f
							}</a>
						<br />
					</c:forEach>
				</div>
			</c:if>
		</center>
	</div>
</body>
</html>
