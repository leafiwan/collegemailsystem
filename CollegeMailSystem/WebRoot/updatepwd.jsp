<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'updatepwd.jsp' starting page</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

</head>

<body>
	<form action="mailBox.do" method="post">
		<input type="hidden" id="method" name="method" value="updatePassword" />
		旧密码: <input type="password" id="old_pwd" name="old_pwd" /><br />
		新密码: <input type="password" id="new_pwd" name="new_pwd" /><br />
		新密码确认: <input type="password" id="new_pwdcfm" name="new_pwdcfm" /><br />
		<input type="submit" value="修改密码" />
	</form>
	<c:if test="${!empty (msg)}">
		<p style="color:red;" class="errorMsg">${msg}</p>
	</c:if>
</body>
</html>
