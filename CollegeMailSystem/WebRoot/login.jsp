<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="<%=basePath%>" />

<title>高校邮件系统登录</title>
<meta http-equiv="keywords" content="高校邮件系统,邮件" />
<meta http-equiv="description" content="校内邮件" />
<style type="text/css">
body {
	background: rgb(174,221,129);
	margin-top: 0;
	font-family: "Microsoft Yahei", "微软雅黑", Tahoma, Arial, Helvetica,
		STHeiti;
}

#wrapper {
	width: 960px;
	height: 567px;
	position: absolute;
	top: 0;
	left: 50%;
	margin-left: -480px;
}

h1 {
	width: 100%;
	height: 95px;
	position: absolute;
	top: 172px;
	text-indent: -999999em;
}

h2 {
	width: 100%;
	height: 23px;
	position: absolute;
	font-size: 32px;
	top: 222px;
	left: 180px;
}

form {
	width: 100%;
	text-align: center;
	position: absolute;
	top: 370px;
}

form .input {
	padding: 3px;
	font-size: 16px;
	width: 170px;
	height: 23px;
	line-height: 23px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	border-radius: 6px;
	background: #FFFFFF;
	border: 1px solid #245f95;
	-webkit-box-shadow: 0 0 8px #666666;
	-moz-box-shadow: 0 0 8px #666666;
	box-shadow: 0 0 8px #666666;
}

#register {
	width: 100%;
	display: block;
	position: absolute;
	top: 470px;
	text-align: center;
}

#register a {
	text-decoration: none;
	color: #fff;
	padding: 5px 40px;
	border: 1px solid #0431B4;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	font-size: 14px;
	background: #013ADF;
}

.button {
	background: rgb(107,194,53);
	width: 50px;
	color: #ffffff;
	font-size: 16px;
	padding: 5px 8px;
	text-decoration: none;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	border-radius: 6px;
	-webkit-box-shadow: 0px 1px 3px #666666;
	-moz-box-shadow: 0px 1px 3px #666666;
	text-shadow: 1px 1px #184bb3;
	border: solid #04226b 1px;
	background: -webkit-gradient(linear, 0 0, 0 100%, from(#7FFF00),
		to(#458B00) );
	background: -moz-linear-gradient(top, #7FFF00, #458B00);
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7FFF00',
		endColorstr='#458B00' );
	cursor: pointer;
	font-family: "Microsoft Yahei", "微软雅黑", Tahoma, Arial, Helvetica,
		STHeiti;
}

.button:hover {
	background: #008B45;
}

#remember {
	width: 420px;
	height: 20px;
	line-height: 20px;
	text-align: left;
	margin: 0 auto;
	font-size: 12px;
	color: #008B45;
	text-shadow: 1px 1px #a9c4fb;
	position: relative;
}

#footer {
	color: #008B45;
	text-align: center;
	font-size: 12px;
	position: absolute;
	width: 100%;
	top: 540px;
	text-shadow: 1px 1px #a9c4fb;
}

#remember a {
	text-decoration: none;
	color: #008B45;
	position: absolute;
	right: 0;
}

#remember a:hover {
	color: rgb(6,128,67);
	text-shadow: none;
}

label.inner {
	display: none;
	cursor: text;
	position: absolute;
	left: 0.5em;
	top: 0px;
	color: #555;
	height: 31px;
	line-height: 31px;
}

div.wrapper {
	margin: 0;
	padding: 0;
	float: left;
	width: 178px;
	margin: 0 2px;
	height: 31px;
	line-height: 31px;
	position: relative;
}

.errorMsg {
	width: 170px;
	margin: 0 auto;
	padding: 5px 10px;
	background: rgb(107 , 194 ,53);
	position: relative;
	top: 305px;
	text-align: center;
	font-size: 13px;
	color: #fff;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	border-radius: 5px;
	border: 1px solid #000;
}

#form-wrapper {
	width: 415px;
	margin: 0 auto;
}
</style>

<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/commons.js"></script>
<script type="text/javascript">
	$(function() {
		optimizeCheckbox($('#remember input'));
		initInnerLabelInput($("form input[name='address']"));
		initInnerLabelInput($("form input[name='password']"));
	});
</script>
</head>

<body>
	<div id="wrapper">
		<h2>高校邮件系统</h2>
		<c:if test="${!empty (msg)}">
			<div class="errorMsg">
				<p>${msg}</p>
			</div>
		</c:if>
		<form action="mailBox.do" method="post">
			<input type="hidden" name="method" value="isMailBoxExist" />
			<div id="form-wrapper">
				<div class="wrapper">
					<label class="inner" for="address">邮箱</label> 
					<input class="input" type="text" name="address" />
				</div>
				<div class="wrapper">
					<label class="inner" for="password">密码</label> 
					<input type="password" class="input" name="password" />
				</div>
				<input type="submit" class="button" value="登录" />
				<p id="remember">
					<input type="checkbox" name="rememberMe" /><label for="rememberMe">记住密码</label>
					<a href="#">忘记密码?</a>
				</p>
			</div>
		</form>
	</div>
</body>
</html>
