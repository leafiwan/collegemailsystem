<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <base href="<%=basePath%>" />
    
    <title>共享书籍 - 蚂蚁书屋</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="keywords" content="蚂蚁书屋,共享书籍" />
	<meta http-equiv="description" content="蚂蚁书屋共享书籍" />
	
	
	
	<link rel="stylesheet" href="css/common.css" type="text/css" />	
	<link rel="stylesheet" href="css/other.css" type="text/css" />	
	
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/commons.js"></script>
	<script type="text/javascript">
	
	$(function() {
		convertAnchorToSubmit($('#shareSubmit'));
	});
	
	</script>
  </head>
  
  <body>
    
    <%@include file="commons/header.jsp" %>
    
	<div id="main">
		<h2 class="header2">共享书籍</h2>	
		<form action="" method="post" id="share">
			<s:if test="#parameters['msg'][0] == 'wrongISBN'">
				<div class="tips error">
					ISBN编号错误
				</div>
			</s:if>
			<p>
				<label for="isbn">请输入书籍的 ISBN 编号：</label>
				<a id="whatIsISBN" href="/html/help.html#what-is-isbn" target="_blank">什么是 ISBN 编号？</a>
			</p>
			<p>
				<input class="input" name="isbn" />
			</p>
			<p>
				<a id="shareSubmit" href="javascript:void(0);" class="sdbtn"><span class="sdbtn-inner"></span>提交</a>
			</p>
		</form>
		<div class="clr"></div>
	</div>   
    
    <%@include file="commons/footer.jsp" %>
  </body>
</html>
