<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <base href="<%=basePath%>" />
    
    <title>借阅请求 - 蚂蚁书屋</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="keywords" content="蚂蚁书屋,借阅请求" />
	<meta http-equiv="description" content="我的借阅请求" />
	
	<link rel="stylesheet" href="css/common.css" type="text/css" />	
	<link rel="stylesheet" href="css/home.css" type="text/css" />	
	
	
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/jquery.blockUI.js"></script>
	<script type="text/javascript" src="js/commons.js"></script>
	<script type="text/javascript" src="js/borrowRequest.js"></script>
	<script type="text/javascript">
	
		$(initMinePage);
	
	</script>
	
  </head>
  
  <body>
    
    <%@include file="../commons/header.jsp" %>
    
	<div id="main">
		<div id="left-bar">
			<%@ include file="../commons/leftbar-nav.jsp" %>
		</div>
		
		<div id="content-wrapper">
				<h2 class="header2">借阅请求</h2>
				<div class="tab-div">
					<ul class="tabs">
						<s:if test="type == 0">
							<li class="current"><a href="javascript:void(0);">收到的请求(<s:property value="currentUser.receivedBorrowRequestsCount" />)</a></li>
							<li><a href="/borrowRequest/mine?type=1">发出的请求(<s:property value="currentUser.sendedBorrowRequestsCount" />)</a></li>
						</s:if>
						<s:else>
							<li><a href="/borrowRequest/mine">收到的请求(<s:property value="currentUser.receivedBorrowRequestsCount" />)</a></li>
							<li class="current"><a href="javascript:void(0);">发出的请求(<s:property value="currentUser.sendedBorrowRequestsCount" />)</a></li>
						</s:else>
						
						<div class="clr"></div>
					</ul>
					<div class="tab-content">
						<s:if test="requests.empty">
							<div class="tips">
								没有任何请求
							</div>
						</s:if>
						<s:else>
							<div class="moudule" id="borrowRequest-list">
								<s:if test="type == 0">
									<s:iterator value="requests">
									<div class="item">
										<a class="savatar"  href="/user/<s:property value="user.id" />"><img alt="<s:property value="user.name" />" src="<s:property value="user.smallAvatar" />" /></a>
										<div class="info">
											<a href="/user/<s:property value="user.id" />"><s:property value="user.name" escape="false" /></a> 想向我借阅<a href="/book/<s:property value="sharing.book.id" />">《<s:property value="sharing.book.title" />》</a>
											<span class="date">(<s:date name="requestDate" nice="true" />)</span>
											<a href="/book/<s:property value="sharing.book.id" />" class="scover"><img src="<s:property value="sharing.book.smallImg" />" alt="<s:property value="sharing.book.title" escape="false" />" /></a>
											<div class="operations">
												<a class="agree" href="/borrowRequest/accept?rid=<s:property value="id" />">同意</a>
												<a class="disagree" href="/borrowRequest/reject?rid=<s:property value="id" />">拒绝</a>
											</div>
										</div>
										<div class="clr"></div>
									</div>
									</s:iterator>
								</s:if>
								<s:else>
									<s:iterator value="requests">
									<div class="item">
										<a class="savatar" href="/user/<s:property value="sharing.user.id" />"><img alt="<s:property value="sharing.user.name" />" src="<s:property value="sharing.user.smallAvatar" />" /></a>
										<div class="info">
											<a href="/user/<s:property value="sharing.user.id" />"><s:property value="sharing.user.name" escape="false" /></a> 尚未处理我借阅<a href="/book/<s:property value="sharing.book.id" />">《<s:property value="sharing.book.title" escape="false" />》</a>的请求
											<span class="date">(<s:date name="requestDate" nice="true" />)</span>
											<a href="/book/<s:property value="sharing.book.id" />" class="scover"><img src="<s:property value="sharing.book.smallImg" />" alt="<s:property value="sharing.book.title" escape="false" />" /></a>
											<div class="operations">
												<a class="cancel" href="/borrowRequest/cancel?rid=<s:property value="id" />">取消请求</a>
											</div>
										</div>
										<div class="clr"></div>
									</div>
									</s:iterator>
								</s:else>
							
							</div>
							<jshy:paginator value="page" />
						</s:else>
						
						
						<!-- 
						<div class="moudule" id="borrowRequest-list">
							
						</div>
						 -->
					</div>
				</div>
		</div>
		
		<div id="right-bar">
		
		</div>
		
		<div class="clr"></div>
	</div>   
    
    <%@include file="../commons/footer.jsp" %>
  </body>
</html>
