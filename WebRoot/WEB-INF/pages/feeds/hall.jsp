<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <base href="<%=basePath%>" />
    
    <title><s:property value="#session.get(@com.jieshuhuiyou.Config@SESSION_KEY_USER_NAME)" escape="false" />的新鲜事 - 蚂蚁书屋</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="keywords" content="蚂蚁书屋,新鲜事" />
	<meta http-equiv="description" content="我在蚂蚁书屋关注人的新鲜事" />
	<script type="text/javascript" src="js/html5.js"></script>
	
	
	
	<link rel="stylesheet" href="css/common.css" type="text/css" />	
	<link rel="stylesheet" href="css/hall.css" type="text/css" />	
	
	
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/commons.js"></script>
	<script type="text/javascript" src="js/hall.js"></script>
	
	<script>
	//获取textarea光标，插入数据的方法
	function getValue(objid,str){ 
	var myField=document.getElementById(""+objid);
	//IE浏览器
	  if (document.selection) { 
	    myField.focus(); 
	    sel = document.selection.createRange(); 
	    sel.text = str; 
	    sel.select(); 
	  } 
	
	  //火狐/网景 浏览器 
	  else if (myField.selectionStart || myField.selectionStart == '0') 
	  { 
	  //得到光标前的位置
	    var startPos = myField.selectionStart; 
	    //得到光标后的位置
	    var endPos = myField.selectionEnd; 
	    // 在加入数据之前获得滚动条的高度 
	    var restoreTop = myField.scrollTop; 
	    myField.value = myField.value.substring(0, startPos) + str + myField.value.substring(endPos, myField.value.length); 
	    //如果滚动条高度大于0
	    if (restoreTop > 0) { 
	      // 返回
	      myField.scrollTop = restoreTop;
	    } 
	    myField.focus(); 
	    myField.selectionStart = startPos + str.length; 
	    myField.selectionEnd = startPos + str.length; 
	  } 
	  else { 
	    myField.value += str; 
	    myField.focus(); 
	  } 
}
	</script>
	
	<script>
	$(function(){
		//状态模块的脚本
	var input = $('#input');
	var facesPanel = $('#facesPanel');
	var face = $('#face');
	var isFocus = false;
	var feedList = $('#feed-list');
	var page = 1;
	var pageSize = 10;
	
	input.bind('keyup change',function(){
			$(this).val($(this).val().replaceAll('\n',''));
			isFocus = true;
			var n = parseInt(256 - $(this).val().length);
			if(n > 0)
			$('#cNum').html(n);
			else
			$('#cNum').html('<span style="color:red">' + n + '</span>');
		});
		
		//发表状态
		$('#publish').bind('click',function(){
		if(confirm('确定发表吗？')){
				if(input.val().length < 257 && input.val().length != 0 && input.val() != '写写你的心情吧...'){
					$.get('/hall/addCondition',{'condition':input.val()},function(){
					feedList.html("");
					ajaxGetFeedsList(page = 1,pageSize,feedList);
					input.val('写写你的心情吧...');
					input.animate({height:'40px'},500);
					isFocus = false;
					});
				}else{
					alert('对不起，您输入字数过多，或者内容为空！');
				}
			}
		});
		
		//鼠标移出状态事件绑定
		input.bind('focusout',function(evt){
      	  
			if($(this).val() == ""){
				$(this).val('写写你的心情吧...');
				isFocus = false;
				$(this).animate({height:'40px'},500);
			}
		});
		
		//鼠标移入状态事件绑定
		input.bind('focus',function(){
			if(!isFocus){
				$(this).val('');
				isFocus = true;
				$(this).animate({height:'80px'},500);
			}
		});
		
		//异步加载表情
		$('#face').bind('click',function(){
		isFocus = true;
		facesPanel.css('visibility','visible');
			
		if(facesPanel.html() == ""){
			$.get('/hall/returnFaces',function(data){
				var faces = strToJson(data.facesJson);
				facesPanel.html('');
				for(i = 0; i< faces.length-2;i++){
				  if(i % 12 == 0 && i != 0)
				   facesPanel.append('<br/>');
				   facesPanel.append('<img class="faceSingle" title="' + faces[i].substr(0,faces[i].indexOf(".")) + '" src="images/face/' + faces[i] + '"/>');
				}
				
				$('#facesPanel img').each(function(){
				$(this).bind('click',function(){
				    if(input.val() == "写写你的心情吧...")
				    input.val('');
					getValue("input",'@' + $(this).attr('title') + '@');
					input.animate({height:'80px'},500);
				});
				});
				
			});
		}
		
		});
		
		
		$('body').bind('click',function(evt){
		  var _event = evt ? evt : window.event;//事件
      	  var _target = evt ? evt.target : window.event.srcElement;
      	  if(_target != document.getElementById("facesPanel") && _target != document.getElementById("face")){
      	  	facesPanel.css('visibility','hidden');
      	  }
		});
	
	});
	
	</script>

	<script>
	//feedlist脚本
	$(function(){
	
		var more = $('#feed-more');
		var feedList = $('#feed-list');
		
		//更多新鲜事 样式事件绑定
		more.bind('mouseover',function(){
		$(this).css('background','#CEE1EE');
		});
		
		more.bind('mouseout',function(){
		$(this).css('background','#F0F5F8');
		});
		
		var page = 1;
		var pageSize = 10;
		
		ajaxGetFeedsList(page = 1,pageSize,feedList);
			
		//更多新鲜事异步加载
		more.bind('click',function(){
		ajaxGetFeedsList(page = page + 1,pageSize,feedList);
		});
	
		
	});
		
	</script>	
	
	

	
  </head>
  
  <body>
    
    <%@include file="../commons/header.jsp" %>
    
	<div id="main">
		<div id="left-bar">
			<%@ include file="../commons/leftbar-nav.jsp" %>
		</div>
		
		<div id="content-wrapper">
				<div id="emotionBox">
				<span class="zt">状态</span>
				<textarea id="input" style="width:99%;overflow-y:auto;max-width:99%;">写写你的心情吧...</textarea>
				<span id="face"><img src="images/face/微笑.gif" style="margin-top:5px;"/>表情</span>
				<div id="facesPanel"></div>
				<span  style="position:absolute; right:10px; bottom:5px;"><span id='cNum'>256</span>&nbsp;&nbsp;&nbsp;&nbsp;<button id="publish">发布</button></span>
				</div>
				
				<div id="feed-list">
				
				</div>
				<div  id="feed-more">更多新鲜事...</div>		
		</div>
		
		<div id="right-bar">
		<div class="moudule">
					<h3 class="moudule-header">从这里我能看到哪些新鲜事</h3>
					<div class="moudule-content">
						1.我关注的人借了什么书<br/>
						2.我关注的人还了什么书<br/>
						3.我关注的人，作为分享者，给还书者打的分<br/>
						4.我关注的人，作为还书者，被分享者打的分<br/>
						5.我关注的人分享了哪本书<br/>
						6.我关注的书新的书评<br/>
						7.我关注的书的新共享者<br/>
						8.我关注的书新借阅者 <br/>
					</div>
				</div>
		</div>
		
		<div class="clr"></div>
	</div>   
    
    <%@include file="../commons/footer.jsp" %>
  </body>
</html>
