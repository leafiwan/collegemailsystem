<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <base href="<%=basePath%>" />
    
    <title>我的书 - 蚂蚁书屋</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="keywords" content="蚂蚁书屋,借阅请求" />
	<meta http-equiv="description" content="我的借阅请求" />
	
	<link rel="stylesheet" href="css/common.css" type="text/css" />	
	<link rel="stylesheet" href="css/home.css" type="text/css" />	
	
	
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/jquery.blockUI.js"></script>
	<script type="text/javascript" src="js/commons.js"></script>
	<script type="text/javascript">
	
		$(function() {
			
			$('#borrow-list .item .contact-content').hide();
			
			$('#borrow-list .item .view-contact').click(function() {
				popup($(this).next().html());
			});
			
			
			$('#borrow-list .item .return').click(function() {
				var confirmContent = '确认您已经将书籍归还了？';
				if($(this).hasClass('provider')) {
					confirmContent = '确认您已经收到归还的书籍了？';
				}
				
				var link = $(this);
				if(confirm(confirmContent)) {
					link.unbind('click');
					link.text('处理中...');
					$.get(link.attr('href'), function(data) {
						if(data.result == 'none') {
							alert('不存在相应的记录');
						} else if(data.result == 'success') {
							link.after('<span class="returned">已归还</span>');
							link.remove();
							window.location.reload();
						} else {
							alert('发生了错误，请稍后重试');
						}
					});
				}
				
				return false;
			});
			
			
			//By Michael
			//信誉反馈
			var stars = $('.evaluateBox img');
			var evaluateBox = $('.evaluateBox');
			
			stars.each(function(i){
			//当鼠标放在上面时
			$(this).bind('mouseover',function(){
					//初始化
					var start = parseInt(i / 5) * 5;
			        for(n = start;n <= start + 4;n++){
						$(stars[n]).attr('src','images/level/starA.gif');
					}		
					
					for(n = start;n <= start + i % 5;n++){
						$(stars[n]).attr('src','images/level/starB.gif');
						$(stars[n]).attr('title',(start + i % 5 + 1) * 2 + '分');
					}			
			}); //-- end bind
			
			//当鼠标离开时
			$(this).bind('mouseout',function(){
					//初始化
					var start = parseInt(i / 5) * 5;
			        for(n = start;n <= start + 4;n++){
						$(stars[n]).attr('src','images/level/starA.gif');
					}		
			}); //-- end bind
			
			//鼠标点击事件
			$(this).bind('click',function(){
			    var score =  (i % 5 + 1) * 2;
				if(confirm('少年,你确定给对方的信誉度评价为' + score + '分吗?')){
				var borrowerId = $(evaluateBox[parseInt(i / 5)]).attr('borrowerId');
				var providerId = $(evaluateBox[parseInt(i / 5)]).attr('providerId');
				var bookId = $(evaluateBox[parseInt(i / 5)]).attr('bookId');
				$.get('/borrow/evaluate',{'score':score,'borrowerId':borrowerId,'providerId':providerId,'bookId':bookId},function(data){
					$(evaluateBox[parseInt(i / 5)]).html('<span class="evaluateText">谢谢您的配合，已进行评价！</span>');
				});
				}
			}); //-- end bind
			
			}); //-- end each
			
		});
	
	</script>
	
  </head>
  
  <body>
    
    <%@include file="../commons/header.jsp" %>
    
	<div id="main">
		<div id="left-bar">
			<%@ include file="../commons/leftbar-nav.jsp" %>
		</div>
		
		<div id="content-wrapper">
				<h2 class="header2">我的书</h2>
				<div class="tab-div">
					<ul class="tabs">
						<s:if test="type == 0">
							<li class="current"><a href="javascript:void(0);">借阅的书(<s:property value="currentUser.borrowedCount" />)</a></li>
							<li><a href="/borrow/mine?type=1">借出的书(<s:property value="currentUser.borrowingCount" />)</a></li>
						</s:if>
						<s:else>
							<li><a href="/borrow/mine">借阅的书(<s:property value="currentUser.borrowedCount" />)</a></li>
							<li class="current"><a href="javascript:void(0);">借出的书(<s:property value="currentUser.borrowingCount" />)</a></li>
						</s:else>
						
						<div class="clr"></div>
					</ul>
					<div class="tab-content">
						<s:if test="requests.empty">
							<div class="tips">
								没有相应的书
							</div>
						</s:if>
						<s:else>
							<div class="moudule" id="borrow-list">
								<s:if test="type == 0">
									<s:iterator value="borrows">
									<div class="item">
										<a class="savatar"  href="/user/<s:property value="sharing.user.id" />"><img alt="<s:property value="sharing.user.name" />" src="<s:property value="sharing.user.smallAvatar" />" /></a>
										<div class="info">
											<a href="/user/<s:property value="sharing.user.id" />"><s:property value="sharing.user.name" escape="false" /></a> 把<a href="/book/<s:property value="sharing.book.id" />">《<s:property value="sharing.book.title" />》</a>借给了我
											<span class="date">(<s:date name="borrowDate" nice="true" />)</span>
											<a href="/book/<s:property value="sharing.book.id" />" class="scover"><img src="<s:property value="sharing.book.smallImg" />" alt="<s:property value="sharing.book.title" escape="false" />" /></a>
											<div class="operations">
												<s:if test="returned">
													<span class="returned">归还于 <s:date name="returnDate" format="yyyy-MM-dd" /></span>
												</s:if>
												<s:else>
													<a class="return borrower" href="/borrow/returnBook?bid=<s:property value="id" />">还书</a>
													<a class="view-contact" href="javascript:void(0);">查看联系方式</a>
													<div class="contact-content">
														<h3>您可以通过以下方式和<s:property value="sharing.user.name" escape="false" />取得联系</h3>
														<ul>
															<s:if test="sharing.user.phoneNumber != null && sharing.user.phoneNumber != ''">
																<li>电话：<s:property value="sharing.user.phoneNumber" /></li>
															</s:if>
															<s:if test="sharing.user.qq != null && sharing.user.qq != ''">
																<li>QQ： <s:property value="sharing.user.qq" /></li>
															</s:if>
															<li><a href="/message/create/<s:property value="sharing.user.id" escape="false" />">发私信</a></li>
														</ul>
													</div>
												</s:else>
											</div>
										</div>
										<div class="clr"></div>
									</div>
									</s:iterator>
								</s:if>
								<s:else>
									<s:iterator value="borrows">
									<div class="item">
										<a class="savatar" href="/user/<s:property value="user.id" />"><img alt="<s:property value="user.name" />" src="<s:property value="user.smallAvatar" />" /></a>
										<div class="info">
											<a href="/user/<s:property value="user.id" />"><s:property value="user.name" escape="false" /></a> 向我借了<a href="/book/<s:property value="sharing.book.id" />">《<s:property value="sharing.book.title" escape="false" />》</a>
											<span class="date">(<s:date name="borrowDate" nice="true" />)</span>
											<a href="/book/<s:property value="sharing.book.id" />" class="scover"><img src="<s:property value="sharing.book.smallImg" />" alt="<s:property value="sharing.book.title" escape="false" />" /></a>
											<div class="operations">
												<s:if test="returned">
													<span class="returned">归还于 <s:date name="returnDate" format="yyyy-MM-dd" /></span><br/>
													<s:if test="currentUser.id == sharing.user.id && !evaluated">
													<br/>
											
													<span style='cursor:pointer' class='evaluateBox' borrowerId='<s:property value="id"/>' providerId='<s:property value="sharing.user.id"/>' bookId='<s:property value="sharing.book.id"/>'>
													<span class='evaluateText'>对借方进行信誉评价</span>
													<br/>
													<span class='evaluateImg'>
													<img src='images/level/starA.gif'/>
													<img src='images/level/starA.gif'/>
													<img src='images/level/starA.gif'/>
													<img src='images/level/starA.gif'/>
													<img src='images/level/starA.gif'/>
													</span>
													</span>
													</s:if>
													<s:else>
													<br/>
													<span class='evaluateText'>
													已进行评价！
													</span>
													</s:else>
												</s:if>
												<s:else>
													<a class="return provider" href="/borrow/returnBook?bid=<s:property value="id" />">确认还书</a>
													
													<a class="view-contact" href="javascript:void(0);">查看联系方式</a>
													<div class="contact-content">
														<h3>您可以通过以下方式和<s:property value="user.name" escape="false" />取得联系</h3>
														<ul>
															<s:if test="user.phoneNumber != null && user.phoneNumber != ''">
																<li>电话：<s:property value="user.phoneNumber" /></li>
															</s:if>
															<s:if test="user.qq != null && user.qq != ''">
																<li>QQ： <s:property value="user.qq" /></li>
															</s:if>
															<li><a href="/message/create/<s:property value="user.id" escape="false" />">发私信</a></li>
														</ul>
													</div>												
												</s:else>
											</div>
										</div>
										<div class="clr"></div>
									</div>
									</s:iterator>
									
								</s:else>
							</div>
							<jshy:paginator value="page" />
						</s:else>
						
					</div>
				</div>
		</div>
		
		<div id="right-bar">
			<s:if test="type == 0">
				<div class="moudule">
					<h3 class="moudule-header">怎样取到借阅的书</h3>
					<div class="moudule-content">
						共享者同意将书籍借给您之后，书籍将会成为「我借阅的书」。如果您还没有拿到书籍，请尽快与共享者取得联系。点击「查看联系方式」链接可以看到共享者的联系信息。
					</div>
				</div>
			</s:if>
			<s:else>
				<div class="moudule">
						<h3 class="moudule-header">怎样将书成功借出</h3>
						<div class="moudule-content">
							您同意将书籍借出之后，书籍将会成为「我借出的书」。如果您还没有将书籍给借阅者，请尽快与其取得联系。点击「查看联系方式」链接可以看到借阅者的联系信息。
						</div>
				</div>
			</s:else>
		</div>
		
		<div class="clr"></div>
	</div>   
    
    <%@include file="../commons/footer.jsp" %>
  </body>
</html>
