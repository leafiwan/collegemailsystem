<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <base href="<%=basePath%>" />
    
    <title>《<s:property value="book.title" escape="false" />》的书评 - 蚂蚁书屋</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="keywords" content="蚂蚁书屋,<s:property value="book.title" escape="false" />的书评" />
	<meta http-equiv="description" content="<s:property value="book.title" escape="false" />" />
	
	<link rel="stylesheet" href="css/common.css" type="text/css" />
	<link rel="stylesheet" href="css/book.css" type="text/css" />
	
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/commons.js"></script>
	<script type="text/javascript">


	</script>
	
  </head>
  
  <body>
  	<%@include file="../commons/header.jsp" %>
  	
  	<div id="main">
		<h2 class="header2">《<s:property value="book.title" escape="false" />》的书评</h2>
		
		<div id="left-column">
			<div id="reviews-meta">
				<s:if test="!reviews.empty">
					共 <s:property value="book.reviewsCount" /> 篇书评
				</s:if>
				<a href="/book/<s:property value="book.id" />/review">写书评</a>
			</div>
			<div class="moudule">
				<div class="moudule-content">
					<s:if test="reviews.empty">
						<div class="tips">
							还没有人写书评
						</div>
					</s:if>
					<s:else>					
						<s:iterator value="reviews">
							<div class="review-list-item">
								<a href="/user/<s:property value="author.id" />" class="avatar"><img src="<s:property value="author.smallAvatar" />" /></a>
								<div class="text">
									<div class="meta">
										<h4><a href="/user/<s:property value="author.id" />"><s:property value="author.name" escape="none" /></a>：<a href="/review/<s:property value="id" />"><s:property value="title" escape="false" /></a></h4>								
									</div>
									<div class="content">
										<s:if test="content.length() > 150">
											<s:property value="content.substring(0, 150)" escape="false" />...<a class="more" href="/review/<s:property value="id" />">(阅读全文)</a>
										</s:if>
										<s:else>
											<s:property value="content" escape="false" />
										</s:else>												
									</div>
								</div>
								<div class="clr"></div>
							</div>
						</s:iterator>		
					</s:else>	
				</div>
			</div>
		</div>
		
		<div id="right-column">
			<div class="moudule">
				<div class="moudule-content">
					<a class="back-link" href="/book/<s:property value="book.id" />">&lt;&lt;回到《<s:property value="book.title" />》的页面</a>
				</div>
			</div>

			<div class="moudule" id="book-info">
				<h3 class="moudule-header">书籍信息</h3>
				<div class="moudule-content">
					<img src="<s:property value="book.middleImg" />" class="cover" />
					<p>
						<span class="label">作者:</span> <s:property value="book.author" escape="false" /><br />
						<span class="label">出版社:</span> <s:property value="book.publisher" escape="false" /><br />
						<span class="label">出版时间:</span> <s:property value="book.publishDate" escape="false" /><br />
					</p>
				</div>
			</div>
		</div>

		
		<div class="clr"></div>
	</div>
  	
  	<%@include file="../commons/footer.jsp" %>
  </body>
</html>
