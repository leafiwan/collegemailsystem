<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <base href="<%=basePath%>" />
    
    <title><s:property value="title" escape="false" /> - 蚂蚁书屋</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="keywords" content="蚂蚁书屋,书评" />
	<meta http-equiv="description" content="<s:property value="title" escape="false" />" />
	
	<link rel="stylesheet" href="css/common.css" type="text/css" />
	<link rel="stylesheet" href="css/book.css" type="text/css" />
	
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/commons.js"></script>
	<script type="text/javascript">


	</script>
	
  </head>
  
  <body>
  	<%@include file="../commons/header.jsp" %>
  	
  	<div id="main">
		<h2 class="header2"><s:property value="title" escape="false" /></h2>
		
		<div id="left-column">
			<div id="review-meta">
				<a href="/user/<s:property value="author.id" />"><s:property value="author.name" escape="false" /></a>发表于 <s:date name="date" nice="true" /> 
				/
				<s:property value="commentsCount" /> 条评论
			</div>
			<div id="review-content">
				<jshy:prefixAndSuffixProperty regExp=".+" value="content" prefix="<p>" suffix="</p>" escape="false" />
			</div>
			
			<div class="moudule">
				<h3 class="moudule-header">评论</h3>
				<div class="moudule-content">
					<s:if test="comments.elements.empty">
						<div class="tips">
							暂时还没有人评论
						</div>
					</s:if>
					<s:else>
						<s:iterator value="comments.elements">
							<div class="comment-item">
								<a class="savatar" href="/user/<s:property value="author.id" />"><img src="<s:property value="author.smallAvatar" />" /></a>
								<div class="right">
									<div class="header">
										<a href="/user/<s:property value="author.id" />"><s:property value="author.name" escape="false" /></a>
										<span class="des">
											(<s:if test="author.description.length() > 15">
												<s:property value="author.description.substring(0, 15)" escape="false" />...
											</s:if>
											<s:else>
												<s:property value="author.description" escape="false" />
											</s:else>)
										</span>
										<span class="date">
											<s:date name="publishDate" nice="true" />
										</span>
									</div>
									<div class="content">
										<jshy:prefixAndSuffixProperty regExp=".+" value="content" prefix="<p>" suffix="</p>" escape="false" />
									</div>
								</div>
								<div class="clr"></div>
							</div>
						</s:iterator>
						
						<jshy:paginator value="comments" base="/review/%{id}" />
						
					</s:else>
				</div>
			</div>
			
			<div class="comment-form">
				<h3 class="moudule-header">发表评论</h3>
				<form action="/comment/create-review-comment" method="post">
					<p>
						<textarea class="input" name="content" rows="10" cols="60"></textarea>
					</p>
					<p>
						<input type="hidden" name="reviewId" value="<s:property value="id" />" />
						<input class="button" type="submit" value="评论" />
					</p>					
				</form>
			</div>
						
		</div>
		
		<div id="right-column">
			<div class="moudule">
				<div class="moudule-content">
					<a class="back-link" href="/book/<s:property value="book.id" />/reviews">&lt;&lt;回到《<s:property value="book.title" />》的书评列表</a>
				</div>
			</div>
			
			<div class="moudule" id="author-info">
				<h3 class="moudule-header">作者信息</h3>
				<div class="moudule-content">
					<a class="savatar" href="/user/<s:property value="author.id" />"><img src="<s:property value="author.smallAvatar" />" /></a>
					<div class="meta">
						<a href="/user/<s:property value="author.id" />"><s:property value="author.name" escape="false" /></a>
					</div>
					<div class="describe">
						(<s:if test="author.description.length() > 15">
							<s:property value="author.description.substring(0, 15)" escape="false" />...
						</s:if>
						<s:else>
							<s:property value="author.description" escape="false" />
						</s:else>)
					</div>
				</div>
			</div>
			
			
			<div class="moudule" id="book-info">
				<h3 class="moudule-header">书籍信息</h3>
				<div class="moudule-content">
					<img src="<s:property value="book.middleImg" />" class="cover" />
					<p>
						<span class="label">作者:</span> <s:property value="book.author" escape="false" /><br />
						<span class="label">出版社:</span> <s:property value="book.publisher" escape="false" /><br />
						<span class="label">出版时间:</span> <s:property value="book.publishDate" escape="false" /><br />
					</p>
				</div>
			</div>
		</div>

		
		<div class="clr"></div>
	</div>
  	
  	<%@include file="../commons/footer.jsp" %>
  </body>
</html>
