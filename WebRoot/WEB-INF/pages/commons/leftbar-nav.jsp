<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="jshy" uri="/jshy-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<div id="user-meta">
	<img src="<s:property value="#session.get(@com.jieshuhuiyou.Config@SESSION_KEY_USER_SMALL_AVATAR)" escape="false" />" />
	<s:property value="#session.get(@com.jieshuhuiyou.Config@SESSION_KEY_USER_NAME)" escape="false" />
	<div class="clr"></div>
</div>
<ul id="side-nav">
	<li>
		<a href="/borrowRequest/mine"><span class="icon" id="srequest"></span>请求</a>
	</li>
	<li>
		<a href="/sharing/mine"><span class="icon" id="sshare"></span>我的共享</a>
	</li>
	<li>
		<a href="/borrow/mine"><span class="icon" id="sbook"></span>我的书</a>
	</li>
	<li>
		<a href="/message/mine"><span class="icon" id="schat"></span>私信</a>
	</li>
	<li>
		<a href="/notification/mine"><span class="icon" id="snotification"></span>通知</a>
	</li>
	<li>
		<a href="/hall/mine"><span class="icon" id="sfeeds"></span>新鲜事</a>
	</li>
</ul>