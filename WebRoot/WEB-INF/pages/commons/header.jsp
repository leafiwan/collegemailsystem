<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="jshy" uri="/jshy-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

    <div id="header">
		<div class="content">
			<h1><a id="header-logo" href="/">蚂蚁书屋</a></h1>
			<ul id="nav">
				<li><a id="thome" href="/home">首页</a></li>
				<li><a id="tbookbase" href="/bookbase">书库</a></li>
<%--				<li><a id="tdiscover" href="#">发现</a></li>--%>
				<jshy:isLogin>
					<li><a id="tshare" href="share">共享</a></li>
				</jshy:isLogin>			
			</ul>
			<form id="top-search" action="/search">
				<label for="keyword" class="inner">搜索书名或 ISBN</label>
				<input type="text" name="keyword" />
				<a id="search-icon" href="javascript:void(0);">搜索</a>
			</form>
			
			
			<ul id="user-nav">
				<jshy:isLogin>
					<li><a href="/user/<s:property value="#session.get(@com.jieshuhuiyou.Config@SESSION_KEY_USER_ID)" />"><s:property value="#session.get(@com.jieshuhuiyou.Config@SESSION_KEY_USER_NAME)" escape="false" /></a></li>
					<li><a href="/settings/">设置</a></li>
					<li><a href="/logout">退出</a></li>
				</jshy:isLogin>	
				
				<jshy:isNotLogin>
					<li><a href="/">登录</a></li>
				</jshy:isNotLogin>
			</ul>
			
		</div>
	</div>
