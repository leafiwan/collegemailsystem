<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <base href="<%=basePath%>" />
    
    <title>头像设置 - 蚂蚁书屋</title>
    
    <meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" /> 
	<meta http-equiv="keywords" content="蚂蚁书屋,资料设置" />
	<meta http-equiv="description" content="蚂蚁书屋资料设置" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<link rel="stylesheet" href="css/common.css" type="text/css" />
	<link rel="stylesheet" href="css/settings.css" type="text/css" />
	<link rel="stylesheet" href="imageareaselection/css/imgareaselect-default.css" type="text/css" />
	
	
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/commons.js"></script>
	<script type="text/javascript" src="imageareaselection/scripts/jquery.imgareaselect.min.js"></script>
	<script type="text/javascript">
		
		// become true when the preview the first time being activated
		var selected = false;
	
		function preview(img, selection) {
			
			if(!selected) {
				$('#preview img').attr('src', $('#avatar').attr('src'));
				selected = true;
			}
		    var scaleX = 48 / (selection.width || 1);
		    var scaleY = 48 / (selection.height || 1);
		  
		    $('#preview img').css({
		        width: Math.round(scaleX * $('#avatar').width()) + 'px',
		        height: Math.round(scaleY * $('#avatar').height()) + 'px',
		        marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px',
		        marginTop: '-' + Math.round(scaleY * selection.y1) + 'px'
		    });
		}
	
		$(function() {
			// init avatar editor trigger
			$('#large-avatar-panel').children().click(function() {
				$("#click-edit").hide();
				if(!selected) {
					
					$('#avatar').imgAreaSelect({ aspectRatio: '1:1',
						onSelectChange: preview,
						onSelectEnd: function(img, selection) {
							$('#edit-avatar-panel input[name="x1"]').val(selection.x1);
				            $('#edit-avatar-panel input[name="y1"]').val(selection.y1);
				            $('#edit-avatar-panel input[name="x2"]').val(selection.x2);
				            $('#edit-avatar-panel input[name="y2"]').val(selection.y2); 
						}, x1:0,y1:0,x2:60,y2:60 });
				}
						
			}); 
			
			// init submit anchors
			convertAnchorToSubmit($('#edit-submit'));
			convertAnchorToSubmit($('#upload-submit'));
		});
	</script>

  </head>
  
  <body>
    
    <%@ include file="../commons/header.jsp" %>
    
	<div id="main">
		<h2 class="header2">个人设置</h2>
		
		<div id="settings">
			<div class="tab-div">
				<ul class="tabs">
					<li><a href="settings/profile">基本资料</a></li>
					<li><a href="settings/password">密码设置</a></li>
					<li class="current"><a href="javascript:void(0);">头像设置</a></li>
					<div class="clr"></div>
				</ul>
				<div class="tab-content">
						<s:if test="#parameters['msg'] != null">
						 	<%
								Map<String, String> tips = new HashMap<String, String>();
								tips.put("wrong","出现了错误");
								tips.put("selectedAreaWrong","头像区域选择错误");								
								tips.put("improperAspectRatio","图片的宽高比请控制在 2 以内");
								tips.put("uploadFail","上传出错，请稍后再试");
								tips.put("fileTooLarge","图片不能超过2M");
								tips.put("WrongfileType", "图片格式错误");
								tips.put("noFileSelected", "请选择图片文件");
								
								tips.put("success", "新的头像已经保存");
							 %>
							 <s:if test="#parameters['msg'][0] == 'success'">
								 <div class="tips success">
									<%=tips.get("success") %>
								 </div>
							 </s:if>
							 <s:else>
							 	<div class="tips error">
									<%=tips.get(request.getParameter("msg")) %>
								</div>
							 </s:else>
						 </s:if>
					<div id="avatar-main">
				
						<s:if test="user.largeAvatar != @com.jieshuhuiyou.Config@USER_AVATAR_DEFUALT_LARGE">
							<div id="edit-avatar" class="moudule">
								<h3 class="moudule-header">编辑头像</h3>
								<div class="moudule-content">
									<div id="large-avatar-panel">
										<img id="avatar" src="<s:property value="user.largeAvatar" escape="false" />" />
										<div id="click-edit">
											点击编辑头像
										</div>
									</div>
									<div id="edit-avatar-panel">
										<div id="preview">
											<img src="<s:property value="user.smallAvatar" escape="false" />" />
											<div class="clr"></div>
										</div>
										<div class="clr"></div>								
										<form id="edit-form" method="post" action="/settings/avatar-edit">
											<input type="hidden" name="x1" />
											<input type="hidden" name="y1" />
											<input type="hidden" name="x2" />
											<input type="hidden" name="y2" />									
											<a id="edit-submit" class="sdbtn submit" href="javascript:void(0);"><span class="sdbtn-inner"></span>保存新设置</a>
										</form>
									</div>
									
									<div class="clr"></div>
								</div>						
							</div>
						</s:if> 
				
						<div id="upload-avatar" class="moudule">
							<h3 class="moudule-header">上传新头像</h3>
							<div class="moudule-content">
								<form action="/settings/avatar-upload" method="post" enctype="multipart/form-data">
									<p style="height: 50px; padding-top: 4px;line-height: 165%">
										<label for="">本地图片：</label><input type="file" name="avatar" />
									</p>
									<p style="margin-top: 8px;">
										<a class="sdbtn submit" id="upload-submit" href="javascript:void(0);"><span class="sdbtn-inner"></span>上传</a>								
										<div class="clr"></div>
										<p style="font-size: 12px;">
										如果上传成功但头像未更新，请多刷新几次页面
										</p>
									</p>									
								</form>
							</div>
							
						</div>	
						
						<div class="clr"></div>			
					</div>
						
				</div>
			</div>
		</div>
		
		<div class="clr"></div>
	</div>    
	
	<%@ include file="../commons/footer.jsp" %>
  </body>
</html>
