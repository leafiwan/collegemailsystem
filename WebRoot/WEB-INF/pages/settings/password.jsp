<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <base href="<%=basePath%>" />
    
    <title>密码设置 - 蚂蚁书屋</title>
    
	<meta http-equiv="keywords" content="蚂蚁书屋,密码设置" />
	<meta http-equiv="description" content="蚂蚁书屋密码设置" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<link rel="stylesheet" href="css/common.css" type="text/css" />
	<link rel="stylesheet" href="css/settings.css" type="text/css" />
	
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/commons.js"></script>
	<script type="text/javascript">
		$(function() {
			convertAnchorToSubmit($('#info-submit'));
		});
	</script>

  </head>
  
  <body>
    
    <%@ include file="../commons/header.jsp" %>
    
	<div id="main">
		<h2 class="header2">个人设置</h2>
		
		<div id="settings">
			<div class="tab-div">
				<ul class="tabs">
					<li><a href="settings/profile">基本资料</a></li>
					<li class="current"><a href="javascript:void(0);">密码设置</a></li>
					<li><a href="settings/avatar">头像设置</a></li>
					<div class="clr"></div>
				</ul>
				<div class="tab-content">
					
					 
					 <s:if test="#parameters['msg'] != null">
					 	<%
							Map<String, String> tips = new HashMap<String, String>();
							tips.put("wrong","出现了错误");
							tips.put("originPwdEmpty","原密码不能为空");
							tips.put("originPwdFormatWrong","原密码不正确");
							tips.put("originWrong","原密码不正确");
							tips.put("newPwdEmpty","新密码不能为空");
							tips.put("newPwdFormatWrong", "新密码请控制在 5-20 个字符之间");
							tips.put("confirmWrong", "两次输入的密码不想同");
							
							tips.put("success", "修改密码成功");
						 %>
						 <s:if test="#parameters['msg'][0] == 'success'">
							 <div class="tips success">
								<%=tips.get("success") %>
							 </div>
						 </s:if>
						 <s:else>
						 	<div class="tips error">
								<%=tips.get(request.getParameter("msg")) %>
							</div>
						 </s:else>
					 </s:if>
					<form id="info" action="/settings/do-password" method="post">
						<dl>
							<dt><label for="originPwd">原密码：</label></dt>
							<dd><input id="originPwd" class="input" name="originPwd" type="password" /></dd>
							<dt><label for="newPwd">新密码：</label></dt>
							<dd><input id="newPwd" class="input" name="newPwd" type="password" /></dd>
							<dt><label for="password2">确认密码：</label></dt>
							<dd><input id="password2" class="input" name="password2" type="password" /></dd>
							<dt></dt>
							<!--
							<dd><input type="submit" value="提交" /></dd>
							-->
							<dd>
								<input type="hidden" name="action" value="password" />
								<a id="info-submit" href="javascript:void(0);" class="sdbtn submit"><span class="sdbtn-inner"></span>提交</a>
							</dd>
						</dl>
					</form>
				</div>
			</div>
		</div>
		
		<div class="clr"></div>
	</div>    
	
	<%@ include file="../commons/footer.jsp" %>
  </body>
</html>
