<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <base href="<%=basePath%>" />
    
    <title>资料设置 - 蚂蚁书屋</title>
    
	<meta http-equiv="keywords" content="蚂蚁书屋,资料设置" />
	<meta http-equiv="description" content="蚂蚁书屋资料设置" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<link rel="stylesheet" href="css/common.css" type="text/css" />
	<link rel="stylesheet" href="css/settings.css" type="text/css" />
	
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	
	<script type="text/javascript" src="/js/commons.js"></script>
	
	<script type="text/javascript">
		$(function() {
			convertAnchorToSubmit($('#info-submit'));
			
			$('#school-list').hide();
			
			$('#change-school').click(function() {
				$(this).hide();
				$('#school').hide();
				$('#school-list').show();
			});
		});
	</script>

  </head>
  
  <body>
    
    <%@ include file="../commons/header.jsp" %>
    
	<div id="main">
		<h2 class="header2">个人设置</h2>
		
		<div id="settings">
			<div class="tab-div">
				<ul class="tabs">
					<li class="current"><a href="javascript:void(0);">基本资料</a></li>
					<li><a href="settings/password">密码设置</a></li>
					<li><a href="settings/avatar">头像设置</a></li>
					<div class="clr"></div>
				</ul>
				<div class="tab-content">
					
					 <s:if test="#parameters['msg'] != null">
					 	<%
							Map<String, String> tips = new HashMap<String, String>();
							tips.put("wrong","出现了错误");
							tips.put("nameEmpty","名字不能为空");
							tips.put("nameWrong","名字长度请控制在 2-10 个字符之间");
							tips.put("qqWrong", "QQ号填写错误");
							tips.put("phoneWrong", "电话号码填写错误");
							tips.put("desWrong", "个人描述只能在 140 个字之内");
							tips.put("success", "修改资料成功");
						 %>
						 <s:if test="#parameters['msg'][0] == 'success'">
							 <div class="tips success">
								<%=tips.get("success") %>
							 </div>
						 </s:if>
						 <s:else>
						 	<div class="tips error">
								<%=tips.get(request.getParameter("msg")) %>
							</div>
						 </s:else>
					 </s:if>
					 					
					<form id="info" action="/settings/do-profile" method="post">
						<dl>
							<dt><label for="realName">姓名：</label></dt>
							<dd><input id="name" class="input" name="realName" type="text" value="<s:property value="user.name" escape="false" />"  /></dd>
							<dt><label for="qqNumber">QQ：</label></dt>
							<dd><input id="qqNumber" class="input" name="qqNumber" type="text" value="<s:property value="user.qq" escape="false" />"  /></dd>
							<dt><label for="cellphone">手机：</label></dt>
							<dd><input id="cellphone" class="input" name="cellphone" type="text" value="<s:property value="user.phoneNumber" escape="false" />"  /></dd>
							<dt><label for="school">学校：</label></dt>
							<dd>
								<span id="school"><s:property value="user.school.name" escape="false" /> (<a id="change-school" href="javascript:void(0);">更改</a>)</span>
								<select id="school-list" name="school_id">
									<s:iterator value="schools">
										<s:if test="id == user.school.id">
											<option selected="selected" value="<s:property value="id" />"><s:property value="name" /></option>
										</s:if>
										<s:else>
											<option value="<s:property value="id" />"><s:property value="name" /></option>
										</s:else>
									</s:iterator>
								</select>
							</dd>
							<dt><label for="selfDes">个人描述：</label></dt>
							<dd><textarea id="description" class="input" name="selfDes"><s:property value="user.description" escape="false" /></textarea></dd>
							<dt></dt>
							<dd>
								<input type="hidden" name="action" value="profile" />
								<a id="info-submit" href="javascript:void(0);" class="sdbtn submit"><span class="sdbtn-inner"></span>提交</a>
							</dd>
						</dl>
					</form>
				</div>
			</div>
		</div>
		
		<div class="clr"></div>
	</div>    
	
	<%@ include file="../commons/footer.jsp" %>
  </body>
</html>
