<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <base href="<%=basePath%>" />
    
    <title>我的共享 - 蚂蚁书屋</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="keywords" content="蚂蚁书屋,我的共享" />
	<meta http-equiv="description" content="我在蚂蚁书屋共享的书籍" />
	
	<link rel="stylesheet" href="css/common.css" type="text/css" />	
	<link rel="stylesheet" href="css/home.css" type="text/css" />	
	
	
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/jquery.blockUI.js"></script>
	<script type="text/javascript" src="js/commons.js"></script>

	<script type="text/javascript">
	
		$(function() {
			
			$("#sharing-list .item .cancel, #sharing-list .item .notAvailable").hide();
			
			$("#sharing-list .item").hover(function() {
				$(this).children('.cancel, .notAvailable').fadeIn(200);
			}, function() {
				$(this).children('.cancel, .notAvailable').fadeOut(200);
			});
			
			
			$('#sharing-list .item .cancel').click(function() {
				if(confirm('您确定要取消这个分享吗？')) {
					var link = $(this);
					var url = link.attr('href');
					
					$.get(url, function(data) {
						if(data.result == 'success') {
							link.parents('.item').slideUp();
						} else if(data.result == 'notAvailable') {
							alert('书籍正被借出，不能取消此分享');
						} else if(data.result == 'none') {
							alert('找不到对应的分享，可能已经被删除');
						} else {
							alert('发生了错误，请稍后重试');
						}
					});
				}
				return false;
			});
			
		});
	
	</script>
	
  </head>
  
  <body>
    
    <%@include file="../commons/header.jsp" %>
    
	<div id="main">
		<div id="left-bar">
			<%@ include file="../commons/leftbar-nav.jsp" %>
		</div>
		
		<div id="content-wrapper">
				<h2 class="header2">我的共享</h2>
				<s:if test="currentUser.sharingCount == 0">
					<div class="tips">
						我还没有开始  <a href="/share">分享</a> 书籍
					</div>
				</s:if>
				<s:else>					
					<div class="moudule" id="sharing-list">
						<div class="totals">
							一共分享了 <s:property value="currentUser.sharingCount" /> 本书籍
						</div>
						<div class="moudule-content">
							<s:iterator value="sharings">
								<div class="item">
									<a href="/book/<s:property value="book.id" />" class="scover"><img src="<s:property value="book.smallImg" />" alt="<s:property value="book.title" />" /></a>
									<div class="info">
										<a href="/book/<s:property value="book.id" />"><s:property value="book.title" escape="false" /></a>
										<p>
											<s:property value="book.author" escape="false" /> / <s:property value="book.publisher" escape="false" /> / <s:property value="book.pages" escape="false" /> 页
										</p>
										<p>
											<s:property value="borrowedTimes" /> 人向我借过 / 共享于 <s:date name="shareDate" format="yyyy-MM-dd" />
										</p>
									</div>
									<s:if test="avalible">
										<a class="cancel" href="/sharing/cancel?sid=<s:property value="id" />">取消共享</a>
									</s:if>
									<s:else>
										<span class="notAvailable">正借出</span>
									</s:else>
									
									<div class="clr"></div>
								</div>
							</s:iterator>
							
							<jshy:paginator value="page" />
						</div>
					</div>
				</s:else>
				
		</div>
		
		<div id="right-bar">
		
		</div>
		
		<div class="clr"></div>
	</div>   
    
    <%@include file="../commons/footer.jsp" %>
  </body>
</html>
