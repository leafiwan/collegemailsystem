<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="jshy" uri="/jshy-tags" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<base href="<%=basePath%>" />

		<title>私信 - 蚂蚁书屋</title>

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="keywords" content="蚂蚁书屋,私信" />

		<link rel="stylesheet" href="css/common.css" type="text/css" />
		<link rel="stylesheet" href="css/home.css" type="text/css" />


		<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="js/jquery.blockUI.js"></script>
		<script type="text/javascript" src="js/commons.js"></script>

		<script type="text/javascript">
	
</script>

	</head>

	<body>

		<%@include file="../commons/header.jsp"%>

		<div id="main">
			<div id="left-bar">
				<%@ include file="../commons/leftbar-nav.jsp"%>
			</div>

			<div id="content-wrapper">
				<h2 class="header2">
					私信
				</h2>
				<div class="moudule" id="message-list">
					<div class="moudule-content">
						<div class="tab-div">
							<ul class="tabs">
								<s:if test="operationType == 1">
									<li class="current">
										<a href="/message/mine/inbox">收到的私信</a>
									</li>
									<li>
										<a href="/message/mine/sentbox">发出的私信</a>
									</li>
								</s:if>
								<s:else>
									<li>
										<a href="/message/mine/inbox">收到的私信</a>
									</li>
									<li class="current">
										<a href="/message/mine/sentbox">发出的私信</a>
									</li>
								</s:else>
								<div class="clr"></div>
							</ul>
							<div class="tab-content">
								<s:if test="messagePage.elements.isEmpty()">
									<div class="tips">
										没有任何私信
									</div>
								</s:if>
								<s:else>
									<table>
										<thead>
											<tr>
												<th class="title">
													标题
												</th>
												<th class="user">
													<s:if test="operationType == 1">
														发信人
													</s:if>
													<s:if test="operationType == 2">
														收信人
													</s:if>
												</th>
												<th class="date">
													时间
												</th>
											</tr>
										</thead>
										<tbody>							
											<s:iterator value="messagePage.elements">
												<tr>
													<td class="title">
														<s:if test="operationType == 1">
															<s:if test="!isRead">
																<a class="unread" href="/message/<s:property value="id"/>"><s:property value="title" /></a>
															</s:if>
															<s:else>
																<a href="/message/<s:property value="id"/>"><s:property value="title" /></a>
															</s:else>
														</s:if>
														<s:else>
															<a href="/message/<s:property value="id"/>"><s:property value="title" /></a>
														</s:else>											
													</td>
													<td class="user">
														<s:if test="operationType == 1">
															<a href="/user/<s:property value="sendUser.id" />"><s:property value="sendUser.name" /></a>
														</s:if>
														<s:if test="operationType == 2">
															<a href="/user/<s:property value="receiveUser.id" />"><s:property value="receiveUser.name" /></a>
														</s:if>
														
													</td>
													<td class="date">
														<s:date name="date"/>
													</td>
												</tr>
											</s:iterator>
										</tbody>
									</table>
								</s:else>							
							</div>
							<jshy:paginator value="messagePage" />
						</div>
					</div>
				</div>
			</div>

			<div id="right-bar">

			</div>

			<div class="clr"></div>
		</div>

		<%@include file="../commons/footer.jsp"%>
	</body>
</html>
