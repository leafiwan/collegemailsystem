<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <base href="<%=basePath%>" />
    
    <title>给<s:property value="receiveUser.name" escape="false" />写私信 - 蚂蚁书屋</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="keywords" content="蚂蚁书屋,给<s:property value="recieveUser.name" escape="false" />写私信" />
	
	<link rel="stylesheet" href="css/common.css" type="text/css" />
	<link rel="stylesheet" href="css/home.css" type="text/css" />
	
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/commons.js"></script>
	<script type="text/javascript">
		
		$(function() {
			convertAnchorToSubmit($('#message-create-form .sdbtn'));
		});

	</script>
	
  </head>
  
  <body>
  	<%@include file="../commons/header.jsp" %>
  	
  	<div id="main">
		<h2 class="header2">给<s:property value="receiveUser.name" escape="false" />发私信</h2>
		
		<div id="left-column">
				<s:if test="#parameters['msg'] != null">
					<%
							Map<String, String> tips = new HashMap<String, String>();
							tips.put("wrongDay", "借出天数错误");
							tips.put("requirementsTooLong", "要求请不要超过 300 个字符");
					%>
					<div class="tips error">
						<%=tips.get(request.getParameter("msg"))%>
					</div>
				</s:if>
			<form id="message-create-form" action="/message/send" method="post">
				<p>
					<s:if test="preMsg != null">
						回复：<s:property value="preMsg.title" escape="false" />
						<input type="hidden" name="preMsgId" value="<s:property value="preMsg.id" />" />
						<input type="hidden" id="title" class="input" name="title" value="temptitle" />
					</s:if>
					<s:else>
						标题：<input id="title" class="input" name="title" />
					</s:else>
				</p>
				<p>
					内容：
				</p>
				<p>
					<textarea id="content" class="input" name="content"></textarea>
				</p>
				<p>
					<input type="hidden" name="receiveUserId" value="<s:property value="receiveUser.id"/>" />
					<a href="javascript:void(0);" class="sdbtn"><span class="sdbtn-inner"></span>提交</a>
				</p>
			</form>
						
		</div>
		
		<div id="right-column">
			<div class="moudule" id="user-info">
				<h3 class="moudule-header">收件人</h3>
				<div class="moudule-content">
					<a href="/user/<s:property value="receiveUser.id"/>" class="savatar">
						<img src="<s:property value="receiveUser.smallAvatar"/>" />
					</a>
					<div class="info">
						<a href="/user/<s:property value="receiveUser.id"/>">
							<s:property value="receiveUser.name" escape="false" />
						</a>
						<br />
						(<s:if test="recieveUser.description.length() > 15">
							<s:property value="receiveUser.description.substring(0, 15)" escape="false" />...
						</s:if>
						<s:else>
							<s:property value="receiveUser.description" escape="false" />
						</s:else>)
					</div>
					<div class="clr"></div>
				</div>
			</div>
		</div>
		
		<div class="clr"></div>
	</div>
  	
  	<%@include file="../commons/footer.jsp" %>
  </body>
</html>
