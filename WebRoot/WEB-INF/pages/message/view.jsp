<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <base href="<%=basePath%>" />
    
    <title><s:property value="message.title" escape="false" /> - 蚂蚁书屋</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="keywords" content="蚂蚁书屋,私信标题" />
	
	<link rel="stylesheet" href="css/common.css" type="text/css" />
	<link rel="stylesheet" href="css/home.css" type="text/css" />
	
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/commons.js"></script>
	<script type="text/javascript">
		
	</script>
	
  </head>
  
  <body>
  	<%@include file="../commons/header.jsp" %>
  	
  	<div id="main">
		<h2 class="header2"><s:property value="message.title" /></h2>
		
		<div id="left-column">
				<div class="moudule" id="message">
					<div class="meta">
						<span>发信人：<a href="/user/<s:property value="message.sendUser.id" />"><s:property value="message.sendUser.name" /></a></span>
						<span>发信时间： <s:date name="message.date" /></span>
					</div>
					<div class="moudule-content">
						<div>
							<s:property value="message.content"/>
						</div>
						<div>
							<p>
								<a href="/message/create/<s:property value="id" />?preMsgId=<s:property value="id" />">回复</a>
							</p>
						</div>
					</div>
				</div>
		</div>
		
		<div id="right-column">
			<div class="moudule">
				<div class="moudule-content">
					<a class="back-link" href="/message/mine">&lt;&lt;回到私信列表</a>
				</div>
			</div>
		</div>

		<div class="clr"></div>
	</div>
  	
  	<%@include file="../commons/footer.jsp" %>
  </body>
</html>
