<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>JSP for send message</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
  	<form action="/message/send" method="post">
  		<input type="hidden" name="receiveUserId" value="<s:property value="receiveUser.id"/>" />
  		发送给: <input type="text" name="receiveUserName" readonly="readonly" value="<s:property value="receiveUser.name"/>"/><br/>
  		标题: <input type="text" name="title" /><br />
  		内容: <textarea rows="20" cols="20" name="content"></textarea><br/>
  		<input type="submit" />
  		<s:property escape="false" value="#parameters['errorMsg']"/>
  		<s:debug></s:debug>
  	</form>
  </body>
</html>
