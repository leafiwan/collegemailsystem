<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <base href="<%=basePath%>" />
    
    <title>“<s:property value="#parameters['keyword']" />”的搜索结果 - 蚂蚁书屋</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="keywords" content="蚂蚁书屋,搜索" />

	<link rel="stylesheet" href="css/common.css" type="text/css" />	
	<link rel="stylesheet" href="css/other.css" type="text/css" />	
	
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/commons.js"></script>
	<script type="text/javascript">
	
	</script>
	
  </head>
  
  <body>
    
    <%@include file="commons/header.jsp" %>
    
	<div id="main">
		<h2 class="header2">“<s:property value="#parameters['keyword']" escape="false" />”的搜索结果</h2>	
		<div>
			<form action="/search" id="s-search">
				<input class="input" size="50" type="text" name="keyword" value="<s:property value="#parameters['keyword']" escape="false" />" />
				<input class="button" type="submit" value="搜索"/>
			</form>
		</div>
		<div id="left-column">
			<s:if test="books.empty">
				<div class="tips">
					找不到相应的书籍
				</div>
			</s:if>
			<s:else>		
				<div class="moudule" id="search-results">
					<div class="moudule-content">
						<s:iterator value="books">
						<div class="item">
							<a href="/book/<s:property value="id" />"><img class="cover" src="<s:property value="smallImg" />" alt="<s:property value="title" escape="false" />" title="<s:property value="title" escape="false" />" /></a>
							<div class="info">
								<a href="/book/<s:property value="id" />" class="title"><s:property value="title" escape="false" /></a>
								<br />
								<s:property value="author" escape="false" /> / <s:property value="publisher" escape="false" /> / <s:property value="pages" />页
								<br/>
								<s:property value="rate" /> 分 /  <s:property value="inventory" /> 库存
							</div>
							<div class="clr"></div>
						</div>
						</s:iterator>
					</div>		
				</div>
			</s:else>
			
			<jshy:paginator value="page" />

		</div>
		
		<div id="right-column">
			
		</div>
		
		<div class="clr"></div>
	</div>   
    
    <%@include file="commons/footer.jsp" %>
  </body>
</html>
