<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <base href="<%=basePath%>" />
    
    <title>书库 - 蚂蚁书屋</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="keywords" content="蚂蚁书屋,书库" />
	<meta http-equiv="description" content="蚂蚁书屋书库" />

	<link rel="stylesheet" href="css/common.css" type="text/css" />	
	<link rel="stylesheet" href="css/other.css" type="text/css" />	
	
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/commons.js"></script>
	<script type="text/javascript">
	
	
	
	$(function() {
		
		var start = 0;
		
		function more() {
			
			$.get('/json/bookbase', {totals: start}, function(data) {
				var moreBtn = $('#bookbase .more');
				
				if(moreBtn.length) {
					moreBtn.unbind('click').text('载入中...');
				}
					
				var books = data.books;
				
				start += books.length;
				
				for(var i = 0; i < books.length; i++) {
					$('#bookbase').children('.moudule-content').append('<a class="item" href="/book/' + books[i].id + 
						'"><img src="' + books[i].middleImg + '" title="《' + books[i].title + '》" /></a>');
				}
				
				$("#loader-panel").hide();	
					
				if(books.length == data.pageSize) {
					if(moreBtn.length) {
						moreBtn.click(more);
					}
					else {
						moreBtn = $('<a href="javascript:void(0);" class="more">查看更多...</a>');
						$('#bookbase').append(moreBtn);
						moreBtn.click(more);
					}
					moreBtn.text('查看更多...');
				} else {
					moreBtn.unbind('click').text('没有更多书籍了').addClass('noMore');
				}
				
			});
		}
	
	
	
		more();
	});
	
	</script>
	
  </head>
  
  <body>
    
    <%@include file="commons/header.jsp" %>
    
	<div id="main">
		<h2 class="header2">书库</h2>	
		
		<div id="left-column">
			<div class="moudule" id="bookbase">
				<div class="moudule-content">
					
				</div>
				<div class="clr"></div>
				<div id="loader-panel">
					<img src="/images/ajax-loader-bar.gif" alt="载入中..."/>
				</div>				
			</div>
		</div>
		
		<div id="right-column">
			<div class="moudule">
				<h3 class="moudule-header">这些书是哪儿来的？</h3>
				<div class="moudule-content">
					蚂蚁书屋的书全部来自会员的共享，会员们之间可以自由地相互借阅
				</div>
			</div>
			<div class="moudule">
				<h3 class="moudule-header">我怎样才能借阅这些书？</h3>
				<div class="moudule-content">
					您需要注册为蚂蚁书屋的会员，然后您就可以向书籍的共享者借阅了
				</div>
			</div>
		</div>
		
		<div class="clr"></div>
	</div>   
    
    <%@include file="commons/footer.jsp" %>
  </body>
</html>
