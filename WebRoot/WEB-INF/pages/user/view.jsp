<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <base href="<%=basePath%>" />
    
    <title><s:property value="name" escape="false" /> - 蚂蚁书屋</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="keywords" content="蚂蚁书屋,<s:property value="name" escape="false" />" />
	<meta http-equiv="description" content="<s:property value="name" escape="false" />在蚂蚁书屋的页面" />
	
	<link rel="stylesheet" href="css/common.css" type="text/css" />
	<link rel="stylesheet" href="css/user.css" type="text/css" />
	
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/commons.js"></script>
	<script type="text/javascript" src="js/level.js"></script>
	<script type="text/javascript">
	//显示信誉度与积分
	$(function(){
		var cLevelBox = $('#cLevelBox');
		var score = <s:property value="user.credit"/>
		var starBNum = parseInt(score / 2);
		var starANum = 5 - starBNum;
		for(n=0;n<starBNum;n++){
		   cLevelBox.append('<img class="cLevel" src="images/level/starB.gif"/>');
		}
		for(n=0;n<starANum;n++){
		   cLevelBox.append('<img class="cLevel" src="images/level/starA.gif"/>');
		}
		cLevelBox.append('&nbsp;<s:property value="user.credit"/>');
		
	});
	
	
	</script>
	
  </head>
  
  <body>
    
    <%@include file="../commons/header.jsp" %>
    
    <div id="main">
		<h2 class="header2"><s:property value="name" escape="false" />的主页</h2>
		
		<div id="left-column">
			<div class="moudule" id="borrowed-books">
				<h3 class="moudule-header"><a href="javascript:void(0);">借阅的书</a></h3>
				<div class="moudule-content">
					<s:if test="borrows.isEmpty()">
						<div class="tips">
							<s:property value="name" escape="false" />还未开始借阅书籍
						</div>
					</s:if>
					<s:iterator value="borrows">
						<div class="book-item">
							<a class="cover" href="book/<s:property value="sharing.book.id" />"><img src="<s:property value="sharing.book.middleImg" escape="false" />" title="《<s:property value="sharing.book.title" escape="false" />》" /></a>
							<a title="《<s:property value="sharing.book.title" escape="false" />》" href="book/<s:property value="sharing.book.id" />"><s:property value="sharing.book.title" escape="false" /></a>
						</div>
					</s:iterator>
					
					<div class="clr"></div>
				</div>		
			</div>
			
			<div class="moudule" id="shared-books">
				<h3 class="moudule-header"><a href="javascript:void(0);">共享的书</a></h3>
				<div class="moudule-content">
					<s:if test="providedBooks.isEmpty()">
						<div class="tips">
							<s:property value="name" escape="false" />没有共享书籍
						</div>
					</s:if>
					<s:iterator value="providedBooks">
						<div class="book-item">
							<a class="cover" href="book/<s:property value="id" />"><img src="<s:property value="middleImg" escape="false" />" title="《<s:property value="title" escape="false" />》" /></a>
							<a title="《<s:property value="title" escape="false" />》" href="book/<s:property value="id" />"><s:property value="title" escape="false" /></a>
						</div>
					</s:iterator>
					<div class="clr"></div>
				</div>
				
			</div>
			
			<div class="moudule" id="reviews">
				<h3 class="moudule-header">书评</h3>
				<s:if test="reviews.empty">
					<div class="tips">
						没有写过书评
					</div>
				</s:if>
				<s:else>
					<div class="moudule-content">
						<s:iterator value="reviews">
							<div class="review-item">
								<div class="left">
									<h4><a href="/review/<s:property value="id" />"><s:property value="title" escape="false" /></a></h4>
									<div class="content">
										<s:if test="content.length() > 150">
											<s:property value="content.substring(0, 150)" escape="false" />...<a class="more" href="/review/<s:property value="id" />">(阅读全文)</a>
										</s:if>
										<s:else>
											<s:property value="content" escape="false" />
										</s:else>
									</div>
								</div>
								<a class="scover" href="/book/<s:property value="book.id" />">
									<img src="<s:property value="book.smallImg" />" title="<s:property value="book.title" escape="false" />" />
								</a>
								<div class="clr"></div>
							</div>
						</s:iterator>
					</div>
				</s:else>
				
			</div>
						
		</div>
		
		<div id="right-column">
			<div id="user-info">
				<div id="user-meta">
					<img src="<s:property value="largeAvatar" />" />
						<a href="javascript:void(0);"><s:property value="name" escape="false" /></a>
						<br />
						借阅数： <s:property value="borrowedCount" escape="false" />
						<br />
						共享数： <s:property value="sharingCount" escape="false" />
						<br />
						积分：<s:property value="user.integral"/>
						<br /> 
						<span style='float:left'>信誉度：</span>
						<span  id='cLevelBox'></span> <br/>
						<a id="send-msg" href="/message/create/<s:property value="id" />">发私信</a>
					</p>
					<div class="clr"></div>
				</div>
				<h3>个人描述</h3>
				<div>
					<jshy:prefixAndSuffixProperty regExp=".+" value="description" prefix="" suffix="<br />" escape="false" />
				</div>
			</div>
		</div>
		
		<div class="clr"></div>
	</div>
    
    <%@include file="../commons/footer.jsp" %>
      
  </body>
</html>
