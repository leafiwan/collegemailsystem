<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>安装蚂蚁书屋</title>

  </head>
  
  <body>
  	<h1>安装蚂蚁书屋</h1>
  	
  	安装程序将会初始化蚂蚁书屋并且根据下面的表单建立一个管理员账户
  	
  	<form method="post" action="/admin/install">
  		<p>
  			<label for="email">邮箱：</label>
  			<input type="text" name="email" />
  		</p>
  		<p>
  			<label for="password">密码：</label>
  			<input type="password" name="password" />
  		</p>
  		<p>
  			<label for="adminPassword">后台密码：</label>
  			<input type="password" name="adminPassword" />
  		</p>
  		<p>
  			<label for="name">姓名：</label>
  			<input type="text" name="name" />
  		</p>
  		<p>
  			<label for="schoolName">学校名：</label>
  			<input type="text" name="schoolName" />
  		</p>
  		<p>
  			<input type="submit" value="开始安装" />
  		</p>
  	</form>
  </body>
</html>
