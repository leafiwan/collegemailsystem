<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <base href="<%=basePath%>" />
    
    <title>为《<s:property value="title" escape="false" />》写书评 - 蚂蚁书屋</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="keywords" content="蚂蚁书屋,书评" />
	<meta http-equiv="description" content="为《<s:property value="title" escape="false" />》写书评" />
	
	<link rel="stylesheet" href="css/common.css" type="text/css" />
	<link rel="stylesheet" href="css/book.css" type="text/css" />
	
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/commons.js"></script>
	<script type="text/javascript">
		
		$(function() {
			convertAnchorToSubmit($('#review-form .sdbtn'));
		});

	</script>
	
  </head>
  
  <body>
  	<%@include file="../commons/header.jsp" %>
  	
  	<div id="main">
		<h2 class="header2">为《<s:property value="title" escape="false" />》写书评</h2>
		
		<div id="left-column">
				<s:if test="#parameters['msg'] != null">
					<%
							Map<String, String> tips = new HashMap<String, String>();
							tips.put("titleTooLong", "标题太长，请不要超过 50 个字");
							tips.put("titleEmpty", "请给您的书评写一个标题");
							tips.put("contentEmpty", "内容不能为空");
							tips.put("contentTooLong", "内容太长，请不要超过 65535 个字");
					%>
					<div class="tips error">
						<%=tips.get(request.getParameter("msg"))%>
					</div>
				</s:if>
			<form id="review-form" action="/review/create" method="post">
				<p>
					标题：<input id="title" type="text" class="input" name="title" />
				</p>
				<p>
					内容：
				</p>
				<p>
					<textarea class="input" id="content" name="content"></textarea>
				</p>
				<p>
					<input type="hidden" name="bid" value="<s:property value="id" />" />
					<a href="javascript:void(0);" class="sdbtn"><span class="sdbtn-inner"></span>发布</a>
				</p>
			</form>
						
		</div>
		
		<div id="right-column">
			<div class="moudule">
				<div class="moudule-content">
					<a class="back-link" href="/book/<s:property value="id" />">&lt;&lt;回到《<s:property value="title" />》的页面</a>
				</div>
			</div>
			<div class="moudule" id="book-info">
				<h3 class="moudule-header">书籍信息</h3>
				<div class="moudule-content">
					<img src="<s:property value="middleImg" />" class="cover" />
					<p>
						<span class="label">作者:</span> <s:property value="author" escape="false" /><br />
						<span class="label">出版社:</span> <s:property value="publisher" escape="false" /><br />
						<span class="label">出版时间:</span> <s:property value="publishDate" escape="false" /><br />
					</p>
				</div>
			</div>
		</div>

		
		<div class="clr"></div>
	</div>
  	
  	<%@include file="../commons/footer.jsp" %>
  </body>
</html>
