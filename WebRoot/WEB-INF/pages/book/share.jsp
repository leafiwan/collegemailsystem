<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <base href="<%=basePath%>" />
    
    <title>共享《<s:property value="title" escape="false" />》 - 蚂蚁书屋</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="keywords" content="蚂蚁书屋,共享《<s:property value="title" escape="false" />》" />
	<meta http-equiv="description" content="共享《<s:property value="title" escape="false" />》的页面" />
	
	<link rel="stylesheet" href="css/common.css" type="text/css" />
	<link rel="stylesheet" href="css/book.css" type="text/css" />
	
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/commons.js"></script>
	<script type="text/javascript">
		
		$(function() {
			convertAnchorToSubmit($('#book-share-form .sdbtn'));
		});

	</script>
	
  </head>
  
  <body>
  	<%@include file="../commons/header.jsp" %>
  	
  	<div id="main">
		<h2 class="header2">共享《<s:property value="title" escape="false" />》</h2>
		
		<div id="left-column">
				<s:if test="#parameters['msg'] != null">
					<%
							Map<String, String> tips = new HashMap<String, String>();
							tips.put("wrongDay", "借出天数错误");
							tips.put("requirementsTooLong", "要求请不要超过 300 个字符");
					%>
					<div class="tips error">
						<%=tips.get(request.getParameter("msg"))%>
					</div>
				</s:if>
			<form id="book-share-form" action="/sharing/add" method="post">
				<p>
					我希望借阅者在 <input id="maxBorrowDay" class="input" type="text" name="maxBorrowDay" /> 天内还书
				</p>
				<p>
					对借阅者的其他要求:
				</p>
				<p>
					<textarea id="requirements" class="input" name="requirements"></textarea>
				</p>
				<p>
					<input type="hidden" name="bookId" value="<s:property value="id" />" />
					<s:if test="isbn13 != null">
						<input type="hidden" name="isbn" value="<s:property value="isbn13" />" />
					</s:if>
					<s:elseif test="isbn10 != null">
						<input type="hidden" name="isbn" value="<s:property value="isbn10" />" />
					</s:elseif>
					<s:else>
						<input type="hidden" name="isbn" value="<s:property value="#parameters['isbn'][0]" />" />
					</s:else>
					<a href="javascript:void(0);" class="sdbtn"><span class="sdbtn-inner"></span>提交</a>
				</p>
			</form>
						
		</div>
		
		<div id="right-column">
			<div class="moudule" id="book-info">
				<h3 class="moudule-header">书籍信息</h3>
				<div class="moudule-content">
					<img src="<s:property value="middleImg" />" class="cover" />
					<p>
						<span class="label">作者:</span> <s:property value="author" escape="false" /><br />
						<span class="label">出版社:</span> <s:property value="publisher" escape="false" /><br />
						<span class="label">出版时间:</span> <s:property value="publishDate" escape="false" /><br />
					</p>
				</div>
			</div>
		</div>

		
		<div class="clr"></div>
	</div>
  	
  	<%@include file="../commons/footer.jsp" %>
  </body>
</html>
