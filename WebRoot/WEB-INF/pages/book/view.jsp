<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <base href="<%=basePath%>" />
    
    <title><s:property value="title" escape="false" /> - 蚂蚁书屋</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="keywords" content="蚂蚁书屋,<s:property value="title" escape="false" />" />
	<meta http-equiv="description" content="<s:property value="title" escape="false" />的页面" />
	
	<link rel="stylesheet" href="css/common.css" type="text/css" />
	<link rel="stylesheet" href="css/book.css" type="text/css" />
	
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/commons.js"></script>
	<script type="text/javascript">
	
	
	
	</script>
	
  </head>
  
  <body>
    
    <%@include file="../commons/header.jsp" %>
    
    <div id="main">
		<h2 class="header2"><s:property value="title" escape="false" /></h2>
		
		<div id="left-column">
			<div id="book-info">
				<div id="book-meta">
					<img id="cover" src="<s:property value="middleImg" escape="false" />" />
					<p>
						<span class="label">作者：</span><s:property value="author" escape="false" /><br />
						<span class="label">出版社：</span><s:property value="publisher" escape="false" /><br />
						<span class="label">出版时间：</span><s:property value="publishDate" escape="false" /><br />
						<span class="label">页数：</span><s:property value="pages" escape="false" /><br />
						<span class="label">定价：</span><s:property value="price" escape="false" /><br />
						<span class="label">ISBN：</span><s:property value="isbn13" escape="false" /><br />
					</p>
					<div class="clr"></div>
				</div>
				
				<div id="book-data">
					<p class="data-item">
						<span class="label">库存</span><br />
						<s:property value="inventory" escape="false" />
					</p>
					<span class="separator"></span>
					<p class="data-item">
						<span class="label">书评</span><br />
						<s:property value="reviewsCount" />
					</p>
					<span class="separator"></span>
					<p class="data-item">
						<span class="label">评分</span><br />
						<s:property value="rate" />
					</p>
				</div>
				
				<div id="operation">
					<jshy:hasPermission disallowRoles="Book.BORROWINGER,Book.REQUESTER,Book.PROVIDER">
						<a class="operation-item" href="/book/<s:property value="id" />/borrow">借阅</a>
						<a class="operation-item" href="/book/<s:property value="id" />/share">共享</a>
					</jshy:hasPermission>
					
					<jshy:hasPermission allowRoles="Book.PROVIDER">
						<p>
							我共享了这本书
						</p>
					</jshy:hasPermission>
					
					<jshy:hasPermission allowRoles="Book.BORROWINGER">
						<p>
							我正在借阅这本书
						</p>
					</jshy:hasPermission>
					
					<jshy:hasPermission allowRoles="Book.REQUESTER">
						<p>
							我正在请求借阅这本书
						</p>
					</jshy:hasPermission>				
					<jshy:hasPermission allowRoles="Book.BORROWER">
						<p>
							我借阅过这本书
						</p>
					</jshy:hasPermission>
				</div>
<%--				<jshy:hasPermission allowRoles="Book.PROVIDER">--%>
<%--					<a id="unshare" href="javascript:void(0);">取消共享</a>--%>
<%--				</jshy:hasPermission>--%>
				
				
							
				<div class="clr"></div>
			</div>
			
			<div id="other-opt" class="moudule">
				<div class="moudule-content">
					<a href="/book/<s:property value="id" />/review">写书评</a>
				</div>
			</div>
			
			<div id="intro" class="moudule">
				<h3 class="moudule-header">内容简介</h3>
				<div class="moudule-content">
						<jshy:prefixAndSuffixProperty regExp=".+" value="intro" prefix="<p>" suffix="</p>" escape="false" />
				</div>
			</div>
			
			<div id="author-intro" class="moudule">
				<h3 class="moudule-header">作者简介</h3>
				<div class="moudule-content">
						<jshy:prefixAndSuffixProperty regExp=".+" value="authorIntro" prefix="<p>" suffix="</p>" escape="false" />					
				</div>
			</div>
			
			<div id="book-review" class="moudule">
				<h3 class="moudule-header">书评 <a href="/book/<s:property value="id" />/reviews" class="more">(全部)</a></h3>
				<div class="moudule-content">
					<s:if test="reviews.empty">
						<div class="tips">
							暂时还没有人发表书评
						</div>
					</s:if>
					<s:else>
						<s:iterator value="reviews">
							<div class="review-list-item">
								<a href="/user/<s:property value="author.id" />" class="avatar"><img src="<s:property value="author.smallAvatar" />" /></a>
								<div class="text">
									<div class="meta">
										<h4><a href="/user/<s:property value="author.id" />"><s:property value="author.name" escape="none" /></a>：<a href="/review/<s:property value="id" />"><s:property value="title" escape="false" /></a></h4>								
									</div>
									<div class="content">
										<s:if test="content.length() > 150">
											<s:property value="content.substring(0, 150)" escape="false" />...<a class="more" href="/review/<s:property value="id" />">(阅读全文)</a>
										</s:if>
										<s:else>
											<s:property value="content" escape="false" />
										</s:else>												
									</div>
								</div>
								<div class="clr"></div>
							</div>
						</s:iterator>
						
					</s:else>
						
				</div>
			</div>
			
			
			
		</div>
		
		<div id="right-column">
			<div class="moudule" id="borrowed-users">
				<h3 class="moudule-header"><a href="javascript:void(0);">谁借过这本书</a></h3>
				<div class="moudule-content">
					<s:iterator value="borrowers">
						<div class="item">
							<a class="avatar" href="/user/<s:property value="id" />"><img src="<s:property value="smallAvatar" />" /></a>
							<a href="/user/<s:property value="id" />"><s:property value="name" /></a>
						</div>
					</s:iterator>		
					<div class="clr"></div>
				</div>
			</div>
			
			
			
			<div class="moudule" id="shared-users">
				<h3 class="moudule-header"><a href="javascript:void(0);">谁共享了这本书</a></h3>
				<div class="moudule-content">
					<s:iterator value="providers">
						<div class="item">
							<a class="avatar" href="/user/<s:property value="id" />"><img src="<s:property value="smallAvatar" />" /></a>
							<a href="/user/<s:property value="id" />"><s:property value="name" /></a>
						</div>
					</s:iterator>									
					<div class="clr"></div>
				</div>
			</div>
		</div>
		<div class="clr"></div>
	</div>
    
    
    <%@include file="../commons/footer.jsp" %>
    
  </body>
</html>
