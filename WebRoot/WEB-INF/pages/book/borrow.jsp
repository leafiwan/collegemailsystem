<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <base href="<%=basePath%>" />
    
    <title>借阅《<s:property value="title" escape="false" />》 - 蚂蚁书屋</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="keywords" content="蚂蚁书屋,借阅《<s:property value="title" escape="false" />》" />
	<meta http-equiv="description" content="借阅《<s:property value="title" escape="false" />》的页面" />
	
	<link rel="stylesheet" href="css/common.css" type="text/css" />
	<link rel="stylesheet" href="css/book.css" type="text/css" />
	
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/jquery.blockUI.js"></script>
	<script type="text/javascript" src="js/commons.js"></script>
	<script type="text/javascript" src="js/book.js"></script>
	<script type="text/javascript">
	
		$(initBorrowPage);
	
	</script>
	
  </head>
  
  <body>
  	<%@include file="../commons/header.jsp" %>
  	
  	<div id="main">
		<h2 class="header2">借阅《<s:property value="title" escape="false" />》</h2>
		<ul class="range">
			范围：
			<s:if test="range == 0">
				<li><a href="/book/<s:property value="id" />/borrow?range=1">全部</a></li>
				<li><a href="javascript:void(0);" class="current">同校</a></li>
			</s:if>
			<s:else>
				<li><a href="javascript:void(0);" class="current">全部</a></li>
				<li><a href="/book/<s:property value="id" />/borrow">同校</a></li>
			</s:else>
		</ul>
		<form id="request-form" method="post" action="/sharing/request">
			<input type="hidden" name="sid" value="0" />
		</form>
		
		<div id="left-column">
			<s:if test="bookSharings.elementsCount > 0">
				<div id="provider-list">
					<s:iterator value="bookSharings.elements">
						<div class="item">
							<a class="avatar" href="/user/<s:property value="user.id" />"><img src="<s:property value="user.smallAvatar" />" title="<s:property value="user.name" />" /></a>
							<div class="item-info">
								<p>
									<a _sid="<s:property value="id" />" href="/user/<s:property value="user.id" />"><s:property value="user.name" /></a> 愿意借出 <s:property value="maxBorrowDay" /> 天
								</p>
								<s:if test="currentUser.integral > 0">
								<a class="borrow-btn sdbtn" href="javascript:void(0);"><span class="sdbtn-inner"></span>向他借阅</a>						
								<div class="requirements">
									<s:property value="requirements" escape="false" />
								</div>	
								</s:if>
								<s:else>
									<s:i18n name="">
										<s:text name="integralNotEnough"></s:text>
									</s:i18n>
								</s:else>
							</div>
							<div class="clr"></div>
						</div>
					</s:iterator>
					<div class="clr"></div>
				</div>
			</s:if>
			<s:else>
				<div class="tips">没有相应的分享</div>
			</s:else>
			
						
		</div>
		
		<div id="right-column">
			<div class="moudule">
				<h3 class="moudule-header">借阅期限是什么？</h3>
				<div class="moudule-content">
					借阅期限是共享者在共享书籍时填写的，表示他愿意将本书借给您多少天。借阅时，请结合自己的实际情况参考借阅期限。
				</div>
			</div>
			<div class="moudule">
				<h3 class="moudule-header">超出借阅期限会怎样？</h3>
				<div class="moudule-content">
					如果您借阅的时间超出了借阅期限，共享者有可能会在还书之后给您差评，这将影响到您的信誉情况。如果您因为一些特殊原因不能按时还书，请务必与共享者进行沟通。
				</div>
			</div>
		</div>

		
		<div class="clr"></div>
	</div>

  	<%@include file="../commons/footer.jsp" %>
  </body>
</html>
