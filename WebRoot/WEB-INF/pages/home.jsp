<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <base href="<%=basePath%>" />
    
    <title>首页 - 蚂蚁书屋</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="keywords" content="蚂蚁书屋,首页" />
	<meta http-equiv="description" content="蚂蚁书屋的首页,所有重要的信息" />
	
	<link rel="stylesheet" href="css/common.css" type="text/css" />	
	<link rel="stylesheet" href="css/home.css" type="text/css" />	
	
	
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/commons.js"></script>
	<script type="text/javascript">
		$(function() {
			// ajax of notifications
			/*$('#h-notifications ul li a.notification').click(function() {
				
				var ids = new Array();
				
				var hrefVal = $(this).attr('href');
				// find notifications whose href attribute as same as $(this)'s
				var sameUrlLinks = $('#h-notifications ul li a.notification[href="' + hrefVal +'"]');
				$.each(sameUrlLinks, function() {
					ids.push($(this).attr('_nid'));
				});
				
				var ajaxUrl = '/notification/delete?' + $.param({ids: ids}, true);
						
				$.get(ajaxUrl);
				
				sameUrlLinks.parents('li').remove();
				
				if($('#h-notifications ul li a.notification').length == 0) {
					$('#h-notifications').remove();
				}
				
			});*/
			
			$('#h-notifications .operations .ignore').click(function() {
				$('#h-notifications').slideUp(); 
				var lis = $('.moudule-content ul li');
				var ids = new Array();
				for(n = 0;n < lis.length;n++){
				ids.push($(lis[n]).attr('ids'));
				ids.push(6);
				var ajaxUrl = '/notification/delete?' + $.param({ids: ids}, true);
				$.get(ajaxUrl);
				}
				
			});
		});
	</script>
	
  </head>
  
  <body>
    
    <%@include file="commons/header.jsp" %>
    
	<div id="main">
		<div id="left-bar">
			<%@ include file="commons/leftbar-nav.jsp" %>
		</div>
		
		<div id="content-wrapper">
				<s:if test="!displayedNotifications.empty">
					<div class="moudule" id="h-notifications">
						<h3 class="moudule-header">通知</h3>
						<div class="moudule-content">
							<ul>
								<s:iterator value="displayedNotifications">			
									<li ids='<s:property value="notification.id"/>'><s:property value="html" escape="false"/></li>
								</s:iterator>
							</ul>
							<div class="operations">
								<a class="ignore" href="javascript:void(0);">忽略</a>
								<a class="view-all" href="/notification/mine">查看全部</a>
								<div class="clr"></div>
							</div>					
						</div>
					</div>
				</s:if>
				
				<div id="borrowed-books" class="moudule">
					<h3 class="moudule-header">我借阅的书</h3>
					<div class="moudule-content">
						<s:if test="borrowedBooks.isEmpty()">
							<div class="tips">
								我还没有借阅任何书，去 <a href="bookbase">书库</a> 看看有没有想读的书
							</div>		
						</s:if>
						<s:iterator value="borrowedBooks">
							<div class="item">
								<a class="cover" href="/book/<s:property value="id" />"><img src="<s:property value="smallImg" escape="false" />" /></a>
								<div class="info">
									<a href="/book/<s:property value="id" />"><s:property value="title" escape="false" /></a><br />
									<s:property value="author" escape="false" /><br />
									<s:property value="publisher" escape="false" /><br />
									<s:property value="publishDate" escape="false" />
								</div>
								<div class="clr"></div>
							</div>
						</s:iterator>
						<div class="clr"></div>
					</div>
				</div>
				
				<div id="shared-books" class="moudule">
					<h3 class="moudule-header">我共享的书</h3>
					<div class="moudule-content">
						<s:if test="sharedBooks.isEmpty()">
							<div class="tips">
								我还没有 <a href="share">共享</a> 任何书
							</div>
						</s:if>
						<s:iterator value="sharedBooks">
							<div class="item">
								<a class="cover" href="/book/<s:property value="id" />"><img src="<s:property value="smallImg" escape="false" />" /></a>
								<div class="info">
									<a href="/book/<s:property value="id" />"><s:property value="title" escape="false" /></a><br />
									<s:property value="author" escape="false" /><br />
									<s:property value="publisher" escape="false" /><br />
									<s:property value="publishDate" escape="false" />
								</div>
								<div class="clr"></div>
							</div>
						</s:iterator>
						<div class="clr"></div>
					</div>
				</div>
		</div>
		
		<div id="right-bar">
		
		</div>
		
		<div class="clr"></div>
	</div>   
    
    <%@include file="commons/footer.jsp" %>
  </body>
</html>
