<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <base href="<%=basePath%>" />
    
    <title>通知 - 蚂蚁书屋</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="keywords" content="蚂蚁书屋,通知" />
	<meta http-equiv="description" content="我在蚂蚁书屋所有的通知列表" />
	
	<link rel="stylesheet" href="css/common.css" type="text/css" />	
	<link rel="stylesheet" href="css/home.css" type="text/css" />	
	
	
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/commons.js"></script>

	<script type="text/javascript">
		$(function() {
			
			$('#notification-list ul li .ignore').each(function(){
				var url = "/notification/delete?nid=" + $(this).attr('_nid');
				$(this).bind('click',function(){
				$.get(url);
				$(this).parents('li').slideUp();
				$("#total").text(parseInt($("#total").text()) - 1);
				});
			});
			/*
			$('#notification-list ul li a.notification').click(function() {				var url = "/notification/delete?ids=";
				var hrefVal = $(this).attr('href');
				var ids = new Array();
				
				var hrefVal = $(this).attr('href');
				// find notifications whose href attribute as same as $(this)'s
				var sameUrlLinks = $('#notification-list ul li a.notification[href="' + hrefVal +'"]');
				$.each(sameUrlLinks, function() {
					ids.push($(this).attr('_nid'));
				});
				
				var ajaxUrl = '/notification/delete?' + $.param({ids: ids}, true);
				console.log(ajaxUrl);
				$.get(ajaxUrl);
				
				sameUrlLinks.parents('li').remove();
				$("#total").text(parseInt($("#total").text()) - sameUrlLinks.length);
				
			});
			
			*/
			$('#notification-list ul li').hover(function() {
				$(this).children('a.ignore').fadeIn(100);
			}, function() {
				$(this).children('a.ignore').fadeOut(100);
			});
			
			
		});
	</script>
	
  </head>
  
  <body>
    
    <%@include file="../commons/header.jsp" %>
    
	<div id="main">
		<div id="left-bar">
			<%@ include file="../commons/leftbar-nav.jsp" %>
		</div>
		
		<div id="content-wrapper">
				<h2 class="header2">通知</h2>
				<s:if test="page.elementsCount == 0">
					<div class="tips">
						没有未读的通知
					</div>
				</s:if>
				<s:else>					
					<div class="moudule" id="notification-list">
						<div class="meta">
							共有 <span id="total"><s:property default="0" value="page.elementsCount" /></span> 条未读通知
						</div>
						<div class="moudule-content">
							<ul>
								<s:iterator value="page.elements">
									<li>
										<s:property value="html" escape="false" />
										<a  _nid='<s:property value="notification.id"/>' class="ignore" href="javascript:void(0);">忽略</a>
									</li>
								</s:iterator>
							</ul>
						</div>
					</div>
					<jshy:paginator value="page" />
				</s:else>
				
		</div>
		
		<div id="right-bar">
		
		</div>
		
		<div class="clr"></div>
	</div>   
    
    <%@include file="../commons/footer.jsp" %>
  </body>
</html>
