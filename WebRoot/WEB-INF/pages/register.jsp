<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <base href="<%=basePath%>" />
    
    <title>注册 - 蚂蚁书屋</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="keywords" content="蚂蚁书屋,注册" />

	<link rel="stylesheet" href="css/common.css" type="text/css" />	
	<link rel="stylesheet" href="css/other.css" type="text/css" />	
	
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/commons.js"></script>
	<script type="text/javascript">
	
	</script>
	
  </head>

<body>
	<%@include file="commons/header.jsp" %>
    
	<div id="main">
		<h2 class="header2">注册账户</h2>	
		
<%--		<s:if test="#parameters['msg'] != null">--%>
<%--			<div class="tips wrong">--%>
<%--				<s:property value="#parameters['msg']" />--%>
<%--			</div>--%>
<%--		</s:if>--%>
		
		<div id="left-column">
			<form id="register-form" action="/user/register" method="post">
				<p>
					<label for="email">邮箱：</label>
					<input class="input" type="text" name="email" />
				</p>
				<p>
					<label for="password">密码：</label>
					<input class="input" type="password" name="password" />
				</p>
				<p>
					<label for="password2">确认密码：</label>
					<input class="input" type="password" name="password2" />
				</p>
				<p>
					<label for="name">真实姓名：</label>
					<input class="input" type="text" name="name" />
				</p>
				<p>
					<label for="school_id">所在学校：</label>
					<select class="input" name="school_id">
						<s:iterator value="schools">
							<option value="<s:property value="id" />"><s:property value="name" /></option>
						</s:iterator>
					</select>
				</p>
				<p>
					<label></label>
					<input class="button" type="submit" value="注册" />
				</p>
			</form>
		</div>
		
		<div id="right-column">
			<div class="moudule">
				<h3 class="moudule-header">欢迎注册</h3>
				<div class="moudule-content">
					得益于大家的分享精神，蚂蚁书屋能够帮助您借到想阅读的书籍。<br />
					我们希望您也做一个爱分享的人。
				</div>
			</div>
		</div>
		
		<div class="clr"></div>
	</div>   
    
    <%@include file="commons/footer.jsp" %>
  </body>

</html>
