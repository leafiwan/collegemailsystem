<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <base href="<%=basePath%>" />
    
    <title>蚂蚁书屋</title>
    
	<meta http-equiv="keywords" content="蚂蚁书屋,图书馆,社会化图书馆" />
	<meta http-equiv="description" content="社会化图书馆，借阅想看的书，交志同道合的朋友" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style type="text/css">
	
		body {
			background: #0f73fc;
			margin-top: 0;
			font-family: "Microsoft Yahei","微软雅黑",Tahoma,Arial,Helvetica,STHeiti;
		}
		
		#wrapper {
			width: 960px;
			height: 567px;
			position: absolute;
			top: 0;
			left: 50%;
			margin-left: -480px;
			background: url(images/index-bg.jpg) center no-repeat;
		}
		
		h1 {
			width: 100%;
			height: 95px;
			position: absolute;
			top: 172px;
			background: url(images/index-logo.png) center no-repeat;
			text-indent: -999999em;	
		}
	
		
		h2 {
			width: 100%;
			height: 23px;
			text-indent: -99999em;
			position: absolute;
			top: 282px;
			background: url(images/index-slogn.png) center no-repeat;
			
		}
		
		
		form {
			
			width: 100%;
			text-align: center;
			position: absolute;
			top: 370px;
		
		}
		
		
		form .input {
			padding: 3px;
			font-size: 16px;
			width: 170px;
			height: 23px;
			line-height: 23px;
			-webkit-border-radius: 6px;
			-moz-border-radius: 6px;
			border-radius: 6px;
			background: #FFFFFF;
			border: 1px solid #245f95;
			-webkit-box-shadow: 0 0 8px #666666;
			-moz-box-shadow: 0 0 8px #666666;
			box-shadow: 0 0 8px #666666;
		}
		
		#register {
			width: 100%;
			display: block;
			position: absolute;
			top: 470px;
			text-align: center;
		}
		
		#register a {
			text-decoration: none;
			color: #fff;
			padding: 5px 40px;
			border: 1px solid #0431B4;
			-webkit-border-radius: 4px;
			-moz-border-radius: 4px;
			border-radius: 4px;
			font-size: 14px;
			background: #013ADF;
		}
		
		.button {
		  background: #1556db;
		  width: 50px;
		  color: #ffffff;
		  font-size: 16px;
		  padding: 5px 8px;
		  text-decoration: none;
		  -webkit-border-radius: 6px;
		  -moz-border-radius: 6px;
		  border-radius: 6px;
		  -webkit-box-shadow: 0px 1px 3px #666666;
		  -moz-box-shadow: 0px 1px 3px #666666;
		  text-shadow: 1px 1px #184bb3;
		  border: solid #04226b 1px;
		  background: -webkit-gradient(linear, 0 0, 0 100%, from(#4e93cc), to(#1e56c7));
		  background: -moz-linear-gradient(top, #4e93cc, #1e56c7);
		  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#4e93cc',endColorstr='#1e56c7');
		  cursor: pointer;
		  font-family: "Microsoft Yahei","微软雅黑",Tahoma,Arial,Helvetica,STHeiti;
		}
		
		.button:hover {
		  background: #3a8cfb;
		}
		
		#remember {
			width: 420px;
			height: 20px;
			line-height: 20px;
			text-align: left;
			margin: 0 auto;
			font-size: 12px;
			color: #04226b;
			text-shadow: 1px 1px #a9c4fb;
			position: relative;
		}
		
		#footer {
			color: #04226b;
			text-align: center;
			font-size: 12px;
			position: absolute;
			width: 100%;
			top: 540px;
			text-shadow: 1px 1px #a9c4fb;
		}
		
		#remember a {
			text-decoration: none;
			color: #04226b;
			position: absolute;
			right: 0;
		}
		
		#remember a:hover {
			color: #d9e4fa;
			text-shadow: none;
		}
		
		label.inner {
			display: none;
			cursor: text;
			position: absolute;
			left: 0.5em;
			top: 0px;
			color: #555;
			height: 31px;
			line-height: 31px;
		}
		
		div.wrapper {
			margin: 0;
			padding: 0;
			float: left;
			width: 178px;
			margin: 0 2px;
			height: 31px;
			line-height: 31px;
			position: relative;
		}
		
		.errorMsg {
			width: 170px;
			margin: 0 auto;
			padding: 5px 10px;
			background: #0040FF;
			position: relative;
			top: 335px;
			text-align: center;
			font-size: 13px;
			color: #fff;
			-moz-border-radius: 5px;
			-webkit-border-radius: 5px;
			border-radius: 5px;
			border: 1px solid #013ADF;
		}
		
		#form-wrapper {
			width: 415px;
			margin: 0 auto;
		}
		
	</style>
	
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/commons.js"></script>
	<script type="text/javascript">
	
		$(function() {
			optimizeCheckbox($('#remember input'));
			initInnerLabelInput($("form input[name='email']"));
			initInnerLabelInput($("form input[name='password']"));
		});
	
	</script>	
  </head>
  
  <body>
      <div id="wrapper">
            <h1>蚂蚁书屋</h1>
            <h2>书，非借不能读也</h2>
            <s:if test="#parameters['msg'] != null">
            	<div class="errorMsg">
            		<s:if test="#parameters['msg'][0] == 'emailEmpty'">请填写 E-mail</s:if>
            		<s:elseif test="#parameters['msg'][0] == 'emailWrong'">E-mail 格式错误</s:elseif>
            		<s:elseif test="#parameters['msg'][0] == 'pwdEmpty'">请填写密码</s:elseif>
            		<s:elseif test="#parameters['msg'][0] == 'pwdWrong'">密码长度在 5-20 个字符之间</s:elseif>
            		<s:elseif test="#parameters['msg'][0] == 'notMatch'">邮箱或密码错误</s:elseif>
            	</div>
            </s:if>
            
            	<form action="user/login" method="post">
            		<div id="form-wrapper">
						<div class="wrapper">
							<label class="inner" for="email">邮箱</label>
							<input class="input" type="text" name="email" />
						</div>
						<div class="wrapper">
							<label class="inner" for="password">密码</label>
							<input type="password" class="input" name="password" />
						</div>
		                <input type="submit" class="button" value="登录" />
						<p id="remember">
							<input type="checkbox" name="rememberMe" /><label for="rememberMe">记住密码</label>
							<a href="/user/resetPassword">忘记密码?</a>
						</p>
					</div>
	            </form>
            
            <div id="register">
        	    <a href="/register">注册账户</a>
            </div>
            
            <div id="footer">
                &copy; 2012 蚂蚁书屋
            </div>
	</div>
	
  </body>
</html>
