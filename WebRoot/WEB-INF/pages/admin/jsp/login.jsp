<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>后台登录 - 蚂蚁书屋</title>

  </head>
  
  <body>
    
    <h1>后台登录</h1>
    <form action="/admin/administrator/login" method="post">
    	<label for="newPassword">请输入后台密码：</label>
    	<input type="password" name="newPassword" />
    	<input type="submit" value="登录" />
    </form> 
    
  </body>
</html>
