<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="jshy" uri="/jshy-tags" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>My JSP 'book-list.jsp' starting page</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">
		<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<script type="text/javascript">
	function comfirmDelete(){
		confirm("确认删除？");
	}
	</script>

	</head>

	<body>
		<h2>
			编辑图书
		</h2>
		<form action="/admin/book/search" method="post">
			<table align="left" border="0">
				<tr><td>按ISBN搜索：</td><td><input type="text" name="isbnInfo"></td></tr>
				<tr><td>按书名搜索：</td><td><input type="text" name="nameInfo"></td></tr>
				<tr><td>按作者搜索：</td><td><input type="text" name="authorInfo"></td></tr>
				<tr><td>按出版社搜索：</td><td><input type="text" name="publisherInfo"></td></tr>
			</table>
			<input type="submit" value="搜索">
		</form>
		<br />
		<br />
		<s:if test="books.elements.isEmpty()">
			没有任何图书
		</s:if>
		<s:else>
		<table align="left" border="0">
			<s:iterator value="books.elements">
				<tr>
					<td><s:property value="title" escape="false" /></td>
					<td><s:property value="author" escape="false" /></td>
					<td><s:property value="publisher" escape="false" /></td>
					<td><a href="/admin/book/editor?bookId=<s:property value="id" escape="false" />">编辑</a></td>
					<td><a href="/admin/book/delete?bookId=<s:property value="id" escape="false" />" onclick="comfirmDelete();">删除</a></td>
				</tr>
			</s:iterator>
		</table>
		</s:else>
		<br />
		<br />
		<br />
		<br />
		<div style="clear: left;">
			<jshy:paginator value="books" />
		</div>
	</body>
</html>
