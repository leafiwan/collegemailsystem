<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'book-update.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
  	<h2>《<s:property value="book.title" escape="false"/>》的编辑</h2>
  	<form action="admin/book/update" method="post">
  		<input type="hidden" name="bookId" value="<s:property value="book.id" escape="false" />" />
  	  	作者简介:<textarea name="authorIntro" rows="8" cols="60"><s:property value="book.authorIntro" escape="false" /></textarea><br/>
  	  	书籍简介:<textarea name="intro" rows="8" cols="60"><s:property value="book.intro" escape="false" /></textarea><br/>
  	  	<input type="submit" value="提交"/>
  	</form>
  	<br />
  	<br />
  	<br />
  	<br />
  	<br />
  	<a href="/admin/book/view?operationType=2">返回书籍列表</a>
  </body>
</html>
