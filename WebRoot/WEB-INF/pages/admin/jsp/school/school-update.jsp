<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="jshy" uri="/jshy-tags"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'school-update.jsp' starting page</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

</head>

<body>
	<h2>编辑学校</h2>
	<s:if test="schools.elements.isEmpty()">
			还没有添加学校：<a href="/admin/school/add">单机此处添加</a>
	</s:if>
	<table align="left" border="0">
		<s:iterator value="schools.elements">
			<tr>
				<td><s:property value="id" escape="false" /></td>
				<td><s:property value="name" escape="false" /></td>
				<td><s:property value="addrees" escape="false" /></td>
				<td>
					<a href="/admin/school/editor?schoolId=<s:property value="id" escape="false" />">编辑</a>
				</td>
			</tr>
		</s:iterator>
	</table>
	<div style="clear: left;">
		<jshy:paginator value="schools" />
	</div>
	<s:property escape="false" value="errorMsg" />
</body>
</html>
