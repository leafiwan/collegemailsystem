<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'school-editor.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
  	<h2>学校：<s:property value="school.name" escape="false"/>的编辑</h2>
  	<form action="admin/school/update" method="post">
  		<input type="hidden" name="schoolId" value="<s:property value="school.id" escape="false" />" />
  	  	学校名：<input type="text" name="schoolName" value="<s:property value="school.name" escape="false" />" /><br/>
  	  	学校地址: <input type="text" name="schoolAddress" value="<s:property value="school.address" escape="false" />" /><br/>
  	  	<input type="submit" value="提交"/>
  	</form>
  	<br />
  	<br />
  	<br />
  	<br />
  	<br />
  	<a href="/admin/school/view?operationType=2">返回学校列表</a>

  </body>
</html>
