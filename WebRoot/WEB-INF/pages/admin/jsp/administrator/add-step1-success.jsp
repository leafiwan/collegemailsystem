<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="jshy" uri="/jshy-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>添加管理员</title>
    
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
    <h1>将<s:property value="user.name" />(<s:property value="user.email" />)设成管理员</h1>
    <form method="post" action="/admin/administrator/add">
    	<input type="hidden" name="step" value="2" />
    	<input type="hidden" name="uid" value="<s:property value="user.id" />" />
    	<p>
    		请输入后台密码：<input type="password" name="newPassword" />
    	</p>
    	<p>
    		<input type="submit" value="提交"/>
    	</p>
    </form>
  </body>
</html>
