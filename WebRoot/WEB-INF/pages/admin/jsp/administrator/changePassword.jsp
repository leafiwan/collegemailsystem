<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="jshy" uri="/jshy-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>修改后台密码</title>
    
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
    <h1>修改<s:property value="user.name" />(<s:property value="user.email" />)的后台密码</h1>
    <form method="post" action="/admin/administrator/changePassword">
    	<input type="hidden" name="aid" value="<s:property value="id" />" />
    	<p>
    		请输入后台密码：<input type="password" name="newPassword" />
    	</p>
    	<p>
    		<input type="submit" value="提交"/>
    	</p>
    </form>
  </body>
</html>
