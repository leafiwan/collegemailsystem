<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="jshy" uri="/jshy-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>管理员管理</title>

  </head>
  
  <body>
	<h1>管理员管理</h1>
	<ul>
		<s:iterator value="administrators.elements">
			<li>
				<a href="/user/<s:property value="user.id"/>"><s:property value="user.name" /></a>
				<a href="/admin/administrator/changePassword?aid=<s:property value="id" />">修改后台密码</a>
				<a href="/admin/administrator/delete?aid=<s:property value="id" />">删除</a>
			</li>
		</s:iterator>
	</ul>
	<jshy:paginator value="administrators"/>
  </body>
  
</html>
