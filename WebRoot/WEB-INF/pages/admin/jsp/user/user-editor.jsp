<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'user-editor.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
  	<h2>用户：<s:property value="user.name" escape="false"/>的编辑</h2>
  	<form action="admin/user/update" method="post">
  		<input type="hidden" name="userId" value="<s:property value="user.id" escape="false" />" />
  	  	用户名：<input type="text" name="username" value="<s:property value="user.name" escape="false" />" /><br/>
  	  	email: <input type="text" name="email" value="<s:property value="user.email" escape="false" />" /><br/>
  	  	密码：<input type="password" name="password" /><br/>
  	  	自我介绍：<textarea name="description" rows="8" cols="60"><s:property value="user.description" escape="false" /></textarea><br/>
  	  	学校选择：
  	  	<select name="schoolName" >
  	  		<s:iterator value="schools">
  	  			<s:if test="id == user.school.id">
  	  				<option selected="selected">
  	  					<s:property value="name" escape="false"/>
  	  				</option>
  	  			</s:if>
  	  			<s:else>
  	  				<option >
  	  					<s:property value="name" escape="false"/>
  	  				</option>
  	  			</s:else>
  	  		</s:iterator>
  	  	</select>
  	  	<input type="submit" value="提交"/>
  	</form>
  	<br />
  	<br />
  	<br />
  	<br />
  	<br />
  	<a href="/admin/user/view?operationType=2">返回用户列表</a>
  </body>
</html>
