<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="jshy" uri="/jshy-tags" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>My JSP 'user-list.jsp' starting page</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">
		<!--
		<link rel="stylesheet" type="text/css" href="styles.css">
		-->
		<script type="text/javascript">
		function comfirmBlock(){
			confirm("确认关黑屋？");
		}
		</script>

	</head>

	<body>
		<h2>
			编辑用户
		</h2>
		<s:if test="users.elements.isEmpty()">
			没有任何用户
		</s:if>
		<form action="/admin/user/search" method="post">
			<table align="left" border="0">
				<tr>
					<td>
						按用户名搜索：
					</td>
					<td>
						<input type="text" name="nameInfo">
					</td>
				</tr>
				<tr>
					<td>
						按email搜索：
					</td>
					<td>
						<input type="text" name="emailInfo">
					</td>
				</tr>
				<tr>
					<td>
						按所在学校搜索：
					</td>
					<td>
						<input type="text" name="schoolInfo">
					</td>
				</tr>
			</table>
			<input type="submit" value="搜索">
		</form>
		<br />
		<br />
		<br />
		<br />
		<table align="left" border="0">
			<s:iterator value="users.elements">
				<tr>
					<td>
						<s:property value="id" escape="false" />
					</td>
					<td>
						<s:property value="name" escape="false" />
					</td>
					<td>
						<s:property value="school.name" escape="false" />
					</td>
					<td>
						<a href="/admin/user/editor?userId=<s:property value="id" escape="false" />">编辑</a>
					</td>
					<td>
						<s:if test="block">
							<a href="/admin/user/release?userId=<s:property value="id" escape="false" />">释放他</a>
						</s:if>
						<s:else>
							<a href="/admin/user/block?userId=<s:property value="id" escape="false" />" onclick="comfirmBlock();">关黑屋</a>
						</s:else>
					</td>
					<td>
						<a href="/admin/administrator/add?step=1&uid=<s:property value="id"/>">设为管理员</a>
					</td>
				</tr>
			</s:iterator>
		</table>
		<br />
		<br />
		<br />
		<br />
		<div style="clear: left;">
			<jshy:paginator value="users" />
		</div>
	</body>
</html>
