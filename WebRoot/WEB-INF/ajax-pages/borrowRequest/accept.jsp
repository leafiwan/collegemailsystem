<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<div>
	<h3>我已经同意 <s:property value="user.name" escape="false"/> 借阅《<s:property value="sharing.book.title" escape="false"/>》的请求</h3>
	
	<div>
		我可以通过下列方式与 <s:property value="user.name" escape="false" />取得联系：
		<ul>
			<s:if test="user.phoneNumber != null">
				<li>打电话： <s:property value="user.phoneNumber" /></li>
			</s:if>
			<s:if test="user.qq != null">
				<li>QQ： <s:property value="user.qq" /></li>
			</s:if>
			<li>发私信</li>
		</ul>
	</div>
	
</div>