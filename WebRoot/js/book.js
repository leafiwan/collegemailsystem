/**
 * 	JS functions for operations to books
 * 	version: 0.1.0
 * 	author: PSJay
 * 	require: commons.js
 */


/**
 *  JS init function for book's borrow page
 *  should be called when DOM is ready
 */
function initBorrowPage() {	
	$('#provider-list .borrow-btn').click(function() {
		var requirements = $(this).siblings('.requirements').html();
		var providerName = $(this).prev().children('a').text();
		var sid = $(this).prev().children('a').attr('_sid');
		
		// set sharing id into request form
		$('#request-form input[name="sid"]').val(sid);
		
		var content = '<div class="requirements-popup"><h3>' + providerName + '对借阅者的要求</h3><div class="requirements">' + requirements + '</div></div>';
		content = $(content);
		
		var agree = $('<a href="javascript:void(0);" class="sdbtn agree"><span class="sdbtn-inner"></span>同意</a>');
		var cancel = $('<a href="javascript:void(0);" class="sdbtn cancel"><span class="sdbtn-inner"></span>算了</a>');
		var operations = $('<div class="operations"></div>');
		
		agree.click(function() {
			$('#request-form').submit();
		});
		
		cancel.click(function() {
			$.unblockUI();
		});
		
		operations.append(agree).append(cancel);
		operations.append('<div class="clr"></div>');
		content.append(operations);
		popup(content);
	});
}