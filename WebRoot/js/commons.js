/***************************************************************
*
*	This is a Javascript file for ant bookbase's common use
*	author: PSJay
*	version: 0.1.0
*	require: jQuery 1.6+, jQuery.blockUI
*
***************************************************************/

/**
*	called when DOM is ready
*/
$(function() {
	// init top-search 
	var ts = $('#top-search input');
	if(ts) {
		initInnerLabelInput(ts);
		$("#search-icon").click(function() {
			$(this).parent().submit();
		});
	}
	
	
	//init sliding door buttons
	var sdBtns = $('.sdbtn');
	if(sdBtns) {
		initSlidingDoorBtn(sdBtns);
	}
});

/******************************************
*	Common Functions
*******************************************/

/**
*	Optimize a chechbox element
*	Click the checkbox's label, the checkbox will be clicked automatically
*/
function optimizeCheckbox(checkbox) {
	var name = $(checkbox).attr('name');
	$("label[for='" + name + "']").click(function() {
		$(checkbox).click();
	});
}


/**
*	Display a label into a input element
*/
function initInnerLabelInput(input) {
	var label = $("label[for='" + $(input).attr('name') + "']");
	label.show();
	label.click(function() {
		input.focus();
	});
	
	$(input).bind('mouseup keyup', function() {
		if($(this).val().length > 0) {
			label.hide();
		} else {
			label.show();
		}
	}).blur(function() {
		if($(this).val().length == 0) {
			label.show();
		}
	});
}


/**
*	initialize a sliding door button
*/
function initSlidingDoorBtn(btn) {
	btn.hover(function() {
		$(this).addClass('hover');
	}, function() {
		$(this).removeClass('hover');
	});
}


/**
 * 	Add form-submit ability to an anchor
 * 	The anchor must be in a form tag
 * 	Use the 'enter' key as the submit shortcut
 */
function convertAnchorToSubmit(anchor) {
	var form = anchor.parents('form');
	$(anchor).click(function() {
		form.submit();
	});
	// shortcut
	form.keyup(function(event) {
		if($(document.activeElement)[0].tagName.toUpperCase() != 'TEXTAREA' ) {
			if(event.which == 13) {
				form.submit();
			}
		}
	});
	
}



/**
 *	The default options for pop up DIV
 *	Override some jquery.blockUI's default options, most of them are CSS attributes
 */
var popupDefaultOptions = {
	overlayCSS : {
		backgroundColor : '#fff',
		opacity : 0.7,
		cursor : 'default'
	},
	css : {
		border : 'none',
		textAlign : 'left',
		cursor : 'default'
	}
};


/**
 * 	Show a pop up DIV
 * 	parameters :
 * 		content is what you want to show in pop up DIV
 * 		options: options to pop up DIV (as same as jquery.blockUI's option object)
 */
function popup(content, options) {
	// wrap content
	var wrapString = '<div id="popup">' + 
		'<div class="header">' +
			'<a class="close"><img src="/images/popup-close.gif" title="关闭" /></a>' +
		'</div>' +
		'<div class="content">' +
		'</div>' +
	'</div>';
	
	var wrapObject = $(wrapString);
	wrapObject.children('.content').append(content);

	var realOption = {};
	
	//clone options
	for(elements in popupDefaultOptions) {
		realOption[elements] = popupDefaultOptions[elements];
	}
	
	if(options) {
		for(elements in options) {
			realOption[elements] = options[elements];
		}
	}
	
	realOption.message = wrapObject;
	
	
	
	$.blockUI(realOption);
	
	// bind close function to close button
	$('#popup .close').click(function() {
		$.unblockUI();
	});
}















