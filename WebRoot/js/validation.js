/**
 *	A simple form-validation framework
 *	based on jQuery
 *	@author PSJay 
 *	@version	1.0
 */


/*
 * Usage of validate function
 * The parameter wrapper is a jQuery object which wraps the input components you want to validate, 
 * which means, the wrapper have to be the PARENT NODE of the input components,
 * generally , It's a form jQuery object.
 * The input components' name attribute should be different.
 * More information about the options parameter, see below
 *
 * This is a sample for options parameter of validate function,
 * I think this clip of script is a human readable, so I won't give more explanation 

	[{
			name: "email",
			validators: [{
				type: "required",
				params: {trim: true},
				msg: "email is required",
				callback: showErrorMsg
			}, {
				type: "regExp",
				params:/^(?:[a-z\d]+[_\-\+\.]?)*[a-z\d]+@(?:([a-z\d]+\-?)*[a-z\d]+\.)+([a-z]{2,})+$/i,
				msg:"wrong email address",
				callback: showErrorMsg
			}]
		},{
			name: "name",
			validators:[{
				type: "required",
				params: {trim: true},
				msg: "name is required",
				callback: showErrorMsg
			}, {
				type: "length",
				params: {trim: true, min: 2, max: 10},
				msg: "the length of name should be 2 to 10",
				callback: showErrorMsg
			}]
		}]
	
*/
//

function validate(wrapper, options) {
	validatorFacoty = new ValidatorFactoty();
	inputs = $(wrapper).find(':input');
	for(i = 0; i < inputs.length; i++) {
		$(inputs[i]).blur(function() {
			validateItem($(this));
		});
	}
	
		if($(wrapper)[0].tagName.toUpperCase() == "FORM") {
			$(wrapper).submit(function() {
				var errorsCount = 0;
				inputs = $(wrapper).find(':input');
				for(var i = 0; i < inputs.length; i++) {
					if(!validateItem($(inputs[i])))
						errorsCount++;
				}
				if(errorsCount > 0) {
					return false;
				}
				return true;
			});
		}
	
	function validateItem(item) {
		console.log('a');
		opt = findOption( item.attr('name') );
		if(opt == null) {
			return true;
		}

		value = item.val();
		
		if(value == "") {
			// is required
			for(var i = 0; i < opt.validators.length; i++) {
				var temp = 0;
				if(opt.validators[i].type.toUpperCase() != "REQUIRED") {
					temp++;
				}
			}
			if(temp == opt.validators.length) {
				return true;	
			}
		}
		
		for(j = 0; j < opt.validators.length; j++) {
			validator = validatorFacoty.getValidator(opt.validators[j].type,
					opt.validators[j].params);
			if(!validator.validate(value)) {
				opt.validators[j].callback(item, opt.validators[j].msg);
				return false;
			}
		}
		return true;
	}
	
	function findOption(n) {
		if(!n) {
			return null;
		}
		for(i = 0; i < options.length; i++) {
			if(options[i].name === n) {
				return options[i];
			}
		}
		return null;
	}
}

function ValidatorFactoty() {
	
}

ValidatorFactoty.prototype.getValidator = function(type, params) {
	switch(type) {
	case "required":
		return new RequiredValidator(params);
		break;
	case "length":
		return new LengthValidator(params);
		break;
	case "regExp":
		return new RegExpValidator(params);
		break;
	case "condition":
		return new ConditionValidator(params);
		break;
	}
};

function RequiredValidator(params) {
	this.params = arguments[0]? arguments[0] : true;
	
}

RequiredValidator.prototype.validate = function(string) {
	
	if(this.params.trim) {
		 if($.trim(string).length == 0)
		 	return false;
		 return true;
	} else {
		if (string.length == 0 )
			return false;
		return true;
	}
};

function LengthValidator(params) {
	this.params = params;
	if( this.params.trim == undefined )
		this.params.trim = true;
}

LengthValidator.prototype.validate = function(string) {
	if(this.params.trim) {
		string = $.trim(string);
	}
	return string.length >= this.params.min && string.length <= this.params.max;
};

function RegExpValidator(params) {
	this.params = params;
}
RegExpValidator.prototype.validate = function(string) {
	if (!this.params.exec(string)) {
		return false;
	}
	return true;
};


function ConditionValidator(params) {
	this.params = params;
}
ConditionValidator.prototype.validate = function() {
	return this.params();
};