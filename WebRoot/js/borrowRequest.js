/**
 *  @author: psjay
 *  @version: 0.0.1
 *  @date: 2012-1-29
 */


/**
 *  init borrow request list page
 */
function initMinePage() {
	// accept ajax
	$('.agree').click(function() {
		var link = $(this);
		$.get(link.attr('href'), function(data) {
			popup($(data));
			link.parents('.item').slideUp();
		});
		
		return false;
	});
	
	// reject and cancel ajax
	$('.disagree, .cancel').click(function() {
		var link = $(this);
		$.get(link.attr('href'), function(data) {
			link.parents('.item').slideUp();
		});
		
		return false;
	});
	
}
