
//模板
//此feed为借书，还书，分享新书,就某本书的还书打分
var FEED_HTML_TYPE_A = 
'<article class="feed">' + 
'<aside>' + 
'<figure class="user-head">' + 
'<img  src="event.user.largeAvatar"/>' + 
'<span class="follow" userId = "event.user.id" userFollowUrl="event.userFollowUrl">userForUfText</span>' +
'</figure>' + 
'</aside>' + 
'<h3><a href="/user/event.user.id">event.user.name</a>&nbsp;:&nbsp;event.content.text</h3>' + 
'<div class="feed-source">' + 
'<div class="source-box">' + 
'<figure class="book-img"><img src="event.book.smallImg"/></figure> ' + 
'书名：event.book.title<br/>' + 
'作者：event.book.author<br/>' + 
'评分：event.book.rate<br/>' + 
'共享数：event.book.count<br/>' + 
'<span class="followBook" bookId = "event.book.id" bookFollowUrl = "event.content.bookFollowUrl"><a>bookForUfText</a></span>&nbsp;&nbsp;<a href="/book/event.book.id">详情</a>' + 
'</div>' + 
'<div class="feed-detail">' + 
'event.timeline' + 
'</div>' + 
'<div class="line"></div>' + 
'</div>' + 
'</article>';

//此feed用于书评
var FEED_HTML_TYPE_B =
'<article class="feed">' + 
'<aside>' + 
'<figure class="user-head">' + 
'<img src="event.user.largeAvatar"/>' + 
'<span class="follow" userId = "event.user.id" userFollowUrl="event.userFollowUrl">userForUfText</span>' +
'</figure>' + 
'</aside>' + 
'<h3><a href="/user/event.user.id">event.user.name</a>&nbsp;:&nbsp;event.content.text</h3>' + 
'<div class="feed-source">' + 
'<div class="source-box">' + 
'<article>' + 
'<h4>event.content.review.title</h4>' + 
'event.content.review.content<a href="/review/event.content.review.id">[原文]</a>' + 
'</article>' + 
'</div>' + 
'<div class="feed-detail">' + 
'event.timeline' + 
'</div>' + 
'<div class="line"></div>' + 
'</div>' + 
'</article>';

//用于状态
var FEED_HTML_TYPE_C = 
'<article class="feed">' + 
'<aside>' + 
'<figure class="user-head">' + 
'<img src="event.user.largeAvatar"/>' + 
'<span class="follow" userId = "event.user.id" userFollowUrl="event.userFollowUrl">userForUfText</span>' +
'</figure>' + 
'</aside>' + 
'<div class="emotion-box">' + 
'<h3><a href="/user/event.user.id">event.user.name</a>&nbsp;:&nbsp;event.content.text</h3>' + 
'</div>' + 
'<div class="feed-detail-r">' + 
'event.timeline' + 
'</div>' + 
'<div class="line"></div>' + 
'</article>';
	

//替换所有
String.prototype.replaceAll = function(s1, s2) {  
	raRegExp = new RegExp(s1,"g");
	return this.replace(raRegExp,s2);
}  ;

var isEnd = false;

function ajaxGetFeedsList(page,pageSize,feedList){
	if(! isEnd){
	$.get('/hall/feeds',{'page':page,'pageSize':pageSize},function(data){
		//将数据填充进模板
		replaceTemplate(data,feedList);
		
		if(data.eventList.length == 0){
			$('#feed-more').html('亲，已木有新鲜事了！');
			isEnd = true;
		}else
			isEnd = false;
		});
	}
	
	//要延迟执行才行 fuck
	setTimeout(function(){
			$('.follow').each(function(i){
				$(this).bind('mouseover',function(i){
					$(this).css('cursor','pointer');
				});
				
				$(this).bind('click',function(){
					if(confirm('确认是否' + $(this).text() + '此人?')){
						$.get($(this).attr('userFollowUrl'),{'id':$(this).attr('userId')},function(){
							
								feedList.html("");
								isEnd = false;
								ajaxGetFeedsList(page = 1,pageSize,feedList);
								
							
							
						});
					}
					
					
					
				});
			});
	},500);
	
	setTimeout(function(){
		$('.followBook').each(function(i){
			$(this).bind('mouseover',function(i){
				$(this).css('cursor','pointer');
			});
			
			$(this).bind('click',function(){
				if(confirm('确认是否' + $(this).text() + '?')){
					$.get($(this).attr('bookFollowUrl'),{'id':$(this).attr('bookId')},function(){
						
							feedList.html("");
							isEnd = false;
							ajaxGetFeedsList(page = 1,pageSize,feedList);
							
						
					});
				}
				
				
				
			});
		});
},500);
		
		
}

function replaceTemplate(data,feedList){
	for(n = 0;n<data.eventList.length;n++){		
		var template = null;
		var html = null;
		var rateHtml = "";
		
		var content = strToJson(data.eventList[n].content);
		//选择模板
		html_type = content.html_type;
		switch(html_type){
		case 'A':template = FEED_HTML_TYPE_A;break;
		case 'B':template = FEED_HTML_TYPE_B;break;
		case 'C':template = FEED_HTML_TYPE_C;break;
		default:template = null;
		}		
		
		if(template){		
			
		//将模板中的公共数据换成异步加载来得数据
		template =  template.replaceAll('event.user.largeAvatar',content.user.largeAvatar)
						   .replaceAll('event.user.id',content.user.id)
						   .replaceAll('event.user.name',content.user.name)
						   .replaceAll('event.timeline',data.eventList[n].timeline.replaceAll('T','&nbsp;&nbsp;'))
						   .replaceAll('event.content.text',content.text);
		
		//替换用户是否被关注模块
		if(!data.eventList[n].me){
		if(data.eventList[n].userHasFollowed != null){
			//如果没有被关注
			if(data.eventList[n].userHasFollowed == false){
				template = template.replaceAll('event.userFollowUrl', '/hall/followUser')
								   .replaceAll('userForUfText','&nbsp;&nbsp;&nbsp;<a>关注</a>');
			}else{
				template = template.replaceAll('event.userFollowUrl', '/hall/unfollowUser')
				   .replaceAll('userForUfText','<a>取消关注</a>');
			}
		}}else{
			template = template.replaceAll('userForUfText','&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)">自己</a>')
							   .replaceAll('class="follow"','class="follow-m"');
		}
		
		//替换风格C没有的属性
		if(html_type != 'C'){
		//设置星星的数量
		for(i = 0;i< parseInt(content.book.rate/2);i++){
			rateHtml = rateHtml + '<img src="images/level/starB.gif"/>';
		}	
		
		for(i = 0;i< 5 - parseInt(content.book.rate/2);i++){
			rateHtml = rateHtml + '<img src="images/level/starA.gif"/>';
		}
		rateHtml = rateHtml + '&nbsp' + content.book.rate;
		
		template = template.replaceAll('event.book.id',content.book.id)
	   .replaceAll('event.book.title',content.book.title)
	   .replaceAll('event.book.author',content.book.author)
	   .replaceAll('event.book.rate',rateHtml)
	   .replaceAll('event.book.count',content.book.count)
	   .replaceAll('event.book.smallImg',content.book.smallImg);
		
		//设置是否被关注
		if(data.eventList[n].bookHasFollowed != null){
			//如果没有被关注
			if(data.eventList[n].bookHasFollowed == false){
				template = template.replaceAll('event.content.bookFollowUrl', '/hall/followBook')
								   .replaceAll('bookForUfText','关注此书');
			}else{
				template = template.replaceAll('event.content.bookFollowUrl', '/hall/unfollowBook')
				   .replaceAll('bookForUfText','取消关注');
			}
		}
		
		}
        
        //当为样式b时，新的数据
        if(html_type == 'B'){
        	template = template.replaceAll('event.content.review.title',content.review.title)
			     	   .replaceAll('event.content.review.content',content.review.content)
			     	   .replaceAll('event.content.review.id',content.review.id);
        } 
        
        
		}
		
        feedList.append(template);
       
	}
}

//将字符串转换成json形式
function strToJson(str){
	return eval('(' + str + ')');
}